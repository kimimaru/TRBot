﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Parsing;
using TRBot.Permissions;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Removes an input macro.
    /// </summary>
    public sealed class RemoveMacroCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"#macroname\"";

        public RemoveMacroCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.REMOVE_INPUT_MACRO_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to remove input macros.");
                    return;
                }
            }

            string macroName = arguments[0].ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                InputMacro macro = context.Macros.FirstOrDefault(m => m.MacroName == macroName);

                if (macro == null)
                {
                    QueueMessage(args.ServiceName, $"Input macro \"{macroName}\" could not be found.");
                    return;
                }

                context.Macros.Remove(macro);
                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Removed input macro \"{macroName}\".");
        }
    }
}
