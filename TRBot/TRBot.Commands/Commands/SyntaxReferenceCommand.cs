﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Text;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// A helpful reference to the input syntax.
    /// </summary>
    public class SyntaxReferenceCommand : BaseCommand
    {
        private enum SyntaxReferenceTypes
        {
            Inputs,
            Duration,
            Simultaneous,
            Delay,
            Hold,
            Release,
            Repeated,
            Multiplayer,
            Macros,
            DynamicMacros,
            Comments
        }
        
        private readonly  Dictionary<SyntaxReferenceTypes, string> SyntaxRefTypeToDatabaseStringDict = new Dictionary<SyntaxReferenceTypes, string>()
        {
            { SyntaxReferenceTypes.Inputs, SettingsConstants.SYNTAX_REFERENCE_INPUTS_MESSAGE },
            { SyntaxReferenceTypes.Duration, SettingsConstants.SYNTAX_REFERENCE_DURATION_MESSAGE },
            { SyntaxReferenceTypes.Simultaneous, SettingsConstants.SYNTAX_REFERENCE_SIMULTANEOUS_MESSAGE },
            { SyntaxReferenceTypes.Delay, SettingsConstants.SYNTAX_REFERENCE_DELAY_MESSAGE },
            { SyntaxReferenceTypes.Hold, SettingsConstants.SYNTAX_REFERENCE_HOLD_MESSAGE },
            { SyntaxReferenceTypes.Release, SettingsConstants.SYNTAX_REFERENCE_RELEASE_MESSAGE },
            { SyntaxReferenceTypes.Repeated, SettingsConstants.SYNTAX_REFERENCE_REPEATED_MESSAGE },
            { SyntaxReferenceTypes.Multiplayer, SettingsConstants.SYNTAX_REFERENCE_MULTIPLAYER_MESSAGE },
            { SyntaxReferenceTypes.Macros, SettingsConstants.SYNTAX_REFERENCE_MACROS_MESSAGE },
            { SyntaxReferenceTypes.DynamicMacros, SettingsConstants.SYNTAX_REFERENCE_DYNAMIC_MACROS_MESSAGE },
            { SyntaxReferenceTypes.Comments, SettingsConstants.SYNTAX_REFERENCE_COMMENTS_MESSAGE },
        };

        private string UsageMessage = string.Empty;

        public SyntaxReferenceCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            StringBuilder stringBuilder = new StringBuilder("Pass one of these arguments for the respective syntax: ", 100);
            
            string[] syntaxReferenceTypeNames = EnumUtility.GetNames<SyntaxReferenceTypes>.EnumNames;

            for (int i = 0; i < syntaxReferenceTypeNames.Length; i++)
            {
                stringBuilder.Append(syntaxReferenceTypeNames[i].ToLowerInvariant());

                if (i < (syntaxReferenceTypeNames.Length - 1))
                {
                    stringBuilder.Append(',').Append(' ');
                }
            }

            UsageMessage = stringBuilder.ToString();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (EnumUtility.TryParseEnumValue(arguments[0].ToLowerInvariant(), out SyntaxReferenceTypes syntaxRefType) == false)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (SyntaxRefTypeToDatabaseStringDict.TryGetValue(syntaxRefType, out string syntaxMessageDBKey) == false)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string referenceMessage = DatabaseMngr.GetSettingString(syntaxMessageDBKey, UsageMessage);

            QueueMessage(args.ServiceName, referenceMessage);
        }
    }
}
