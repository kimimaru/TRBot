﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection.Matrix
{
    /// <summary>
    /// Constants regarding Matrix.
    /// </summary>
    public static class MatrixConstants
    {
        /// <summary>
        /// The file storing Matrix login settings.
        /// </summary>
        public const string LOGIN_SETTINGS_FILENAME = "MatrixLoginSettings.txt";

        /// <summary>
        /// The max length of the localpart of a Matrix user ID.
        /// </summary>
        /// <remarks>https://spec.matrix.org/latest/appendices/#user-identifiers</remarks>
        public const int MAX_MXID_LOCALPART_LENGTH = 255;
    }
}
