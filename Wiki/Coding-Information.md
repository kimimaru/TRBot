# Coding Information
This document serves to highlight important information regarding TRBot development.

Our main repository is on Codeberg: https://codeberg.org/kimimaru/TRBot.git

We are also on Matrix at [#TRBot-Dev:matrix.org](https://matrix.to/#/!hTfcbsKMAuenQAetQm:matrix.org?via=matrix.org). Feel free to ask any questions or discuss development there!

# Important Overview
- TRBot is developed in C# and .NET 7
- Data is handled in a SQLite database via Entity Framework Core 7
- TRBot aims to utilize SOLID principles as best as it can. Some of the codebase is a mixed bag, but recent efforts have been made to improving it.
- Manual dependency injection is used heavily throughout the project. If contributing a pull request, please maintain this practice.
  - An IoC library may be used to help with DI in the future, but the interchangeable elements involving the database (Ex. Dynamically-created virtual controller instances) will first need to be considered.

# Preprocessor Directives
- `SYSTEM_WIDE_INSTALL` - Indicates a system-wide install if defined in `TRBot.Main`'s csproj file. The only difference is folder paths are changed.

# Command Line Arguments
```
-s, --system-wide      Run TRBot as a system-wide install, using the local
                       application data folder for paths such as data. If this
                       option is omitted, TRBot will default to system-wide
                       paths if compiled with the "SYSTEM_WIDE_INSTALL" flag.
                       Cannot be used with "-p" or "--portable".

  -p, --portable       Run TRBot as portable, using the executable folder for
                       paths such as data. If this option is omitted, TRBot will
                       default to portable paths if compiled WITHOUT the
                       "SYSTEM_WIDE_INSTALL" flag. Cannot be used with "-s" or
                       "--system-wide".

  -v, --version        Display TRBot application version.

  --help               Display this help screen.
```

# Folder paths
- Folder paths are resolved through `IFolderPathResolver`, located in the `TRBot.Utilities` project. It contains properties pointing to file paths to the folders used by TRBot, including the **Data** folder.
- The `FolderPathResolver` implementation is defined at the root of the application and is injected into all relevant classes. **Please use it!**
- For system-wide installs, the folders use the "TRBot" subdirectory inside of `Environment.SpecialFolder.LocalApplicationData`.
- For portable installs, the folders are relative to the directory of the executable itself.

# Data-Related
- Data-related code can be found in the `TRBot.Data` project.
- **Always** open a new database context with `DatabaseMngr.OpenContext()` or `DatabaseMngr.OpenCloseContext(DBContextAction<BotDBContext> dbContextAction)`. **Please do not instantiate a `BotDBContext` manually!**
- **Always** wrap the `BotDBContext` inside a `using`. This guarantees that there are no memory leaks leading to funky behavior.
- Make uses of the database context as concise as possible. This is recommended due to errors in the past where ordinary-looking code produced odd behavior. Keep things brief and to the point!
- Use `.AsNoTracking()` or `.AsNoTrackingWithIdentityResolution()` with read-only queries to speed up performance. **Make sure to eagerly load necessary tables with `.Include()` or `.ThenInclude()`**! Lazy-loading doesn't work for read-only queries.
- Alternatively, apply a read-only query on the entire context by setting the change tracker's `QueryTrackingBehavior` to `NoTracking` or `NoTrackingWithIdentityResolution`. The same rules as the above apply with respect to lazy loading.
- `IDatabaseManager<BotDBContext>` and `BotDBContext` contains many useful extension methods to obtain information from the database.
- If a database context is open, please use the `BotDBContext` extension methods instead of `IDatabaseManager<BotDBContext>` extension methods. The latter will cause it to re-open the database context, causing funky issues.
-To add new default data, add them to `DefaultDataProvider.cs` in the `TRBot.Main` namespace. When adding a new category of data that isn't there, modify `DataInitializer.InitDefaultData`. Add to the count of new entries, and please don't add entries if they're already present to prevent overwriting existing data.

# Parsing Inputs
- Inputs are parsed through the `TRBot.Input.Parsing.IParser` interface. There is currently only one implementation, `StandardParser`.
- It's recommended to create an instance of `StandardParserFactory` and use `Create` to make a parser configured consistently with the rest of the project. This may be revisited in the future if there ends up being more parser implementations.
- The `StandardParser` relies on `ParserComponents` to piece together the regex. The standard order of components is as follows:
  1. `PortParserComponent`
  2. `HoldParserComponent`
  3. `ReleaseParserComponent`
  4. `InputParserComponent`
  5. `PercentParserComponent`
  6. `MillisecondParserComponent`
  7. `SecondParserComponent`
  8. `SimultaneousParserComponent`
- Call `IParser.ParseInputs` to get the parsed input sequence. If the `ParsedInputResult` is `Valid`, everything is good to go. Otherwise, print the `Error` to get a detailed message on what went wrong with parsing. 

## Important Notes
- Parsing inputs using `IParser.ParseInputs` is fairly simple, but there is some work required to prepare the input string beforehand. These are done through `IPreparsers`, which can be passed into the constructor of a `StandardParser`. Their functions include removing whitespace, expanding repeated inputs, and populating macros. These will be handled automatically when creating a parser via `StandardParser.CreateStandard`.
- The standard order of `IPreparser`s is as follows:
  1. `RemoveWhitespacePreparser`
  2. `LowercasePreparser`
  3. `RemoveCommentsParser`
  4. `InputMacroPreparser`
  5. `InputSynonymPreparser`
  6. `RemoveCommentsPreparser`
  7. `ExpandPreparser`
  8. `RemoveWhitespacePreparser`
  9. `LowercasePreparser`

- It's recommended to parse inputs with an opened database context so all the information needed to create the standard parser configuration is available. Use the context with read-only queries to further speed up parsing.
- Pass in an instance of a `StandardParserValidator` to the `StandardParser` if intending to execute the inputs after parsing. This will perform validation when each input is parsed. The validation includes controller port verification, input restrictions on a particular user, and invalid button combos.
  - **Without validation, TRBot may behave unexpectedly when executing inputs!** The `InputHandler` performs **no** validation checks in order to maximize performance. For example, if an input with an invalid controller port goes through the `IInputHandler`, TRBot will act up!

# Executing Inputs on the Virtual Controller
- The `IInputHandler` interface provides a standard API for executing inputs. `InputHandler` is the implementation injected everywhere at the root of the application. These classes reside in the `TRBot.Input` project.
- `IInputHandler.CarryOutInput` will start carrying out the input on a separate Task. It's recommended to await this Task in an asynchronous method if it's necessary to wait for it to complete to avoid blocking other parts of the code. The `InputHandler` implementation first converts the supplied lists into arrays to improve performance when executing the inputs.
- `InputHandler.ExecuteInput` is where the inputs are executed, and it is internally called by `InputHandler.CarryOutInput` on a separate task. **This is a very tight, performant method, so be extremely careful with any changes**. In particular, once the first loop starts, all the code within it has be to as optimal as possible to minimize delays between inputs.
  - If you find any bugs or have any other suggestions for improving this code, **please file an issue first!** It cannot be stressed how important this method is, as it's the core of TRBot's functionality.

## Important Notes
- Before calling `IInputHandler.CarryOutInput`, make sure `IInputHandler.InputsHalted` is false. If it's true, do not call `IInputHandler.CarryOutInput`, as something important is going on, which may include changing the virtual controller implementation, changing the number of virtual controllers, or a player desiring all ongoing inputs to be stopped. Failure to do this will likely result in a crash, as the current implementation forgoes these checks for maximum performance.
- The `IGameConsole` passed into `IInputHandler.CarryOutInput` should be a **brand new instance** constructed from one in the database. This ensures that everything that was valid **at that time** will still be valid while executing the input. Basically, this guarantees that if someone removes an input or even the current console while executing an input sequence, nothing will go haywire.

# Service-Related
- An `IClientServiceManager` (residing in `TRBot.Connection`) instance is created at the root of the application and injected into core components. Use this with the static `ClientServiceNames` class to send messages to specific services.
- Invoked events pass in an object that contains the name of the service the event was dispatched from. Call `IClientServiceManager.GetClientService(string)` with the service name to get the client service.
- To send a message to all services, use the `IEventDispatcher.BroadcastMessage` extension method from `TRBot.Connection`. This is shorthand for dispatching a `broadcast_message` event, which all services acknowledge.

## Important Notes
- Each service has its own `MsgHandler` for sending messages. They all follow the same message throttling settings in the database.

# Logging
- Use an instance of the `ITRBotLogger` interface for logging. The default one is created at the root of the application and logs to a file and the console. This class lives in the `TRBot.Utilities.Logging` project.
- Internally, TRBot uses Serilog for logging. Use the appropriate methods for the types of information - for example, `Information` should be for general logs whereas `Error` or `Fatal` should be used if something went wrong.
- Keep in mind that even if logs aren't output based on the log settings, it will still be doing the work when it's constructing the string. If the work involved is expensive (Ex. parsing or reverse parsing) and exclusive to the log, consider commenting it out and uncommenting it when it's needed.

# Fetching Users
- Use the `IDatabaseManager<BotDBContext>.GetUserByExternalID` or `BotDBContext.GetUserByExternalIDNoOpen` extension methods to fetch a user by their external ID, which is an ID given by the service they're talking on. This ID is available in many incoming events.
- If you need the user's external ID (Ex. username passed into command argument), call `IDatabaseManager<BotDBContext>.GetUserExternalIDFromNameAndService`.

# Contributing
If you find bugs or want to request features, please file an [issue](https://codeberg.org/kimimaru/TRBot/issues/new). [Pull requests](https://codeberg.org/kimimaru/TRBot/pulls) are also highly encouraged!

TRBot is free software; as such, you can run, study, modify, and distribute it for any purpose under the terms of the GNU Affero General Public License v3.0. See the [License](../LICENSE) for more information and [Dependency Licenses](../Dependency%20Licenses) file for the licenses of third party libraries used by TRBot.
