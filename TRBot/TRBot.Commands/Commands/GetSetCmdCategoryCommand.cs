﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that gets or sets a given command's category.
    /// </summary>
    public sealed class GetSetCmdCategoryCommand : BaseCommand
    {
        private readonly string UsageMessage = $"Usage - \"command name\", \"categories to set (optional)\"";
        private string AllCategoriesStr = string.Empty;

        public GetSetCmdCategoryCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            string msgAddition = "The available categories are: ";

            //Add all options
            string[] names = EnumUtility.GetNames<CommandCategories>.EnumNames;
            for (int i = 0; i < names.Length; i++)
            {
                if (EnumUtility.TryParseEnumValue(names[i], out CommandCategories categories) == true
                    && categories == CommandCategories.None)
                {
                    continue;
                }

                msgAddition += names[i] + ", ";
            }

            if (msgAddition.Length >= 2)
            {
                msgAddition = msgAddition.Remove(msgAddition.Length - 2, 2);
                AllCategoriesStr = msgAddition + " - Specify more than one category with a comma-separated list (Ex. \"User,Admin\")";
            }
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count < 1 || arguments.Count > 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }
            
            string commandName = arguments[0].ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {    
                CommandData command = context.Commands.FirstOrDefault(c => c.Name == commandName);

                if (command != null)
                {
                    if (arguments.Count == 1)
                    {
                        QueueMessage(args.ServiceName, $"The categories for the \"{commandName}\" command are: {command.Category}");
                        return;
                    }

                    if (EnumUtility.TryParseEnumValue(arguments[1], out CommandCategories categoriesToSet) == false)
                    {
                        QueueMessage(args.ServiceName, $"Invalid category specified. {AllCategoriesStr}");
                        return;
                    }

                    command.Category = categoriesToSet;
                    context.SaveChanges();

                    QueueMessage(args.ServiceName, $"Set the categories for the \"{commandName}\" command to \"{categoriesToSet}\"!");

                    return;
                }
            }

            QueueMessage(args.ServiceName, $"No command named \"{commandName}\" exists.");
        }
    }
}
