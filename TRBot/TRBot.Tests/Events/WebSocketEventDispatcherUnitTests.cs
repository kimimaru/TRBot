/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using NUnit.Framework;
using NSubstitute;
using TRBot.Utilities.Logging;
using TRBot.Events;
using TRBot.Connection;
using TRBot.WebSocket;

namespace TRBot.Tests.Events
{
    [TestFixture]
    public class WebSocketEventDispatcherUnitTests
    {
        private IWebSocketManager WebSocketMngr = null;
        private ITRBotLogger Logger = null;

        public WebSocketEventDispatcherUnitTests()
        {
            WebSocketMngr = Substitute.For<IWebSocketManager>();
            Logger = Substitute.For<ITRBotLogger>();

            SetupMocks();
        }

        private WebSocketEventDispatcher CreateDispatcher()
        {
            return new WebSocketEventDispatcher(string.Empty, WebSocketMngr, Logger);
        }

        private void SetupMocks()
        {
            WebSocketMngr.IsServerListening.Returns(false);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        [TestCase(50)]
        [TestCase(100)]
        public void EventDispatcherInvokesEvent(int registerCount)
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");
            
            for (int i = 0; i < registerCount; i++)
            {
                evtDispatcher.AddListener<EvtConnectedArgs>("test", testInterface.ConnectedType);
            }

            evtDispatcher.DispatchEvent<EvtConnectedArgs>("test", null);

            testInterface.ReceivedWithAnyArgs(registerCount).ConnectedType(null);
        }

        public static EvtConnectedArgs[][] CorrectArgsCases()
        {
            return new EvtConnectedArgs[][]
            {
                new EvtConnectedArgs[] { null },
                new EvtConnectedArgs[] { new EvtConnectedArgs() },
                new EvtConnectedArgs[] { null, new EvtConnectedArgs() }
            };
        }

        [TestCaseSource(nameof(CorrectArgsCases))]
        public void EventDispatcherInvokesEventsWithCorrectArgs(EvtConnectedArgs[] args)
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");
            evtDispatcher.AddListener<EvtConnectedArgs>("test", testInterface.ConnectedType);

            for (int i = 0; i < args.Length; i++)
            {
                evtDispatcher.DispatchEvent<EvtConnectedArgs>("test", args[i]);
                testInterface.Received().ConnectedType(args[i]);
            }
        }

        [Test]
        public void EventDispatcherThrowsOnRegisteringAfterRegister()
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");

            Assert.Throws<InvalidOperationException>(() =>
            evtDispatcher.RegisterEvent<EvtReconnectedArgs>("test"));
        }

        [Test]
        public void EventDispatcherThrowsOnAddingMixedTypes()
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");
            evtDispatcher.AddListener<EvtConnectedArgs>("test", testInterface.ConnectedType);

            Assert.Throws<InvalidOperationException>(() =>
                evtDispatcher.AddListener<EvtReconnectedArgs>("test", testInterface.ReconnectedType));
        }

        [Test]
        public void EventDispatcherThrowsOnRemovingMixedTypes()
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");
            evtDispatcher.AddListener<EvtConnectedArgs>("test", testInterface.ConnectedType);

            Assert.Throws<InvalidOperationException>(() =>
                evtDispatcher.RemoveListener<EvtReconnectedArgs>("test", testInterface.ReconnectedType));
        }

        [Test]
        public void EventDispatcherUnregistersEvent()
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");
            evtDispatcher.AddListener<EvtConnectedArgs>("test", testInterface.ConnectedType);

            evtDispatcher.UnregisterEvent("test");
            
            Assert.Throws<InvalidOperationException>(() =>
                evtDispatcher.DispatchEvent<EvtConnectedArgs>("test", null));

            testInterface.DidNotReceiveWithAnyArgs().ConnectedType(null);
        }

        [Test]
        public void EventDispatcherThrowsOnDispatchingMixedTypes()
        {
            var evtDispatcher = CreateDispatcher();
            var testInterface = Substitute.For<ITestInterface>();

            evtDispatcher.RegisterEvent<EvtConnectedArgs>("test");
            evtDispatcher.AddListener<EvtConnectedArgs>("test", testInterface.ConnectedType);

            Assert.Throws<InvalidOperationException>(() =>
                evtDispatcher.DispatchEvent<EvtReconnectedArgs>("test", null));
        }
    }

    public interface ITestInterface
    {
        void ConnectedType(EvtConnectedArgs connectedArgs);
        void ReconnectedType(EvtReconnectedArgs reconnectedArgs);
    }
}
