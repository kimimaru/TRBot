﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Input.Parsing;
using TRBot.Input.Consoles;

namespace TRBot.Input
{
    /// <summary>
    /// An interface for post-processing inputs
    /// </summary>
    public interface IPostParser
    {
        /// <summary>
        /// Inserts artificial blank inputs after input subsequences if blank inputs do not exist or aren't the longest duration in the subsequence.
        /// This is used to add artificial delays after each input.
        /// The given game console must have a blank input defined for this to work.
        /// <para>
        /// This is expected to go over the maximum input duration, as the delays are inserted after the input.
        /// Otherwise, it'll be cumbersome as players will have to account for the delays when writing input sequences.
        /// That said, aim to keep the <paramref name="midInputDelay"> a low value.
        /// </para>
        /// </summary>
        /// <param name="inputSequence">The parsed input sequence.</param>
        /// <param name="defaultPort">The default controller port to use for the new inputs.</param>
        /// <param name="midInputDelay">The duration of the inserted blank inputs, in milliseconds.</param>
        /// <param name="gameConsole">The game console to validate blank inputs for.</param>
        /// <param name="validBlankInput">A validated blank input.</param>
        /// <returns>Data regarding the mid input delays.</returns>
        MidInputDelayData InsertMidInputDelays(in ParsedInputSequence inputSequence,
            in int defaultPort, in int midInputDelay, IGameConsole gameConsole, InputData validBlankInput);
    }
}
