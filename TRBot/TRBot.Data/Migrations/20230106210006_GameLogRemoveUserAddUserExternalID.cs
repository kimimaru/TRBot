﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    public partial class GameLogRemoveUserAddUserExternalID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "User",
                schema: "gamelogs",
                table: "GameLogs",
                newName: "UserExternalID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserExternalID",
                schema: "gamelogs",
                table: "GameLogs",
                newName: "User");
        }
    }
}
