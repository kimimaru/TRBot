﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Views a user's level.
    /// </summary>
    public sealed class LevelCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"username (optional)\"";

        public LevelCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string userExternalID = args.Command.UserMessage.UserExternalID;

            if (arguments.Count == 1)
            {
                userExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(arguments[0], args.ServiceName);
            }

            User levelUser = DatabaseMngr.GetUserByExternalID(userExternalID);

            if (levelUser == null)
            {
                QueueMessage(args.ServiceName, $"User does not exist in database!");
                return;
            }

            QueueMessage(args.ServiceName, $"{levelUser.Name} is level {levelUser.Level}, {((PermissionLevels)levelUser.Level)}!");
        }
    }
}
