/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using NUnit.Framework;
using TRBot.Utilities;

namespace TRBot.Tests.Utilities
{
    [TestFixture]
    public class FolderPathResolverUnitTests
    {
        public FolderPathResolverUnitTests()
        {

        }

        private static IEnumerable<TestCaseData> PathTestCases()
        {
            yield return new TestCaseData(
                null, null, null, null,
                null, null, null, null);
            yield return new TestCaseData(
                string.Empty, string.Empty, string.Empty, string.Empty,
                string.Empty, string.Empty, string.Empty, string.Empty);
            yield return new TestCaseData(
                "testRoot", "testData", "testLogs", "testCrashLogs",
                "testRoot", "testData", "testLogs", "testCrashLogs");
            yield return new TestCaseData(
                "/path", "/home/data/path", "/home/logs/path", "/home/crashlogs/path",
                "/path", "/home/data/path", "/home/logs/path", "/home/crashlogs/path");
        }

        [TestCaseSource(nameof(PathTestCases))]
        public void TestPathsAreCorrectlyAssigned(string rootPath, string dataPath, string logsPath,
            string crashLogsPath, string expectedRootPath, string expectedDataPath, string expectedLogsPath,
            string expectedCrashLogsPath)
        {
            FolderPathResolver resolver = new FolderPathResolver(rootPath, dataPath, logsPath, crashLogsPath);

            Assert.That(resolver.RootFolderPath, Is.EqualTo(expectedRootPath));
            Assert.That(resolver.DataFolderPath, Is.EqualTo(expectedDataPath));
            Assert.That(resolver.LogsFolderPath, Is.EqualTo(expectedLogsPath));
            Assert.That(resolver.CrashLogsFolderPath, Is.EqualTo(expectedCrashLogsPath));
        }
    }
}
