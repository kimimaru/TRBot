# LiveSplitOne Intro
[LiveSplitOne](https://github.com/LiveSplit/LiveSplitOne) is a cross-platform version of the popular LiveSplit application. With LiveSplitOne, you can display a speedrun timer and track time on your playthroughs.

TRBot has the ability to directly control a locally deployed instance of LiveSplitOne through the [LiveSplitOneCommand](../TRBot/TRBot.Integrations/LiveSplitOne/Commands/LiveSplitOneCommand.cs). Internally, this sets up a WebSocket server which LiveSplitOne can then connect to and receive messages from.

There is some setup required, so keep reading on to see how to achieve this integration.

# Setting up database entries
Since TRBot's third-party integration code is strictly separated from the other libraries, you will need to manually add the required data to the database. If you do not know how to modify TRBot's database, please see the [managing data guide](./Managing-Data.md).
1. Open your database file by however means you prefer.
2. In the **CommandData** table, add a new record. Set the new record's `ClassName` to **TRBot.Integrations.LiveSplitOne.LiveSplitOneCommand**. The other columns are your preference, but it's recommended to have `Level` as VIP (20) and `Enabled` to 1 so the command is immediately available.
3. In the **Settings** table, set the `ValueInt` of **websocket_server_enabled** to 1 to enable the WebSocket server.
4. In the **Settings** table, set the `ValueStr` of **websocket_server_address** to the address and port you want the WebSocket server to run on (default is "ws://127.0.0.1:4350").
5. In the **Settings** table, add a new record with the `Key` as **lso_websocket_path**. Set its `ValueStr` to the relative path for the WebSocket service (default is "/", the root).

Step 5 is optional; if the setting isn't found, it'll use the default "/". If you have other WebSocket services running on your machine, you may want to change this value.

Now simply run TRBot, and the WebSocket server will be started automatically. As the LiveSplitOne service is tied to the `LiveSplitOneCommand`, the service will be open as long as this command is present in memory. This also means that reloading data with the `ReloadCommand` will close connection to the server on LiveSplitOne's side, so be mindful of this.

# Connecting with LiveSplitOne
Before you begin, please build and deploy your own LiveSplitOne instance following [these instructions](https://github.com/LiveSplit/LiveSplitOne#build-instructions). **The live version of LiveSplitOne on the web will not work!**

Open LiveSplitOne in your browser (the default URL is "127.0.0.1:8080"), then click "Connect to Server" on the left. Type in "ws://127.0.0.1:(port)(path)", substituting "(port)" for the port of the server (obtained from **websocket_server_address**) and "(path)" for the path of the service if it's not the root. For example, the default is "ws://127.0.0.1:4350". If the port was 6754 and the path was "/lso", you would enter "ws://127.0.0.1:6754/lso". If everything works out, you will establish a connection and see a "Disconnect" button on the left of the LiveSplitOne interface.

Remember: **TRBot must be running to keep the server alive!**

# Controlling LiveSplitOne
LiveSplitOne has several commands you can issue to control it, all available through TRBot's [LiveSplitOneCommand](../TRBot/TRBot.Integrations/LiveSplitOne/Commands/LiveSplitOneCommand.cs). Simply invoke the command without any arguments, and it will tell you all the available LiveSplitOne commands you can issue. As LiveSplitOne is still in active development, these commands may change over time.

Some examples (assuming the command is named "split" in the database):
- "!split start" - Starts a split.
- "!split reset" - Resets the timer.
- "!split togglepause" - Pauses/unpauses the timer.
- "!split setgametime 5" - Sets the game timer to have 5 seconds on the clock.
