## Connecting TRBot to XMPP
This continues from the [initial setup](./Setup-Init.md), if you choose to connect TRBot over XMPP.

The service name for XMPP in the **Services** table of the **TRBotData.db** database is `xmpp`.

TRBot can connect over XMPP to a one-to-one or multi-user channel of your choice. TRBot can use XMPP WebSockets if the server supports it. The default template file is "XMPPLoginSettings.txt", located in the Data folder. The file path can be changed with the `ConfigFile` row in the database.

- *RoomType* = The type of room to connect to. 0 = multi-user chat, and 1 = one-to-one chat.
- *ConnectionType* = How to connect to the XMPP server. 0 = TCP, and 1 = WebSocket.
- *Username* = The username of the XMPP account you want to log in as (Ex. "user" in "user@test.xyz").
- *Domain* = The XMPP server to log into (Ex. "test.xyz" in "user@test.xyz").
- *Resource* = The resource to specify for your JID. This can be empty.
- *RoomName* = The one-to-one chat or multi-user chat room to enter and send messages to.
- *MUCSettings* = Settings applied only to multi-user chats.
  - *RoomPassword* = The password to the multi-user chat room. Leave as "" if there is no password.
- *WebSocketSettings* = Settings applied only while connecting over XMPP WebSockets.
  - *WebsocketUri* = The WebSocket URI the XMPP server provides (Ex. "wss://test.xyz:443/xmpp-websocket").

XMPP support in TRBot is relatively new. If you find problems with it or would like to request a feature not currently available, please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new).

TRBot internally uses [SharpXMPP](https://github.com/vitalyster/SharpXMPP) as a library to handle the XMPP integration.
