﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Lets a user duel another for credits.
    /// </summary>
    public class DuelCommand : BaseCommand
    {
        private const string ACCEPT_DUEL_ARG = "accept";
        private const string DENY_DUEL_ARG = "deny"; 

        /// <summary>
        /// The dictionary to hold data on pending duels.
        /// <para>The key is the external ID of the user that is being dueled.</para>
        /// </summary>
        private Dictionary<string, DuelData> DuelRequests = null;

        private string UsageMessage = "Usage: \"user to duel - or (accept/deny)\" \"bet amount (int)\"";
        private Random Rand = new Random();

        public DuelCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            if (DuelRequests == null)
            {
                DuelRequests = new Dictionary<string, DuelData>();
            }
        }

        public override void CleanUp()
        {
            if (DuelRequests != null)
            {
                DuelRequests.Clear();
                DuelRequests = null;
            }

            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count == 0 || arguments.Count > 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string userName = args.Command.UserMessage.Username;
            string userExternalID = args.Command.UserMessage.UserExternalID;
            string opponentArg = arguments[0];

            //Get the users
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User thisUser = context.Users.AsNoTracking()
                    .Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                //Confirm this user is in the database
                if (thisUser == null)
                {
                    QueueMessage(args.ServiceName, "You cannot duel because you are not in the database!");
                    return;
                }

                //Confirm the user has the ability to duel
                if (thisUser.HasEnabledAbility(PermissionConstants.DUEL_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to duel!");
                    return;
                }
            }

            //Handle accepting or denying a duel with only one argument
            if (arguments.Count == 1)
            {
                if (opponentArg == ACCEPT_DUEL_ARG || opponentArg == DENY_DUEL_ARG)
                {
                    AcceptDenyDuel(args, userExternalID, opponentArg);
                }
                else
                {
                    QueueMessage(args.ServiceName, UsageMessage);
                }

                return;
            }

            string opponentExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(opponentArg, args.ServiceName);
            string opponentName = string.Empty;

            //Prevent dueling yourself
            if (userExternalID == opponentExternalID)
            {
                QueueMessage(args.ServiceName, "You cannot duel yourself!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User thisUser = context.Users.AsNoTracking()
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                //Get the opponent
                User opponentUser = context.Users.AsNoTracking()
                    .Include(u => u.Stats)
                    .Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility)
                    .FirstOrDefault(u => u.ExternalID == opponentExternalID);

                if (opponentUser == null)
                {
                    QueueMessage(args.ServiceName, $"{opponentArg} is not in the database!");
                    return;
                }

                opponentName = opponentUser.Name;

                if (thisUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "You can't duel if you're opted out of bot stats!");
                    return;
                }

                //Confirm the opponent has the ability to duel
                if (opponentUser.HasEnabledAbility(PermissionConstants.DUEL_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "The one you're attempting to duel does not have the ability to duel!");
                    return;
                }

                if (opponentUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "The one you're attempting to duel is opted out of bot stats, so you can't duel them!");
                    return;
                }
            }

            long duelTimeout = DatabaseMngr.GetSettingInt(SettingsConstants.DUEL_TIMEOUT, 60000L);
            string creditsName = DatabaseMngr.GetCreditsName();

            //Check if the duel expired and replace it with this one if so
            if (DuelRequests.TryGetValue(opponentExternalID, out DuelData opponentDuelData) == true)
            {
                TimeSpan diff = DateTime.UtcNow - opponentDuelData.DuelStartTimeUTC;
                if (diff.TotalMilliseconds >= duelTimeout)
                {
                    DuelRequests.Remove(opponentExternalID);
                }
                else
                {
                    QueueMessage(args.ServiceName, $"{opponentName} still has a pending duel!");
                    return;
                }
            }

            //Check if you have a pending duel and replace it with this one if it's expired
            if (DuelRequests.TryGetValue(userExternalID, out DuelData thisUserDuelData) == true)
            {
                TimeSpan diff = DateTime.UtcNow - thisUserDuelData.DuelStartTimeUTC;
                if (diff.TotalMilliseconds >= duelTimeout)
                {
                    DuelRequests.Remove(userExternalID);
                }
                else
                {
                    string duelerName = DatabaseMngr.GetUserByExternalID(thisUserDuelData.UserExternalIDDueling)?.Name;

                    QueueMessage(args.ServiceName, $"You have a pending duel with {duelerName}! Respond with \"accept\" or \"deny\" as an argument.");
                    return;
                }
            }

            string betStr = arguments[1];

            long betAmount = -1L;
            bool success = long.TryParse(betStr, out betAmount);
            if (success == false || betAmount <= 0)
            {
                QueueMessage(args.ServiceName, $"Please specify a positive whole number of {creditsName.Pluralize(0)} greater than 0!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User thisUser = context.Users.AsNoTracking()
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                User opponentUser = context.Users.AsNoTracking()
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == opponentExternalID);

                //Validate credit amounts
                if (thisUser.Stats.Credits < betAmount || opponentUser.Stats.Credits < betAmount)
                {
                    QueueMessage(args.ServiceName, $"Either you or the one you're dueling does not have enough {creditsName.Pluralize(0)} for this duel!");
                    return;
                }
            }

            //Add to the duel requests
            DuelRequests.Add(opponentExternalID, new DuelData(userExternalID, betAmount, DateTime.UtcNow));

            TimeSpan totalTime = TimeSpan.FromMilliseconds(duelTimeout);

            QueueMessage(args.ServiceName, $"{userName} has requested to duel {opponentName} for {betAmount} {creditsName.Pluralize(betAmount)}! Pass \"{ACCEPT_DUEL_ARG}\" as an argument to duel or \"{DENY_DUEL_ARG}\" as an argument to refuse. The duel expires in {totalTime.TotalMinutes} minute(s), {totalTime.Seconds} second(s)!");
        }

        private void AcceptDenyDuel(EvtChatCommandArgs args, string thisUserExternalID, string argument)
        {
            long duelTimeout = DatabaseMngr.GetSettingInt(SettingsConstants.DUEL_TIMEOUT, 60000L);
            string creditsName = DatabaseMngr.GetCreditsName();

            if (DuelRequests.ContainsKey(thisUserExternalID) == false)
            {
                QueueMessage(args.ServiceName, "You are not in a duel or your duel has expired!");
                return;
            }

            DuelData data = DuelRequests[thisUserExternalID];
            DuelRequests.Remove(thisUserExternalID);

            TimeSpan diff = DateTime.UtcNow - data.DuelStartTimeUTC;
            
            //The duel expired
            if (diff.TotalMilliseconds >= duelTimeout)
            {
                QueueMessage(args.ServiceName, "You are not in a duel or your duel has expired!");
                return;
            }
            
            long betAmount = data.BetAmount;
            string opponentExternalID = data.UserExternalIDDueling;

            string thisUserName = DatabaseMngr.GetUserByExternalID(thisUserExternalID)?.Name;
            string opponentName = DatabaseMngr.GetUserByExternalID(opponentExternalID)?.Name;

            //If the duel is denied, exit early
            if (argument == DENY_DUEL_ARG)
            {
                QueueMessage(args.ServiceName, $"{thisUserName} has denied to duel with {opponentName} and miss out on a potential {data.BetAmount} {creditsName.Pluralize(data.BetAmount)}!");
                return;
            }

            string winnerName = string.Empty;
            string winnerExternalID = string.Empty;
            string loserName = string.Empty;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User thisUser = context.GetUserByExternalID(thisUserExternalID);
                User opponentUser = context.GetUserByExternalID(opponentExternalID);

                if (opponentUser == null)
                {
                    QueueMessage(args.ServiceName, $"Your opponent, {opponentName}, is no longer in the database!");
                    return;
                }

                //First confirm both users have enough credits for the duel, as they could've lost some in that time
                if (thisUser.Stats.Credits < betAmount || opponentUser.Stats.Credits < betAmount)
                {
                    QueueMessage(args.ServiceName, $"At least one user involved in the duel no longer has enough {creditsName.Pluralize(0)} for the duel. The duel is off!");
                    return;
                }

                //Check if either user is now opted out
                if (thisUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false
                    || opponentUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "At least one user involved in the duel is now opted out of bot stats. The duel is off!");
                    return;
                }

                //50/50 chance of either user winning
                int val = Rand.Next(0, 2);

                //This user won
                if (val == 0)
                {
                    thisUser.Stats.Credits += betAmount;
                    opponentUser.Stats.Credits -= betAmount;

                    winnerName = thisUser.Name;
                    winnerExternalID = thisUser.ExternalID;
                    loserName = opponentUser.Name;
                }
                //The opponent won
                else
                {
                    thisUser.Stats.Credits -= betAmount;
                    opponentUser.Stats.Credits += betAmount;

                    winnerName = opponentUser.Name;
                    winnerExternalID = opponentUser.ExternalID;
                    loserName = thisUser.Name;
                }

                //Save the outcome of the duel
                context.SaveChanges();
            }
            
            (int, int) leaderBoard = LeaderboardHelper.GetPositionOnNumericStatLeaderboard(DatabaseMngr, winnerExternalID, nameof(UserStats.Credits));

            QueueMessage(args.ServiceName, $"{winnerName} won the bet against {loserName} for {betAmount} {creditsName.Pluralize(betAmount)} and is now rank {leaderBoard.Item1}/{leaderBoard.Item2} on the leaderboard PogChamp!");
        }

        /// <summary>
        /// Represents data about participants in a duel.
        /// </summary>
        public struct DuelData
        {
            public string UserExternalIDDueling;
            public long BetAmount;
            public DateTime DuelStartTimeUTC;

            public DuelData(string userExternalIDDueling, long betAmount, DateTime duelStartTimeUTC)
            {
                UserExternalIDDueling = userExternalIDDueling;
                BetAmount = betAmount;
                DuelStartTimeUTC = duelStartTimeUTC;
            }

            public override bool Equals(object obj)
            {
                if (obj is DuelData dd)
                {
                    return (this == dd);
                }

                return false;
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hash = 23;
                    hash = (hash * 37) + ((UserExternalIDDueling == null) ? 0 : UserExternalIDDueling.GetHashCode());
                    hash = (hash * 37) + BetAmount.GetHashCode();
                    hash = (hash * 37) + DuelStartTimeUTC.GetHashCode();
                    return hash;
                } 
            }

            public static bool operator==(DuelData a, DuelData b)
            {
                return (a.BetAmount == b.BetAmount && a.DuelStartTimeUTC == b.DuelStartTimeUTC
                        && a.UserExternalIDDueling == b.UserExternalIDDueling);
            }

            public static bool operator!=(DuelData a, DuelData b)
            {
                return !(a == b);
            }
        }
    }
}
