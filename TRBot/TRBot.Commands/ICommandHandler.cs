﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Manages commands.
    /// </summary>
    public interface ICommandHandler
    {
        void Initialize();

        void CleanUp();

        void HandleCommand(EvtChatCommandArgs args);

        BaseCommand GetCommand(string commandName);

        bool GetCommand<T>(out string commandName) where T : BaseCommand;

        bool AddCommand(string commandName, string commandTypeName, string valueStr,
            in long level, in bool commandEnabled, in bool displayInHelp, in CommandCategories categories);

        bool AddCommand(string commandName, BaseCommand command);

        bool RemoveCommand(string commandName);
    }
}
