﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Data
{
    /// <summary>
    /// Represents a meme.
    /// </summary>
    public class Meme
    {
        /// <summary>
        /// The ID of the meme.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The name of the meme.
        /// </summary>
        public string MemeName { get; set; } = string.Empty;

        /// <summary>
        /// The value of the meme.
        /// </summary>
        public string MemeValue { get; set; } = string.Empty;

        public Meme()
        {

        }

        public Meme(string memeName, string memeValue)
        {
            MemeName = memeName;
            MemeValue = memeValue;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 29;
                hash = (hash * 41) + MemeName.GetHashCode();
                hash = (hash * 41) + MemeValue.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"\"{MemeName}\":\"{MemeValue}\"";
        }
    }
}
