/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Text;
using System.Text.RegularExpressions;

namespace TRBot.Input.Parsing
{
    /// <summary>
    /// A pre-parser that removes comments.
    /// </summary>
    public class RemoveCommentsPreparser : IPreparser
    {
        private const string COMMENT_REGEX = @"(\/\/)([^\/]+)?(\/\/)";

        public RemoveCommentsPreparser()
        {
            
        }

        /// <summary>
        /// Pre-parses a string to prepare it for the parser.
        /// </summary>
        /// <param name="message">The message to pre-parse.</param>
        /// <returns>A string containing the modified message.</returns>
        public string Preparse(string message)
        {
            MatchCollection matches = Regex.Matches(message, COMMENT_REGEX, RegexOptions.IgnoreCase | RegexOptions.Compiled);

            //No comments
            if (matches.Count == 0)
            {
                return message;
            }

            StringBuilder strBuilder = new StringBuilder(message);

            for (int i = matches.Count - 1; i >= 0; i--)
            {
                Match match = matches[i];

                //Remove the comments
                strBuilder.Remove(match.Index, match.Length);
            }

            return strBuilder.ToString();
        }
    }
}