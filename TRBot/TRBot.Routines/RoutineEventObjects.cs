﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Newtonsoft.Json;
using System;
using TRBot.Events;

namespace TRBot.Routines
{
    public class InputVoteArgs : EvtBaseEventArgs
    {
        [JsonProperty("userExternalId")]
        public string UserExternalID = string.Empty;

        [JsonProperty("username")]
        public string Username = string.Empty;

        [JsonProperty("serviceName")]
        public string ServiceName = string.Empty;

        [JsonProperty("inputStr")]
        public string InputStr = string.Empty;

        [JsonProperty("curVotes")]
        public long CurVotes = 0;

        public InputVoteArgs()
        {

        }

        public InputVoteArgs(string userExternalID, string username, string serviceName, string inputStr, in long curVotes)
        {
            UserExternalID = userExternalID;
            Username = username;
            ServiceName = serviceName;
            InputStr = inputStr;
            CurVotes = curVotes;
        }
    }

    public class InputVoteEndArgs : EvtBaseEventArgs
    {
        [JsonProperty("chosenInputStr")]
        public string ChosenInputStr = string.Empty;

        public InputVoteEndArgs(string chosenInputStr)
        {
            ChosenInputStr = chosenInputStr;
        }
    }

    public class InputModeVoteArgs : EvtBaseEventArgs
    {
        [JsonProperty("userExternalId")]
        public string UserExternalID = string.Empty;
        
        [JsonProperty("username")]
        public string Username = string.Empty;

        [JsonProperty("serviceName")]
        public string ServiceName = string.Empty;

        [JsonProperty("inputMode")]
        public string InputMode = string.Empty;

        [JsonProperty("curVotes")]
        public long CurVotes = 0;

        public InputModeVoteArgs(string userExternalID, string username, string serviceName, string inputMode, in long curVotes)
        {
            UserExternalID = userExternalID;
            Username = username;
            ServiceName = serviceName;
            InputMode = inputMode;
            CurVotes = curVotes;
        }
    }

    public class InputModeVoteEndArgs : EvtBaseEventArgs
    {
        [JsonProperty("winningInputMode")]
        public string WinningInputMode = string.Empty;

        [JsonProperty("winningVotes")]
        public long WinningVotes = 0;

        public InputModeVoteEndArgs(string winningInputMode, in long winningVotes)
        {
            WinningInputMode = winningInputMode;
            WinningVotes = winningVotes;
        }
    }
}
