TRBot v1.6.

- Upgraded project from .NET Core 2.1 to 3.1.
- [ChatterBot](https://github.com/gunthercox/ChatterBot) integration implemented via `ChatCommand`.
- More input options:
  - Globally restrict inputs based on access level.
  - Console-specific input synonyms are defined in **BotData.txt**.
    - Add one with `AddInputSynoynmCommand`, remove one with `RemoveInputSynonymCommand`, and list them with `ListInputSynonymCommand`.
  - Added input callbacks (Ex. logging when state 1 was saved when "ss1" is pressed).
- More configuration options:
  - The bot's character message limit can be defined in **Settings.txt**.
  - The bot's periodic message can be defined in **Settings.txt**.
- More commands:
  - `ReverseCommand` reverses the viewer's message.
  - `ExerciseCommand` gives the viewer an input exercise to solve for credits.
  - `ClearGameLogsCommand` clears all game logs.
  - `ExportBotDataCommand` exports a copy of all bot data files into a sub-folder of the **Data** folder.
- Code refactoring:
  - Abstracted client integrations into an `IClientService` interface, and implemented one for Twitch.
  - Added a service-agnostic event handler base, `IEventHandler`, and implemented one for Twitch.
  - Decoupled message sending and routine updates by moving them to separate classes.
  - All external file operatings are protected under a single locked object, with the exception of chat bot prompt and response files (due to the interaction with the Python script).
- Fixed thread-safety issues by replacing many Dictionaries with ConcurrentDictionary.
- Let viewers opt out of bot stats and clear their stats.
  - `OptStatsCommand` toggles opting out of stats, and `ClearStatsCommand` clears stats.
- Renamed "group duel" to "group bet" and fixed `GroupBetRoutine` ending before the specified time.
- Fixed the working directory not always pointing to where TRBot is launched from.
- Added license notice to the top of each source file. 

Please read the [wiki](https://github.com/teamradish/TRTwitchPlaysBot/wiki/Getting-Started) for information on getting started.
