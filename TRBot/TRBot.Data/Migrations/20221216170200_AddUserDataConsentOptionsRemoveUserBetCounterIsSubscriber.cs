﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    public partial class AddUserDataConsentOptionsRemoveUserBetCounterIsSubscriber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BetCounter",
                schema: "userstats",
                table: "UserStats");

            migrationBuilder.DropColumn(
                name: "IsSubscriber",
                schema: "userstats",
                table: "UserStats");

            migrationBuilder.AddColumn<long>(
                name: "ConsentOptions",
                schema: "users",
                table: "Users",
                type: "INTEGER",
                nullable: false,
                defaultValue: 27L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConsentOptions",
                schema: "users",
                table: "Users");

            migrationBuilder.AddColumn<long>(
                name: "BetCounter",
                schema: "userstats",
                table: "UserStats",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "IsSubscriber",
                schema: "userstats",
                table: "UserStats",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
