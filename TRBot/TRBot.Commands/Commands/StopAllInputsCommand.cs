﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that stops all ongoing inputs.
    /// </summary>
    public sealed class StopAllInputsCommand : BaseCommand
    {
        public StopAllInputsCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            //Check for sufficient permissions
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
                if (user == null || user.HasEnabledAbility(PermissionConstants.STOP_ALL_INPUTS_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to stop all running inputs!");
                    return;
                }
            }

            Task t = InputHndlr.StopThenResumeAllInputs();
            t.Wait();

            QueueMessage(args.ServiceName, "Stopped all running inputs!");
        }
    }
}
