## Connecting TRBot to Matrix
This continues from the [initial setup](./Setup-Init.md), if you choose to connect TRBot over Matrix.

The service name for Matrix in the **Services** table of the **TRBotData.db** database is `matrix`.

TRBot can connect to a given Matrix account and read messages from rooms. Only unencrypted rooms are supported at the moment. The default template file is "MatrixLoginSettings.txt", located in the Data folder. The file path can be changed with the `ConfigFile` row in the database.

- *ServerAddress* = The URL of the homeserver to connect to (Ex. "https://matrix-client.matrix.org").
- *BotMatrixID* = The Matrix ID of the account to connect as (Ex. "@mybot:matrix.org").
- *BotPassword* = The password of the Matrix account.
- *DeviceID* = An identifier to use for the bot's session. It could be empty or anything, really.
- *RoomID* = The Matrix room to read messages from. It starts with "!" and can be found in most Matrix clients when sharing room invites.

Matrix support in TRBot is new. If you find problems with it or would like to request a feature not currently available, please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new).

TRBot internally uses [Matrix-CS-SDK](https://codeberg.org/kimimaru/Matrix-CS-SDK) as a library to handle the Matrix integration.

## Bonus: Connecting Matrix to Discord and other platforms
TRBot can indirectly accept inputs from other services, such as Discord, Telegram, and even email through [Matrix bridges](https://matrix.org/ecosystem/bridges/). In this case, the messages will be routed through Matrix, thus users that send inputs will be stored in the database as Matrix users.