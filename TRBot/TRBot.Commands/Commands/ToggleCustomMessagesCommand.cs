﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Toggles custom messages on or off.
    /// </summary>
    public sealed class ToggleCustomMessagesCommand : BaseCommand
    {
        private string UsageMessage = "Usage - no arguments (get value) or \"true\" or \"false\"";
        
        public ToggleCustomMessagesCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            long customMessagesEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.CUSTOM_MESSAGES_ENABLED, 1L);
            bool enabled = (customMessagesEnabled >= 1L);

            //See if they're enabled
            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"Custom messages are currently {(enabled == true ? "enabled" : "disabled")}. Pass \"true\" as an argument to enable them or \"false\" to disable them.");
                return;
            }

            //Invalid number of arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string boolStr = arguments[0].ToLowerInvariant();

            //Parse
            if (bool.TryParse(boolStr, out bool parsedBool) == false)
            {
                QueueMessage(args.ServiceName, "Invalid argument! Specify either \"true\" or \"false\" to enable or disable custom messages.");
                return;
            }
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Set the value and save
                Settings customMessageSetting = context.GetSetting(SettingsConstants.CUSTOM_MESSAGES_ENABLED);
                customMessageSetting.ValueInt = (parsedBool == true) ? 1L : 0L;

                context.SaveChanges();
            }
            
            QueueMessage(args.ServiceName, $"{(parsedBool == true ? "Enabled" : "Disabled")} custom messages!");
        }
    }
}
