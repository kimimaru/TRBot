﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using TRBot.Connection;
using TRBot.CustomCode;
using TRBot.Data;
using TRBot.Utilities;
using TRBot.Routines;
using TRBot.Permissions;
using TRBot.Utilities.Logging;
using TRBot.Events;
using TRBot.WebSocket;
using TRBot.Input;
using TRBot.Input.VirtualControllers;
using static TRBot.Data.DataEventObjects;

namespace TRBot.Commands
{
    /// <summary>
    /// Manages commands.
    /// </summary>
    public class CommandHandler : ICommandHandler
    {
        private ConcurrentDictionary<string, BaseCommand> AllCommands = new ConcurrentDictionary<string, BaseCommand>(Environment.ProcessorCount * 2, 32);
        
        private IDatabaseManager<BotDBContext> DatabaseMngr = null;
        private ITRBotLogger Logger = null;
        private IFolderPathResolver FolderPathResolver = null;
        private IRoutineHandler RoutineHandler = null;
        private ICustomCodeHandler CustomCodeHandler = null;
        private IClientServiceManager ClientServiceMngr = null;
        private IVirtualControllerContainer VControllerContainer = null;
        private IEventDispatcher EvtDispatcher = null;
        private IWebSocketManager WebSocketMngr = null;
        private IInputHandler InputHndlr = null;
        private IStringNumberParser StringNumParser = null;

        /// <summary>
        /// Additional assemblies to look in when adding commands.
        /// This is useful if the command's Type is outside this assembly.
        /// </summary>
        private Assembly[] AdditionalAssemblies = Array.Empty<Assembly>();

        public CommandHandler(IDatabaseManager<BotDBContext> databaseMngr, ITRBotLogger logger,
            IFolderPathResolver folderPathResolver, IRoutineHandler routineHandler,
            ICustomCodeHandler customCodeHandler,
            IClientServiceManager clientServiceMngr, IVirtualControllerContainer vControllerContainer, IEventDispatcher evtDispatcher,
            IWebSocketManager webSocketMngr, IInputHandler inputHndlr, IStringNumberParser stringNumParser,
            Assembly[] additionalAssemblies)
        {
            DatabaseMngr = databaseMngr;
            Logger = logger;
            FolderPathResolver = folderPathResolver;
            RoutineHandler = routineHandler;
            CustomCodeHandler = customCodeHandler;
            ClientServiceMngr = clientServiceMngr;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHndlr;
            StringNumParser = stringNumParser;

            AdditionalAssemblies = additionalAssemblies;
        }

        public void Initialize()
        {
            EvtDispatcher.RegisterEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA);
            EvtDispatcher.AddListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);

            PopulateCommandsFromDB();
        }

        public void CleanUp()
        {
            EvtDispatcher.UnregisterEvent(DataEventNames.RELOAD_DATA);
            EvtDispatcher.RemoveListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);

            RoutineHandler = null;

            CleanUpCommands();
        }

        public void HandleCommand(EvtChatCommandArgs args)
        {
            IClientService service = ClientServiceMngr.GetClientService(args.ServiceName);
            string commandToLower = args.Command.CommandText.ToLower();

            if (AllCommands.TryGetValue(commandToLower, out BaseCommand command) == false)
            {
                return;
            }
            
            if (command == null)
            {
                service.MsgHandler.QueueMessage($"Command {commandToLower} is null! Not executing.");
                return;
            }

            //Return if the command is disabled
            if (command.Enabled == false)
            {
                return;
            }

            long userLevel = 0L;

            //Check if the user has permission to perform this command
            User user = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
            
            if (user != null)
            {
                userLevel = user.Level;
            }

            if (userLevel < command.Level)
            {
                service.MsgHandler.QueueMessage($"You need at least level {command.Level}, {(PermissionLevels)command.Level}, to perform that command!");
                return;
            }

            //Execute the command
            command.ExecuteCommand(args);
        }

        public BaseCommand GetCommand(string commandName)
        {
            AllCommands.TryGetValue(commandName, out BaseCommand command);

            return command;
        }

        public bool GetCommand<T>(out string commandName) where T : BaseCommand
        {
            foreach (KeyValuePair<string, BaseCommand> kvPair in AllCommands)
            {
                if (kvPair.Value is T)
                {
                    commandName = kvPair.Key;
                    return true;
                }
            }

            commandName = string.Empty;
            return false;
        }

        public bool AddCommand(string commandName, string commandTypeName, string valueStr,
            in long level, in bool commandEnabled, in bool displayInHelp, in CommandCategories categories)
        {
            //Custom command - load it
            if (string.IsNullOrEmpty(commandTypeName) == true)
            {
                BaseCommand customCommand = LoadCustomCommand(commandName, valueStr);
                if (customCommand == null)
                {
                    EvtDispatcher.DispatchEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE,
                        new EvtBroadcastMessageArgs($"Unable to add command \"{commandName}\": Custom code failed."));
                    return false;
                }
                else
                {
                    Logger.Information($"Successfully compiled custom command \"{commandName}\"");
                }

                customCommand.Enabled = commandEnabled;
                customCommand.ClassName = commandTypeName;
                customCommand.DisplayInHelp = displayInHelp;
                customCommand.CommandCategory = categories;
                customCommand.Level = level;
                customCommand.ValueStr = valueStr;

                return AddCommand(commandName, customCommand);
            }

            Type commandType = Type.GetType(commandTypeName, false, true);
            if (commandType == null && AdditionalAssemblies?.Length > 0)
            {
                //Look for the type in our other assemblies
                for (int i = 0; i < AdditionalAssemblies.Length; i++)
                {
                    Assembly asm = AdditionalAssemblies[i];

                    commandType = asm.GetType(commandTypeName, false, true);
                    
                    if (commandType != null)
                    {
                        Logger.Debug($"Found \"{commandTypeName}\" in assembly \"{asm.GetName()}\"!");
                        break;
                    }
                }                
            }

            if (commandType == null)
            {
                EvtDispatcher.DispatchEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE,
                    new EvtBroadcastMessageArgs($"Cannot find command type \"{commandTypeName}\" for command \"{commandName}\" in all provided assemblies."));
                return false;
            }

            BaseCommand command = null;

            //Try to create an instance
            try
            {
                command = (BaseCommand)Activator.CreateInstance(commandType, Array.Empty<object>());
                command.Enabled = commandEnabled;
                command.ClassName = commandTypeName;
                command.DisplayInHelp = displayInHelp;
                command.CommandCategory = categories;
                command.Level = level;
                command.ValueStr = valueStr;
            }
            catch (Exception e)
            {
                EvtDispatcher.DispatchEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE,
                    new EvtBroadcastMessageArgs($"Unable to add command \"{commandName}\": \"{e.Message}\""));
            }

            return AddCommand(commandName, command);
        }

        public bool AddCommand(string commandName, BaseCommand command)
        {
            if (command == null)
            {
                Logger.Warning("Cannot add null command.");
                return false;
            }

            //Clean up the existing command before overwriting it with the new value
            if (AllCommands.TryGetValue(commandName, out BaseCommand existingCmd) == true)
            {
                existingCmd.CleanUp();
            }

            //Set and initialize the command
            AllCommands[commandName] = command;
            AllCommands[commandName].SetRequiredData(this, RoutineHandler, Logger, FolderPathResolver,
                CustomCodeHandler, DatabaseMngr, ClientServiceMngr, VControllerContainer, EvtDispatcher,
                WebSocketMngr, InputHndlr, StringNumParser);
            AllCommands[commandName].Initialize();

            return true;
        }

        public bool RemoveCommand(string commandName)
        {
            bool removed = AllCommands.Remove(commandName, out BaseCommand command);
            
            //Clean up the command
            command?.CleanUp();

            return removed;
        }

        private void CleanUpCommands()
        {
            foreach (KeyValuePair<string, BaseCommand> cmd in AllCommands)
            {
                cmd.Value.CleanUp();
            }
        }

        private void PopulateCommandsFromDB()
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                foreach (CommandData cmdData in context.Commands)
                {
                    AddCommand(cmdData.Name, cmdData.ClassName, cmdData.ValueStr, cmdData.Level,
                        cmdData.Enabled > 0, cmdData.DisplayInList > 0, cmdData.Category);
                }
            }
        }

        private void OnDataReloaded(EvtDataReloadedArgs e)
        {
            if (e.HardReload == false)
            {
                UpdateCommandsFromDB();
            }
            else
            {
                //Clean up and clear all commands
                CleanUpCommands();
                AllCommands.Clear();

                PopulateCommandsFromDB();
            }
        }

        private void UpdateCommandsFromDB()
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                List<string> encounteredCommands = new List<string>(context.Commands.Count());

                foreach (CommandData cmdData in context.Commands)
                {
                    string commandName = cmdData.Name;
                    if (AllCommands.TryGetValue(commandName, out BaseCommand baseCmd) == true)
                    {
                        //Remove this command if the type name is different so we can reconstruct it
                        if (baseCmd.ClassName != cmdData.ClassName)
                        {
                            RemoveCommand(commandName);

                            baseCmd = null;
                        }
                    }

                    //Add this command if it doesn't exist and should
                    if (baseCmd == null)
                    {
                        //Add this command
                        AddCommand(commandName, cmdData.ClassName, cmdData.ValueStr,
                            (int)cmdData.Level, cmdData.Enabled > 0, cmdData.DisplayInList > 0, cmdData.Category);
                    }
                    else
                    {
                        baseCmd.Level = (int)cmdData.Level;
                        baseCmd.ClassName = cmdData.ClassName;
                        baseCmd.Enabled = cmdData.Enabled > 0;
                        baseCmd.DisplayInHelp = cmdData.DisplayInList > 0;
                        baseCmd.CommandCategory = cmdData.Category;
                        baseCmd.ValueStr = cmdData.ValueStr;
                    }

                    encounteredCommands.Add(commandName);
                }

                //Remove commands that are no longer in the database
                foreach (string cmd in AllCommands.Keys)
                {
                    if (encounteredCommands.Contains(cmd) == false)
                    {
                        RemoveCommand(cmd);
                    }
                }
            }
        }

        private BaseCommand LoadCustomCommand(string commandName, string filePath)
        {
            //First try to read the file as an absolute path
            string codeText = FileHelpers.ReadFromTextFile(filePath);

            //If that wasn't found, try a relative path
            if (string.IsNullOrEmpty(codeText) == true)
            {
                codeText = FileHelpers.ReadFromTextFile(FolderPathResolver.DataFolderPath, filePath);
            }

            if (string.IsNullOrEmpty(codeText) == true)
            {
                EvtDispatcher.BroadcastMessage("Invalid source file for custom command. Double check its location.", Serilog.Events.LogEventLevel.Warning);
                return null;
            }

            Logger.Information($"Compiling custom command \"{commandName}\" from code file \"{filePath}\"...");

            CustomCodeCreationData<BaseCommand> cstmCmdCData = new CustomCodeCreationData<BaseCommand>(null, false, string.Empty);

            try
            {
                //Find a way to do this async down the road so it doesn't halt the thread
                var cstmCmdCTask = CustomCodeHandler.CompileCodeAndReturnObject<BaseCommand>(codeText);
                cstmCmdCTask.Wait();

                cstmCmdCData = cstmCmdCTask.Result;

                //Print error message
                if (cstmCmdCData.Success == false)
                {
                    EvtDispatcher.DispatchEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE,
                        new EvtBroadcastMessageArgs(cstmCmdCData.ErrorMessage, Serilog.Events.LogEventLevel.Warning));
                    return null;
                }
            }
            catch (CompilationErrorException e)
            {
                EvtDispatcher.BroadcastMessage($"Compiler error on custom command \"{commandName}\". {e.Message}", Serilog.Events.LogEventLevel.Warning);
                return null;
            }
            catch (Exception e)
            {
                EvtDispatcher.BroadcastMessage($"Error with custom command \"{commandName}\". {e.Message}", Serilog.Events.LogEventLevel.Warning);
                return null;
            }

            //Return the command
            return cstmCmdCData.NewObject;
        }
    }
}
