﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Removes a meme.
    /// </summary>
    public sealed class RemoveMemeCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"memename\"";

        public RemoveMemeCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count < 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.REMOVE_MEME_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to remove memes.");
                    return;
                }
            }

            //Check both for convenience - the argument as a string will have quotes included for multi-word memes
            string memeNameArg = arguments[0];
            string memeNameAsStr = args.Command.ArgumentsAsString;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Meme meme = context.Memes.FirstOrDefault(m => m.MemeName == memeNameArg || m.MemeName == memeNameAsStr);
                
                //Remove the meme if found
                if (meme != null)
                {
                    string memeName = meme.MemeName;

                    context.Memes.Remove(meme);
                    context.SaveChanges();

                    QueueMessage(args.ServiceName, $"Removed meme \"{memeName}\"!");
                }
                else
                {
                    QueueMessage(args.ServiceName, $"Meme not found.");
                }
            }
        }
    }
}
