﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Input.VirtualControllers;
using TRBot.Input.Consoles;

namespace TRBot.Main
{
    /// <summary>
    /// The Nintendo Switch.
    /// </summary>
    public sealed class SwitchConsole : GameConsole
    {
        public SwitchConsole()
        {
            Name = "switch";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(38)
            {
                { "left",       ConsoleHelpers.CreateAxisInput("left", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "right",      ConsoleHelpers.CreateAxisInput("right", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "up",         ConsoleHelpers.CreateAxisInput("up", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "down",       ConsoleHelpers.CreateAxisInput("down", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },
                { "rleft",      ConsoleHelpers.CreateAxisInput("rleft", (int)GlobalAxisVals.AXIS_RX, 0.5d, 0d, 0.5d) },
                { "rright",     ConsoleHelpers.CreateAxisInput("rright", (int)GlobalAxisVals.AXIS_RX, 0.5d, 1d, 0.5d) },
                { "rup",        ConsoleHelpers.CreateAxisInput("rup", (int)GlobalAxisVals.AXIS_RY, 0.5d, 0d, 0.5d) },
                { "rdown",      ConsoleHelpers.CreateAxisInput("rdown", (int)GlobalAxisVals.AXIS_RY, 0.5d, 1d, 0.5d) },

                { "dleft",      ConsoleHelpers.CreateButtonInput("dleft", (int)GlobalButtonVals.BTN1) },
                { "dright",     ConsoleHelpers.CreateButtonInput("dright", (int)GlobalButtonVals.BTN2) },
                { "dup",        ConsoleHelpers.CreateButtonInput("dup", (int)GlobalButtonVals.BTN3) },
                { "ddown",      ConsoleHelpers.CreateButtonInput("ddown", (int)GlobalButtonVals.BTN4) },
                { "a",          ConsoleHelpers.CreateButtonInput("a", (int)GlobalButtonVals.BTN9) },
                { "b",          ConsoleHelpers.CreateButtonInput("b", (int)GlobalButtonVals.BTN10) },
                { "x",          ConsoleHelpers.CreateButtonInput("x", (int)GlobalButtonVals.BTN11) },
                { "y",          ConsoleHelpers.CreateButtonInput("y", (int)GlobalButtonVals.BTN12) },
                { "minus",      ConsoleHelpers.CreateButtonInput("minus", (int)GlobalButtonVals.BTN13) },
                { "plus",       ConsoleHelpers.CreateButtonInput("plus", (int)GlobalButtonVals.BTN14) },
                { "l",          ConsoleHelpers.CreateButtonInput("l", (int)GlobalButtonVals.BTN15) },
                { "r",          ConsoleHelpers.CreateButtonInput("r", (int)GlobalButtonVals.BTN16) },
                { "zl",         ConsoleHelpers.CreateButtonInput("zl", (int)GlobalButtonVals.BTN17) },
                { "zr",         ConsoleHelpers.CreateButtonInput("zr", (int)GlobalButtonVals.BTN18) },
                { "ls",         ConsoleHelpers.CreateButtonInput("ls", (int)GlobalButtonVals.BTN19) },
                { "rs",         ConsoleHelpers.CreateButtonInput("rs", (int)GlobalButtonVals.BTN20) },
                { "#",          ConsoleHelpers.CreateBlankInput("#") },
                { ".",          ConsoleHelpers.CreateBlankInput(".") },

                //Spare buttons
                { "sb1",        ConsoleHelpers.CreateButtonInput("sb1", (int)GlobalButtonVals.BTN21) },
                { "sb2",        ConsoleHelpers.CreateButtonInput("sb2", (int)GlobalButtonVals.BTN22) },
                { "sb3",        ConsoleHelpers.CreateButtonInput("sb3", (int)GlobalButtonVals.BTN23) },
                { "sb4",        ConsoleHelpers.CreateButtonInput("sb4", (int)GlobalButtonVals.BTN24) },
                { "sb5",        ConsoleHelpers.CreateButtonInput("sb5", (int)GlobalButtonVals.BTN25) },
                { "sb6",        ConsoleHelpers.CreateButtonInput("sb6", (int)GlobalButtonVals.BTN26) },
                { "sb7",        ConsoleHelpers.CreateButtonInput("sb7", (int)GlobalButtonVals.BTN27) },
                { "sb8",        ConsoleHelpers.CreateButtonInput("sb8", (int)GlobalButtonVals.BTN28) },
                { "sb9",        ConsoleHelpers.CreateButtonInput("sb9", (int)GlobalButtonVals.BTN29) },
                { "sb10",       ConsoleHelpers.CreateButtonInput("sb10", (int)GlobalButtonVals.BTN30) },
                { "sb11",       ConsoleHelpers.CreateButtonInput("sb11", (int)GlobalButtonVals.BTN31) },
                { "sb12",       ConsoleHelpers.CreateButtonInput("sb12", (int)GlobalButtonVals.BTN32) },
            });

            InvalidInputCombos = new List<InvalidInputCombo>();
        }
    }
}
