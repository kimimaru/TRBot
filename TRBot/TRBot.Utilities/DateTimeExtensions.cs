﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Utilities
{
    /// <summary>
    /// Extension methods regarding DateTime.
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Obtains a consistent string representation of a DateTime object.
        /// </summary>
        /// <param name="dateTime">The DateTime object to convert to a string.</param>
        /// <returns>A string from a DateTime formatted as "year-month-day hours:minutes:seconds".</returns>
        public static string ToConsistentString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
