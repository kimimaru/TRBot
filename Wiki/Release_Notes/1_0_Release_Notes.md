Initial TRBot release, providing Twitch connection, vJoy integration for controller input, and more.

Please read the [wiki](https://github.com/teamradish/TRTwitchPlaysBot/wiki) for getting started.
