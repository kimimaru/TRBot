﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Data
{
    /// <summary>
    /// Data consent options. Flag values of 0 and 1 indicate opt-out and opt-in statuses respectively.
    /// This is a bitwise field.
    /// </summary>
    [Flags]
    public enum UserDataConsentOptions
    {
        None = 0,

        /// <summary>
        /// Determines consent options for user stats like credits.
        /// </summary>
        Stats = 1 << 0,

        /// <summary>
        /// Determines consent options for triggering memes.
        /// </summary>
        Memes = 1 << 1,

        /// <summary>
        /// Determines consent options for storing messages used for simulate data.
        /// </summary>
        Simulate = 1 << 2,

        /// <summary>
        /// Determines consent options for recent inputs.
        /// </summary>
        RecentInputs = 1 << 3,

        /// <summary>
        /// Determines consent options for tutorial messages.
        /// </summary>
        Tutorial = 1 << 4
    }
}
