﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Data
{
    /// <summary>
    /// Represents a custom message.
    /// </summary>
    public class CustomMessage
    {
        /// <summary>
        /// The ID of the message.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The contents of the message.
        /// </summary>
        public string Message { get; set; } = string.Empty;

        /// <summary>
        /// The value the message is triggered on.
        /// </summary>
        public long TriggerValue { get; set; } = 0L;

        /// <summary>
        /// Whether the message is enabled or not.
        /// </summary>
        public long Enabled { get; set; } = 1L;

        /// <summary>
        /// A helper property to get the message's enabled state.
        /// </summary>
        public bool IsEnabled => (Enabled > 0L);

        public CustomMessage()
        {

        }

        public CustomMessage(string message, in long triggerValue)
        {
            Message = message;
            TriggerValue = triggerValue;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 2;
                hash = (hash * 11) + (string.IsNullOrEmpty(Message) == true ? 0 : Message.GetHashCode());
                hash = (hash * 11) + TriggerValue.GetHashCode();
                hash = (hash *11) + Enabled.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"\"{Message}\":{TriggerValue}";
        }
    }
}
