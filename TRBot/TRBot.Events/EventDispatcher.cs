﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Concurrent;
using TRBot.Utilities.Logging;

namespace TRBot.Events
{
    /// <summary>
    /// Dispatches events in the application.
    /// </summary>
    public class EventDispatcher : IEventDispatcher
    {
        /// <summary>
        /// Whether extra features from the event dispatcher are enabled.
        /// The features are implementation-dependent.
        /// </summary>
        public bool ExtraFeaturesEnabled { get; protected set; } = true;

        protected ITRBotLogger Logger = null;

        private ConcurrentDictionary<string, Type> EventTypes = new ConcurrentDictionary<string, Type>();
        private ConcurrentDictionary<string, MulticastDelegate> Events = new ConcurrentDictionary<string, MulticastDelegate>();
        private ConcurrentDictionary<string, int> EventRegisterCounts = new ConcurrentDictionary<string, int>();

        public EventDispatcher(ITRBotLogger logger)
        {
            Logger = logger;
        }

        public virtual void Dispose()
        {
            EventTypes.Clear();
            Events.Clear();
            EventRegisterCounts.Clear();
        }

        public virtual void EnableExtraFeatures()
        {
            ExtraFeaturesEnabled = true;
        }

        public virtual void DisableExtraFeatures()
        {
            ExtraFeaturesEnabled = false;
        }

        //NOTE: Unsure about keeping this, hence why it's not public
        //Useful only in tests? Verify this
        private void RegisterEvent(string eventName, Type eventType)
        {
            if (eventType == null)
            {
                throw new ArgumentNullException(nameof(eventType));
            }

            if (typeof(EventArgs).IsAssignableFrom(eventType) == false)
            {
                throw new ArgumentException($"{eventType.Name} is not {nameof(EventArgs)} or derived from it!");
            }
            
            if (EventTypes.TryGetValue(eventName, out Type type) == false)
            {
                EventTypes[eventName] = eventType;

                EventRegisterCounts[eventName] = 1;

                Logger.Debug($"Newly registered event type \"{eventType.Name}\" for event \"{eventName}\"");

                return;
            }
            
            if (eventType != type)
            {
                throw new InvalidOperationException($"Trying to register event \"{eventName}\" of type \"{eventType.Name}\" when it's already registered with type \"{type.Name}\". Unregister it first to register the new type.");
            }

            //Track how many times this event has been registered
            EventRegisterCounts[eventName] += 1;

            Logger.Debug($"Registered event \"{eventName}\", which now has {EventRegisterCounts[eventName]} other registers");
        }

        public void RegisterEvent<T>(string eventName) where T : EventArgs
        {
            RegisterEvent(eventName, typeof(T));
        }

        public void UnregisterEvent(string eventName)
        {
            if (EventRegisterCounts.TryGetValue(eventName, out int registerCount) == true)
            {
                registerCount -= 1;

                //There are still some events registered
                if (registerCount > 0)
                {
                    EventRegisterCounts[eventName] = registerCount;

                    Logger.Debug($"Unregistered event \"{eventName}\", which has {registerCount} other register(s) remaining");
                    return;
                }

                //Remove the events once it's no longer registered with anything 
                if (EventTypes.TryRemove(eventName, out Type type) == true)
                {
                    Logger.Debug($"Completely unregistered event type \"{type.Name}\" for \"{eventName}\"");
                }

                EventRegisterCounts.TryRemove(eventName, out int count);
            }
        }

        public bool HasRegisteredEvent(string eventName)
        {
            return EventTypes.ContainsKey(eventName);
        }

        public void AddListener<T>(string eventName, Action<T> action) where T : EventArgs
        {
            Type actionType = typeof(T);

            if (EventTypes.TryGetValue(eventName, out Type eventType) == false)
            {
                Logger.Debug($"No type registered for event \"{eventName}\". The added listener must match the type that will get registered.");
            }

            if (eventType != null && eventType != actionType)
            {
                throw new InvalidOperationException($"Trying to add listener type \"{actionType.Name}\", which doesn't match the registered type \"{eventType.Name}\"");
            }

            if (Events.TryGetValue(eventName, out MulticastDelegate evtDel) == false)
            {
                Events[eventName] = action;
            }
            else
            {
                Events[eventName] = (MulticastDelegate)Delegate.Combine(evtDel, action);
            }

            Logger.Debug($"Added listener for event \"{eventName}\"");
        }
        
        public void RemoveListener<T>(string eventName, Action<T> action) where T : EventArgs
        {
            Type actionType = typeof(T);

            if (EventTypes.TryGetValue(eventName, out Type eventType) == false)
            {
                Logger.Debug($"No type registered for event \"{eventName}\". The final listener must match the type that will get registered.");
            }

            if (eventType != null && eventType != actionType)
            {
                throw new InvalidOperationException($"Trying to remove listener type \"{actionType.Name}\", which doesn't match the registered type \"{eventType.Name}\"");
            }

            if (Events.TryGetValue(eventName, out MulticastDelegate evtDel) == false)
            {
                return;
            }
            
            evtDel = (MulticastDelegate)Delegate.Remove(evtDel, action);
            Events[eventName] = evtDel;
            
            Logger.Debug($"Removed listener from event \"{eventName}\"");
            
            if (evtDel == null)
            {
                Events.TryRemove(eventName, out var val);
                Logger.Debug($"No listeners remaining on event \"{eventName}\"");
            }
        }

        public void RemoveAllListeners(string eventName)
        {
            if (Events.ContainsKey(eventName) == true)
            {
                Events[eventName] = null;
                Events.TryRemove(eventName, out var val);
            }
        }

        public virtual void DispatchEvent<T>(string eventName, T eventData) where T : EventArgs
        {
            if (Events.TryGetValue(eventName, out MulticastDelegate evtDel) == false)
            {
                Logger.Debug($"No listeners for dispatched event \"{eventName}\"");
                return;
            }

            if (EventTypes.TryGetValue(eventName, out Type eventType) == false)
            {
                throw new InvalidOperationException($"No type registered with event \"{eventName}\". Register a type for this event before dispatching it.");
            }

            Type dataType = typeof(T);

            if (eventType != typeof(T))
            {
                throw new InvalidOperationException($"Mismatched types: Event type = \"{eventType.Name}\" and data type = \"{dataType.Name}\"");
            }

            //Casting and taking advantage of the type system is faster than calling DynamicInvoke on the MulticastDelegate
            Action<T> actionDel = (Action<T>)evtDel;

            Logger.Debug($"Dispatching event \"{eventName}\"");

            try
            {
                actionDel.Invoke(eventData);
            }
            catch (Exception e)
            {
                Logger.Error($"Error dispatching event \"{eventName}\". {e.Message}\n{e.StackTrace}");
            }
        }

        public virtual void DispatchEventWithoutExtraFeatures<T>(string eventName, T eventData) where T : EventArgs
        {
            DispatchEvent<T>(eventName, eventData);
        }
    }
}
