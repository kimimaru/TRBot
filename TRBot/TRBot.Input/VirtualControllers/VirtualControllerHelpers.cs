﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Utilities;
using TRBot.Utilities.Logging;

namespace TRBot.Input.VirtualControllers
{
    /// <summary>
    /// Contains helper methods regarding virtual controllers.
    /// </summary>
    public static class VirtualControllerHelpers
    {
        /// <summary>
        /// Returns the default type of virtual controller of a given operating system. 
        /// </summary>
        /// <param name="operatingSystem">The operating system to get the default virtual controller type for.</param>
        /// <returns>The default virtual controller type of the given operating system.</returns>
        public static VirtualControllerTypes GetDefaultVControllerTypeForPlatform(in TRBotOSPlatform.OS operatingSystem)
        {
            switch (operatingSystem)
            {
                case TRBotOSPlatform.OS.Windows: return VirtualControllerTypes.vJoy;
                case TRBotOSPlatform.OS.GNULinux: return VirtualControllerTypes.uinput;
                default: return VirtualControllerTypes.Dummy;
            }
        }

        /// <summary>
        /// Returns the default virtual controller manager of a given operating system. 
        /// </summary>
        /// <param name="operatingSystem">The operating system to get the default virtual controller manager for.</param>
        /// <returns>The default virtual controller manager of the given operating system.</returns>
        public static IVirtualControllerManager GetDefaultVControllerMngrForPlatform(in TRBotOSPlatform.OS operatingSystem,
            ITRBotLogger logger)
        {
            switch (operatingSystem)
            {
                case TRBotOSPlatform.OS.Windows: return new VJoyControllerManager(logger);
                case TRBotOSPlatform.OS.GNULinux: return new UInputControllerManager(logger);
                default: return new DummyControllerManager(logger);
            }
        }
        
        /// <summary>
        /// Returns the virtual controller manager of a given virtual controller type. 
        /// </summary>
        /// <param name="vControllerType">The virtual controller type to get the virtual controller manager for.</param>
        /// <returns>The virtual controller manager for the given virtual controller type.</returns>
        public static IVirtualControllerManager GetVControllerMngrForType(in VirtualControllerTypes vControllerType,
            ITRBotLogger logger)
        {
            switch (vControllerType)
            {
                case VirtualControllerTypes.vJoy: return new VJoyControllerManager(logger);
                case VirtualControllerTypes.uinput: return new UInputControllerManager(logger);
                default: return new DummyControllerManager(logger);
            }
        }

        /// <summary>
        /// Tells if a given virtual controller type is supported by a given operating system.
        /// </summary>
        /// <param name="vControllerType">The virtual controller type.</param>
        /// <param name="operatingSystem">The operating system.</param>
        /// <returns>true if the given virtual controller type is supported by the given operating system, otherwise false.</returns>
        public static bool IsVControllerSupported(in VirtualControllerTypes vControllerType, in TRBotOSPlatform.OS operatingSystem)
        {
            switch (vControllerType)
            {
                case VirtualControllerTypes.vJoy:
                    return operatingSystem == TRBotOSPlatform.OS.Windows;
                case VirtualControllerTypes.uinput:
                    return operatingSystem == TRBotOSPlatform.OS.GNULinux;
                case VirtualControllerTypes.Dummy:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Validates a given virtual controller type by switching an unsupported type to a supported one on a given operating system.
        /// </summary>
        /// <param name="lastVControllerType">The virtual controller type.</param>
        /// <param name="operatingSystem">The operating system.</param>
        /// <returns>A valid virtual controller type on the given operating system.</returns>
        public static VirtualControllerTypes ValidateVirtualControllerType(in VirtualControllerTypes lastVControllerType, in TRBotOSPlatform.OS operatingSystem)
        {
            return ValidateVirtualControllerType((long)lastVControllerType, operatingSystem);
        }

        /// <summary>
        /// Validates a given virtual controller type by switching an unsupported type to a supported one on a given operating system.
        /// </summary>
        /// <param name="lastVControllerType">The virtual controller type.</param>
        /// <param name="operatingSystem">The operating system.</param>
        /// <returns>A valid virtual controller type on the given operating system.</returns>
        public static VirtualControllerTypes ValidateVirtualControllerType(in long lastVControllerType, in TRBotOSPlatform.OS operatingSystem)
        {
            VirtualControllerTypes newVControllerType = (VirtualControllerTypes)lastVControllerType;

            //Check if the virtual controller type is supported on this platform
            if (IsVControllerSupported(newVControllerType, operatingSystem) == false)
            {
                //It's not supported, so return a supported value to prevent issues on this platform
                newVControllerType = VirtualControllerHelpers.GetDefaultVControllerTypeForPlatform(operatingSystem);
            }

            return newVControllerType;
        }
    }
}
