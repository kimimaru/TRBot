﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Scripting;

namespace TRBot.CustomCode
{
    /// <summary>
    /// Handles compiling and executing custom code.
    /// </summary>
    public interface ICustomCodeHandler
    {
        /// <summary>
        /// Compiles and executes the given C# code.
        /// </summary>
        /// <param name="code">A string representing the C# code to execute.</param>
        /// <param name="globals">An optional object instance whose members can be accessed by the code as global variables.</param>
        /// <param name="globalsType">The type of the optional global object.</param>
        /// <returns>A Task regarding the code execution.</returns>
        /// <exception cref="CompilationErrorException"></exception>
        Task ExecuteCode(string code, object globals, Type globalsType);

        /// <summary>
        /// Compiles C# code and returns a <see cref="CustomCodeCreationData{T}"/> with the given object of type T if successful.
        /// </summary>
        /// <param name="code">The C# code to run for the script.
        /// The code must return an instance of an object of type T.</param>
        /// <returns>A Task of CustomCommandCreationData, containing the new command and an error message.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="CompilationErrorException"></exception>
        /// <exception cref="ArgumentException"></exception>
        Task<CustomCodeCreationData<T>> CompileCodeAndReturnObject<T>(string code);
    }
}