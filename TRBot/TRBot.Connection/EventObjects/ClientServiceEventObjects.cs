/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Serilog.Events;
using TRBot.Events;

namespace TRBot.Connection
{
    //These classes are common abstractions on top of the service APIs
    //Very specific events should instead be registered with the service's specific data type

    #region Event Data Objects

    public abstract class EvtBaseMsgData
    {
        [JsonProperty("userExternalId")]
        public string UserExternalID { get; protected set; } = string.Empty;

        [JsonProperty("username")]
        public string Username { get; protected set; } = string.Empty;

        protected EvtBaseMsgData(string userExternalID, string username)
        {
            UserExternalID = userExternalID;
            Username = username;
        }
    }

    public class EvtUserMsgData : EvtBaseMsgData
    {
        [JsonProperty("message")]
        public string Message { get; protected set; } = string.Empty;

        public EvtUserMsgData(string userExternalID, string username, string message)
         : base (userExternalID, username)
        {
            Message = message;
        }
    }

    public class EvtChatCommandData
    {
        [JsonProperty("argumentsAsList")]
        public List<string> ArgumentsAsList { get; protected set; } = null;

        [JsonProperty("argumentsAsString")]
        public string ArgumentsAsString { get; } = string.Empty;

        [JsonProperty("userMessage")]
        public EvtUserMsgData UserMessage { get; protected set; } = null;

        [JsonProperty("commandIdentifier")]
        public string CommandIdentifier { get; } = string.Empty;

        [JsonProperty("commandText")]
        public string CommandText { get; } = string.Empty;

        public EvtChatCommandData(List<string> argsAsList, string argsAsStr, EvtUserMsgData usrMsgData,
            string cmdIdentifier, string cmdText)
        {
            ArgumentsAsList = argsAsList;
            ArgumentsAsString = argsAsStr;
            UserMessage = usrMsgData;
            CommandIdentifier = cmdIdentifier;
            CommandText = cmdText;
        }
    }

    public class EvtErrorData
    {
        [JsonProperty("message")]
        public string Message { get; protected set; } = string.Empty;

        public EvtErrorData(string message)
        {
            Message = message;
        }
    }

    #endregion

    #region Event Args Objects

    public class EvtUserMessageArgs : EvtBaseEventArgs
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;

        [JsonProperty("userMessage")]
        public EvtUserMsgData UserMessage { get; init; } = null;
    }

    public class EvtUserInputArgs<T> : EvtBaseEventArgs
    {
        [JsonProperty("userMessage")]
        public EvtUserMsgData UserMessage { get; init; } = null;

        [JsonProperty("parsedSequence")]
        public T ParsedSequence { get; init; }

        [JsonProperty("userLevel")]
        public long UserLevel { get; init; } = 0L;

        [JsonProperty("serviceName")]
        public string ServiceName { get; init; }

        [JsonProperty("messageEventId")]
        public string MessageEventID { get; init; }

        public EvtUserInputArgs()
        {

        }

        public EvtUserInputArgs(EvtUserMsgData userMsg, T parsedSequence,
            in long userLevel, string serviceName, string messageEventID)
        {
            UserMessage = userMsg;
            ParsedSequence = parsedSequence;
            UserLevel = userLevel;
            ServiceName = serviceName;
            MessageEventID = messageEventID;
        }
    }

    public class EvtChatCommandArgs : EvtBaseEventArgs
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; set; } = string.Empty;

        [JsonProperty("command")]
        public EvtChatCommandData Command { get; set; } = null;
    }

    public class EvtConnectedArgs : EvtBaseEventArgs
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;

        [JsonProperty("botUsername")]
        public string BotUsername { get; init; } = string.Empty;
    }

    public class EvtConnectionErrorArgs : EvtBaseEventArgs
    {
        [JsonProperty("error")]
        public EvtErrorData Error { get; init; } = null;

        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;

        [JsonProperty("botUsername")]
        public string BotUsername { get; init; } = string.Empty;
    }

    public class EvtReconnectedArgs : EvtBaseEventArgs
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;
    }

    public class EvtDisconnectedArgs : EvtBaseEventArgs
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;
    }

    public class EvtBroadcastMessageArgs : EvtBaseEventArgs
    {
        [JsonProperty("message")]
        public string Message { get; init; } = string.Empty;

        [JsonProperty("logLevel")]
        public LogEventLevel LogLevel { get; init; } = default;

        public EvtBroadcastMessageArgs()
        {

        }

        public EvtBroadcastMessageArgs(string message)
        {
            Message = message;
        }

        public EvtBroadcastMessageArgs(string message, in LogEventLevel logLevel)
        {
            Message = message;
            LogLevel = logLevel;
        }
    }

    public class EvtBotMessageArgs : EvtBaseEventArgs
    {
        [JsonProperty("message")]
        public string Message { get; init; } = string.Empty;

        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;

        [JsonProperty("logLevel")]
        public LogEventLevel LogLevel { get; init; } = default;

        public EvtBotMessageArgs()
        {

        }

        public EvtBotMessageArgs(string message, string serviceName, Serilog.Events.LogEventLevel logLevel)
        {
            Message = message;
            ServiceName = serviceName;
            LogLevel = logLevel;
        }

        public EvtBotMessageArgs(string message, string serviceName, int logLevel)
            : this(message, serviceName, (Serilog.Events.LogEventLevel)logLevel)
        {
        }
    }

    public class EvtOutgoingBotMessageArgs : EvtBaseEventArgs
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; init; } = string.Empty;

        [JsonProperty("userMessage")]
        public EvtUserMsgData UserMessage { get; init; } = null;

        public EvtOutgoingBotMessageArgs()
        {

        }
    }

    #endregion
}
