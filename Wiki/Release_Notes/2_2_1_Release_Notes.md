TRBot v2.2.1, featuring bug fixes, quality of life changes, and LiveSplitOne integration.

Summary of changes:
- **The license has been changed from AGPL 3.0 or later to AGPL 3.0 only**.
- [LiveSplitOne](https://github.com/LiveSplit/LiveSplitOne) integration! Control your speedrun timer directly through TRBot!
- Added a built-in Game Boy Color console.
- Added a `LowercasePreparser` to fix regressions with text casing preventing inputs from being executed.
- Added convenient new moderation commands for silencing, unsilencing, and listing silenced users.
- Fixed an oversight in `ParsedInput` equality checks, fixing input exercises.
- Improved exception handling for executing arbitrary code with the `ExecCommand` or `ExecFromFileCommand`. Exceptions with arbitrary code will no longer take down the entire bot.
- Added the ability to have a set of periodic messages the bot rotates among.
- Added permissions to add/remove input macros, input synonyms, and memes.
- Added a permission for stopping all inputs.
- External files, such as the game message, bingo pipe, and chatbot pipe, now read paths **relative to the executable location** instead of the Data folder if absolute paths are not in use. In the database, this means "GameMessage.txt" turns to "Data/GameMessage.txt".
- The bingo and chatbot pipes are now by default created in the OS temp folder. This prevents them persisting in the data folder, fixing issues copying/pasting the folder.
- Updated TwitchLib from 3.1.0 to 3.2.0.

To run the main application, locate the **TRBot.Main** binary for your platform and execute it.

Please read the [wiki](https://codeberg.org/kimimaru/TRBot/src/tag/2.2.1/Wiki/Home.md) for your use-case.

**IMPORTANT:** Please read the [migration guide](https://codeberg.org/kimimaru/TRBot/src/tag/2.2.1/Wiki/Migrating-Data.md) and download the **TRBotDataMigrationTool** if you're upgrading from 1.8!

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.2.1/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.

**NOTE:** Due to an [open bug](https://github.com/go-gitea/gitea/issues/677) in Gitea, this release has duplicate binary downloads. The files are identical, so there's no issue downloading one over the other.
