﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Displays or changes the current way to send custom messages.
    /// </summary>
    public sealed class GetSetCustomMessageSendTypeCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"custom message send type (string/int)\"";
        private string CachedSendTypesStr = string.Empty;
        
        public GetSetCustomMessageSendTypeCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            //Show all send types
            CustomMessageSendTypes[] sendTypesArr = EnumUtility.GetValues<CustomMessageSendTypes>.EnumValues;

            for (int i = 0; i < sendTypesArr.Length; i++)
            {
                CustomMessageSendTypes sendType = sendTypesArr[i];

                CachedSendTypesStr += sendType.ToString();

                if (i < (sendTypesArr.Length - 1))
                {
                    CachedSendTypesStr += ", ";
                }
            }
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            long curSendType = DatabaseMngr.GetSettingInt(SettingsConstants.CUSTOM_MESSAGE_SEND_TYPE, 0L);
            CustomMessageSendTypes sendType = (CustomMessageSendTypes)curSendType;

            //See the virtual controller
            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current custom message send type is {sendType}. To set the send type, add one as an argument: {CachedSendTypesStr}");
                return;
            }

            //Invalid number of arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has the ability to set the send type
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.SET_CUSTOM_MESSAGE_SEND_TYPE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You don't have the ability to set the custom message send type!");
                    return;
                }
            }

            string sendTypeStr = arguments[0];

            //Parse
            if (EnumUtility.TryParseEnumValue(sendTypeStr, out CustomMessageSendTypes parsedSendType) == false)
            {
                QueueMessage(args.ServiceName, $"Please enter a custom message send type: {CachedSendTypesStr}");
                return;
            }

            //Same send type
            if (parsedSendType == sendType)
            {
                QueueMessage(args.ServiceName, $"The current custom message send type is already {sendType}!");
                return;
            }
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Set the value and save
                Settings sendTypeSetting = context.GetSetting(SettingsConstants.CUSTOM_MESSAGE_SEND_TYPE);
                sendTypeSetting.ValueInt = (long)parsedSendType;

                context.SaveChanges();
            }
            
            QueueMessage(args.ServiceName, $"Changed the custom message send type from {sendType} to {parsedSendType}!");
        }
    }
}
