/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Input.Parsing
{
    /// <summary>
    /// Parser validation data.
    /// </summary>
    public struct ParserValidationData
    {
        public ParserValidationTypes ParserValidationType;
        public string ErrorMessage;

        public ParserValidationData(in ParserValidationTypes parserValidationType, string errorMessage)
        {
            ParserValidationType = parserValidationType;
            ErrorMessage = errorMessage;
        }

        public override bool Equals(object obj)
        {
            if (obj is ParserValidationData parserValData)
            {
                return (this == parserValData);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 37;
                hash = (hash * 31) + ParserValidationType.GetHashCode();
                hash = (hash * 31) + ((ErrorMessage == null) ? 0 : ErrorMessage.GetHashCode());
                return hash;
            }
        }

        public static bool operator==(ParserValidationData a, ParserValidationData b)
        {
            return (a.ParserValidationType == b.ParserValidationType && a.ErrorMessage == b.ErrorMessage);
        }

        public static bool operator!=(ParserValidationData a, ParserValidationData b)
        {
            return !(a == b);
        }
    }
}