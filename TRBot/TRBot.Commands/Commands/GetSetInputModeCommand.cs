﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;
using TRBot.Permissions;
using TRBot.Routines;

namespace TRBot.Commands
{
    /// <summary>
    /// Displays or changes the current input mode.
    /// </summary>
    public sealed class GetSetInputModeCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"input mode (string/int)\"";
        private string CachedInputModesStr = string.Empty;
        
        public GetSetInputModeCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            //Show all input modes
            InputModes[] inputModeArr = EnumUtility.GetValues<InputModes>.EnumValues;

            for (int i = 0; i < inputModeArr.Length; i++)
            {
                InputModes inputMode = inputModeArr[i];

                CachedInputModesStr += inputMode.ToString();

                if (i < (inputModeArr.Length - 1))
                {
                    CachedInputModesStr += ", ";
                }
            }
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            long curInputMode = DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE, 0L);
            InputModes inpMode = (InputModes)curInputMode;

            //See the input mode
            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current input mode is {inpMode}. To set the input mode, add one as an argument: {CachedInputModesStr}");
                return;
            }

            //Invalid number of arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has the ability to set the mode
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.SET_INPUT_MODE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You don't have the ability to set the input mode!");
                    return;
                }
            }

            string inputModeStr = arguments[0];

            //Parse
            if (EnumUtility.TryParseEnumValue(inputModeStr, out InputModes parsedInputMode) == false)
            {
                QueueMessage(args.ServiceName, $"Please enter a valid input mode: {CachedInputModesStr}");
                return;
            }

            //Same mode
            if (parsedInputMode == inpMode)
            {
                QueueMessage(args.ServiceName, $"The current input mode is already {inpMode}!");
                return;
            }
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Set the value and save
                Settings inputModeSetting = context.GetSetting(SettingsConstants.INPUT_MODE);
                inputModeSetting.ValueInt = (long)parsedInputMode;

                context.SaveChanges();
            }
            
            QueueMessage(args.ServiceName, $"Changed the input mode from {inpMode} to {parsedInputMode}!");

            //If we set it to Anarchy, check if the Democracy routine is active and remove it if so
            if (parsedInputMode == InputModes.Anarchy)
            {
                BaseRoutine democracyRoutine = RoutineHandler.FindRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME);
                if (democracyRoutine != null)
                {
                    RoutineHandler.RemoveRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME);
                }
            }
            //If we set it to Democracy, add the routine if it's not already active
            else if (parsedInputMode == InputModes.Democracy)
            {
                DemocracyRoutine democracyRoutine = RoutineHandler.FindRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME) as DemocracyRoutine;

                if (democracyRoutine == null)
                {
                    long votingTime = DatabaseMngr.GetSettingInt(SettingsConstants.DEMOCRACY_VOTE_TIME, 10000L);

                    democracyRoutine = new DemocracyRoutine(votingTime);

                    RoutineHandler.AddRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME, democracyRoutine);
                }
            }
        }
    }
}
