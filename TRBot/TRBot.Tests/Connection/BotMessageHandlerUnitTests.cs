/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NSubstitute;
using NUnit.Framework;
using TRBot.Connection;
using TRBot.Utilities.Logging;
using Serilog.Events;

namespace TRBot.Tests.Connection
{
    [TestFixture]
    public class BotMessageHandlerUnitTests
    {
        private IBotMessageHandler BotMsgHandler = null;
        private ITRBotLogger Logger = null;
        private IClientService ClientService = null;

        [SetUp]
        public void Setup()
        {
            Logger = Substitute.For<ITRBotLogger>();
            ClientService = Substitute.For<IClientService>();
            BotMsgHandler = new BotMessageHandler(Logger, MessageThrottlingOptions.None,
                new MessageThrottleData(30000L, 20L), 500, true);

            ClientService.CanSendMessages.ReturnsForAnyArgs(true);
            ClientService.IsConnected.ReturnsForAnyArgs(true);
            ClientService.IsInitialized.ReturnsForAnyArgs(true);

            BotMsgHandler.SetClientService(ClientService);
        }

        [TearDown]
        public void Teardown()
        {

        }

        private static TestCaseData[] VerifyQueueMessageProducesCorrectNumberOfMessagesTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(10,
                    new IServiceMessage[]
                    { 
                        new BasicMessage("test", false, string.Empty, LogEventLevel.Information),
                    },
                    1),
                new TestCaseData(10,
                    new IServiceMessage[]
                    {
                        new BasicMessage("test test1", false, string.Empty, LogEventLevel.Information),
                    },
                    1),
                new TestCaseData(10,
                    new IServiceMessage[]
                    {
                        new BasicMessage("test", false, string.Empty, LogEventLevel.Information),
                        new BasicMessage("test1", false, string.Empty, LogEventLevel.Information),
                    },
                    2),
                new TestCaseData(10,
                    new IServiceMessage[]
                    {
                        new BasicMessage("test test test", false, string.Empty, LogEventLevel.Information),
                    },
                    2),
                new TestCaseData(1,
                    new IServiceMessage[]
                    {
                        new BasicMessage("this is a test string", false, string.Empty, LogEventLevel.Information),
                    },
                    21),
                new TestCaseData(1,
                    new IServiceMessage[]
                    {
                        new BasicMessage("this is a ", false, string.Empty, LogEventLevel.Information),
                        new BasicMessage("test string", false, string.Empty, LogEventLevel.Information),
                    },
                    21),
            };
        }

        [TestCaseSource(nameof(VerifyQueueMessageProducesCorrectNumberOfMessagesTestCases))]
        public void VerifyQueueMessageProducesCorrectNumberOfMessages(int charLimit, IServiceMessage[] texts, int expectedMsgCount)
        {
            BotMsgHandler.BotMessageCharLimit = charLimit;

            for (int i = 0; i < texts.Length; i++)
            {
                BotMsgHandler.QueueMessage(texts[i]);
            }

            Assert.That(BotMsgHandler.ClientMessageCount, Is.EqualTo(expectedMsgCount));
        }

        private static TestCaseData[] VerifyQueueMessageStringProducesCorrectNumberOfMessagesTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(10,
                    new string[] { "test" },
                    1),
                new TestCaseData(10,
                    new string[] { "test test1" },
                    1),
                new TestCaseData(10,
                    new string[] { "test", "test1" },
                    2),
                new TestCaseData(10,
                    new string[] { "test test test" },
                    2),
                new TestCaseData(1,
                    new string[] { "this is a test string" },
                    21),
                new TestCaseData(1,
                    new string[] { "this is a ", "test string" },
                    21),
            };
        }

        [TestCaseSource(nameof(VerifyQueueMessageStringProducesCorrectNumberOfMessagesTestCases))]
        public void VerifyQueueMessageStringProducesCorrectNumberOfMessages(int charLimit, string[] texts, int expectedMsgCount)
        {
            BotMsgHandler.BotMessageCharLimit = charLimit;

            for (int i = 0; i < texts.Length; i++)
            {
                BotMsgHandler.QueueMessage(texts[i]);
            }

            Assert.That(BotMsgHandler.ClientMessageCount, Is.EqualTo(expectedMsgCount));
        }

        [Test]
        public void VerifyNoMessagesQueuedWhenMessageQueuingTurnedOff()
        {
            BotMsgHandler.ToggleMessageQueueing(false);

            BotMsgHandler.QueueMessage("Test message");

            Assert.That(BotMsgHandler.ClientMessageCount, Is.EqualTo(0));
        }
    }
}
