﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Data;
using TRBot.Routines;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// Allows a user to enter a group bet for a chance to win credits.
    /// </summary>
    public class EnterGroupBetCommand : BaseCommand
    {
        private List<string> AddedGroupBetNames = new List<string>();

        public EnterGroupBetCommand()
        {

        }

        public override void CleanUp()
        {
            for (int i = 0; i < AddedGroupBetNames.Count; i++)
            {
                string routineName = AddedGroupBetNames[i];

                GroupBetRoutine groupBetRoutine = RoutineHandler.FindRoutine(routineName) as GroupBetRoutine;
                if (groupBetRoutine != null)
                {
                    groupBetRoutine.GroupBetEnded -= OnGroupBetEnded;
                }

                RoutineHandler.RemoveRoutine(routineName);
            }

            AddedGroupBetNames.Clear();
            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 1)
            {
                QueueMessage(args.ServiceName, "Please specify a bet amount!");
                return;
            }

            string creditsName = DatabaseMngr.GetCreditsName();
            string userName = args.Command.UserMessage.Username;
            string userExternalID = args.Command.UserMessage.UserExternalID;

            //Check if we can parse the bet amount
            long betAmount = -1L;
            bool success = long.TryParse(arguments[0], out betAmount);
            if (success == false || betAmount <= 0)
            {
                QueueMessage(args.ServiceName, $"Please specify a positive whole number of {creditsName.Pluralize(0)} greater than 0!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user exists
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
                if (user == null)
                {
                    QueueMessage(args.ServiceName, "The user calling this does not exist in the database!");
                    return;
                }

                //No ability to enter group bets
                if (user.HasEnabledAbility(PermissionConstants.GROUP_BET_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to participate in a group bet!");
                    return;
                }

                //Validate credit amount
                if (user.Stats.Credits < betAmount)
                {
                    QueueMessage(args.ServiceName, $"You don't have enough {creditsName.Pluralize(0)} to bet this much!");
                    return;
                }

                if (user.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "You can't participate in the group bet since you opted out of bot stats.");
                    return;
                }
            }

            //Get total time and minimum participants required
            long groupBetTime = DatabaseMngr.GetSettingInt(SettingsConstants.GROUP_BET_TOTAL_TIME, 120000L);
            int groupBetMinUsers = (int)DatabaseMngr.GetSettingInt(SettingsConstants.GROUP_BET_MIN_PARTICIPANTS, 3L);

            string routineName = RoutineConstants.GROUP_BET_ROUTINE_NAME_PREFIX + args.ServiceName;

            GroupBetRoutine groupBetRoutine = RoutineHandler.FindRoutine(routineName) as GroupBetRoutine;

            //We haven't started the group bet, so start it up
            if (groupBetRoutine == null)
            {
                IClientService service = ClientServiceMngr.GetClientService(args.ServiceName);

                groupBetRoutine = new GroupBetRoutine(routineName, groupBetTime, groupBetMinUsers, service);
                RoutineHandler.AddRoutine(routineName, groupBetRoutine);

                groupBetRoutine.GroupBetEnded -= OnGroupBetEnded;
                groupBetRoutine.GroupBetEnded += OnGroupBetEnded;

                AddedGroupBetNames.Add(routineName);
            }

            //From hereon, use the routine's total time and min participant values
            //The database values could have changed in between calls of this command
            //and thus wouldn't be applicable to the group bet created for the first participant

            //See if the user is already in the group bet
            bool prevParticipant = groupBetRoutine.TryGetParticipant(userExternalID,
                out GroupBetRoutine.ParticipantData participantData);

            groupBetRoutine.AddOrUpdateParticipant(userExternalID, betAmount);

            string message = string.Empty;

            //Newly added since they were not previously there
            if (prevParticipant == false)
            {
                message = $"{userName} entered the group bet with {betAmount} {creditsName.Pluralize(betAmount)}! You now have a chance to be chosen as the winner of the group bet.";
                
                int participantCount = groupBetRoutine.ParticipantCount;
                
                if (participantCount < groupBetRoutine.MinParticipants)
                {
                    int diff = groupBetRoutine.MinParticipants - groupBetRoutine.ParticipantCount;
                    message += $" {diff} more user(s) are required to start the group bet!";
                }

                QueueMessage(args.ServiceName, message);

                //Check if we have enough participants now
                if (participantCount == groupBetRoutine.MinParticipants)
                {
                    TimeSpan timeSpan = TimeSpan.FromMilliseconds(groupBetRoutine.MillisecondsForBet);

                    QueueMessage(args.ServiceName, $"The group bet has enough participants and will start in {timeSpan.Minutes} minute(s) and {timeSpan.Seconds} second(s), so join before then if you want in! A random winner will be chosen from the participants.");
                }
            }
            else
            {
                QueueMessage(args.ServiceName, $"{userName} adjusted their group bet from {participantData.ParticipantBet} to {betAmount} {creditsName.Pluralize(betAmount)}!");
            }
        }

        private void OnGroupBetEnded(string routineName)
        {
            GroupBetRoutine groupBetRoutine = RoutineHandler.FindRoutine(routineName) as GroupBetRoutine;
            if (groupBetRoutine != null)
            {
                groupBetRoutine.GroupBetEnded -= OnGroupBetEnded;
            }

            AddedGroupBetNames.Remove(routineName);
        }
    }
}
