﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using static TRBot.Data.DataEventObjects;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that notifies all data should be reloaded.
    /// </summary>
    public sealed class ReloadCommand : BaseCommand
    {
        private const string SOFT_RELOAD_ARG = "soft";
        private const string HARD_RELOAD_ARG = "hard";

        private string UsageMessage = $"Usage - Soft reload: \"{SOFT_RELOAD_ARG}\" or no args | Hard reload: \"{HARD_RELOAD_ARG}\"";

        public ReloadCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            EvtDispatcher.RegisterEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA);
        }

        public override void CleanUp()
        {
            EvtDispatcher.UnregisterEvent(DataEventNames.RELOAD_DATA);
            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //Soft reload with no arguments
            if (arguments.Count == 0)
            {
                ReloadData(new EvtDataReloadedArgs(false, args.ServiceName));
                return;
            }

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string arg1 = arguments[0].ToLowerInvariant();

            //Soft reloads
            if (arg1 == SOFT_RELOAD_ARG)
            {
                ReloadData(new EvtDataReloadedArgs(false, args.ServiceName));
            }
            else if (arg1 == HARD_RELOAD_ARG)
            {
                ReloadData(new EvtDataReloadedArgs(true, args.ServiceName));
            }
            else
            {
                QueueMessage(args.ServiceName, UsageMessage);
            }
        }

        private void ReloadData(EvtDataReloadedArgs e)
        {
            QueueMessage(e.ServiceName, "Finished reloading of data!");
            EvtDispatcher.DispatchEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, e);
        }
    }
}
