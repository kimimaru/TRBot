TRBot v2.5.1, featuring a minor bug fix and system-wide install option for inclusion in package managers.

Summary of changes:
- Upgraded NetIRC from version 1.1.0-preview.3 to 1.1.1 to fix the IRC service not reconnecting unless the service is entirely removed then readded.
- Added a system-wide install option, available by compiling **TRBot.Main** with the `SYSTEM_WIDE_INSTALL` preprocessor directive. This is primarily for inclusion in package managers.

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.1/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
