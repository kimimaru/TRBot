﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Input.VirtualControllers;
using TRBot.Input.Consoles;

namespace TRBot.Main
{
    /// <summary>
    /// A desktop computer.
    /// </summary>
    public sealed class PCConsole : GameConsole
    {
        public PCConsole()
        {
            Name = "pc";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(18)
            {
                { "mleft",      ConsoleHelpers.CreateAxisInput("mleft", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "mright",     ConsoleHelpers.CreateAxisInput("mright", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "mup",        ConsoleHelpers.CreateAxisInput("mup", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "mdown",      ConsoleHelpers.CreateAxisInput("mdown", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },

                { "lclick",     ConsoleHelpers.CreateButtonInput("lclick", (int)GlobalButtonVals.BTN5) },
                { "mclick",     ConsoleHelpers.CreateButtonInput("mclick", (int)GlobalButtonVals.BTN6) },
                { "rclick",     ConsoleHelpers.CreateButtonInput("rclick", (int)GlobalButtonVals.BTN7) },
                { "return",     ConsoleHelpers.CreateButtonInput("return", (int)GlobalButtonVals.BTN8) },
                { "space",      ConsoleHelpers.CreateButtonInput("space", (int)GlobalButtonVals.BTN9) },
                { "q",          ConsoleHelpers.CreateButtonInput("q", (int)GlobalButtonVals.BTN10) },
                { "w",          ConsoleHelpers.CreateButtonInput("w", (int)GlobalButtonVals.BTN11) },
                { "e",          ConsoleHelpers.CreateButtonInput("e", (int)GlobalButtonVals.BTN12) },
                { "r",          ConsoleHelpers.CreateButtonInput("r", (int)GlobalButtonVals.BTN13) },
                { "a",          ConsoleHelpers.CreateButtonInput("a", (int)GlobalButtonVals.BTN14) },
                { "s",          ConsoleHelpers.CreateButtonInput("s", (int)GlobalButtonVals.BTN15) },
                { "d",          ConsoleHelpers.CreateButtonInput("d", (int)GlobalButtonVals.BTN16) },
                { "p",          ConsoleHelpers.CreateButtonInput("p", (int)GlobalButtonVals.BTN17) },
                { "#",          ConsoleHelpers.CreateBlankInput("#") },
                { ".",          ConsoleHelpers.CreateBlankInput(".") }
            });

            InvalidInputCombos = new List<InvalidInputCombo>();
        }
    }
}
