﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.IO;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that exports a database table to a text file.
    /// </summary>
    public sealed class ExportDatasetToTextCommand : BaseCommand
    {
        private delegate void ExportDelegate(EvtChatCommandArgs args, BotDBContext context);

        private string UsageMessage = "Usage: \"settings\", \"gamelogs\", \"commands\", \"memes\", \"macros\", \"synonyms\", \"consoles\", \"users\", or \"permabilities\"";
        private Dictionary<string, ExportDelegate> ExportDict = null;

        public ExportDatasetToTextCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            ExportDict = new Dictionary<string, ExportDelegate>(9)
            {
                { "settings", ExportSettings },
                { "gamelogs", ExportGamelogs },
                { "commands", ExportCommands },
                { "memes", ExportMemes },
                { "macros", ExportMacros },
                { "synonyms", ExportSynonyms },
                { "consoles", ExportConsoles },
                { "users", ExportUsers },
                { "permabilities", ExportPermAbilities },
                { "routines", ExportRoutines },
                { "displayranks", ExportDisplayRanks },
                { "services", ExportServices },
            };
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not the correct number of arguments
            if (argCount != 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string tableName = arguments[0].ToLowerInvariant();

            //Check if this table name exists
            if (ExportDict.TryGetValue(tableName, out ExportDelegate invokedAction) == false)
            {
                QueueMessage(args.ServiceName, "Invalid table name!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                invokedAction.Invoke(args, context);
            }
        }

        private void ExportSettings(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "settings", context.SettingCollection);
        }

        private void ExportGamelogs(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "gamelogs", context.GameLogs);
        }

        private void ExportCommands(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "commands", context.Commands);
        }

        private void ExportMemes(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "memes", context.Memes);
        }

        private void ExportMacros(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "macros", context.Macros);
        }

        private void ExportSynonyms(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "synonyms", context.InputSynonyms);
        }

        private void ExportConsoles(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "consoles", context.Consoles);
        }

        private void ExportUsers(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "users", context.Users);
        }

        private void ExportPermAbilities(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "permabilities", context.PermAbilities);
        }

        private void ExportRoutines(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "routines", context.Routines);
        }

        private void ExportDisplayRanks(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "displayranks", context.DisplayRanks);
        }

        private void ExportServices(EvtChatCommandArgs args, BotDBContext context)
        {
            ExportTableToText(args, "services", context.Services);
        }

        private void ExportTableToText<T>(EvtChatCommandArgs args, string dataName, DbSet<T> dbSet) where T : class
        {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;

            string json = JsonConvert.SerializeObject(dbSet, Formatting.Indented, serializerSettings);
            string dateStr = DebugUtility.GetFileFriendlyTimeStamp(DateTime.Now);

            string fileName = $"{dataName} - {dateStr}.txt";

            string finalPath = Path.Combine(FolderPathResolver.DataFolderPath, fileName);

            if (FileHelpers.SaveToTextFile(finalPath, json) == false)
            {
                QueueMessage(args.ServiceName, $"Failed saving \"{dataName}\" to a file. Please double check your folder permissions and try again.");
            }
            else
            {
                QueueMessage(args.ServiceName, $"Successfully exported data for \"{dataName}\"!");
            }
        }
    }
}
