﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A simple command that sends a message.
    /// It will first look for the given from the database, and if not found, use the message as it is.
    /// </summary>
    public class MessageCommand : BaseCommand
    {
        public MessageCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            string sentMessage = DatabaseMngr.GetSettingString(ValueStr, ValueStr);

            //The message we want to send is null or empty
            if (string.IsNullOrEmpty(sentMessage) == true)
            {
                QueueMessage(args.ServiceName, "This command should say something, but the sent message is empty!");
                return;
            }

            //Replace any first parameters with the user's name
            sentMessage = sentMessage.Replace("{0}", args.Command.UserMessage.Username);
            
            QueueMessage(args.ServiceName, sentMessage, string.Empty);
        }
    }
}
