﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Connection;
using TRBot.Data;
using static TRBot.Data.DataEventObjects;

namespace TRBot.Routines
{
    /// <summary>
    /// A routine that attempts to reconnect to the client service after detecting a disconnection.
    /// </summary>
    public class ReconnectRoutine : BaseRoutine
    {
        /// <summary>
        /// The disconnected client service that's reconnecting.
        /// </summary>
        private IClientService DisconnectedService = null;
        
        private string DisconnectedServiceName = string.Empty;
        
        /// <summary>
        /// The amount of time to attempt another reconnect.
        /// </summary>
        private long ReconnectTime = 5000L;

        /// <summary>
        /// The max number of reconnect attempts.
        /// </summary>
        private readonly int MaxReconnectAttempts = 100;

        private DateTime CurReconnectTimeStamp = default(DateTime);
        private int CurReconnectionAttempts = 0;

        private bool InReconnection = false;

        public ReconnectRoutine(IClientService disconnectedService, string disconnectedServiceName)
            : base(true)
        {
            DisconnectedService = disconnectedService;
            DisconnectedServiceName = disconnectedServiceName;
        }

        public override void Initialize()
        {
            base.Initialize();

            EvtDispatcher.RegisterEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA);
            EvtDispatcher.AddListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnReload);

            ReconnectTime = DatabaseMngr.GetSettingInt(SettingsConstants.RECONNECT_TIME, 5000L);
        }

        public override void CleanUp()
        {
            EvtDispatcher.UnregisterEvent(DataEventNames.RELOAD_DATA);
            EvtDispatcher.RemoveListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnReload);

            base.CleanUp();
        }

        private void OnReload(EvtDataReloadedArgs e)
        {
            //Fetch the new reconnect time
            ReconnectTime = DatabaseMngr.GetSettingInt(SettingsConstants.RECONNECT_TIME, 10000L);
        }

        public override void UpdateRoutine(in DateTime currentTimeUTC)
        {
            //If connected, simply return
            if (DisconnectedService.IsConnected == true)
            {
                InReconnection = false;
                CurReconnectionAttempts = 0;

                Logger.Verbose($"Service \"{DisconnectedServiceName}\" is connected once again! No longer attempting to reconnect.");
                return;
            }

            //Check if we should attempt to reconnect
            if (DisconnectedService.IsConnected == false && InReconnection == false)
            {
                InReconnection = true;

                CurReconnectTimeStamp = currentTimeUTC;

                Logger.Verbose($"Started reconnection for \"{DisconnectedServiceName}\" since it's not in a reconnection state.");
            }

            if (InReconnection == true && CurReconnectionAttempts < MaxReconnectAttempts)
            {
                //Check the difference in time
                TimeSpan timeDiff = currentTimeUTC - CurReconnectTimeStamp;

                //See if it exceeds the threshold
                if (timeDiff.TotalMilliseconds < ReconnectTime)
                {
                    return;
                }

                InReconnection = false;
                CurReconnectionAttempts++;
                CurReconnectTimeStamp = currentTimeUTC;

                Logger.Information($"Attempting reconnect #{CurReconnectionAttempts} to service \"{DisconnectedServiceName}\"...");

                if (CurReconnectionAttempts >= MaxReconnectAttempts)
                {
                    Logger.Error($"Exceeded max reconnection attempts of {MaxReconnectAttempts} for service \"{DisconnectedServiceName}\". Please disable the service in the database, check your internet connection, then re-enable the service.");
                }

                //Double check yet again just to make sure the client isn't already connected before trying to reconnect
                if (DisconnectedService.IsConnected == false)
                {
                    //Attempt a reconnect
                    try
                    {
                        DisconnectedService.Connect();
                    }
                    catch (Exception e)
                    {
                        Logger.Error($"Unable to reconnect to service \"{DisconnectedServiceName}\" - {e.Message}\n{e.StackTrace}");
                    }
                }
            }
        }
    }
}
