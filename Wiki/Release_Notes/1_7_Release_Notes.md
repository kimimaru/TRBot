TRBot v1.7.

* New multi-controller input syntax: `"&1right &2left"` will press "right" on player 1's controller followed by "left" on player 2's controller.
* Virtual controllers now have an accurate input tracker telling which input names, buttons, and axes are pressed/released, making it easy to get more information about their states. This fixes incorrect input callback execution times (now properly invoked on `UpdateController`).
* Greatly improved the performance of expanding repeated inputs, and allowed repetition for triple digits (Ex. `"[a]*999"`).
* Fixed the bot freezing entirely if an unhandled exception is thrown while running a command.
* More configuration options:
  * All existing bot messages and message settings moved to a `MsgSettings` field in **Settings.txt**.
    * Existing messages are migrated to this new structure on startup.
  * Messages displayed upon being hosted, users subscribing, the bot reconnecting, and welcoming a new user are now configurable and can be turned off by setting them to empty strings (`""`).
* More commands:
  * `ListPressedInputsCommand` will list the currently pressed buttons and axes on a given virtual controller port.
* Allowed whitespace in input synonyms.

Please see the [Getting Started](https://github.com/teamradish/TRTwitchPlaysBot/wiki/Getting-Started) wiki page for setting up TRBot.
