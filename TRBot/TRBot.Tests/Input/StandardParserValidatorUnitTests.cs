/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using TRBot.Utilities.Logging;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities;

namespace TRBot.Tests.Input
{
    [TestFixture]
    public class StandardParserValidatorUnitTests
    {
        private ITRBotLogger Logger = null;
        private IGameConsole GameCnsole = null;
        private IVirtualControllerManager VControllerManager = null;
        private HashSet<string> RestrictedInputs = null;

        private StandardParserValidator StandardParserValidatr = null;

        [SetUp]
        public void Setup()
        {
            Logger = Substitute.For<ITRBotLogger>();
            GameCnsole = Substitute.For<IGameConsole>();
            VControllerManager = CreateMockVControllerMngr(4);
            RestrictedInputs = CreateRestrictedInputs();

            SetupMocks();

            StandardParserValidatr = new StandardParserValidator(RestrictedInputs, 10, GameCnsole,
                VControllerManager, Logger);
        }

        private void SetupMocks()
        {
            GameCnsole.Name.ReturnsForAnyArgs("Console");

            Dictionary<string, InputData> inputDict = new Dictionary<string, InputData>()
                {
                    { "a", new InputData("a", 0, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "b", new InputData("b", 1, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "x", new InputData("x", 2, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "y", new InputData("y", 3, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "start", new InputData("start", 4, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "select", new InputData("select", 5, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "l", new InputData("l", 6, 4, InputTypes.Button, 0, 1, 0d) },
                    { "r", new InputData("r", 7, 5, InputTypes.Button, 0, 1, 0d) },
                    { "up", new InputData("up", 0, 0, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "down", new InputData("down", 0, 0, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "left", new InputData("left", 0, 1, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "right", new InputData("right", 0, 1, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "rup", new InputData("rup", 0, 2, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "rdown", new InputData("rdown", 0, 2, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "rleft", new InputData("rleft", 0, 3, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "rright", new InputData("rright", 0, 3, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "#", new InputData("#", 0, 0, InputTypes.Blank, 0.5d, 0d, 0.5d, 0) },
                    
                    //These two are restricted inputs
                    { "res1", new InputData("res1", 8, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "res2", new InputData("res2", 9, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },

                    //These inputs cannot be used since they're higher than the user's level
                    { "higher1", new InputData("higher1", 10, 0, InputTypes.Button, 0.5d, 1d, 0.5d, 20) },
                    { "higher2", new InputData("higher2", 11, 0, InputTypes.Button, 0.5d, 1d, 0.5d, 20) },
                };

            GameCnsole.ConsoleInputs.ReturnsForAnyArgs(inputDict);

            List<InvalidInputCombo> invalidCombos = new List<InvalidInputCombo>()
            {
                new InvalidInputCombo("a, b, start, select",
                    new List<InputData>()
                    {
                        GameCnsole.ConsoleInputs["a"],
                        GameCnsole.ConsoleInputs["b"],
                        GameCnsole.ConsoleInputs["start"],
                        GameCnsole.ConsoleInputs["select"],
                    }),
                new InvalidInputCombo("a, b, right",
                    new List<InputData>()
                    {
                        GameCnsole.ConsoleInputs["a"],
                        GameCnsole.ConsoleInputs["b"],
                        GameCnsole.ConsoleInputs["right"],
                    }),
                new InvalidInputCombo("l, r, y, x",
                    new List<InputData>()
                    {
                        GameCnsole.ConsoleInputs["l"],
                        GameCnsole.ConsoleInputs["r"],
                        GameCnsole.ConsoleInputs["y"],
                        GameCnsole.ConsoleInputs["x"],
                    }),
                new InvalidInputCombo("l, y, x",
                    new List<InputData>()
                    {
                        GameCnsole.ConsoleInputs["l"],
                        GameCnsole.ConsoleInputs["y"],
                        GameCnsole.ConsoleInputs["x"],
                    }),
                new InvalidInputCombo("l, r, y",
                    new List<InputData>()
                    {
                        GameCnsole.ConsoleInputs["l"],
                        GameCnsole.ConsoleInputs["r"],
                        GameCnsole.ConsoleInputs["y"],
                    }),
            };

            GameCnsole.InvalidInputCombos.ReturnsForAnyArgs(invalidCombos);
        }

        private static TestCaseData[] ValidatesControllerPortTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(0, ParserValidationTypes.Passed),
                new TestCaseData(1, ParserValidationTypes.Passed),
                new TestCaseData(2, ParserValidationTypes.Passed),
                new TestCaseData(3, ParserValidationTypes.Passed),

                new TestCaseData(-1, ParserValidationTypes.Failed),
                new TestCaseData(4, ParserValidationTypes.Failed),
                new TestCaseData(5, ParserValidationTypes.Failed),
            };
        }

        [TestCaseSource(nameof(ValidatesControllerPortTestCases))]
        public void ValidatesControllerPort(int controllerPort, ParserValidationTypes expectedValidationType)
        {
            ParserValidationData validationData = StandardParserValidatr.ValidateControllerPort(controllerPort);

            Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidationType));
        }

        private static TestCaseData[] ValidatesRestrictedInputTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData("a", ParserValidationTypes.Passed),
                new TestCaseData("b", ParserValidationTypes.Passed),
                new TestCaseData("y", ParserValidationTypes.Passed),
                new TestCaseData("x", ParserValidationTypes.Passed),

                new TestCaseData("res1", ParserValidationTypes.Failed),
                new TestCaseData("res2", ParserValidationTypes.Failed),
            };
        }

        [TestCaseSource(nameof(ValidatesRestrictedInputTestCases))]
        public void ValidatesRestrictedInputs(string inputName, ParserValidationTypes expectedValidationType)
        {
            ParserValidationData validationData = StandardParserValidatr.ValidateRestrictedInputs(inputName);

            Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidationType));
        }

        private static TestCaseData[] ValidatesPermissionLevelsTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData("a", ParserValidationTypes.Passed),
                new TestCaseData("b", ParserValidationTypes.Passed),
                new TestCaseData("y", ParserValidationTypes.Passed),
                new TestCaseData("x", ParserValidationTypes.Passed),
                new TestCaseData("res1", ParserValidationTypes.Passed),
                new TestCaseData("res2", ParserValidationTypes.Passed),

                new TestCaseData("higher1", ParserValidationTypes.Failed),
                new TestCaseData("higher2", ParserValidationTypes.Failed),
            };
        }

        [TestCaseSource(nameof(ValidatesPermissionLevelsTestCases))]
        public void ValidatesPermissionLevels(string inputName, ParserValidationTypes expectedValidationType)
        {
            ParserValidationData validationData = StandardParserValidatr.ValidatePermissionLevels(inputName);

            Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidationType));
        }

        private static TestCaseData[] ValidatesInputCombosNonePrePressedTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 0d, 0),
                        CreateParsedInput("b", false, false, 0d, 0),
                        CreateParsedInput("x", false, false, 0d, 0),
                        CreateParsedInput("y", false, false, 0d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                    }),
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 0d, 0),
                        CreateParsedInput("b", false, false, 0d, 0),
                        CreateParsedInput("start", false, false, 0d, 0),
                        CreateParsedInput("select", false, false, 0d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }),
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("a", false, true, 0d, 0),
                        CreateParsedInput("b", false, true, 0d, 0),
                        CreateParsedInput("start", false, true, 0d, 0),
                        CreateParsedInput("select", false, true, 0d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                    }),
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 0d, 0),
                        CreateParsedInput("b", false, false, 0d, 0),
                        CreateParsedInput("select", false, false, 0d, 0),
                        CreateParsedInput("start", false, false, 0d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }),
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 0d, 0),
                        CreateParsedInput("b", false, false, 0d, 0),
                        CreateParsedInput("right", false, false, 100d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }),
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 0d, 0),
                        CreateParsedInput("b", false, false, 0d, 0),
                        CreateParsedInput("a", false, true, 0d, 0),
                        CreateParsedInput("right", false, false, 100d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                    }),
                new TestCaseData(new ParsedInput[]
                    {
                        CreateParsedInput("l", false, false, 0d, 0), //P - l
                        CreateParsedInput("y", false, false, 0d, 0), //P - l,y
                        CreateParsedInput("x", false, false, 0d, 0), //F - l,y,x
                        CreateParsedInput("x", false, true, 0d, 0), //P - l,y
                        CreateParsedInput("r", false, false, 100d, 0), //F - l,y,r
                        CreateParsedInput("x", false, false, 100d, 0), //F - l,y,r,x
                        CreateParsedInput("x", false, true, 100d, 0), //F - l,y,r
                        CreateParsedInput("r", false, true, 100d, 0), //P - l,y
                        CreateParsedInput("l", false, true, 0d, 0), //P - y
                        CreateParsedInput("x", false, false, 100d, 0), //P - y,x
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                    }),
            };
        }

        [TestCaseSource(nameof(ValidatesInputCombosNonePrePressedTestCases))]
        public void ValidatesInputCombosNonePrePressed(ParsedInput[] parsedInputs,
            ParserValidationTypes[] expectedValidations)
        {
            for (int i = 0; i < parsedInputs.Length; i++)
            {
                ParserValidationData validationData = StandardParserValidatr.ValidateInputCombos(parsedInputs[i]);

                Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidations[i]));   
            }
        }

        private static TestCaseData[] ValidatesInputCombosWithPrePressedTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(
                    new string[] { "a", "b" },
                    new ParsedInput[] { CreateParsedInput("up", false, false, 100d, 0) },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                    }),
                new TestCaseData(
                    new string[] { "a", "b" },
                    new ParsedInput[] { CreateParsedInput("right", false, false, 100d, 0) },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Failed,
                    }),
                new TestCaseData(
                    new string[] { "a", "b", "right" },
                    new ParsedInput[] { CreateParsedInput("#", false, false, 100d, 0) },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Failed,
                    }),
                new TestCaseData(
                    new string[] { "a", "b", "start", "select" },
                    new ParsedInput[] { CreateParsedInput("a", false, true, 100d, 0) },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                    }),
                new TestCaseData(
                    new string[] { "l", "y", "r" },
                    new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 100d, 0), //F - l,y,r,a
                        CreateParsedInput("r", false, true, 100d, 0), //P - l,y,a
                        CreateParsedInput("x", false, false, 100d, 0), //F - l,y,a,x
                        CreateParsedInput("r", false, false, 100d, 0), //F - l,y,a,x,r
                        CreateParsedInput("r", false, true, 100d, 0), //F - l,y,a,x
                        CreateParsedInput("x", false, true, 100d, 0), //P - l,y,a
                        CreateParsedInput("b", false, false, 100d, 0), //P - l,y,a,b
                        CreateParsedInput("right", false, false, 100d, 0), //F - l,y,a,b,right
                        CreateParsedInput("a", false, true, 100d, 0), //P - l,y,b,right
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                    }),
            };
        }

        [TestCaseSource(nameof(ValidatesInputCombosWithPrePressedTestCases))]
        public void ValidatesInputCombosWithPrePressed(string[] prePressedInputs, ParsedInput[] parsedInputs,
            ParserValidationTypes[] expectedValidations)
        {
            for (int i = 0; i < prePressedInputs.Length; i++)
            {
                string inputName = prePressedInputs[i];
                SetInputPressedOnController(VControllerManager.GetController(0), GameCnsole.ConsoleInputs[inputName]);
            }

            this.StandardParserValidatr = new StandardParserValidator(RestrictedInputs, 10, GameCnsole,
                VControllerManager, Logger);

            for (int i = 0; i < parsedInputs.Length; i++)
            {
                ParserValidationData validationData = StandardParserValidatr.ValidateInputCombos(parsedInputs[i]);

                Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidations[i]));   
            }
        }

        private static TestCaseData[] ValidatesInputCombosAcrossPortsTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(
                    new string[] { "a", "b" },
                    new int[] { 0, 1 },
                    new ParsedInput[]
                    {
                        CreateParsedInput("start", true, false, 100d, 0),
                        CreateParsedInput("select", false, false, 100d, 0),
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                    }),
                new TestCaseData(
                    Array.Empty<string>(),
                    Array.Empty<int>(),
                    new ParsedInput[]
                    {
                        CreateParsedInput("l", false, false, 100d, 0), //P - l
                        CreateParsedInput("y", false, false, 100d, 0), //P - l,y
                        CreateParsedInput("x", false, false, 100d, 1), //P - (0: l,y) (1: x)
                        CreateParsedInput("r", false, false, 100d, 0), //F - (0: l,r,y) (1: x)
                        CreateParsedInput("y", false, false, 100d, 1), //P - (0: l,r,y) (1: x,y)
                        CreateParsedInput("l", false, false, 100d, 1), //F - (0: l,r,y) (1: x,y,l)
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }),
                new TestCaseData(
                    new string[] { "start", "select", "a", "b", "start", "select", "a", "b" },
                    new int[] { 0, 0, 0, 0, 1, 1, 1, 1 },
                    new ParsedInput[]
                    {
                        CreateParsedInput("up", false, false, 100d, 0), //F - (0: start,select,a,b)
                        CreateParsedInput("up", false, true, 100d, 1), //F - (1: start,select,a,b)
                        CreateParsedInput("start", false, true, 100d, 0), //P - (0: select,a,b)
                        CreateParsedInput("start", false, true, 100d, 1), //P - (1: select,a,b)
                        CreateParsedInput("start", false, false, 100d, 1), //F - (1: select,a,b,start)
                        CreateParsedInput("down", false, false, 100d, 0), //P - (0: select,a,b,down)
                        CreateParsedInput("down", false, false, 100d, 1), //F - (1: select,a,b,start,down)
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }),
            };
        }

        [TestCaseSource(nameof(ValidatesInputCombosAcrossPortsTestCases))]
        public void ValidatesInputCombosAcrossPorts(string[] prePressedInputs, int[] preParsedPorts, ParsedInput[] parsedInputs,
            ParserValidationTypes[] expectedValidations)
        {
            for (int i = 0; i < prePressedInputs.Length; i++)
            {
                string inputName = prePressedInputs[i];
                SetInputPressedOnController(VControllerManager.GetController(preParsedPorts[i]), GameCnsole.ConsoleInputs[inputName]);
            }

            this.StandardParserValidatr = new StandardParserValidator(RestrictedInputs, 10, GameCnsole,
                VControllerManager, Logger);

            for (int i = 0; i < parsedInputs.Length; i++)
            {
                ParserValidationData validationData = StandardParserValidatr.ValidateInputCombos(parsedInputs[i]);

                Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidations[i]));   
            }
        }

        private static TestCaseData[] ValidatesInputCombosForValidLevelTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(
                    (long)Permissions.PermissionLevels.Moderator,
                    (long)Permissions.PermissionLevels.Moderator,
                    new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 100d, 0), // P - a
                        CreateParsedInput("b", false, false, 100d, 0), // P - a,b
                        CreateParsedInput("right", false, false, 100d, 0), //P - a,b,right
                        CreateParsedInput("b", false, true, 100d, 0), //P - a,right
                        CreateParsedInput("a", false, true, 100d, 0), //P - right
                        CreateParsedInput("b", false, false, 100d, 0), //P - right,b
                        CreateParsedInput("a", false, false, 100d, 0), //P - right,b,a
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                    }
                ),
                new TestCaseData(
                    (long)Permissions.PermissionLevels.Whitelisted,
                    (long)Permissions.PermissionLevels.Moderator,
                    new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 100d, 0), // P - a
                        CreateParsedInput("b", false, false, 100d, 0), // P - a,b
                        CreateParsedInput("right", false, false, 100d, 0), //F - a,b,right
                        CreateParsedInput("b", false, true, 100d, 0), //P - a,right
                        CreateParsedInput("a", false, true, 100d, 0), //P - right
                        CreateParsedInput("b", false, false, 100d, 0), //P - right,b
                        CreateParsedInput("a", false, false, 100d, 0), //F - right,b,a
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }
                ),
                new TestCaseData(
                    (long)Permissions.PermissionLevels.Superadmin,
                    long.MaxValue,
                    new ParsedInput[]
                    {
                        CreateParsedInput("a", false, false, 100d, 0), // P - a
                        CreateParsedInput("b", false, false, 100d, 0), // P - a,b
                        CreateParsedInput("right", false, false, 100d, 0), //F - a,b,right
                        CreateParsedInput("b", false, true, 100d, 0), //P - a,right
                        CreateParsedInput("a", false, true, 100d, 0), //P - right
                        CreateParsedInput("b", false, false, 100d, 0), //P - right,b
                        CreateParsedInput("a", false, false, 100d, 0), //F - right,b,a
                    },
                    new ParserValidationTypes[]
                    {
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Passed,
                        ParserValidationTypes.Failed,
                    }
                ),
            };
        }

        [TestCaseSource(nameof(ValidatesInputCombosForValidLevelTestCases))]
        public void ValidatesInputCombosForValidLevel(long userLevel, long validForLevel,
            ParsedInput[] parsedInputs, ParserValidationTypes[] expectedValidations)
        {
            List<InvalidInputCombo> invalidInputCombos = new List<InvalidInputCombo>();
            invalidInputCombos.Add(new InvalidInputCombo("a, b, right",
                    new List<InputData>()
                    {
                        GameCnsole.ConsoleInputs["a"],
                        GameCnsole.ConsoleInputs["b"],
                        GameCnsole.ConsoleInputs["right"],
                    }, validForLevel));

            GameCnsole.InvalidInputCombos.ReturnsForAnyArgs(invalidInputCombos);
            StandardParserValidatr = new StandardParserValidator(RestrictedInputs, userLevel, GameCnsole,
                VControllerManager, Logger);

            for (int i = 0; i < parsedInputs.Length; i++)
            {
                ParserValidationData validationData = StandardParserValidatr.ValidateInputCombos(parsedInputs[i]);

                Assert.That(validationData.ParserValidationType, Is.EqualTo(expectedValidations[i]));   
            }
        }

        private void SetInputPressedOnController(IVirtualController vController, InputData inputData)
        {
            if (inputData.InputType == InputTypes.Button)
            {
                GameCnsole.GetButtonValue(inputData.Name, out Arg.Any<InputButton>())
                    .Returns(callInfo =>
                    {
                        callInfo[1] = new InputButton((uint)inputData.ButtonValue);
                        return true;   
                    });
                vController.GetButtonState((uint)inputData.ButtonValue).Returns(ButtonStates.Pressed);
            }
            
            if (inputData.InputType == InputTypes.Axis)
            {
                GameCnsole.GetAxisValue(inputData.Name, out Arg.Any<InputAxis>())
                    .Returns(callInfo =>
                    {
                        callInfo[1] = new InputAxis(inputData.AxisValue, inputData.MinAxisVal,
                            inputData.MaxAxisVal, inputData.DefaultAxisVal);
                        return true;   
                    });
                vController.GetAxisState(inputData.AxisValue, inputData.DefaultAxisVal).Returns(100d);
            }
        }

        private void SetInputsPressedOnController(IVirtualController vController, InputData[] inputs)
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                SetInputPressedOnController(vController, inputs[i]);
            }
        }

        private IVirtualControllerManager CreateMockVControllerMngr(int numControllers)
        {
            IVirtualControllerManager vControllerMngr = Substitute.For<IVirtualControllerManager>();

            vControllerMngr.Initialized.ReturnsForAnyArgs(true);
            vControllerMngr.MinControllers.ReturnsForAnyArgs(1);
            vControllerMngr.MaxControllers.ReturnsForAnyArgs(numControllers);
            vControllerMngr.ControllerCount.ReturnsForAnyArgs(numControllers);
            
            for (int i = 0; i < numControllers; i++)
            {
                IVirtualController vControllerMock = Substitute.For<IVirtualController>();
                vControllerMock.IsAcquired.ReturnsForAnyArgs(true);
                vControllerMngr.GetController(i).Returns(vControllerMock);
            }

            return vControllerMngr;
        }

        private HashSet<string> CreateRestrictedInputs()
        {
            return new HashSet<string>()
            {
                { "res1"}, { "res2" }
            };
        }

        private static ParsedInput CreateParsedInput(string inputName, bool hold, bool release, double percent, int port)
        {
            return new ParsedInput(inputName, hold, release, percent, 1, InputDurationTypes.Milliseconds, port);
        }
    }
}