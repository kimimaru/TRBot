﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using CommandLine;
using CommandLine.Text;
using System;
using System.Runtime.InteropServices;
using TRBot.Build;
using static TRBot.Utilities.TRBotOSPlatform;

namespace TRBot.Main
{
    internal class Program
    {
        [DllImport("libc")]
        private static extern uint geteuid();

        internal class CLIOptions
        {
            [Option('s', "system-wide", Required = false, SetName = "systemwide", HelpText = "Run TRBot as a system-wide install, using the local application data folder for paths such as data. If this option is omitted, TRBot will default to system-wide paths if compiled with the \"SYSTEM_WIDE_INSTALL\" flag. Cannot be used with \"-p\" or \"--portable\".")]
            public bool RunAsSystem { get; set; }

            [Option('p', "portable", Required = false, SetName = "portable", HelpText = "Run TRBot as portable, using the executable folder for paths such as data. If this option is omitted, TRBot will default to portable paths if compiled WITHOUT the \"SYSTEM_WIDE_INSTALL\" flag. Cannot be used with \"-s\" or \"--system-wide\".")]
            public bool RunAsPortable { get; set; }

            [Option('v', "version", Required = false, HelpText = "Display TRBot application version.")]
            public bool Version { get; set; }
        }

        static void Main(string[] args)
        {   
            //Check if running as root and print a warning
            if (CurrentOS == OS.GNULinux)
            {
                uint euid = geteuid();
                
                //Root
                if (euid == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("WARNING!! WARNING!! WARNING!!!");
                    Console.WriteLine();
                    Console.WriteLine("YOU ARE RUNNING TRBOT AS ROOT, WHICH HAS THE POTENTIAL TO CAUSE HARM TO YOUR SYSTEM! UNLESS YOU UNDERSTAND THE RISKS, PLEASE RUN TRBOT AS A NORMAL USER.");
                    Console.WriteLine();
                    Console.WriteLine("WARNING!! WARNING!! WARNING!!!");
                    Console.WriteLine();

                    //Wait a bit so they can read the message
                    System.Threading.Thread.Sleep(4000);
                }
            }

            bool systemWideInstall = false;

            #if SYSTEM_WIDE_INSTALL == true
                systemWideInstall = true;
            #endif

            //Parse CLI arguments
            Parser cliParser = new Parser(GetParserSettings);
            ParserResult<CLIOptions> cliParserResult = cliParser.ParseArguments<CLIOptions>(args);
            
            cliParserResult.WithParsed<CLIOptions>(parsedOptions =>
            {
                if (parsedOptions.Version == true)
                {
                    Console.WriteLine($"TRBot version {BuildInfo.GitBaseTag} (commit {BuildInfo.GitCommitHash} on {BuildInfo.GitCommitDate}).");
                    Environment.Exit(0);
                }
                
                //These two options are marked as mutually exclusive, so it should throw an error during CLI parsing if both are present
                if (parsedOptions.RunAsSystem == true)
                {
                    systemWideInstall = true;
                }
                else if (parsedOptions.RunAsPortable == true)
                {
                    systemWideInstall = false;
                }
            })
            .WithNotParsed(errors => 
            {
                DisplayHelp(cliParserResult);
                Environment.Exit(1);
            });

            cliParser.Dispose();

            using (BotProgram botProgram = new BotProgram(systemWideInstall))
            {
                botProgram.Initialize();

                if (botProgram.Initialized == true)
                {
                    botProgram.Run();
                }
                else
                {
                    Console.WriteLine("Bot failed to initialize. Press any key to continue...");
                    Console.ReadKey();
                }
            }
        }

        private static void GetParserSettings(ParserSettings parserSettings)
        {
            parserSettings.AutoVersion = false;
            parserSettings.AutoHelp = true;
            parserSettings.HelpWriter = null;
        }

        private static void DisplayHelp<T>(ParserResult<T> result)
        {
            HelpText helpText = HelpText.AutoBuild(result, helpTxt =>
            {
                //Configure help text
                helpTxt.AdditionalNewLineAfterOption = true;
                helpTxt.Heading = result.Errors.IsHelp() 
                    ? "Run without arguments or with the following options:"
                    : "Invalid argument or argument combination. Run without arguments or with the following options:";
                helpTxt.Copyright = string.Empty;
                helpTxt.AutoVersion = false;

                return helpTxt;
            }, e => e);

            Console.WriteLine(helpText);
        }
    }
}
