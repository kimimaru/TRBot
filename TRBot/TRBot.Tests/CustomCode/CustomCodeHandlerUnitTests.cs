/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NUnit.Framework;
using System.Reflection;
using System.Threading.Tasks;
using TRBot.CustomCode;

namespace TRBot.Tests.CustomCode
{
    [TestFixture]
    public class CustomCodeHandlerUnitTests
    {
        private CustomCodeHandler CodeHandler = null;

        [SetUp]
        public void Setup()
        {
            CodeHandler = new CustomCodeHandler(
                new Assembly[]
                {
                    typeof(System.Console).Assembly,
                },
                new string[]
                {
                    nameof(System),
                });
        }

        [Test]
        public async Task CustomCodeHandlerCompilesAndExecutesCode()
        {
            TestContainer testContainer = new TestContainer(5000);
            string testCode = "TestValue = 2700;";

            await CodeHandler.ExecuteCode(testCode, testContainer, typeof(TestContainer));

            Assert.That(testContainer.TestValue, Is.EqualTo(2700));
        }

        [Test]
        public async Task CustomCodeHandlerCompilesAndReturnsObject()
        {
            string testCode =
            "using System.Text;" +
            "StringBuilder sb = new StringBuilder(\"This is a message from test code\");" +
            "sb.Append(\'!\');" +
            "return sb.ToString();";
            
            var data = await CodeHandler.CompileCodeAndReturnObject<string>(testCode);

            Assert.That(data.Success, Is.EqualTo(true));
            Assert.That(data.ErrorMessage, Is.EqualTo(string.Empty));
            Assert.That(data.NewObject, Is.TypeOf<string>());
            Assert.That(data.NewObject, Is.Not.EqualTo(null));
            Assert.That(data.NewObject, Is.EqualTo("This is a message from test code!"));
        }

        public class TestContainer
        {
            public int TestValue;

            public TestContainer(int testValue)
            {
                TestValue = testValue;
            }
        }
    }
}