﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that gets a setting in the database.
    /// </summary>
    public sealed class GetDBSettingCommand : BaseCommand
    {
        private const string STRING_ARG = "string";
        private const string INT_ARG = "int";

        private string UsageMessage = $"Usage - \"setting key (string)\", \"string/int (for ValueStr/ValueInt)\"";

        public GetDBSettingCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string settingKey = arguments[0];
            string typeStr = arguments[1].ToLowerInvariant();

            if (typeStr != STRING_ARG && typeStr != INT_ARG)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            Settings setting = DatabaseMngr.GetSetting(settingKey);

            if (setting == null)
            {
                QueueMessage(args.ServiceName, $"No setting with a key of \"{settingKey}\" can be found in the database. Setting keys are case-sensitive.");
                return;
            }

            //Print the database setting
            string settingVal = (typeStr == INT_ARG) ? setting.ValueInt.ToString() : setting.ValueStr;

            QueueMessage(args.ServiceName, settingVal, " ");
        }
    }
}
