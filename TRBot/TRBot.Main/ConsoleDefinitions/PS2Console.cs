﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Input.VirtualControllers;
using TRBot.Input.Consoles;

namespace TRBot.Main
{
    /// <summary>
    /// The PlayStation 2.
    /// </summary>
    public sealed class PS2Console : GameConsole
    {
        public PS2Console()
        {
            Name = "ps2";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(38)
            {
                { "left",       ConsoleHelpers.CreateAxisInput("left", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "right",      ConsoleHelpers.CreateAxisInput("right", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "up",         ConsoleHelpers.CreateAxisInput("up", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "down",       ConsoleHelpers.CreateAxisInput("down", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },
                { "rleft",      ConsoleHelpers.CreateAxisInput("rleft", (int)GlobalAxisVals.AXIS_RX, 0.5d, 0d, 0.5d) },
                { "rright",     ConsoleHelpers.CreateAxisInput("rright", (int)GlobalAxisVals.AXIS_RX, 0.5d, 1d, 0.5d) },
                { "rup",        ConsoleHelpers.CreateAxisInput("rup", (int)GlobalAxisVals.AXIS_RY, 0.5d, 0d, 0.5d) },
                { "rdown",      ConsoleHelpers.CreateAxisInput("rdown", (int)GlobalAxisVals.AXIS_RY, 0.5d, 1d, 0.5d) },

                { "dleft",      ConsoleHelpers.CreateButtonInput("dleft", (int)GlobalButtonVals.BTN1) },
                { "dright",     ConsoleHelpers.CreateButtonInput("dright", (int)GlobalButtonVals.BTN2) },
                { "dup",        ConsoleHelpers.CreateButtonInput("dup", (int)GlobalButtonVals.BTN3) },
                { "ddown",      ConsoleHelpers.CreateButtonInput("ddown", (int)GlobalButtonVals.BTN4) },
                { "square",     ConsoleHelpers.CreateButtonInput("square", (int)GlobalButtonVals.BTN9) },
                { "triangle",   ConsoleHelpers.CreateButtonInput("triangle", (int)GlobalButtonVals.BTN10) },
                { "circle",     ConsoleHelpers.CreateButtonInput("circle", (int)GlobalButtonVals.BTN11) },
                { "cross",      ConsoleHelpers.CreateButtonInput("cross", (int)GlobalButtonVals.BTN12) },
                { "select",     ConsoleHelpers.CreateButtonInput("select", (int)GlobalButtonVals.BTN13) },
                { "start",      ConsoleHelpers.CreateButtonInput("start", (int)GlobalButtonVals.BTN14) },
                { "l1",         ConsoleHelpers.CreateButtonInput("l1", (int)GlobalButtonVals.BTN15) },
                { "r1",         ConsoleHelpers.CreateButtonInput("r1", (int)GlobalButtonVals.BTN16) },
                { "l2",         ConsoleHelpers.CreateButtonInput("l2", (int)GlobalButtonVals.BTN17) },
                { "r2",         ConsoleHelpers.CreateButtonInput("r2", (int)GlobalButtonVals.BTN18) },
                { "l3",         ConsoleHelpers.CreateButtonInput("l3", (int)GlobalButtonVals.BTN19) },
                { "r3",         ConsoleHelpers.CreateButtonInput("r3", (int)GlobalButtonVals.BTN20) },
                { "ss1",        ConsoleHelpers.CreateButtonInput("ss1", (int)GlobalButtonVals.BTN21) },
                { "ss2",        ConsoleHelpers.CreateButtonInput("ss2", (int)GlobalButtonVals.BTN22) },
                { "ss3",        ConsoleHelpers.CreateButtonInput("ss3", (int)GlobalButtonVals.BTN23) },
                { "ss4",        ConsoleHelpers.CreateButtonInput("ss4", (int)GlobalButtonVals.BTN24) },
                { "ss5",        ConsoleHelpers.CreateButtonInput("ss5", (int)GlobalButtonVals.BTN25) },
                { "ss6",        ConsoleHelpers.CreateButtonInput("ss6", (int)GlobalButtonVals.BTN26) },
                { "ls1",        ConsoleHelpers.CreateButtonInput("ls1", (int)GlobalButtonVals.BTN27) },
                { "ls2",        ConsoleHelpers.CreateButtonInput("ls2", (int)GlobalButtonVals.BTN28) },
                { "ls3",        ConsoleHelpers.CreateButtonInput("ls3", (int)GlobalButtonVals.BTN29) },
                { "ls4",        ConsoleHelpers.CreateButtonInput("ls4", (int)GlobalButtonVals.BTN30) },
                { "ls5",        ConsoleHelpers.CreateButtonInput("ls5", (int)GlobalButtonVals.BTN31) },
                { "ls6",        ConsoleHelpers.CreateButtonInput("ls6", (int)GlobalButtonVals.BTN32) },
                { "#",          ConsoleHelpers.CreateBlankInput("#") },
                { ".",          ConsoleHelpers.CreateBlankInput(".") }
            });

            InvalidInputCombos = new List<InvalidInputCombo>(4)
            {
                new InvalidInputCombo("softreset", new List<InputData>()
                {
                    ConsoleInputs["l1"],
                    ConsoleInputs["r1"],
                    ConsoleInputs["l2"],
                    ConsoleInputs["r2"],
                    ConsoleInputs["start"],
                    ConsoleInputs["select"],
                }, (long)TRBot.Permissions.PermissionLevels.Moderator)
            };
        }
    }
}
