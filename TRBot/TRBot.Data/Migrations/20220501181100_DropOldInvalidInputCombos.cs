﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TRBot.Data.Migrations
{
    public partial class DropOldInvalidInputCombos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvalidInputCombos",
                schema: "invalidinputcombos");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "invalidinputcombos");

            migrationBuilder.CreateTable(
                name: "InvalidInputCombos",
                schema: "invalidinputcombos",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GameConsoleID = table.Column<int>(type: "INTEGER", nullable: true),
                    InputID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvalidInputCombos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InvalidInputCombos_Consoles_GameConsoleID",
                        column: x => x.GameConsoleID,
                        principalSchema: "consoles",
                        principalTable: "Consoles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvalidInputCombos_Inputs_InputID",
                        column: x => x.InputID,
                        principalSchema: "inputs",
                        principalTable: "Inputs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvalidInputCombos_GameConsoleID",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                column: "GameConsoleID");

            migrationBuilder.CreateIndex(
                name: "IX_InvalidInputCombos_InputID",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                column: "InputID",
                unique: true);
        }
    }
}
