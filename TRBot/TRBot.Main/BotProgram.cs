﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TRBot.Build;
using TRBot.Input.Parsing;
using TRBot.Connection;
using TRBot.Connection.Twitch;
using TRBot.Connection.WebSocket;
using TRBot.Connection.IRC;
using TRBot.Connection.XMPP;
using TRBot.Connection.Matrix;
using TRBot.Input;
using TRBot.Input.Consoles;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Commands;
using TRBot.CustomCode;
using TRBot.Permissions;
using TRBot.Routines;
using TRBot.Utilities.Logging;
using TRBot.Events;
using TRBot.WebSocket;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.PubSub.Events;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static TRBot.Data.DataEventObjects;

namespace TRBot.Main
{
    public sealed class BotProgram : IDisposable
    {
        public bool Initialized { get; private set; } = false;

        private IClientServiceManager ClientServiceMngr = null;

        private ICommandHandler CmdHandler = null;
        private DatabaseManager DatabaseMngr = null;

        private IRoutineHandler RoutineHndler = null;
        private ITRBotLogger Logger = null;
        private IFolderPathResolver FolderPathResolver = null;
        private IVirtualControllerContainer VControllerContainer = null;
        private IEventDispatcher EvtDispatcher = null;
        private IWebSocketManager WebSocketMngr = null;
        private IInputHandler InputHndlr = null;
        
        private IDefaultDataProvider DefaultDataProvider = new DefaultDataProvider();
        private IDataInitializer DataInitializer = null;

        private ICustomCodeHandler CustomCodeHandler = null;

        private CrashHandler crashHandler = null;

        //Store the function to reduce garbage, since this one is being called constantly
        private Func<Settings, bool> ThreadSleepFindFunc = null;

        #region Message Queue

        /// <summary>
        /// The message queue.
        /// </summary>
        private ConcurrentQueue<MsgQueueData> MsgQueue = new ConcurrentQueue<MsgQueueData>();

        private CancellationTokenSource CancelTokenSource = null;

        #endregion

        public BotProgram(bool isSystemWideInstall)
        {
            Console.WriteLine($"Running TRBot version {BuildInfo.GitBaseTag} (commit {BuildInfo.GitCommitHash} on {BuildInfo.GitCommitDate}).");

            //Set up the folder path resolver
            string rootFolderPath = AppDomain.CurrentDomain.BaseDirectory;
            string crashLogsFolderPath = DebugUtility.DefaultCrashLogPath;

            //For system-wide installs, use the local application data folder instead of the executable's directory
            if (isSystemWideInstall == true)
            {
                Console.WriteLine("Running TRBot as a system-wide install.");
                
                rootFolderPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData,
                    Environment.SpecialFolderOption.Create), "TRBot");
                
                crashLogsFolderPath = Path.Combine(rootFolderPath, DebugUtility.CRASH_LOG_FOLDER_NAME);
            }
            else
            {
                Console.WriteLine("Running TRBot as a portable install.");
            }

            string dataFolderPath = Path.Combine(rootFolderPath, DataConstants.DATA_FOLDER_NAME);
            string logsFolderPath = Path.Combine(rootFolderPath, LoggingConstants.LOGS_FOLDER_NAME);

            FolderPathResolver = new FolderPathResolver(rootFolderPath, dataFolderPath, logsFolderPath,
                crashLogsFolderPath);

            //Set up the logger
            string logPath = Path.Combine(FolderPathResolver.LogsFolderPath, LoggingConstants.LOG_FILE_NAME);
            if (Utilities.FileHelpers.ValidatePathForFile(logPath) == false)
            {
                Console.WriteLine("Logger path cannot be validated. This is a problem! Double check the path is correct.");
            }

            //Set up the logger
            //Cap the size at 10 MiB
            Logger = new TRBotLogger(logPath, Serilog.Events.LogEventLevel.Verbose,
                Serilog.RollingInterval.Day, 1024L * 1024L * 10L, TimeSpan.FromSeconds(60d));

            //Set up the crash handler
            crashHandler = new CrashHandler(Logger, FolderPathResolver, BuildInfo.GitBaseTag, BuildInfo.GitCommitHash, BuildInfo.GitCommitDate);

            //Set up other core dependencies
            InputHndlr = new InputHandler(Logger);
            InputHndlr.ToggleInputHandling(true);
            ThreadSleepFindFunc = FindThreadSleepTime;

            //Call this to set the application start time
            DateTime start = Application.ApplicationStartTimeUTC;
        }

        //Clean up anything we need to here
        public void Dispose()
        {
            if (Initialized == false)
                return;

            StopQueueThread();

            UnsubscribeFromEvents();

            ClientServiceMngr?.Dispose();

            RoutineHndler?.CleanUp();
            CmdHandler?.CleanUp();

            //Dispose and relinquish the virtual controllers when we're done
            VControllerContainer?.VControllerMngr?.Dispose();

            Logger.Dispose();
        }

        //This is the core setup of the application
        //It initializes the database, virtual controllers, command handler, and routines
        //The order matters for some of these; notably, the database must be initialized before accessing any settings
        //If initialization fails somewhere important, log an error message and abort
        internal void Initialize()
        {
            if (Initialized == true)
                return;

            //Use invariant culture
            System.Globalization.CultureInfo.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            //Initialize database
            string databasePath = Path.Combine(FolderPathResolver.DataFolderPath, DataConstants.DATABASE_FILE_NAME);

            Logger.Information($"Validating database at: {databasePath}");
            if (Utilities.FileHelpers.ValidatePathForFile(databasePath) == false)
            {
                Logger.Error($"Cannot create database path at {databasePath}. Check if you have permission to write to this directory. Aborting.");
                return;
            }

            Logger.Information("Database path validated! Initializing database and importing migrations.");

            DatabaseMngr = new DatabaseManager(databasePath);

            try
            {
                DatabaseMngr.InitAndMigrateContext();
            }
            catch (Exception e)
            {
                Logger.Fatal($"Error running migrations. {e.Message}");
                
                if (e.InnerException != null)
                {
                    Logger.Fatal($"Inner exception. {e.Message}\n{e.StackTrace}");
                }

                //There was a problem with the database, so exit
                Environment.Exit(1);
            }

            DataInitializer = new DataInitializer(DatabaseMngr, Logger);
            
            Logger.Information("Checking to initialize default values for missing database entries.");

            //Check for and initialize default values if the database was newly created or needs updating
            int addedDefaultEntries = DataInitializer.InitDefaultData(DefaultDataProvider);

            if (addedDefaultEntries > 0)
            {
                Logger.Information($"Added {addedDefaultEntries} additional entries to the database.");
            }

            //Set the logger's minimum log level
            long logLevel = DatabaseMngr.GetSettingInt(SettingsConstants.LOG_LEVEL, (long)Serilog.Events.LogEventLevel.Information);
            Logger.MinLoggingLevel = (Serilog.Events.LogEventLevel)logLevel;

            Logger.Information($"Initializing WebSocket server!");

            InitWebSocketServer();

            Logger.Information($"Initializing event dispatcher!");

            InitEventDispatcher();

            Logger.Information("Initializing client service manager and services...");

            //Initialize client service
            InitClientServices();

            //If there are no enabled client services, we can't continue
            if (ClientServiceMngr.ServiceCount == 0)
            {
                Logger.Error($"Client services failed to initialize. Make sure the services are enabled, and double check service-specific login settings.");
                return;
            }

            //Subscribe to events
            UnsubscribeFromEvents();
            SubscribeToEvents();

            Logger.Information("Setting up virtual controller manager...");

            InitVControllerManager();

            int controllerCount = 0;

            //Clamp the controller count to the min and max allowed by the virtual controller manager
            using (BotDBContext context = DatabaseMngr.OpenContext())
            { 
                Settings joystickCountSetting = context.GetSetting(SettingsConstants.JOYSTICK_COUNT);

                int minCount = VControllerContainer.VControllerMngr.MinControllers;
                int maxCount = VControllerContainer.VControllerMngr.MaxControllers;

                //Validate controller count
                if (joystickCountSetting.ValueInt < minCount)
                {
                    EvtDispatcher.BroadcastMessage($"Controller count of {joystickCountSetting.ValueInt} in database is invalid. Clamping to the min of {minCount}.");
                    joystickCountSetting.ValueInt = minCount;
                    context.SaveChanges();
                }
                else if (joystickCountSetting.ValueInt > maxCount)
                {
                    EvtDispatcher.BroadcastMessage($"Controller count of {joystickCountSetting.ValueInt} in database is invalid. Clamping to the max of {maxCount}.");
                    joystickCountSetting.ValueInt = maxCount;
                    context.SaveChanges();
                }

                controllerCount = (int)joystickCountSetting.ValueInt;
            }

            VControllerContainer.VControllerMngr.Initialize();
            int acquiredCount = VControllerContainer.VControllerMngr.InitControllers(controllerCount);

            Logger.Information($"Set up virtual controller manager {VControllerContainer.VControllerType} and acquired {acquiredCount} controllers!");

            Logger.Information($"Initializing custom code handler!");

            CustomCodeHandler = new CustomCodeHandler(DefaultCustomCodeDefinitions.References, DefaultCustomCodeDefinitions.Imports);

            Logger.Information($"Initializing routine handler!");

            RoutineHndler = new RoutineHandler(DatabaseMngr, Logger, FolderPathResolver, CustomCodeHandler, ClientServiceMngr,
                VControllerContainer, EvtDispatcher, WebSocketMngr, InputHndlr, null);

            //Initialize routines
            InitRoutines();

            Logger.Information($"Initializing command handler!");

            CmdHandler = new CommandHandler(DatabaseMngr, Logger, FolderPathResolver,
                RoutineHndler, CustomCodeHandler, ClientServiceMngr, VControllerContainer, EvtDispatcher, WebSocketMngr, InputHndlr,
                new CommandStringNumberParser(System.Globalization.CultureInfo.CurrentCulture),
                new System.Reflection.Assembly[]
                { 
                    typeof(TRBot.Integrations.LiveSplitOne.LiveSplitOneSocketService).Assembly
                });

            CmdHandler.Initialize();

            bool inputsEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.INPUTS_ENABLED, 1L) != 0L;
            InputHndlr.ToggleInputHandling(inputsEnabled);

            Logger.Information($"Inputs are currently: {(inputsEnabled == true ? "enabled" : "disabled")}");

            Logger.Information($"Starting message queue thread...");

            //Start the message queue
            StartQueueThread();

            Logger.Information($"Started message queue thread!");

            Initialized = true;

            PostInitialize();
        }

        private void PostInitialize()
        {
            string commandIdentifier = DatabaseMngr.GetCommandIdentifier();
            Logger.Information($"The command identifier is: {commandIdentifier}");

            if (Helpers.HasWhitespace(commandIdentifier) == true)
            {
                Logger.Warning($"Command identifier \"{commandIdentifier}\" has whitespace, which will interfere with parsing commands. Please set a command identifier without whitespace.");
            }

            InputModes inputMode = (InputModes)DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE, 0L);

            Logger.Information($"Current input mode: {inputMode}");

            bool botMessagesEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.BOT_MESSAGES_ENABLED, 1L) != 0L;

            Logger.Information($"Bot messages are currently: {(botMessagesEnabled == true ? "enabled" : "disabled")}");
        }

        internal void Run()
        {
            //Connect all running services now since everything is initialized
            IClientService[] services = ClientServiceMngr.GetAllServices();

            try
            {
                for (int i = 0; i < services.Length; i++)
                {
                    services[i].Connect();
                }
            }
            catch (Exception e)
            {
                Logger.Error($"Unable to connect to client service: {e.Message}\n{e.StackTrace}");
            }

            //Run
            while (true)
            {
                //Store the bot's uptime
                DateTime utcNow = DateTime.UtcNow;

                //Update routines
                RoutineHndler.Update(utcNow);

                int threadSleep = 500;

                //Get the thread sleep value from the database
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings threadSleepSetting = context.SettingCollection.AsNoTracking().FirstOrDefault(ThreadSleepFindFunc);

                    //Clamp to avoid an exception - the code that changes this should handle values below 0,
                    //but the database can manually be changed to have lower values 
                    threadSleep = Math.Clamp((int)threadSleepSetting.ValueInt, 0, int.MaxValue);
                }

                Thread.Sleep(threadSleep);
            }
        }

        private void UnsubscribeFromEvents()
        {
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.INPUT);
            EvtDispatcher.UnregisterEvent(WebSocketEventNames.RECEIVED_EVENT_JSON);
            EvtDispatcher.UnregisterEvent(DataEventNames.INIT_DEFAULT_DATA);
            EvtDispatcher.UnregisterEvent(DataEventNames.RELOAD_DATA);

            //Data
            EvtDispatcher.RemoveListener<EvtInitDataArgs>(DataEventNames.INIT_DEFAULT_DATA, HandleInitDataEvent);
            EvtDispatcher.RemoveListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, HandleReloadBoth);

            //General
            EvtDispatcher.RemoveListener<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, OnUserSentMessage);
            EvtDispatcher.RemoveListener<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, OnChatCommandReceived);
            EvtDispatcher.RemoveListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);
            EvtDispatcher.RemoveListener<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, OnConnectionError);
            EvtDispatcher.RemoveListener<EvtReconnectedArgs>(ClientServiceEventNames.RECONNECTED, OnReconnected);
            EvtDispatcher.RemoveListener<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, OnDisconnected);
            EvtDispatcher.RemoveListener<EvtUserInputArgs<ParsedInputSequence>>(ClientServiceEventNames.INPUT, HandleUserStatsOnInput);
            EvtDispatcher.RemoveListener<EvtJsonArgs>(WebSocketEventNames.RECEIVED_EVENT_JSON, ReceivedIncomingJson);

            //IRC
            EvtDispatcher.RemoveListener<IRCJoinedChannelArgs>(IRCServiceEventNames.IRC_JOINED_CHANNEL, IRCOnJoinedChannel);

            //Twitch
            EvtDispatcher.RemoveListener<OnJoinedChannelArgs>(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL, TwitchOnJoinedChannel);
            EvtDispatcher.RemoveListener<OnNewSubscriberArgs>(TwitchServiceEventNames.TWITCH_NEWSUBSCRIBER, TwitchOnNewSubscriber);
            EvtDispatcher.RemoveListener<OnReSubscriberArgs>(TwitchServiceEventNames.TWITCH_RESUBSCRIBER, TwitchOnReSubscriber);
            EvtDispatcher.RemoveListener<OnWhisperReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_RECEIVED, TwitchOnWhisperReceived);
            EvtDispatcher.RemoveListener<OnWhisperCommandReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_COMMAND_RECEIVED, TwitchOnWhisperCommandReceived);

            //EvtDispatcher.RemoveListener<EventArgs>(Connection.Twitch.TwitchServiceEventNames.PUB_SUB_CONNECTED, TwitchPubSubConnected);
            //EvtDispatcher.RemoveListener<EventArgs>(Connection.Twitch.TwitchServiceEventNames.PUB_SUB_DISCONNECTED, TwitchPubSubDisconnected);
            //EvtDispatcher.RemoveListener<OnPubSubServiceErrorArgs>(Connection.Twitch.TwitchServiceEventNames.PUB_SUB_ERROR, TwitchPubSubError);
            //EvtDispatcher.RemoveListener<OnFollowArgs>(Connection.Twitch.TwitchServiceEventNames.FOLLOW, TwitchOnFollow);
        }

        private void SubscribeToEvents()
        {
            EvtDispatcher.RegisterEvent<EvtUserInputArgs<ParsedInputSequence>>(ClientServiceEventNames.INPUT);
            EvtDispatcher.RegisterEvent<EvtJsonArgs>(WebSocketEventNames.RECEIVED_EVENT_JSON);
            EvtDispatcher.RegisterEvent<EvtInitDataArgs>(DataEventNames.INIT_DEFAULT_DATA);
            EvtDispatcher.RegisterEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA);

            //Data
            EvtDispatcher.AddListener<EvtInitDataArgs>(DataEventNames.INIT_DEFAULT_DATA, HandleInitDataEvent);
            EvtDispatcher.AddListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, HandleReloadBoth);

            //General
            EvtDispatcher.AddListener<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, OnUserSentMessage);
            EvtDispatcher.AddListener<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, OnChatCommandReceived);
            EvtDispatcher.AddListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);
            EvtDispatcher.AddListener<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, OnConnectionError);
            EvtDispatcher.AddListener<EvtReconnectedArgs>(ClientServiceEventNames.RECONNECTED, OnReconnected);
            EvtDispatcher.AddListener<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, OnDisconnected);
            EvtDispatcher.AddListener<EvtUserInputArgs<ParsedInputSequence>>(ClientServiceEventNames.INPUT, HandleUserStatsOnInput);
            EvtDispatcher.AddListener<EvtJsonArgs>(WebSocketEventNames.RECEIVED_EVENT_JSON, ReceivedIncomingJson);

            //IRC
            EvtDispatcher.AddListener<IRCJoinedChannelArgs>(IRCServiceEventNames.IRC_JOINED_CHANNEL, IRCOnJoinedChannel);

            //Twitch
            EvtDispatcher.AddListener<OnJoinedChannelArgs>(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL, TwitchOnJoinedChannel);
            EvtDispatcher.AddListener<OnNewSubscriberArgs>(TwitchServiceEventNames.TWITCH_NEWSUBSCRIBER, TwitchOnNewSubscriber);
            EvtDispatcher.AddListener<OnReSubscriberArgs>(TwitchServiceEventNames.TWITCH_RESUBSCRIBER, TwitchOnReSubscriber);
            EvtDispatcher.AddListener<OnWhisperReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_RECEIVED, TwitchOnWhisperReceived);
            EvtDispatcher.AddListener<OnWhisperCommandReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_COMMAND_RECEIVED, TwitchOnWhisperCommandReceived);

            //EvtDispatcher.AddListener<EventArgs>(Connection.Twitch.TwitchServiceEventNames.PUB_SUB_CONNECTED, TwitchPubSubConnected);
            //EvtDispatcher.AddListener<EventArgs>(Connection.Twitch.TwitchServiceEventNames.PUB_SUB_DISCONNECTED, TwitchPubSubDisconnected);
            //EvtDispatcher.AddListener<OnPubSubServiceErrorArgs>(Connection.Twitch.TwitchServiceEventNames.PUB_SUB_ERROR, TwitchPubSubError);
            //EvtDispatcher.AddListener<OnFollowArgs>(Connection.Twitch.TwitchServiceEventNames.FOLLOW, TwitchOnFollow);
        }

        private void InitVControllerManager()
        {
            VirtualControllerTypes lastVControllerType = (VirtualControllerTypes)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_VCONTROLLER_TYPE, 0L);

            VirtualControllerTypes curVControllerType = VirtualControllerHelpers.ValidateVirtualControllerType(lastVControllerType, TRBotOSPlatform.CurrentOS);

            //Show a message saying the previous value wasn't supported and save the changes
            if (VirtualControllerHelpers.IsVControllerSupported(lastVControllerType, TRBotOSPlatform.CurrentOS) == false)
            {
                EvtDispatcher.BroadcastMessage($"Current virtual controller {lastVControllerType} is not supported by the {TRBotOSPlatform.CurrentOS} platform. Switched it to the default of {curVControllerType} for this platform.");                

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings lastVControllerSetting = context.GetSetting(SettingsConstants.LAST_VCONTROLLER_TYPE);
                    lastVControllerSetting.ValueInt = (long)curVControllerType;
                    context.SaveChanges();
                }
            }

            IVirtualControllerManager controllerMngr = VirtualControllerHelpers.GetVControllerMngrForType(curVControllerType, Logger);

            VControllerContainer = new VirtualControllerContainer(controllerMngr, curVControllerType);
        }

        private void InitRoutines()
        {
            RoutineHndler.Initialize();

            //Add the periodic input routine if it's enabled
            long periodicInputEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_ENABLED, 0L);
            if (periodicInputEnabled > 0L)
            {
                RoutineHndler.AddRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME, new PeriodicInputRoutine());
            }
        }

        private void InitWebSocketServer()
        {
            WebSocketMngr = new WebSocketManager(Logger);

            long webSocketServerEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.WEBSOCKET_SERVER_ENABLED, 0);

            if (webSocketServerEnabled > 0)
            {
                string webSocketAddress = DatabaseMngr.GetSettingString(SettingsConstants.WEBSOCKET_SERVER_ADDRESS, "ws://127.0.0.1:4350");

                bool started = WebSocketMngr.StartServer(webSocketAddress);

                if (started == false)
                {
                    Logger.Warning($"Failed to start WebSocket server. Ensure that {SettingsConstants.WEBSOCKET_SERVER_ADDRESS} is a valid WebSocket address.");
                }
                else
                {
                    Logger.Information($"Started WebSocket server at \"{WebSocketMngr.ServerUrl}\" on port {WebSocketMngr.ServerPort}.");
                }
            }
        }

        private void InitEventDispatcher()
        {
            long evtEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED, 0);
            string evtPath = DatabaseMngr.GetSettingString(SettingsConstants.EVENT_DISPATCHER_WEBSOCKET_PATH, "/evt");

            EvtDispatcher = new WebSocketEventDispatcher(evtPath, WebSocketMngr, Logger);

            if (evtEnabled <= 0)
            {
                Logger.Information($"{SettingsConstants.EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED} setting is false, so disabling extra features.");

                EvtDispatcher.DisableExtraFeatures();
            }
        }

        #region General Client Service Events

        private void OnConnected(EvtConnectedArgs e)
        {
            Logger.Information($"Bot connected to service \"{e.ServiceName}\"!");
            
            //Twitch and IRC send the connect messages to each channel they join, not when they connect to the service
            if (e.ServiceName == ClientServiceNames.TWITCH_SERVICE || e.ServiceName == ClientServiceNames.IRC_SERVICE)
            {
                return;
            }

            string connectMessage = DatabaseMngr.GetSettingString(SettingsConstants.CONNECT_MESSAGE, "Connected!");

            if (string.IsNullOrEmpty(connectMessage) == false)
            {
                IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);
                service.MsgHandler.QueueMessage(connectMessage);
            }
        }

        private void OnConnectionError(EvtConnectionErrorArgs e)
        {
            Logger.Warning($"Failed to connect to {e.ServiceName}: {e.Error.Message}");

            //Fire a disconnect event to reconnect to the service
            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED,
                new EvtDisconnectedArgs()
                {
                    ServiceName = e.ServiceName
                });
        }

        private void OnChatCommandReceived(EvtChatCommandArgs e)
        {
            AddMessageToQueue(new MsgQueueData(MsgQueueTypes.Command, e));
        }

        private void OnUserSentMessage(EvtUserMessageArgs e)
        {
            AddMessageToQueue(new MsgQueueData(MsgQueueTypes.Message, e));
        }

        private void HandleChatCommandReceived(EvtChatCommandArgs e)
        {
            //Add user before processing the command
            User user = GetOrAddUser(e.Command.UserMessage.UserExternalID, e.ServiceName, e.Command.UserMessage.Username, out bool added);

            try
            {
                CmdHandler.HandleCommand(e);
            }
            catch (Exception exc)
            {
                Logger.Error($"Ran into exception on command {e.Command.CommandText}: {exc.Message}\n{exc.StackTrace}"); 
            }
        }

        private void HandleUserSentMessage(EvtUserMessageArgs e)
        {
            //Look for a user with this name
            string userName = e.UserMessage.Username;

            IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);

            User user = GetOrAddUser(e.UserMessage.UserExternalID, e.ServiceName, userName, out bool added);

            if (added == true)
            {
                //Get the new user message and replace the variable with their name
                string newUserMessage = DatabaseMngr.GetSettingString(SettingsConstants.NEW_USER_MESSAGE, $"Welcome to the stream, {userName}!");
                newUserMessage = newUserMessage.Replace("{0}", userName);
                
                service.MsgHandler.QueueMessage(newUserMessage);
            }
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                user = context.GetUserByExternalID(e.UserMessage.UserExternalID);
                
                //Increment message count and save
                if (user != null && user.HasConsentToDataOption(UserDataConsentOptions.Stats) == true)
                {
                    user.Stats.TotalMessageCount++;
                    context.SaveChanges();
                }

                //Check for memes if the user isn't ignoring them
                if (user != null && user.HasConsentToDataOption(UserDataConsentOptions.Memes) == true)
                {
                    string possibleMeme = e.UserMessage.Message.ToLower();
                    Meme meme = context.Memes.AsNoTracking().FirstOrDefault((m) => m.MemeName == possibleMeme);
                    if (meme != null)
                    {
                        //Don't send the meme if the meme is a chat command on this service
                        //This can get here if the meme was initially added on a service in which it's not considered a command
                        if (ConnectionHelpers.IsServiceChatCommand(e.ServiceName, meme.MemeValue) == true)
                        {
                            service.MsgHandler.QueueMessage("This meme would normally perform a chat command, so I can't allow that!");
                        }
                        else
                        {
                            service.MsgHandler.QueueMessage(meme.MemeValue);
                        }
                    }
                }
            }

            bool isValidInput = false;

            //Check for inputs only if input handling is enabled
            if (InputHndlr.InputsEnabled == true)
            {
                //Process the message to see if it's an input 
                isValidInput = ProcessMsgAsInput(e);
            }

            //Check for printing custom messages
            PrintCustomMessages(e.UserMessage.UserExternalID, userName, e.ServiceName);

            //Add only non-inputs to simulate data
            if (isValidInput == true)
            {
                return;
            }

            AddUserMessageToSimulateData(e.UserMessage.UserExternalID, e.UserMessage.Message);
        }

        private void PrintCustomMessages(string userExternalID, string userName, string serviceName)
        {
            //First, make sure custom messages are enabled
            long customMessagesEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.CUSTOM_MESSAGES_ENABLED, 1L);

            if (customMessagesEnabled <= 0)
            {
                return;
            }

            //Use the user's message count as the value for custom messages
            long messageCount = -1L;
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.Users.AsNoTracking()
                    .Include(us => us.Stats)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);
                
                //Ignore if not found or the user is not opted into custom messages
                if (user == null || user.HasConsentToDataOption(UserDataConsentOptions.Tutorial) == false)
                {
                    return;
                }

                messageCount = user.Stats.TotalMessageCount;
            }

            List<string> messages = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Get all messages triggered at this value
                messages = context.CustomMessages.AsNoTracking()
                    .Where(cm => cm.Enabled > 0 && cm.TriggerValue == messageCount)
                    .Select(cm => cm.Message)
                    .Where(msg => msg != null && msg != string.Empty)
                    .ToList();
            }

            //How should the message be sent?
            CustomMessageSendTypes sendType = (CustomMessageSendTypes)DatabaseMngr.GetSettingInt(
                SettingsConstants.CUSTOM_MESSAGE_SEND_TYPE, (long)CustomMessageSendTypes.Room);

            string msgCountString = messageCount.ToString();

            //Send each message through the service
            IClientService service = ClientServiceMngr.GetClientService(serviceName);
            for (int i = 0; i < messages.Count; i++)
            {
                string modifiedMsg = messages[i].Replace("{0}", userName).Replace("{1}", msgCountString);

                //For whisper messages, send it to the specific user
                if (sendType == CustomMessageSendTypes.Whisper)
                {
                    string recipient = userExternalID;

                    //Choose who to send it to
                    //For some services, the username suffices
                    //For others, it needs to be a full ID (Ex. XMPP JID)
                    if (serviceName == ClientServiceNames.TERMINAL_SERVICE
                        || serviceName == ClientServiceNames.TWITCH_SERVICE
                        || serviceName == ClientServiceNames.WEBSOCKET_SERVICE
                        || serviceName == ClientServiceNames.IRC_SERVICE)
                    {
                        recipient = userName;
                    }

                    Logger.Debug($"Custom message whisper recipient = {recipient}");

                    service.MsgHandler.QueueMessage(new TRBot.Connection.WhisperMessage(recipient,
                        modifiedMsg, false, string.Empty, Serilog.Events.LogEventLevel.Information),
                        string.Empty);
                }
                else
                {
                    service.MsgHandler.QueueMessage(modifiedMsg, string.Empty);
                }
            }
        }

        private void AddUserMessageToSimulateData(string userExternalID, string message)
        {
            //Add the message to the user's simulate data
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.Users.Include(u => u.Stats).FirstOrDefault(u => u.ExternalID == userExternalID);

                if (user == null)
                {
                    return;
                }

                string commandIdentifier = context.GetCommandIdentifier();

                bool isCommand = ConnectionHelpers.IsCommand(message, commandIdentifier);
                bool isNullOrWhitespace = string.IsNullOrWhiteSpace(message);

                //Handle simulate data for the user if the message doesn't start with a command
                //Also ensure the user is opted into simulate data
                if (isCommand == false && isNullOrWhitespace == false
                    && user.HasConsentToDataOption(UserDataConsentOptions.Simulate) == true)
                {
                    string simulateHistory = user.Stats.SimulateHistory;

                    //Replace all whitespace with space (' ') for consistency
                    string msgWhitespace = Helpers.ReplaceAllWhitespaceWithSpace(message);

                    //No history - set to a default value
                    if (string.IsNullOrEmpty(simulateHistory) == true)
                    {
                        simulateHistory = string.Empty;
                    }

                    int maxLength = (int)context.GetSettingInt(SettingsConstants.MAX_USER_SIMULATE_STRING_LENGTH, 10000L);

                    //Append a space followed by the user's message
                    if (simulateHistory.Length > 0)
                    {
                        simulateHistory += " " + msgWhitespace;
                    }
                    //Ignore the space if the history is empty
                    else
                    {
                        simulateHistory = msgWhitespace;
                    }

                    int diff = simulateHistory.Length - maxLength;

                    //If we're over the max length, trim the difference from the start of the history
                    if (diff > 0)
                    {
                        simulateHistory = simulateHistory.Remove(0, diff);
                    }

                    //Set and save
                    user.Stats.SimulateHistory = simulateHistory;

                    context.SaveChanges();
                }
            }
        }

        private void OnReconnected(EvtReconnectedArgs e)
        {
            string reconnectedMessage = DatabaseMngr.GetSettingString(SettingsConstants.RECONNECTED_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(reconnectedMessage) == false)
            {
                IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);
                service.MsgHandler.QueueMessage(reconnectedMessage);
            }

            //Remove the reconnect routine for the given service
            RoutineHndler.RemoveRoutine(RoutineConstants.RECONNECT_ROUTINE_NAME_PREFIX + e.ServiceName);
        }

        private void OnDisconnected(EvtDisconnectedArgs e)
        {
            //Check if the service is still here
            IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);

            //Don't add if the service is null
            if (service == null)
            {
                return;
            }

            //Add a reconnect routine to the given service if it doesn't exist
            string reconnectRoutineName = RoutineConstants.RECONNECT_ROUTINE_NAME_PREFIX + e.ServiceName;
            
            BaseRoutine routine = RoutineHndler.FindRoutine(reconnectRoutineName);
            if (routine == null)
            {
                Logger.Warning($"Bot disconnected from \"{e.ServiceName}\"! Please check your internet connection. Starting reconnection attempts...");

                RoutineHndler.AddRoutine(reconnectRoutineName, new ReconnectRoutine(service, e.ServiceName));
            }
        }

        //Handle stats such as increasing valid input count and autopromotion
        private void HandleUserStatsOnInput(EvtUserInputArgs<ParsedInputSequence> e)
        {
            string userName = e.UserMessage.Username;
            string userExternalID = e.UserMessage.UserExternalID;

            //Fetch these values ahead of time to avoid passing the database context through so many methods    
            long autoPromoteEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.AUTO_PROMOTE_ENABLED, 0L);
            long autoPromoteInputReq = DatabaseMngr.GetSettingInt(SettingsConstants.AUTO_PROMOTE_INPUT_REQ, long.MaxValue);
            long autoPromoteLevel = DatabaseMngr.GetSettingInt(SettingsConstants.AUTO_PROMOTE_LEVEL, -1L);
            string autoPromoteMsg = DatabaseMngr.GetSettingString(SettingsConstants.AUTOPROMOTE_MESSAGE, string.Empty);

            bool addedInputCount = false;
            long prevInputCount = 0L;

            //Get the max recorded inputs per-user
            long maxUserRecInps = DatabaseMngr.GetSettingInt(SettingsConstants.MAX_USER_RECENT_INPUTS, 0L);

            //It's a valid input - save it in the user's stats
            //Also record the input if it should be recorded
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.Users
                    .Include(u => u.Stats)
                    .Include(u => u.RecentInputs)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                //Ignore if the user is opted out
                if (user != null && user.HasConsentToDataOption(UserDataConsentOptions.Stats) == true)
                {
                    prevInputCount = user.Stats.ValidInputCount;

                    user.Stats.ValidInputCount++;
                    addedInputCount = true;

                    context.SaveChanges();

                    //Store recent user inputs if the setting is greater than 0
                    if (maxUserRecInps > 0)
                    {
                        //Add the recorded input as they input it
                        //This guarantees that it will fit within the character limit of the service they typed from
                        user.RecentInputs.Add(new RecentInput(e.UserMessage.Message));
                        context.SaveChanges();

                        int diff = user.RecentInputs.Count - (int)maxUserRecInps;

                        //If we're over the max after adding, remove
                        if (diff > 0)
                        {
                            //Order by ascending ID and take the difference
                            //Lower IDs = older entries
                            IEnumerable<RecentInput> shouldRemove = user.RecentInputs.OrderBy(r => r.UserID).Take(diff);

                            foreach (RecentInput rec in shouldRemove)
                            {
                                user.RecentInputs.Remove(rec);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }

            bool autoPromoted = false;

            //Check if auto promote is enabled and auto promote the user if applicable
            if (addedInputCount == true)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    User user = context.Users.Include(u => u.Stats).FirstOrDefault(u => u.ExternalID == userExternalID);
                    
                    //Check if the user was already autopromoted, autopromote is enabled,
                    //and if the user reached the autopromote input count requirement
                    if (user != null && user.Stats.AutoPromoted == 0 && autoPromoteEnabled > 0
                        && user.Stats.ValidInputCount >= autoPromoteInputReq)
                    {
                        //Only autopromote if this is a valid permission level
                        //We may not want to log or send a message for this, as it has potential to be very spammy,
                        //and it's not something the users can control
                        if (PermissionHelpers.IsValidPermissionValue(autoPromoteLevel) == true)
                        {
                            //Mark the user as autopromoted and save
                            user.Stats.AutoPromoted = 1;
                            autoPromoted = true;

                            context.SaveChanges();
                        }
                    }
                }

                //Send a message when a user ranks up
                string rankUpMsg = DatabaseMngr.GetSettingString(SettingsConstants.RANK_UP_MESSAGE, string.Empty);

                if (string.IsNullOrEmpty(rankUpMsg) == false)
                {
                    DisplayRank prevRank = DatabaseMngr.GetUserDisplayRankAtValue(prevInputCount);
                    DisplayRank newRank = DatabaseMngr.GetUserDisplayRankAtValue(prevInputCount + 1);

                    if (prevRank != null && newRank != null && prevRank.Rank != newRank.Rank)
                    {
                        string rankUpMsgFormatted = rankUpMsg;
                        rankUpMsgFormatted = rankUpMsgFormatted.Replace("{0}", userName)
                            .Replace("{1}", newRank.Rank.ToString())
                            .Replace("{2}", newRank.Label.ToString());

                        IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);
                        service.MsgHandler.QueueMessage(rankUpMsgFormatted);
                    }
                }
            }

            if (autoPromoted == true)
            {
                //If the user is already at or above this level, don't set them to it
                //Only set if the user is below
                if (e.UserLevel < autoPromoteLevel)
                {
                    //Adjust abilities and promote to the new level
                    DatabaseMngr.AdjustUserLvlAndAbilitiesOnLevel(e.UserMessage.UserExternalID, autoPromoteLevel);

                    if (string.IsNullOrEmpty(autoPromoteMsg) == false)
                    {
                        PermissionLevels permLvl = (PermissionLevels)autoPromoteLevel;
                        string finalMsg = autoPromoteMsg.Replace("{0}", userName).Replace("{1}", permLvl.ToString());
                        
                        IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);
                        service.MsgHandler.QueueMessage(finalMsg);
                    } 
                }
            }
        }

        #endregion

        #region IRC-specific Events

        private void IRCOnJoinedChannel(IRCJoinedChannelArgs e)
        {
            Logger.Information($"Joined IRC channel \"{e.ChannelName}\"");

            string connectMessage = DatabaseMngr.GetSettingString(SettingsConstants.CONNECT_MESSAGE, "Connected!");

            if (string.IsNullOrEmpty(connectMessage) == false)
            {
                IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.IRC_SERVICE);
                service.MsgHandler.QueueMessage(connectMessage);
            }            
        }

        #endregion

        #region Twitch-specific Events

        private void TwitchOnJoinedChannel(OnJoinedChannelArgs e)
        {
            Logger.Information($"Joined Twitch channel \"{e.Channel}\"");

            string connectMessage = DatabaseMngr.GetSettingString(SettingsConstants.CONNECT_MESSAGE, "Connected!");

            if (string.IsNullOrEmpty(connectMessage) == false)
            {
                IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
                service.MsgHandler.QueueMessage(connectMessage);
            }
        }

        private void TwitchOnWhisperReceived(OnWhisperReceivedArgs e)
        {
            
        }

        private void TwitchOnWhisperCommandReceived(OnWhisperCommandReceivedArgs e)
        {
            
        }

        private void TwitchOnNewSubscriber(OnNewSubscriberArgs e)
        {
            string newSubscriberMessage = DatabaseMngr.GetSettingString(SettingsConstants.NEW_SUBSCRIBER_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(newSubscriberMessage) == false)
            {
                string finalMsg = newSubscriberMessage.Replace("{0}", e.Subscriber.DisplayName);

                IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
                service.MsgHandler.QueueMessage(finalMsg);
            }
        }

        private void TwitchOnReSubscriber(OnReSubscriberArgs e)
        {
            string resubscriberMessage = DatabaseMngr.GetSettingString(SettingsConstants.RESUBSCRIBER_MESSAGE, string.Empty);

            if (string.IsNullOrEmpty(resubscriberMessage) == false)
            {
                string finalMsg = resubscriberMessage.Replace("{0}", e.ReSubscriber.DisplayName).Replace("{1}", e.ReSubscriber.Months.ToString());
                IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
                service.MsgHandler.QueueMessage(finalMsg);
            }
        }

        private void TwitchPubSubConnected(EventArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
            service.MsgHandler.QueueMessage("Twitch PubSub connected!");
        }

        private void TwitchPubSubDisconnected(EventArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
            service.MsgHandler.QueueMessage("Twitch PubSub disconnected!");
        }

        private void TwitchPubSubError(OnPubSubServiceErrorArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
            service.MsgHandler.QueueMessage($"Error with PubSub: {e.Exception.Message}");
        }

        private void TwitchOnFollow(OnFollowArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
            service.MsgHandler.QueueMessage($"Thanks for following, {e.Username} :D !");
        }

        private void TwitchOnEmoteOnlyOff(OnEmoteOnlyOffArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(ClientServiceNames.TWITCH_SERVICE);
            service.MsgHandler.QueueMessage($"{e.Moderator} turned OFF emote only chat!");
        }

        #endregion

        private bool ProcessMsgAsInput(EvtUserMessageArgs e)
        {
            string commandIdentifier = DatabaseMngr.GetCommandIdentifier();

            //Ignore commands as inputs
            if (ConnectionHelpers.IsCommand(e.UserMessage.Message, commandIdentifier) == true)
            {
                return false;
            }

            string userExternalID = e.UserMessage.UserExternalID;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.Users.AsNoTracking().Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility).FirstOrDefault(u => u.ExternalID == userExternalID);

                //Check if the user is silenced and ignore the message if so
                if (user != null && user.HasEnabledAbility(PermissionConstants.SILENCED_ABILITY) == true)
                {
                    return false;
                }
            }

            IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);

            GameConsole usedConsole = null;

            int lastConsoleID = 1;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                lastConsoleID = (int)context.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);
                GameConsole lastConsole = context.Consoles.AsNoTracking().Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos).ThenInclude(ic => ic.InvalidInputs).FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            //If there are no valid inputs, don't attempt to parse
            if (usedConsole == null)
            {
                service.MsgHandler.QueueMessage($"The current console does not point to valid data. Please set a different console to use, or if none are available, add one.");
                return false;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                service.MsgHandler.QueueMessage($"The current console, \"{usedConsole.Name}\", does not have any available inputs.");
            }

            ParsedInputSequence inputSequence = default;
            string userName = e.UserMessage.Username;
            int defaultDur = 200;
            int defaultPort = 0;
            long userLevel = 0L;
            HashSet<string> userRestrictedInputs = null;

            try
            {
                int maxDur = 60000;
                
                //Get default and max input durations
                //Use user overrides if they exist, otherwise use the global values
                User user = null;
                
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    if (string.IsNullOrEmpty(userExternalID) == false)
                    {
                        user = context.Users.AsNoTracking().Include(u => u.RestrictedInputs)
                            .ThenInclude(ri => ri.inputData).FirstOrDefault(u => u.ExternalID == userExternalID);

                        if (user != null)
                        {
                            userRestrictedInputs = user.GetRestrictedInputs();
                        }
                    }
                }

                if (user != null)
                {
                    //Get default controller port
                    defaultPort = (int)user.ControllerPort;
                    
                    //Get level
                    userLevel = user.Level;
                }

                defaultDur = (int)DatabaseMngr.GetUserOrGlobalDefaultInputDur(userExternalID);
                maxDur = (int)DatabaseMngr.GetUserOrGlobalMaxInputDur(userExternalID);

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //Get input synonyms for this console
                    IQueryable<InputSynonym> synonyms = context.InputSynonyms.Where(syn => syn.ConsoleID == lastConsoleID);

                    StandardParserValidator validator = new StandardParserValidator(userRestrictedInputs,
                        userLevel, usedConsole, VControllerContainer.VControllerMngr, Logger);

                    IParser standardParser = new StandardParserFactory(context.Macros, synonyms,
                            usedConsole.GetInputNames(InputNameFiltering.Enabled), defaultPort, VControllerContainer.VControllerMngr.ControllerCount - 1,
                            defaultDur, maxDur, true, validator)
                        .Create();

                    //Parse inputs to get our parsed input sequence
                    inputSequence = standardParser.ParseInputs(e.UserMessage.Message);
                }
            }
            catch (Exception exception)
            {
                string excMsg = exception.Message;

                //Handle parsing exceptions
                service.MsgHandler.QueueMessage($"ERROR PARSING: {excMsg} | {exception.StackTrace}", logLevel: Serilog.Events.LogEventLevel.Warning);
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;
            }

            //Check for non-valid messages
            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                //Display error message for invalid inputs
                if (inputSequence.ParsedInputResult == ParsedInputResults.Invalid)
                {
                    service.MsgHandler.QueueMessage(inputSequence.Error);
                }

                return false;
            }

            #region Parser Post-Process Validation
            
            /* All this validation may be able to be performed faster.
             * Find a way to speed it up.
             */

            long globalInputPermLevel = DatabaseMngr.GetSettingInt(SettingsConstants.GLOBAL_INPUT_LEVEL, 0L);
            int userControllerPort = 0;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(e.UserMessage.UserExternalID);
            
                //Ignore based on user level and permissions
                if (user != null && user.Level < globalInputPermLevel)
                {
                    service.MsgHandler.QueueMessage($"Inputs are restricted to levels {(PermissionLevels)globalInputPermLevel} and above.");
                    return false;
                }

                if (user != null)
                {
                    userControllerPort = (int)user.ControllerPort;
                }
            }

            //Add delays between inputs if we should
            if (DatabaseMngr.GetUserOrGlobalMidInputDelay(e.UserMessage.Username, out long midInputDelay) == true)
            {
                //Get a blank input the user can perform
                InputData blankInpData = usedConsole.GetAvailableBlankInput(userLevel, userRestrictedInputs);

                if (blankInpData != null)
                {
                    IPostParser postParser = new StandardPostParser();
                    MidInputDelayData midInputDelayData = postParser.InsertMidInputDelays(inputSequence,
                        userControllerPort, (int)midInputDelay, usedConsole, blankInpData);

                    //If it's successful, replace the input list and duration
                    if (midInputDelayData.Success == true)
                    {
                        int oldDur = inputSequence.TotalDuration;
                        inputSequence.Inputs = midInputDelayData.NewInputs;
                        inputSequence.TotalDuration = midInputDelayData.NewTotalDuration;
                    }
                }
            }

            #endregion

            //Make sure inputs aren't stopped
            if (InputHndlr.InputsHalted == true)
            {
                //We can't process inputs because they're currently stopped
                service.MsgHandler.QueueMessage("New inputs cannot be processed until all other inputs have stopped.", logLevel: Serilog.Events.LogEventLevel.Warning);
                return true;
            }

            EvtDispatcher.DispatchEvent(ClientServiceEventNames.INPUT,
                new EvtUserInputArgs<ParsedInputSequence>(e.UserMessage, inputSequence, userLevel, e.ServiceName, e.EventID));

            InputModes inputMode = (InputModes)DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE, 0L);

            //If the mode is Democracy, add it as a vote for this input
            if (inputMode == InputModes.Democracy)
            {
                //Set up the routine if it doesn't exist
                BaseRoutine foundRoutine = RoutineHndler.FindRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME);
                DemocracyRoutine democracyRoutine = null;

                if (foundRoutine == null)
                {
                    long voteTime = DatabaseMngr.GetSettingInt(SettingsConstants.DEMOCRACY_VOTE_TIME, 10000L);

                    democracyRoutine = new DemocracyRoutine(voteTime);
                    RoutineHndler.AddRoutine(RoutineConstants.DEMOCRACY_ROUTINE_NAME, democracyRoutine);
                }
                else
                {
                    democracyRoutine = (DemocracyRoutine)foundRoutine;
                }

                democracyRoutine.AddInputSequence(e.ServiceName, userExternalID, userName, e.UserMessage.Message, inputSequence.Inputs);
            }
            //If it's Anarchy, carry out the input
            else
            {
                /************************************
                * Finally carry out the inputs now! *
                ************************************/

                InputHndlr.CarryOutInput(inputSequence.Inputs, usedConsole, VControllerContainer.VControllerMngr);
            }

            return true;
        }

        #region Message Queue Methods

        private void AddMessageToQueue(in MsgQueueData msgQueueData)
        {
            MsgQueue.Enqueue(msgQueueData);
        }

        private void StartQueueThread()
        {
            if (CancelTokenSource != null)
            {
                return;
            }

            CancelTokenSource = new CancellationTokenSource();

            QueueThread(CancelTokenSource.Token).ConfigureAwait(false);
        }

        private void StopQueueThread()
        {
            if (CancelTokenSource == null || CancelTokenSource.IsCancellationRequested == true)
            {
                return;
            }

            CancelTokenSource.Cancel();
            CancelTokenSource.Dispose();
            CancelTokenSource = null;
        }

        public async Task QueueThread(CancellationToken cancelToken)
        {
            while (true)
            {
                await Task.Delay(1);
                
                //End if cancellation was requested
                if (cancelToken.IsCancellationRequested == true)
                {
                    break;
                }

                if (MsgQueue.Count == 0)
                {
                    continue;
                }

                bool dequeued = MsgQueue.TryDequeue(out MsgQueueData msgQueueData);

                if (dequeued == false)
                {
                    continue;
                }

                try
                {
                    switch (msgQueueData.MsgQueueType)
                    {
                        case MsgQueueTypes.Message:
                            HandleUserSentMessage(msgQueueData.EvtArgs as EvtUserMessageArgs);
                            break;
                        case MsgQueueTypes.Command:
                            HandleChatCommandReceived(msgQueueData.EvtArgs as EvtChatCommandArgs);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Logger.Error($"ERROR in {nameof(MsgQueue)} with type \"{msgQueueData.MsgQueueType}\" - {e.Message}\n{e.StackTrace}");
                    continue;
                }
            }
        }

#endregion

        private void InitClientServices()
        {
            ClientServiceMngr = new ClientServiceManager(Logger, EvtDispatcher);
            ClientServiceMngr.Initialize();

            AddRemoveClientServicesFromDB(false, false);
        }

        private bool FindThreadSleepTime(Settings setting)
        {
            return (setting.Key == SettingsConstants.MAIN_THREAD_SLEEP);
        }

        private void HandleReloadBoth(EvtDataReloadedArgs e)
        {
            //Check for changes in the log level
            Serilog.Events.LogEventLevel logLevel = (Serilog.Events.LogEventLevel)DatabaseMngr.GetSettingInt(SettingsConstants.LOG_LEVEL, (long)Serilog.Events.LogEventLevel.Information);
            if (logLevel != Logger.MinLoggingLevel)
            {
                Logger.Information($"Detected change in logging level - changing the logging level from {Logger.MinLoggingLevel} to {logLevel}");
                Logger.MinLoggingLevel = (logLevel);
            }

            //Check for changes in the input handler
            bool inputsEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.INPUTS_ENABLED, 1L) != 0L;
            if (inputsEnabled != InputHndlr.InputsEnabled)
            {
                Logger.Information($"Detected change in the inputs enabled setting - turning inputs {(inputsEnabled == true ? "on" : "off")}!");
                InputHndlr.ToggleInputHandling(inputsEnabled);
            }

            //Check if the periodic input value changed, and enable/disable the routine if we should
            long periodicEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_ENABLED, 0L);
            if (periodicEnabled == 0)
            {
                //Remove the routine
                RoutineHndler.RemoveRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME);
            }
            else
            {
                BaseRoutine periodicInputRoutine = RoutineHndler.FindRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME);

                //Add the routine if it doesn't exist
                if (periodicInputRoutine == null)
                {
                    RoutineHndler.AddRoutine(RoutineConstants.PERIODIC_INPUT_ROUTINE_NAME, new PeriodicInputRoutine()); 
                }
            }

            IClientService clientService = ClientServiceMngr.GetClientService(e.ServiceName);

            //Check if the virtual controller type was changed
            if (LastVControllerTypeChanged() == true)
            {
                VirtualControllerTypes lastVControllerType = (VirtualControllerTypes)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_VCONTROLLER_TYPE, 0L);
                VirtualControllerTypes supportedVCType = VirtualControllerHelpers.ValidateVirtualControllerType(lastVControllerType, TRBotOSPlatform.CurrentOS);

                //Show a message saying the previous value wasn't supported
                if (VirtualControllerHelpers.IsVControllerSupported(lastVControllerType, TRBotOSPlatform.CurrentOS) == false)
                {
                    clientService.MsgHandler.QueueMessage($"Current virtual controller {lastVControllerType} is not supported by the {TRBotOSPlatform.CurrentOS} platform. Switched it to the default of {supportedVCType} for this platform.");                
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        Settings lastVControllerSetting = context.GetSetting(SettingsConstants.LAST_VCONTROLLER_TYPE);
                        lastVControllerSetting.ValueInt = (long)supportedVCType;
                        context.SaveChanges();
                    }
                }

                ChangeVControllerType(e, supportedVCType);
            }
            else
            {
                ReinitVControllerCount(e);
            }

            IClientService[] services = null;

            //Handle message throttling changes
            MessageThrottlingOptions msgThrottle = (MessageThrottlingOptions)DatabaseMngr.GetSettingInt(SettingsConstants.MESSAGE_THROTTLE_TYPE, 0L);
            long msgTime = DatabaseMngr.GetSettingInt(SettingsConstants.MESSAGE_COOLDOWN, 30000L);
            long msgThrottleCount = DatabaseMngr.GetSettingInt(SettingsConstants.MESSAGE_THROTTLE_COUNT, 20L);

            if (msgThrottle != clientService.MsgHandler.CurThrottleOption)
            {
                Logger.Information("Detected change in message throttling type - changing message throttler.");

                services = ClientServiceMngr.GetAllServices();
                for (int i = 0; i < services.Length; i++)
                {
                    services[i].MsgHandler.SetMessageThrottling(msgThrottle, new MessageThrottleData(msgTime, msgThrottleCount));
                }
            }

            //Handle message prefix changes
            string msgPrefix = DatabaseMngr.GetSettingString(SettingsConstants.MESSAGE_PREFIX, string.Empty);
            if (msgPrefix != clientService.MsgSettings.MessagePrefix)
            {
                Logger.Information("Detected change in message prefix - changing message prefix.");

                if (services == null)
                {
                    services = ClientServiceMngr.GetAllServices();
                }

                for (int i = 0; i < services.Length; i++)
                {
                    services[i].MsgSettings.SetMessagePrefix(msgPrefix);
                }
            }

            //Handle bot messages enabled changes
            bool botMessagesEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.BOT_MESSAGES_ENABLED, 1L) != 0L;
            if (botMessagesEnabled != clientService.MsgHandler.MessageQueuingEnabled)
            {
                Logger.Information($"Detected change in bot messages enabled - turning bot messages {(botMessagesEnabled == true ? "on" : "off")}.");
                
                if (services == null)
                {
                    services = ClientServiceMngr.GetAllServices();
                }

                for (int i = 0; i < services.Length; i++)
                {
                    services[i].MsgHandler.ToggleMessageQueueing(botMessagesEnabled);
                }
            }

            //Handle bot message character limit changes
            int botMsgCharLimit = (int)DatabaseMngr.GetSettingInt(SettingsConstants.BOT_MSG_CHAR_LIMIT, 500L);
            if (botMsgCharLimit != clientService.MsgHandler.BotMessageCharLimit)
            {
                Logger.Information("Detected change in bot message character limit - updating bot message character limit.");
                
                if (services == null)
                {
                    services = ClientServiceMngr.GetAllServices();
                }

                for (int i = 0; i < services.Length; i++)
                {
                    services[i].MsgHandler.BotMessageCharLimit = botMsgCharLimit;
                }
            }

            //Handle command identifier changes
            string commandIdentifier = DatabaseMngr.GetCommandIdentifier();
            if (commandIdentifier != clientService.CmdSettings.CommandIdentifier)
            {
                Logger.Information($"Detected change in command identifier - updating service command identifiers to: {commandIdentifier}");
                
                if (Helpers.HasWhitespace(commandIdentifier) == true)
                {
                    Logger.Warning($"Command identifier \"{commandIdentifier}\" has whitespace, which will interfere with parsing commands. Please set a command identifier without whitespace.");
                }

                if (services == null)
                {
                    services = ClientServiceMngr.GetAllServices();
                }

                for (int i = 0; i < services.Length; i++)
                {
                    services[i].CmdSettings.SetCommandIdentifier(commandIdentifier);
                }
            }

            //Handle WebSocket server changes
            long webSocketServerEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.WEBSOCKET_SERVER_ENABLED, 0L);
            if (webSocketServerEnabled <= 0)
            {
                WebSocketMngr.StopServer();
            }
            else
            {
                string webSocketServerAddress = DatabaseMngr.GetSettingString(SettingsConstants.WEBSOCKET_SERVER_ADDRESS, "ws://127.0.0.1:4350");

                WebSocketMngr.StartServer(webSocketServerAddress);
            }

            //Handle event dispatcher changes
            long evtDispatcherExtraFeaturesEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED, 0);
            
            if (evtDispatcherExtraFeaturesEnabled <= 0 && EvtDispatcher.ExtraFeaturesEnabled == true)
            {
                EvtDispatcher.DisableExtraFeatures();
            }
            else if (evtDispatcherExtraFeaturesEnabled > 0 && EvtDispatcher.ExtraFeaturesEnabled == false)
            {
                EvtDispatcher.EnableExtraFeatures();
            }

            //Handle client service changes
            AddRemoveClientServicesFromDB(true, true);
        }

        private bool LastVControllerTypeChanged()
        {
            VirtualControllerTypes vContType = (VirtualControllerTypes)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_VCONTROLLER_TYPE, 0L);

            return (vContType != VControllerContainer.VControllerType);
        }

        private void ChangeVControllerType(EvtDataReloadedArgs e, in VirtualControllerTypes newVControllerType)
        {
            IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);
            
            service.MsgHandler.QueueMessage("Found virtual controller manager change in database. Stopping all inputs and reinitializing virtual controllers.");

            //Fetch the new controller manager
            IVirtualControllerManager controllerMngr = VirtualControllerHelpers.GetVControllerMngrForType(newVControllerType, Logger);

            int prevJoystickCount = (int)DatabaseMngr.GetSettingInt(SettingsConstants.JOYSTICK_COUNT, 1L);
            int newJoystickCount = prevJoystickCount;

            //First, stop all inputs
            Task t = InputHndlr.StopAndHaltAllInputs();
            t.Wait();

            //Dispose the controller manager
            VControllerContainer.VControllerMngr.Dispose();

            VControllerContainer.SetVirtualControllerType(newVControllerType);
            VControllerContainer.SetVirtualControllerManager(controllerMngr);

            VControllerContainer.VControllerMngr.Initialize();

            //Validate virtual controller count for this virtual controller manager
            int minCount = VControllerContainer.VControllerMngr.MinControllers;
            int maxCount = VControllerContainer.VControllerMngr.MaxControllers;

            if (prevJoystickCount < minCount)
            {
                service.MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the min of {minCount}.");
                newJoystickCount = minCount;
            }
            else if (prevJoystickCount > maxCount)
            {
                service.MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the max of {maxCount}.");
                newJoystickCount = maxCount;
            }

            if (prevJoystickCount != newJoystickCount)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //Adjust the value and save
                    Settings joystickCountSetting = context.GetSetting(SettingsConstants.JOYSTICK_COUNT);
                    joystickCountSetting.ValueInt = newJoystickCount;
                    
                    context.SaveChanges();
                }
            }

            int acquiredCount = VControllerContainer.VControllerMngr.InitControllers(newJoystickCount);

            //Resume inputs
            InputHndlr.ResumeRunningInputs();

            service.MsgHandler.QueueMessage($"Changed to virtual controller {VControllerContainer.VControllerType} and acquired {acquiredCount} controllers!");
        }

        private void ReinitVControllerCount(EvtDataReloadedArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);

            int curVControllerCount = VControllerContainer.VControllerMngr.ControllerCount;

            int prevJoystickCount = (int)DatabaseMngr.GetSettingInt(SettingsConstants.JOYSTICK_COUNT, 1L);
            int newJoystickCount = prevJoystickCount;

            int minCount = VControllerContainer.VControllerMngr.MinControllers;
            int maxCount = VControllerContainer.VControllerMngr.MaxControllers;

            //Validate controller count
            if (prevJoystickCount < minCount)
            {
                service.MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the min of {minCount}.");
                newJoystickCount = minCount;
            }
            else if (prevJoystickCount > maxCount)
            {
                service.MsgHandler.QueueMessage($"New controller count of {prevJoystickCount} in database is invalid. Clamping to the max of {maxCount}.");
                newJoystickCount = maxCount;
            }

            if (prevJoystickCount != newJoystickCount)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //Adjust the value and save
                    Settings joystickCountSetting = context.GetSetting(SettingsConstants.JOYSTICK_COUNT);
                    joystickCountSetting.ValueInt = newJoystickCount;

                    context.SaveChanges();
                }
            }

            //Same count, so ignore
            if (curVControllerCount == newJoystickCount)
            {
                return;
            }

            service.MsgHandler.QueueMessage("Found controller count change in database. Stopping all inputs and reinitializing virtual controllers.");

            //First, stop all inputs
            Task t = InputHndlr.StopAndHaltAllInputs();
            t.Wait();

            //Re-initialize the new number of virtual controllers
            VControllerContainer.VControllerMngr.Dispose();
            VControllerContainer.VControllerMngr.Initialize();
            int acquiredCount = VControllerContainer.VControllerMngr.InitControllers(newJoystickCount);

            //Resume inputs
            InputHndlr.ResumeRunningInputs();

            service.MsgHandler.QueueMessage($"Set up and acquired {acquiredCount} controllers!");
        }

        //This method handles adding and removing client services from the database
        private void AddRemoveClientServicesFromDB(in bool addBackupServiceIfNone, in bool autoConnectServices)
        {
            List<ServiceData> services = null; 
            using (BotDBContext dbContext = DatabaseMngr.OpenContext())
            {
                services = dbContext.Services.AsNoTracking().ToList();
            }

            string[] curServices = ClientServiceMngr.GetAllServiceNames();

            bool botMessagesEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.BOT_MESSAGES_ENABLED, 1L) != 0L;

            MessageThrottlingOptions msgThrottleOption = (MessageThrottlingOptions)DatabaseMngr.GetSettingInt(SettingsConstants.MESSAGE_THROTTLE_TYPE, 0L);
            long msgCooldown = DatabaseMngr.GetSettingInt(SettingsConstants.MESSAGE_COOLDOWN, 30000L);
            long msgThrottleCount = DatabaseMngr.GetSettingInt(SettingsConstants.MESSAGE_THROTTLE_COUNT, 20L);
            
            string messagePrefix = DatabaseMngr.GetSettingString(SettingsConstants.MESSAGE_PREFIX, string.Empty);
            MessageThrottleData msgThrottleData = new MessageThrottleData(msgCooldown, msgThrottleCount);

            int botMsgCharLimit = (int)DatabaseMngr.GetSettingInt(SettingsConstants.BOT_MSG_CHAR_LIMIT, 500L);

            string commandIdentifier = DatabaseMngr.GetCommandIdentifier();

            //Services were removed
            if (services.Count < curServices.Length)
            {
                IEnumerable<string> servicesToRemove = services.Select(s => s.ServiceName).Except(curServices);

                //Remove these services
                foreach (string serviceName in servicesToRemove)
                {
                    Logger.Information($"Removing service \"{serviceName}\" since it's no longer in the database.");

                    //Remove the reconnect routine for this service if it's active
                    RoutineHndler.RemoveRoutine(RoutineConstants.RECONNECT_ROUTINE_NAME_PREFIX + serviceName);

                    ClientServiceMngr.RemoveClientService(serviceName);
                }

                if (addBackupServiceIfNone == true)
                {
                    CheckForBackupTerminalService(messagePrefix, msgThrottleOption, msgThrottleData,
                        botMsgCharLimit, botMessagesEnabled, commandIdentifier);
                }

                return;
            }

            //Check for changes
            for (int i = 0; i < services.Count; i++)
            {
                ServiceData serviceData = services[i];
                string serviceName = serviceData.ServiceName;

                IClientService service = ClientServiceMngr.GetClientService(serviceName);

                //Service doesn't exist
                if (service == null)
                {
                    //If the service is now enabled and the service doesn't exist, add it
                    if (serviceData.IsEnabled == true)
                    {
                        IClientService newClientService = CreateClientServiceByName(serviceData, messagePrefix,
                            msgThrottleOption, msgThrottleData, botMsgCharLimit, botMessagesEnabled, commandIdentifier);

                        //The service can be null due to an invalid configuration for specific services or an invalid name
                        if (newClientService != null)
                        {
                            Logger.Information($"Added new enabled service \"{serviceName}\"");

                            ClientServiceMngr.AddClientService(serviceData.ServiceName, newClientService, autoConnectServices);
                        }
                        else
                        {
                            Logger.Information($"Could not set up service \"{serviceName}\" - check your settings and the logs for errors");
                        }
                    }
                }
                //Service exists
                else
                {
                    //If the service is no longer enabled and it's present, remove it
                    if (serviceData.IsEnabled == false)
                    {
                        Logger.Information($"Removing now disabled service \"{serviceName}\"");

                        //Remove the reconnect routine for this service if it's active
                        RoutineHndler.RemoveRoutine(RoutineConstants.RECONNECT_ROUTINE_NAME_PREFIX + serviceName);

                        ClientServiceMngr.RemoveClientService(serviceName);
                    }
                    //Update service settings
                    else
                    {
                        service.MsgSettings.SetLogToLogger(serviceData.IsLoggingToLogger);
                    }
                }
            }

            if (addBackupServiceIfNone == true)
            {
                CheckForBackupTerminalService(messagePrefix, msgThrottleOption, msgThrottleData, botMsgCharLimit, botMessagesEnabled, commandIdentifier);
            }
        }

        private IClientService CreateClientServiceByName(ServiceData serviceData, string messagePrefix,
            in MessageThrottlingOptions msgThrottleOption, in MessageThrottleData msgThrottleData,
            in int botMessageCharLimit, in bool botMessagesEnabled, string commandIdentifier)
        {
            string serviceName = serviceData.ServiceName;

            if (serviceName == ClientServiceNames.TERMINAL_SERVICE)
            {
                return new TerminalClientService(Logger, EvtDispatcher,
                    new BotMessageHandler(Logger, msgThrottleOption, msgThrottleData, botMessageCharLimit, botMessagesEnabled),
                    new MessageSettings(messagePrefix, serviceData.IsLoggingToLogger),
                    new CommandSettings(commandIdentifier));
            }
            else if (serviceName == ClientServiceNames.TWITCH_SERVICE)
            {
                string fileName = string.IsNullOrEmpty(serviceData.ConfigFile) == false
                ? serviceData.ConfigFile
                : Path.Combine(FolderPathResolver.DataFolderPath, TwitchConstants.LOGIN_SETTINGS_FILENAME);

                TwitchLoginSettings twitchSettings = CredentialValidationHelper.ValidateTwitchCredentialsPresent(fileName);

                //If either of these fields are empty, the data is invalid
                if (string.IsNullOrEmpty(twitchSettings.ChannelName) || string.IsNullOrEmpty(twitchSettings.Password)
                    || string.IsNullOrEmpty(twitchSettings.BotName))
                {
                    Logger.Error($"Twitch login settings are invalid. Please fill out all the data in \"{fileName}\" according to the TRBot documentation.");
                    return null;
                }

                ConnectionCredentials credentials = CredentialValidationHelper.MakeCredentialsFromTwitchLogin(twitchSettings);
                
                return new TwitchClientService(credentials, twitchSettings.ChannelName,
                    true, Logger, EvtDispatcher,
                    new BotMessageHandler(Logger, msgThrottleOption, msgThrottleData, botMessageCharLimit, botMessagesEnabled),
                    new MessageSettings(messagePrefix, serviceData.IsLoggingToLogger),
                    new CommandSettings(commandIdentifier));
            }
            else if (serviceName == ClientServiceNames.WEBSOCKET_SERVICE)
            {
                string fileName = string.IsNullOrEmpty(serviceData.ConfigFile) == false
                    ? serviceData.ConfigFile
                    : Path.Combine(FolderPathResolver.DataFolderPath, WebSocketConstants.CONNECTION_SETTINGS_FILENAME);

                WebSocketConnectSettings wsConnectSettings = CredentialValidationHelper.ValidateWebSocketCredentialsPresent(fileName);

                //If the connection field is empty, the data is invalid
                if (string.IsNullOrEmpty(wsConnectSettings.ConnectURL) == true)
                {
                    Logger.Error($"WebSocket connection settings are invalid; there's no given URL to connect to. Please fill out all the data in \"{fileName}\" according to the TRBot documentation.");
                    return null;
                }

                string connectURL = wsConnectSettings.ConnectURL;

                //The connection URL must start with the WebSocket protocol
                if (connectURL.StartsWith(WebSocketConstants.WEBSOCKET_PROTOCOL, StringComparison.Ordinal) == false
                    && connectURL.StartsWith(WebSocketConstants.WEBSOCKET_SECURE_PROTOCOL, StringComparison.Ordinal) == false)
                {
                    Logger.Error($"WebSocket connection URL does not start with \"{WebSocketConstants.WEBSOCKET_PROTOCOL}\" or \"{WebSocketConstants.WEBSOCKET_SECURE_PROTOCOL}\".");
                    return null;
                }

                return new WebSocketClientService(wsConnectSettings.ConnectURL,
                    wsConnectSettings.BotName, Logger, EvtDispatcher,
                    new BotMessageHandler(Logger, msgThrottleOption, msgThrottleData, botMessageCharLimit, botMessagesEnabled),
                    new MessageSettings(messagePrefix, serviceData.IsLoggingToLogger),
                    new CommandSettings(commandIdentifier));
            }
            else if (serviceName == ClientServiceNames.IRC_SERVICE)
            {
                string fileName = string.IsNullOrEmpty(serviceData.ConfigFile) == false
                    ? serviceData.ConfigFile
                    : Path.Combine(FolderPathResolver.DataFolderPath, IRCConstants.LOGIN_SETTINGS_FILENAME);

                IRCLoginSettings ircConnectSettings = CredentialValidationHelper.ValidateIRCCredentialsPresent(fileName);

                //If either of these are empty, the data is invalid
                if (string.IsNullOrEmpty(ircConnectSettings.ServerName) == true || string.IsNullOrEmpty(ircConnectSettings.BotName) == true
                    || string.IsNullOrEmpty(ircConnectSettings.ChannelName) == true)
                {
                    Logger.Error($"IRC login settings are invalid. Please fill out all the data in \"{fileName}\" according to the TRBot documentation.");
                    return null;
                }

                return new IRCClientService(ircConnectSettings, Logger, EvtDispatcher,
                    new BotMessageHandler(Logger, msgThrottleOption, msgThrottleData, botMessageCharLimit, botMessagesEnabled),
                    new MessageSettings(messagePrefix, serviceData.IsLoggingToLogger),
                    new CommandSettings(commandIdentifier));
            }
            else if (serviceName == ClientServiceNames.XMPP_SERVICE)
            {
                string fileName = string.IsNullOrEmpty(serviceData.ConfigFile) == false
                    ? serviceData.ConfigFile
                    : Path.Combine(FolderPathResolver.DataFolderPath, XMPPConstants.LOGIN_SETTINGS_FILENAME);

                XMPPLoginSettings xmppConnectSettings = CredentialValidationHelper.ValidateXMPPCredentialsPresent(fileName);

                //If either of these are empty, the data is invalid
                if (string.IsNullOrEmpty(xmppConnectSettings.RoomName) == true || string.IsNullOrEmpty(xmppConnectSettings.Username) == true
                    || string.IsNullOrEmpty(xmppConnectSettings.Domain) == true)
                {
                    Logger.Error($"XMPP login settings are invalid. Please fill out all the data in \"{fileName}\" according to the TRBot documentation.");
                    return null;
                }

                return new XMPPClientService(xmppConnectSettings, Logger, EvtDispatcher,
                    new BotMessageHandler(Logger, msgThrottleOption, msgThrottleData, botMessageCharLimit, botMessagesEnabled),
                    new MessageSettings(messagePrefix, serviceData.IsLoggingToLogger),
                    new CommandSettings(commandIdentifier));
            }
            else if (serviceName == ClientServiceNames.MATRIX_SERVICE)
            {
                string fileName = string.IsNullOrEmpty(serviceData.ConfigFile) == false
                    ? serviceData.ConfigFile
                    : Path.Combine(FolderPathResolver.DataFolderPath, MatrixConstants.LOGIN_SETTINGS_FILENAME);

                MatrixLoginSettings matrixConnectSettings = CredentialValidationHelper.ValidateMatrixCredentialsPresent(fileName);

                //If either of these are empty, the data is invalid
                if (string.IsNullOrEmpty(matrixConnectSettings.ServerAddress) == true
                    || string.IsNullOrEmpty(matrixConnectSettings.BotMatrixID) == true
                    || string.IsNullOrEmpty(matrixConnectSettings.BotPassword) == true
                    || string.IsNullOrEmpty(matrixConnectSettings.RoomID) == true)
                {
                    Logger.Error($"Matrix login settings are invalid. Please fill out all the data in \"{fileName}\" according to the TRBot documentation.");
                    return null;
                }

                return new MatrixClientService(matrixConnectSettings, Logger, EvtDispatcher,
                    new BotMessageHandler(Logger, msgThrottleOption, msgThrottleData, botMessageCharLimit, botMessagesEnabled),
                    new MessageSettings(messagePrefix, serviceData.IsLoggingToLogger),
                    new CommandSettings(commandIdentifier));
            }

            return null;
        }

        //This method is invoked whenever services are removed
        private void CheckForBackupTerminalService(string messagePrefix, in MessageThrottlingOptions msgThrottleOption,
            in MessageThrottleData msgThrottleData, in int botMsgCharLimit, in bool botMessagesEnabled, string commandIdentifier)
        {
            if (ClientServiceMngr.ServiceCount > 0)
            {
                return;
            }

            //No services are available, so add Terminal as a backup
            Logger.Warning($"No services are available! This renders the bot useless. Falling back to the \"{ClientServiceNames.TERMINAL_SERVICE}\" service.");

            ServiceData terminalData = new ServiceData(ClientServiceNames.TERMINAL_SERVICE, true, false, string.Empty);
            IClientService terminalService = CreateClientServiceByName(terminalData, messagePrefix,
                msgThrottleOption, msgThrottleData, botMsgCharLimit, botMessagesEnabled, commandIdentifier);

            if (terminalService != null)
            {
                Logger.Information($"Added backup \"{ClientServiceNames.TERMINAL_SERVICE}\" service");

                ClientServiceMngr.AddClientService(terminalData.ServiceName, terminalService, true);
            }
            else
            {
                Logger.Error($"Unable to add backup \"{ClientServiceNames.TERMINAL_SERVICE}\" service.");
            }
        }

        private void HandleInitDataEvent(EvtInitDataArgs e)
        {
            IClientService service = ClientServiceMngr.GetClientService(e.ServiceName);

            if (service == null)
            {
                Logger.Warning($"Obtained init data event from invalid service: {e.ServiceName}");
            }

            service?.MsgHandler.QueueMessage("Checking to initialize default values for missing database entries.");

            int entriesAdded = 0;
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Tell it to force initialize defaults
                Settings forceInitSetting = context.GetSetting(SettingsConstants.FORCE_INIT_DEFAULTS);
                if (forceInitSetting != null)
                {
                    forceInitSetting.ValueInt = 1;
                    context.SaveChanges();
                }
            }

            entriesAdded = DataInitializer.InitDefaultData(DefaultDataProvider);

            if (entriesAdded > 0)
            {
                service?.MsgHandler.QueueMessage($"Added {entriesAdded} additional entries to the database. Make sure to reload data if you're expecting new commands.");
            }
            else
            {
                service?.MsgHandler.QueueMessage("No new entries added.");
            }
        }

        //Handle certain events from the JSON
        private void ReceivedIncomingJson(EvtJsonArgs e)
        {
            string evtName = string.Empty;
            string evtDataStr = string.Empty;

            try
            {
                //Get the event type
                JObject jObj = JObject.Parse(e.JsonStr);
                if (jObj.TryGetValue(nameof(EventResponse<EventArgs>.EventType),
                    StringComparison.InvariantCultureIgnoreCase, out JToken nameVal) == true)
                {
                    evtName = (string)nameVal;
                }

                //Get the event object data
                if (jObj.TryGetValue(nameof(EventResponse<EventArgs>.EventData),
                    StringComparison.InvariantCultureIgnoreCase, out JToken dataVal) == true)
                {
                    evtDataStr = dataVal.ToString();
                }
            }
            catch (Exception exc)
            {
                Logger.Warning($"Received invalid JSON - {exc.Message}");
                return;
            }

            if (string.IsNullOrEmpty(evtName) == true)
            {
                Logger.Warning($"Received JSON does not have an {nameof(EventResponse<EventArgs>.EventType)} field");
                return;
            }

            if (string.IsNullOrEmpty(evtDataStr) == true)
            {
                Logger.Warning($"Received JSON does not have an {nameof(EventResponse<EventArgs>.EventData)} field");
                return;
            }

            RedispatchEvent(evtName, evtDataStr);
        }

        private void RedispatchEvent(string eventName, string eventDataJson)
        {
            try
            {
                switch (eventName)
                {
                    case ClientServiceEventNames.MESSAGE:
                        RedispatchEventHelper<EvtUserMessageArgs>(eventName, eventDataJson);
                        break;
                    case ClientServiceEventNames.INPUT:
                        RedispatchEventHelper<EvtUserInputArgs<ParsedInputSequence>>(eventName, eventDataJson);
                        break;
                    case ClientServiceEventNames.BROADCAST_MESSAGE:
                        RedispatchEventHelper<EvtBroadcastMessageArgs>(eventName, eventDataJson);
                        break;
                    case ClientServiceEventNames.BOT_MESSAGE:
                        RedispatchEventHelper<EvtBotMessageArgs>(eventName, eventDataJson);
                        break;
                    case ClientServiceEventNames.OUTGOING_BOT_MESSAGE:
                        RedispatchEventHelper<EvtOutgoingBotMessageArgs>(eventName, eventDataJson);
                        break;
                    case ClientServiceEventNames.COMMAND:
                        RedispatchEventHelper<EvtChatCommandArgs>(eventName, eventDataJson);
                        break;
                    case RoutineEventNames.INPUT_VOTE:
                        RedispatchEventHelper<InputModeVoteArgs>(eventName, eventDataJson);
                        break;
                    case RoutineEventNames.INPUT_VOTE_END:
                        RedispatchEventHelper<InputVoteEndArgs>(eventName, eventDataJson);
                        break;
                    case RoutineEventNames.INPUT_MODE_VOTE:
                        RedispatchEventHelper<InputModeVoteArgs>(eventName, eventDataJson);
                        break;
                    case RoutineEventNames.INPUT_MODE_VOTE_END:
                        RedispatchEventHelper<InputModeVoteEndArgs>(eventName, eventDataJson);
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.Warning($"Unable to redispatch event - {e.Message}");
            }
        }

        private void RedispatchEventHelper<T>(string eventName, string eventDataJson) where T : EventArgs
        {
            //Dispatch without extra features to avoid resending network interactions (Ex. WebSocket resending event)
            T eventArgs = JsonConvert.DeserializeObject<T>(eventDataJson);
            EvtDispatcher.DispatchEventWithoutExtraFeatures<T>(eventName, eventArgs);
        }

        /// <summary>
        /// Obtains a user object from the database.
        /// If the user isn't found, a new one will be added to the database.
        ///
        /// If the user can't be added, this will return null.
        /// </summary>
        /// <param name="externalId">The user's external ID.</param>
        /// <param name="serviceName">The name of the service the user is on.</param>
        /// <param name="userName">The name of the user.</param>
        /// <param name="added">Whether a new user was added to the database.</param>
        /// <returns>A user object with the given <paramref name="externalId"/>. If an invalid string is found, null.</returns>
        private User GetOrAddUser(string externalId, string serviceName, string userName, out bool added)
        {
            //We can't return a user since there's no valid id, service name, or name
            if (string.IsNullOrEmpty(externalId) || string.IsNullOrEmpty(serviceName) || string.IsNullOrEmpty(userName) == true)
            {
                added = false;
                return null;
            }

            User user = DatabaseMngr.GetUserByExternalID(externalId);

            /* Data version < 2.7 compatibility
               If the ExternalID is empty for this user, fill it in and update the data
            */
            if (user == null)
            {
                //Use the lowered version of their name for retrieval, falling back to the legacy way of retrieving users
                string userNameLowered = userName.ToLowerInvariant();

                bool foundUserByName = false;

                using (var context = DatabaseMngr.OpenContext())
                {
                    var mutableUser = context.Users.FirstOrDefault(u => u.Name == userNameLowered);

                    //Update information only if the user doesn't yet have an external ID or a service name
                    if (mutableUser != null
                        && string.IsNullOrEmpty(mutableUser.ExternalID) == true
                        && string.IsNullOrEmpty(mutableUser.ServiceName) == true)
                    {
                        foundUserByName = true;

                        //Update information on the user
                        mutableUser.ExternalID = externalId;
                        mutableUser.ServiceName = serviceName;

                        context.SaveChanges();
                    }
                }

                //If found by name, get again by ID
                if (foundUserByName == true)
                {
                    user = DatabaseMngr.GetUserByExternalID(externalId);

                    //Update this user's abilities since they might not be up-to-date (Ex. migrated with migrator tool)
                    DatabaseMngr.UpdateUserAutoGrantAbilities(user.ExternalID);
                }
            }

            added = false;

            //If the user doesn't exist, add it
            if (user == null)
            {
                long controllerPort = 0L;

                //NOTE: Move this out of here and into the main application by sending a UserCreated event or something

                //Check which port to set if teams mode is enabled
                long teamsModeEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.TEAMS_MODE_ENABLED, 0L);
                if (teamsModeEnabled > 0L)
                {
                    long maxPort = DatabaseMngr.GetSettingInt(SettingsConstants.TEAMS_MODE_MAX_PORT, 3L);

                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        Settings teamsNextPort = context.SettingCollection
                            .FirstOrDefault(s => s.Key == SettingsConstants.TEAMS_MODE_NEXT_PORT);

                        //The player is now on this port
                        controllerPort = teamsNextPort.ValueInt;

                        //Increment the next port value, keeping it in range
                        teamsNextPort.ValueInt = Utilities.Helpers.Wrap(teamsNextPort.ValueInt + 1, 0L, maxPort + 1);

                        //Save port value changes
                        context.SaveChanges();
                    }
                }

                //Give them User permissions and set their port
                user = new User(externalId, serviceName, userName, (long)PermissionLevels.User);
                user.ControllerPort = controllerPort;

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    context.Users.Add(user);

                    //Save the changes so the user object is in the database
                    context.SaveChanges();
                }

                //Update this user's abilities off the bat
                DatabaseMngr.UpdateUserAutoGrantAbilities(user.ExternalID);

                added = true;
            }
            else
            {
                using (var context = DatabaseMngr.OpenContext())
                {
                    //Check if the user's display name has been updated on the service
                    //If so, update it in the database 
                    var mutableUser = context.Users.FirstOrDefault(u => u.ExternalID == externalId);
                    if (mutableUser != null && mutableUser.Name != userName)
                    {
                        mutableUser.Name = userName;

                        context.SaveChanges();
                    }
                }
            }

            return user;
        }
    }
}
