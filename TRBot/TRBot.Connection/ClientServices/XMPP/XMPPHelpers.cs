﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection.XMPP
{
    /// <summary>
    /// Helper methods regarding XMPP.
    /// </summary>
    public static class XMPPHelpers
    {
        /// <summary>
        /// Gets a string representing an XMPP JID.
        /// </summary>
        /// <param name="user">The user associated with the JID.</param>
        /// <param name="domain">The domain associated with the JID.</param>
        /// <param name="resource">The resource associated with the JID.</param>
        /// <returns>A string representing an XMPP JID.</returns>
        /// <remarks>
        /// An example of a full JID is "myuser@jabber.im/computer".
        /// "myuser" is the user, "jabber.im" is the domain, and "computer" is the resource.
        /// </remarks>
        public static string GetJIDString(string user, string domain, string resource)
        {
            string jidStr = $"{user}@{domain}";
            if (string.IsNullOrEmpty(resource) == false)
            {
                jidStr += $"/{resource}";
            }
            return jidStr;
        }
    }
}
