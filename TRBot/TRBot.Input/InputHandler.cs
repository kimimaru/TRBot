﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using TRBot.Input.Parsing;
using TRBot.Input.Consoles;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities.Logging;

namespace TRBot.Input
{
    /// <summary>
    /// Handles carrying out input sequences.
    /// </summary>
    public class InputHandler : IInputHandler
    {
        /// <summary>
        /// If all inputs in a subsequence have a remaining press time greater than this value,
        /// the InputHandler will wait very briefly to save on CPU time.
        /// </summary>
        private const long CPU_TIME_SAVER_INPUT_REMAINING_MS = 20;

        /// <summary>
        /// An event invoked when input handling is enabled or disabled.
        /// </summary>
        public event OnInputHandlingToggled InputHandlingToggledEvent = null;

        /// <summary>
        /// An event invoked when all inputs are halted.
        /// </summary>
        public event OnInputsHalted InputsHaltedEvent = null;

        /// <summary>
        /// Whether inputs are currently enabled.
        /// </summary>
        public bool InputsEnabled { get; private set; } = true;

        /// <summary>
        /// Whether inputs are currently halted.
        /// </summary>
        public bool InputsHalted { get; private set; } = false;

        /// <summary>
        /// The current number of running input tasks.
        /// </summary>
        public int RunningInputCount => Interlocked.CompareExchange(ref RunningInputTasks, 0, 0);

        // <summary>
        // The current number of running input tasks.
        // </summary>
        private volatile int RunningInputTasks = 0;

        private ITRBotLogger Logger = null;

        public InputHandler(ITRBotLogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Toggles input handling on the input handler. If turned off, the input handler should not process inputs.
        /// </summary>
        /// <param name="enabled">If true, enables input handling, otherwise disables it.</param>
        public void ToggleInputHandling(bool enabled)
        {
            //Don't invoke the event if the value is the same
            if (InputsEnabled == enabled)
            {
                return;
            }

            InputsEnabled = enabled;
            InputHandlingToggledEvent?.Invoke(enabled);
        }

        /// <summary>
        /// Cancels all currently running inputs.
        /// After calling this, all inputs are officially cancelled when <see cref="RunningInputCount"/> is 0.
        /// </summary>
        private void CancelRunningInputs()
        {
            InputsHalted = true;

            //Invoke the event
            InputsHaltedEvent?.Invoke();
        }

        /// <summary>
        /// Allows new inputs to be processed.
        /// </summary>
        public void ResumeRunningInputs()
        {
            //Throw an exception if we resume when inputs aren't already cancelled
            if (InputsHalted == false)
            {
                throw new Exception("Inputs are being resumed when they weren't cancelled to begin with. This can lead to corruption if there are ongoing inputs.");
            }

            InputsHalted = false;
        }

        /// <summary>
        /// Stops all ongoing inputs, waiting until all inputs are completely stopped, then keeps inputs halted.
        /// The caller is responsible for calling <see cref="ResumeRunningInputs" /> to resume inputs.
        /// </summary>
        public async Task StopAndHaltAllInputs()
        {
            CancelRunningInputs();

            await WaitAllInputsStopped();
        }

        /// <summary>
        /// Stops all ongoing inputs, waiting until all inputs are completely stopped, then resumes them.
        /// </summary>
        public async Task StopThenResumeAllInputs()
        {
            //Logger.Information("Stopping all inputs!");
            
            CancelRunningInputs();

            await WaitAllInputsStopped();

            ResumeRunningInputs();

            //Logger.Information("All inputs resumed!");
        }

        private async Task WaitAllInputsStopped()
        {
            if (RunningInputCount == 0)
            {
                return;
            }

            const int delay = 1;

            while (RunningInputCount != 0)
            {
                //Logger.Information($"Delaying {delay}ms");
                await Task.Delay(delay);
            }
        }

        public async Task CarryOutInput(List<List<ParsedInput>> inputList, IGameConsole currentConsole,
            IVirtualControllerManager vcManager)
        {
            //Don't execute inputs if they're disabled
            if (InputsEnabled == false)
            {
                Logger.Warning("Attempting to execute inputs despite them being disabled.");
                return;
            }

            //Copy the input list over to an array, which is more performant
            //and lets us bypass redundant copying and bounds checks in certain instances
            //This matters once we've begun processing inputs since we're
            //trying to reduce the delay between pressing and releasing inputs as much as we can
            ParsedInput[][] inputArray = new ParsedInput[inputList.Count][];
            for (int i = 0; i < inputArray.Length; i++)
            {
                inputArray[i] = inputList[i].ToArray();
            }

            await ExecuteInput(inputArray, currentConsole, vcManager);
        }

        private async Task ExecuteInput(ParsedInput[][] inputArray, IGameConsole curConsole,
            IVirtualControllerManager vcMngr)
        {
            /*************************************************************
            * PERFORMANCE CRITICAL CODE                                  *
            * Even the smallest change must be thoroughly tested         *
            *************************************************************/

            //Increment running task count
            Interlocked.Increment(ref RunningInputTasks);

            Stopwatch sw = new Stopwatch();

            List<int> indices = new List<int>(16);

            int controllerCount = vcMngr.ControllerCount;

            int[] nonWaits = new int[controllerCount];

            //This is used to track which controller ports were used across all inputs
            //This helps prevent updating controllers that weren't used at the end
            int[] usedControllerPorts = new int[controllerCount];

            //Don't check for overflow to improve performance
            unchecked
            {
                for (int i = 0; i < inputArray.Length; i++)
                {
                    ParsedInput[] inputs = inputArray[i];

                    indices.Clear();

                    //Press all buttons unless it's a release input
                    for (int j = 0; j < inputs.Length; j++)
                    {
                        indices.Add(j);

                        ParsedInput input = inputs[j];

                        //Don't do anything for a blank input
                        if (curConsole.IsBlankInput(input) == true)
                        {
                            continue;
                        }

                        int port = input.ControllerPort;

                        //Get the controller we're using
                        IVirtualController controller = vcMngr.GetController(port);

                        //These are set to 1 instead of incremented to prevent any chance of overflow
                        nonWaits[port] = 1;
                        usedControllerPorts[port] = 1;

                        if (input.Release == true)
                        {
                            ReleaseInput(input, curConsole, controller);
                        }
                        else
                        {
                            PressInput(input, curConsole, controller);
                        }
                    }

                    //Update the controllers if there are non-wait inputs
                    for (int waitIdx = 0; waitIdx < nonWaits.Length; waitIdx++)
                    {
                        if (nonWaits[waitIdx] > 0)
                        {
                            IVirtualController controller = vcMngr.GetController(waitIdx);

                            controller.UpdateController();
                            nonWaits[waitIdx] = 0;
                        }
                    }

                    //If this is true, we'll delay the current task to save on CPU time
                    bool shouldCPUWait = false;

                    sw.Start();

                    while (indices.Count > 0)
                    {
                        //End the input prematurely
                        if (InputsHalted == true)
                        {
                            goto End;
                        }

                        //If we should wait to save on CPU time, do so now
                        if (shouldCPUWait == true)
                        {
                            await Task.Delay(1);
                        }

                        //Default to true since we don't know how much time is remaining yet
                        shouldCPUWait = true;

                        //Release buttons when we should
                        for (int j = indices.Count - 1; j >= 0; j--)
                        {
                            ParsedInput input = inputs[indices[j]];

                            //Check how much time is remaining for this input's duration
                            long diff = input.Duration - sw.ElapsedMilliseconds;

                            //If there's still time left, continue to the next input
                            if (diff > 0)
                            {
                                //If it's getting close to finishing this input,
                                //indicate that we shouldn't wait to save on CPU time
                                //in case we hold it for too long
                                if (diff < CPU_TIME_SAVER_INPUT_REMAINING_MS)
                                {
                                    shouldCPUWait = false;
                                }
                                
                                continue;
                            }

                            //Logger.Debug($"diff for \"{input.name}{input.duration}\": {diff}");

                            //Release if the input isn't held or released and isn't a blank input
                            if (input.Hold == false && input.Release == false && curConsole.IsBlankInput(input) == false)
                            {
                                int port = input.ControllerPort;
                                
                                //Get the controller we're using
                                IVirtualController controller = vcMngr.GetController(port);

                                ReleaseInput(input, curConsole, controller);

                                //Track that we have a non-wait or hold input so we can update the controller with all input releases at once
                                nonWaits[port] = 1;

                                usedControllerPorts[port] = 1;
                            }

                            indices.RemoveAt(j);
                        }

                        //Update the controllers if there are non-wait inputs
                        for (int waitIdx = 0; waitIdx < nonWaits.Length; waitIdx++)
                        {
                            if (nonWaits[waitIdx] > 0)
                            {
                                IVirtualController controller = vcMngr.GetController(waitIdx);
                                controller.UpdateController();

                                nonWaits[waitIdx] = 0;
                            }
                        }
                    }

                    sw.Reset();
                }
            }

            //End label to skip to if we should cancel early
            End:

            unchecked
            {
                //At the end of it all, release every input
                for (int i = 0; i < inputArray.Length; i++)
                {
                    ParsedInput[] inputs = inputArray[i];
                    for (int j = 0; j < inputs.Length; j++)
                    {
                        ParsedInput input = inputs[j];

                        if (curConsole.IsBlankInput(input) == true)
                        {
                            continue;
                        }

                        //Release if it isn't a wait
                        IVirtualController controller = vcMngr.GetController(input.ControllerPort);
                        ReleaseInput(input, curConsole, controller);
                    }
                }
            
                //Update all used controllers
                for (int i = 0; i < usedControllerPorts.Length; i++)
                {
                    //A value of 0 indicates the port wasn't used
                    if (usedControllerPorts[i] == 0)
                    {
                        continue;
                    }

                    IVirtualController controller = vcMngr.GetController(i);
                    controller.UpdateController();
                }
            }

            //Decrement running tasks
            Interlocked.Decrement(ref RunningInputTasks);
        }

        #region Helper Methods

        private void PressInput(in ParsedInput input, IGameConsole curConsole, IVirtualController vController)
        {
            if (curConsole.IsBlankInput(input) == true)
            {
                return;
            }

            if (curConsole.GetAxis(input, out InputAxis axis) == true)
            {
                vController.PressAxis(axis.AxisVal, axis.MinAxisVal, axis.MaxAxisVal, input.Percent);

                //Release a button with the same name (Ex. L/R buttons on GCN)
                if (curConsole.GetButtonValue(input.Name, out InputButton btnVal) == true)
                {
                    vController.ReleaseButton(btnVal.ButtonVal);
                }
            }
            else if (curConsole.GetButtonValue(input.Name, out InputButton btnVal) == true)
            {
                vController.PressButton(btnVal.ButtonVal);

                //Release an axis with the same name (Ex. L/R buttons on GCN)
                if (curConsole.GetAxisValue(input.Name, out InputAxis value) == true)
                {
                    vController.ReleaseAxis(value.AxisVal, value.DefaultAxisVal);
                }
            }

            vController.SetInputNamePressed(input.Name);
        }

        private void ReleaseInput(in ParsedInput input, IGameConsole curConsole, IVirtualController vController)
        {
            if (curConsole.IsBlankInput(input) == true)
            {
                return;
            }

            if (curConsole.GetAxis(input, out InputAxis axis) == true)
            {
                vController.ReleaseAxis(axis.AxisVal, axis.DefaultAxisVal);
            }
            else if (curConsole.GetButtonValue(input.Name, out InputButton btnVal) == true)
            {
                vController.ReleaseButton(btnVal.ButtonVal);
            }

            vController.SetInputNameReleased(input.Name);
        }

        #endregion
    }
}
