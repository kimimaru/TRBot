﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Main
{
    /// <summary>
    /// Message queue data.
    /// </summary>
    public struct MsgQueueData
    {
        public MsgQueueTypes MsgQueueType; 
        public EventArgs EvtArgs;

        public MsgQueueData(in MsgQueueTypes msgQueueType, EventArgs evtArgs)
        {
            MsgQueueType = msgQueueType;
            EvtArgs = evtArgs;
        }

        public override bool Equals(object obj)
        {
            if (obj is MsgQueueData msgQueueData)
            {
                return (this == msgQueueData);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 41;
                hash = (hash * 31) + (int)MsgQueueType;
                hash = (hash * 31) + EvtArgs?.GetHashCode() ?? 0;
                return hash;
            }
        }

        public static bool operator ==(MsgQueueData a, MsgQueueData b)
        {
            return (a.MsgQueueType == b.MsgQueueType && a.EvtArgs == b.EvtArgs);
        }

        public static bool operator !=(MsgQueueData a, MsgQueueData b)
        {
            return !(a == b);
        }
    }
}
