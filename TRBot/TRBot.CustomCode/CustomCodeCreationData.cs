/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.CustomCode
{
    /// <summary>
    /// Contains data about custom code creation.
    /// </summary>
    public struct CustomCodeCreationData<T>
    {
        /// <summary>
        /// The newly created object.
        /// </summary>
        public T NewObject;
        
        /// <summary>
        /// Tells whether compiling the code was successful.
        /// </summary>
        public bool Success;

        /// <summary>
        /// The error message specified while compiling the code.
        /// </summary>
        public string ErrorMessage;

        public CustomCodeCreationData(T newObject, bool success, string errorMessage)
        {
            NewObject = newObject;
            Success = success;
            ErrorMessage = errorMessage;
        }
    }
}