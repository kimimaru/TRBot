/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NUnit.Framework;
using TRBot.Input.Parsing;

namespace TRBot.Tests.Input.Parsing
{
    [TestFixture]
    public class ParsedInputUnitTests
    {
        [TestCase]
        public void TestEquality()
        {
            ParsedInput a = new ParsedInput("test", false, false, 100d, 200, InputDurationTypes.Milliseconds, 0);

            ParsedInput b = new ParsedInput("test", false, false, 100d, 200, InputDurationTypes.Milliseconds, 0);

            Assert.AreEqual(a == b, true);
        }

        [TestCase(50, 50)]
        [TestCase(30.0001, 30)]
        [TestCase(90.1, 90.1)]
        [TestCase(64.15, 64.15)]
        [TestCase(45.4321, 45.4328)]
        [TestCase(76.23451332, 76.23483627)]
        public void TestPercentageEquality(double percentage1, double percentage2)
        {
            ParsedInput a = new ParsedInput("test", false, false, percentage1, 200, InputDurationTypes.Milliseconds, 0);

            ParsedInput b = new ParsedInput("test", false, false, percentage2, 200, InputDurationTypes.Milliseconds, 0);

            Assert.AreEqual(a == b, true);
        }

        [TestCase(0, 100)]
        [TestCase(97.1, 97.2)]
        [TestCase(97.54, 97.55)]
        [TestCase(46.549, 46.550)]
        [TestCase(29.549, 9297.551)]
        [TestCase(30.001, 30.002)]
        [TestCase(45.4321, 45.4528)]
        [TestCase(33.333, 33.334)]
        public void TestPercentageInequality(double percentage1, double percentage2)
        {
            ParsedInput a = new ParsedInput("test", false, false, percentage1, 200, InputDurationTypes.Milliseconds, 0);

            ParsedInput b = new ParsedInput("test", false, false, percentage2, 200, InputDurationTypes.Milliseconds, 0);

            Assert.AreEqual(a == b, false);
        }
    }
}