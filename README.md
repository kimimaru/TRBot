<div style="text-align: center;">

![TRBot Logo](./Logo/TRBotLogo.png)

<a href="https://liberapay.com/kimimaru"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>
<a href='https://ko-fi.com/X8X23SDSI' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

## Play video games remotely and collaboratively through text

</div>

## What is TRBot?

TRBot lets you play your favorite games collaboratively by typing inputs to control them.

![Jumping into a painting in Super Mario 64 with TRBot](./Wiki/Images/TRBotIntroduction.gif)

Simply put, if you type "a", the character in your game can jump. To enable collaborative play, TRBot reads inputs from online chat platforms, such as Twitch, IRC, XMPP, Matrix, and more. With TRBot, it's possible for hundreds of people to play the same game at the same time.

TRBot has a flexible "easy to learn, hard to master" format. Play as simple as you want then go for the gold with insane plays. Since its inception, TRBot has been used to play numerous games, and it has even helped [discover brand new glitches in classic games](https://twitter.com/MGSrunners/status/1431503754256388098)!

Want to see more examples? [Take a look!](./Wiki/Play-Examples.md)

## [**Download the latest release**](https://codeberg.org/kimimaru/TRBot/releases)

## Uses
- Play all types of games with friends or a community
- Play your favorite games in a new way
- Automation - create macros to grind in an RPG or mash buttons in a minigame
- Accessibility - this quote by one user explains:
  ```
  I use it in two ways.
  1: To stream some twitch plays while I chat with chat. This often coincides with reason for
  2: I have chronic pain and can not play for long without breaks and am always in danger of damaging myself. This program let's chat or my friends play for me :D
  ```
- And much more - feel free to share how you use TRBot!

### For hosts
- Portable - just [download and run](https://codeberg.org/kimimaru/TRBot/releases)! TRBot has great compatibility with most titles since it runs alongside the game.
- Accept messages through Twitch, Matrix, and other platforms like Discord [at the same time](./Wiki/Setup-Init.md)!
- Flexible - change inputs and buttons on the fly or [add your own commands](./Wiki/Custom-Commands.md) to extend TRBot's capabilities.
- Moderation - control access to commands and inputs, and silence trolls independent of platform.
- Sleep and reset prevention - Press a button automatically at regular intervals and restrict button combinations.
- Emulator support - in-built consoles for SNES, PS2, GCN, and more. Several [emulator controller config files](./Controller%20Configs) are available.

### For players
- [Simple yet expressive input format](./Wiki/Syntax-Walkthrough.md). Easy to learn, hard to master.
- Extreme accuracy - play with precision and consistency.
- Multiplayer support - Control [multiple players](./Wiki/Syntax-Walkthrough.md#multi-controller-inputs) separately or simultaneously.

## Streamers running TRBot
- [Type2Play](https://www.twitch.tv/type2play) (stream by the developers of TRBot)
- [TwitchPlaysSpeedruns](https://www.twitch.tv/twitchplaysspeedruns)
- [Nickadabra](https://www.twitch.tv/nickaabra)
- [TheChatPlaysGames](https://www.twitch.tv/thechatplaysgames)
- [BraceYourselfGames](https://www.twitch.tv/braceyourselfgames/) (the developers behind Crypt of the NecroDancer!)
- [f1ashko](https://www.twitch.tv/f1ashko)

## Documentation, setup, development
[![CI Status Develop](https://ci.codeberg.org/api/badges/kimimaru/TRBot/status.svg?branch=develop)](https://ci.codeberg.org/kimimaru/TRBot/branches/develop)

- See the [FAQ](./Wiki/FAQ.md) for answers to commonly asked questions
- [Setup Guide](./Wiki/Setup-Init.md)
- [Building from source](./Wiki/Building.md)
- See the [wiki home](./Wiki/Home.md) for more resources

## Miscellaneous
- [Build applications around TRBot](./Wiki/Event-Dispatcher.md)
- [LiveSplitOne](./Wiki/LiveSplitOne-Integration.md) and [ChatterBot integration](./Wiki/Setup-Chatterbot.md)
- [Free software](https://www.gnu.org/philosophy/free-sw.html) - study, share, and modify TRBot to your heart's content!

## Inspiration
TRBot was created out of the joy of playing games collaboratively through text. Playing through text comes with its own challenges, yet it can be a very rewarding experience. For example, real-time combat in games may be nothing with a controller, but doing it through text is surprisingly difficult! Overcoming unique game challenges together is a core reason behind TRBot's existence.

TRBot was also inspired by the bot used on the TwitchPlays_Everything Twitch channel. The input syntax allows for great precision, making it well-suited for many types of games, whether it's an RPG or a 3D platformer.

## Support
Feel free to ask questions or discuss development on our Matrix room at [#TRBot-Dev:matrix.org](https://matrix.to/#/!hTfcbsKMAuenQAetQm:matrix.org?via=matrix.org). You can also [contact us](mailto:trbotatposteodotde) for support.

Developing software takes considerable time and effort, and we have poured hundreds of hours of our spare time into making TRBot as powerful and feature-rich as it is and freely available to everyone, in both price and freedom. Kindly consider [buying us a coffee](https://ko-fi.com/kimimaru) or [donating](https://liberapay.com/kimimaru/).

## Credits
The original parser was written in Python by Jdog of TwitchPlays_Everything, which greatly helped jump-start TRBot's development.

TRBot's logo was designed by the talented [David Revoy](https://www.davidrevoy.com/), well-known for his Pepper & Carrot comic series.

## License
Copyright © 2019-2023 Thomas "Kimimaru" Deeb

[![AGPL](https://www.gnu.org/graphics/agplv3-155x51.png)](https://www.gnu.org/licenses/agpl-3.0.en.html)

TRBot is free software; you are free to run, study, modify, and redistribute it. Specifically, you can modify and/or redistribute TRBot under the terms of the GNU Affero General Public License v3.0.

In simple terms, if you give someone a copy of TRBot or deploy TRBot to an online service, you must provide a way to obtain the license and source code for that version of TRBot upon request. This includes versions of TRBot that you or someone else modified.

See the [LICENSE](./LICENSE) file for the full terms. See the [Dependency Licenses](./Dependency%20Licenses) file for the licenses of third party libraries used by TRBot. See the [logo license](./Logo/Logo%20License) file for the license of TRBot's logo. Finally, see the [documentation license](./Wiki/Documentation%20License) file for the license of TRBot's documentation.

<a href="https://endsoftwarepatents.org/innovating-without-patents"><img src="https://static.fsf.org/nosvn/esp/logos/innovating-without-patents.svg" width="120" height="120"></a>

## Attribution Banner
If you have found TRBot useful, please spread the word by placing one of our promo banners on your website, blog, video, or stream panel!

[Link to large banner](./Logo/TRBotLogo_Promo.png)
- Markdown code:
    ```
    ![Powered by TRBot](https://codeberg.org/kimimaru/TRBot/raw/branch/master/Logo/TRBotLogo_Promo.png "TRBot Logo")
    ```

[Link to small banner](./Logo/TRBotLogo_Promo_Small.png)
- Markdown code: 
    ```
    ![Powered by TRBot](https://codeberg.org/kimimaru/TRBot/raw/branch/master/Logo/TRBotLogo_Promo_Small.png "TRBot Logo")
    ```

## Contributing
Our main repository is on Codeberg: https://codeberg.org/kimimaru/TRBot.git

Issues and pull requests are highly encouraged. Please file an issue if you encounter any bugs or have a feature request.
