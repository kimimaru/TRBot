﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Data
{
    /// <summary>
    /// Data regarding connected services.
    /// </summary>
    public class ServiceData
    {
        /// <summary>
        /// The service's ID.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The name of the service.
        /// </summary>
        public string ServiceName { get; set; } = string.Empty;

        /// <summary>
        /// Whether the service is enabled or not.
        /// </summary>
        public long Enabled { get; set; } = 1;

        /// <summary>
        /// Whether the service should log messages it sends through its logger.
        /// </summary>
        public long LogToLogger { get; set; } = 1;

        /// <summary>
        /// The file to load for the configuration of this service.
        /// </summary>
        public string ConfigFile { get; set; } = string.Empty;

        public bool IsEnabled => (Enabled == 1L);
        public bool IsLoggingToLogger => (LogToLogger == 1L);

        public ServiceData()
        {

        }

        public ServiceData(string serviceName, in bool serviceEnabled, in bool logToLogger, string configFile)
        {
            ServiceName = serviceName;
            Enabled = serviceEnabled == true ? 1L : 0L;
            LogToLogger = logToLogger == true ? 1L : 0L;
            ConfigFile = configFile;
        }
    }
}
