# Custom Command and Routine changes in TRBot 2.7

TRBot 2.7 introduces changes to user data, including how users are retrieved from the database, and how command arguments are parsed. 

Here's what you need to know about migrating custom commands and routines from versions prior to 2.7 to 2.7:

- `DataHelper.GetUser` and `DataHelper.GetUserNoOpen` have been removed and replaced with `DataHelper.GetUserByExternalID` and `DataHelper.GetUserByExternalIDNoOpen`.
  - To get a user's ExternalID if it's not immediately available (Ex. contained in an event parameter), call `DataHelper.GetUserExternalIDFromNameAndService`.
  - The move to ExternalIDs also makes it more difficult to get a user on a different service than the one a message is sent from. This may be improved in the future.
- Command arguments enclosed in quotes are now regarded as a single argument. For example, `!test "one two" three` consists of two arguments: `one two` and `three`.
  - This way of parsing command arguments is now consistent across all services, including Twitch, which no longer uses TwitchLib's command handler.
