/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.IO;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Input.Consoles;
using TRBot.Input.VirtualControllers;
using TRBot.Permissions;
using TRBot.Utilities;
using static TRBot.Data.SettingsConstants;
using static TRBot.Permissions.PermissionConstants;

namespace TRBot.Main
{
    public class DefaultDataProvider : IDefaultDataProvider
    {
        public DefaultDataProvider()
        {

        }

        #region User Data

        public UserDataConsentOptions DefaultUserDataConsentOptions =>
            UserDataConsentOptions.Stats
            | UserDataConsentOptions.Memes
            | UserDataConsentOptions.RecentInputs
            | UserDataConsentOptions.Tutorial;

        #endregion

        #region Settings

        /// <summary>
        /// Returns a list of Settings objects containing recommended default values.
        /// </summary>
        /// <returns>A list of Settings objects with their default values.</returns>
        public List<Settings> GetDefaultSettings()
        {
            List<Settings> defaultSettings = new List<Settings>()
            {
                SettingsHelper(MAIN_THREAD_SLEEP, 100L),
                SettingsHelper(CREDITS_NAME, "Credits"),
                SettingsHelper(CREDITS_GIVE_TIME, 120000L),
                SettingsHelper(CREDITS_GIVE_AMOUNT, 100L),
                SettingsHelper(DUEL_TIMEOUT, 60000L),
                SettingsHelper(GROUP_BET_TOTAL_TIME, 120000L),
                SettingsHelper(GROUP_BET_MIN_PARTICIPANTS, 3L),
                SettingsHelper(COMMAND_IDENTIFIER, "!"),
                SettingsHelper(CHATBOT_ENABLED, false),
                SettingsHelper(CHATBOT_SOCKET_HOSTNAME, "127.0.0.1"),
                SettingsHelper(CHATBOT_SOCKET_PORT, 7444),
                SettingsHelper(LOG_LEVEL, (long)Serilog.Events.LogEventLevel.Information),
                SettingsHelper(AUTO_PROMOTE_ENABLED, true),
                SettingsHelper(AUTO_PROMOTE_LEVEL, (long)PermissionLevels.Whitelisted),
                SettingsHelper(AUTO_PROMOTE_INPUT_REQ, 50L),
                SettingsHelper(BOT_MESSAGES_ENABLED, true),
                SettingsHelper(BOT_MSG_CHAR_LIMIT, 500L),
                SettingsHelper(PERIODIC_MSG_TIME, 1800000L),
                SettingsHelper(PERIODIC_MSG_PREREQ_MSG_COUNT, 5L),
                SettingsHelper(MESSAGE_THROTTLE_TYPE, (long)MessageThrottlingOptions.MsgCountPerInterval),
                SettingsHelper(MESSAGE_COOLDOWN, 30000L),
                SettingsHelper(MESSAGE_THROTTLE_COUNT, 20L),
                SettingsHelper(MESSAGE_PREFIX, string.Empty),
                SettingsHelper(RECONNECT_TIME, 10000L),
                SettingsHelper(CONNECT_MESSAGE, "Your friendly gameplay bot has connected :D ! Use \"!inputs\" for all buttons, \"!tutorial\" or \"!syntax\" to learn how to play, and \"!help\" for a list of bot commands!"),
                SettingsHelper(RECONNECTED_MESSAGE, "Successfully reconnected to chat!"),
                SettingsHelper(PERIODIC_MESSAGE, "Hi! I'm your friendly gameplay bot :D ! Use \"!inputs\" for all buttons, \"!tutorial\" or \"!syntax\" for how to play, and \"!help\" for a list of bot commands!"),
                SettingsHelper(AUTOPROMOTE_MESSAGE, "{0} has been promoted to {1}! New commands and permissions are available!"),
                SettingsHelper(NEW_USER_MESSAGE, "Welcome to the stream, {0} :D ! We hope you enjoy your stay!"),
                SettingsHelper(BEING_HOSTED_MESSAGE, "Thank you for hosting, {0}!! You rock!"),
                SettingsHelper(NEW_SUBSCRIBER_MESSAGE, "Thank you for subscribing, {0} :D !!"),
                SettingsHelper(RESUBSCRIBER_MESSAGE, "Thank you for subscribing for {1} months, {0} :D !!"),
                SettingsHelper(SOURCE_CODE_MESSAGE, $"This bot is free software licensed under the AGPL v3.0. The code repository and full license terms are at {Build.BuildInfo.GitRepositoryURL.Replace(".git", string.Empty)} - You have the right to obtain source code for the host's deployed version of the software."),
                SettingsHelper(PERIODIC_MESSAGE_ROTATION, PERIODIC_MESSAGE),
                SettingsHelper(RANK_UP_MESSAGE, "{0} has progressed to rank {1}, {2}!"),
                SettingsHelper(GAME_MESSAGE_PATH, Path.Combine(DataConstants.DATA_FOLDER_NAME, "GameMessage.txt")),
                SettingsHelper(GAME_MESSAGE_PATH_IS_RELATIVE, true),
                SettingsHelper(INFO_MESSAGE, "Welcome to the channel! You can play games by submitting messages in chat. Type !inputs to see all available buttons."),
                SettingsHelper(TUTORIAL_MESSAGE, "Hey {0}, read this brief visual guide to learn how to play: https://codeberg.org/kimimaru/TRBot/src/branch/master/Wiki/Syntax-Walkthrough.md"),
                SettingsHelper(DOCUMENTATION_MESSAGE, "Hi {0}, here's documentation on TRBot: https://codeberg.org/kimimaru/TRBot/src/branch/master/Wiki/Home.md"),
                SettingsHelper(DONATE_MESSAGE, "You can further support TRBot's development by donating to the developer. All donations go towards improving TRBot: https://liberapay.com/kimimaru/"),

                SettingsHelper(SYNTAX_REFERENCE_INPUTS_MESSAGE, "Control the game by typing inputs into the chat. See all inputs with \"!inputs\". Perform inputs one after another by typing them sequentially. Ex. \"up right\" will first press 'up' then 'right'."),
                SettingsHelper(SYNTAX_REFERENCE_DURATION_MESSAGE, "Hold inputs for a certain amount of time with \"ms\" or \"s\" modifiers at the end. \"up500ms\" holds 'up' for 0.5 seconds and \"up1s\" holds it for 1 second. \"up1.573s\" is also valid."),
                SettingsHelper(SYNTAX_REFERENCE_SIMULTANEOUS_MESSAGE, "Press inputs at the *same time* by appending \"+\" in between them. \"up+right\" presses both 'up' and 'right' at the exact same time."),
                SettingsHelper(SYNTAX_REFERENCE_DELAY_MESSAGE, "Delay inputs by a certain amount of time using a *blank* input, which is either \"#\" or \".\". Ex. \"up300ms #200ms right\" waits 200 milliseconds before pressing 'right'."),
                SettingsHelper(SYNTAX_REFERENCE_HOLD_MESSAGE, "Add \"_\" before an input to keep it held for the entire sequence. Ex. \"_up #300ms right\" holds 'up' until 'right' is pressed after 300 milliseconds."),
                SettingsHelper(SYNTAX_REFERENCE_RELEASE_MESSAGE, "Release inputs early by adding \"-\" before the input. \"_up #300ms -up right\" will let go of 'up' before pressing 'right'."),
                SettingsHelper(SYNTAX_REFERENCE_REPEATED_MESSAGE, "Repeat inputs by wrapping them in \"[]\" and adding \"*#\" at the end to specify how many times to repeat. \"[up300ms #34ms]*10\" presses 'up' for 300ms and waits 34ms ten times."),
                SettingsHelper(SYNTAX_REFERENCE_MULTIPLAYER_MESSAGE, "Specify a controller port by adding \"&#\" before the input, where \"#\" is the controller number. \"&2_up\" holds 'up' on controller 2 and \"&1-left+&2a\" releases 'left' on controller 1 then presses 'a' on controller 2 at the same time."),
                SettingsHelper(SYNTAX_REFERENCE_MACROS_MESSAGE, "Use \"!macros\" to list macros, which are sequences of inputs that can be saved. Use them by typing them in chat, view them with \"!show\", and add a new one with \"!addmacro\"."),
                SettingsHelper(SYNTAX_REFERENCE_DYNAMIC_MACROS_MESSAGE, "*Dynamic* macros take the form of \"#macroname(*,*)\". Substitute \"*\" with inputs to use the macro with those inputs. Ex. \"#mash(b)\" uses 'b' and \"#mash(y)\" uses 'y'. When creating dynamic macros, specify what to replace in each slot with \"<0>\", \"<1>\", and so on (Ex. \"<0>500ms <1>+down\")."),
                SettingsHelper(SYNTAX_REFERENCE_COMMENTS_MESSAGE, "Anything enclosed in \"//\" is not included in the input sequence and serves as a comment. \"down450ms //hello// up b\" ignores the \"hello\" and holds 'down' for 450ms then presses 'up' then 'b'."),

                SettingsHelper(PERIODIC_INPUT_ENABLED, false),
                SettingsHelper(PERIODIC_INPUT_TIME, 1000 * 60 * 5),
                SettingsHelper(PERIODIC_INPUT_PORT, 0L),
                SettingsHelper(PERIODIC_INPUT_VALUE, string.Empty),
                SettingsHelper(PERIODIC_INPUT_MESSAGE_ENABLED, true),
                SettingsHelper(TEAMS_MODE_ENABLED, false),
                SettingsHelper(TEAMS_MODE_MAX_PORT, 3L),
                SettingsHelper(TEAMS_MODE_NEXT_PORT, 0L),
                SettingsHelper(DEFAULT_INPUT_DURATION, 200L),
                SettingsHelper(MAX_INPUT_DURATION, 60000L),
                SettingsHelper(INPUTS_ENABLED, true),
                SettingsHelper(GLOBAL_MID_INPUT_DELAY_ENABLED, false),
                SettingsHelper(GLOBAL_MID_INPUT_DELAY_TIME, 34L),
                SettingsHelper(MAX_USER_RECENT_INPUTS, 5L),
                SettingsHelper(MAX_USER_SIMULATE_STRING_LENGTH, 30000L),
                SettingsHelper(USER_SIMULATE_CREDIT_COST, 1000L),
                SettingsHelper(WEBSOCKET_SERVER_ENABLED, false),
                SettingsHelper(WEBSOCKET_SERVER_ADDRESS, "ws://127.0.0.1:4350"),
                SettingsHelper(EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED, false),
                SettingsHelper(EVENT_DISPATCHER_WEBSOCKET_PATH, "/evt"),
                SettingsHelper(CUSTOM_MESSAGES_ENABLED, 1L),
                SettingsHelper(CUSTOM_MESSAGE_SEND_TYPE, (long)CustomMessageSendTypes.Room),
                SettingsHelper(DEMOCRACY_VOTE_TIME, 10000L),
                SettingsHelper(DEMOCRACY_RESOLUTION_MODE, (long)DemocracyResolutionModes.ExactSequence),
                SettingsHelper(INPUT_MODE_VOTE_TIME, 60000L),
                SettingsHelper(INPUT_MODE_CHANGE_COOLDOWN, 1000L * 60L * 15L),
                SettingsHelper(INPUT_MODE_NEXT_VOTE_DATE, DateTime.UnixEpoch.ToConsistentString()),
                SettingsHelper(LAST_CONSOLE, 1L),
                SettingsHelper(LAST_VCONTROLLER_TYPE, (long)VirtualControllerHelpers.GetDefaultVControllerTypeForPlatform(TRBotOSPlatform.CurrentOS)),
                SettingsHelper(JOYSTICK_COUNT, 1L),
                SettingsHelper(GLOBAL_INPUT_LEVEL, (long)PermissionLevels.User),
                SettingsHelper(INPUT_MODE, (long)InputModes.Anarchy),
            };

            return defaultSettings;
        }

        private static Settings SettingsHelper(string key, string value_str)
        {
            return new Settings(key, value_str, 0L);
        }

        private static Settings SettingsHelper(string key, in long value_int)
        {
            return new Settings(key, string.Empty, value_int);
        }

        private static Settings SettingsHelper(string key, in bool value)
        {
            return new Settings(key, string.Empty, value == true ? 1L : 0L);
        }

        #endregion

        #region Command Data

        /// <summary>
        /// Returns a list of CommandData objects containing recommended default values.
        /// </summary>
        /// <returns>A list of CommandData objects with their default values.</returns>
        public List<CommandData> GetDefaultCommands()
        {
            long userPerm = (long)PermissionLevels.User;
            long whitelistedPerm = (long)PermissionLevels.Whitelisted;
            long vipPerm = (long)PermissionLevels.VIP;
            long modPerm = (long)PermissionLevels.Moderator;
            long adminPerm = (long)PermissionLevels.Admin;
            long superAdminPerm = (long)PermissionLevels.Superadmin;

            string cmdRootNs = "TRBot.Commands.";

            List<CommandData> defaultCommands = new List<CommandData>()
            {
                new CommandData("sourcecode", cmdRootNs + "MessageCommand", userPerm, true, true, CommandCategories.User, SettingsConstants.SOURCE_CODE_MESSAGE),
                new CommandData("info", cmdRootNs + "MessageCommand", userPerm, true, true, CommandCategories.User, SettingsConstants.INFO_MESSAGE),
                new CommandData("tutorial", cmdRootNs + "MessageCommand", userPerm, true, true, CommandCategories.User, SettingsConstants.TUTORIAL_MESSAGE),
                new CommandData("syntax", cmdRootNs + "SyntaxReferenceCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("documentation", cmdRootNs + "MessageCommand", userPerm, true, true, CommandCategories.User, SettingsConstants.DOCUMENTATION_MESSAGE),
                new CommandData("donate", cmdRootNs + "MessageCommand", userPerm, true, true, CommandCategories.User, SettingsConstants.DONATE_MESSAGE),
                new CommandData("version", cmdRootNs + "VersionCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("console", cmdRootNs + "GetSetConsoleCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("inputs", cmdRootNs + "InputInfoCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("macros", cmdRootNs + "ListMacrosCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("addmacro", cmdRootNs + "AddMacroCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("removemacro", cmdRootNs + "RemoveMacroCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("showmacro", cmdRootNs + "ShowMacroCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("memes", cmdRootNs + "ListMemesCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("addmeme", cmdRootNs + "AddMemeCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("removememe", cmdRootNs + "RemoveMemeCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("viewlog", cmdRootNs + "ViewGameLogCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("stopall", cmdRootNs + "StopAllInputsCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("allabilities", cmdRootNs + "ListPermissionAbilitiesCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("userabilities", cmdRootNs + "ListUserAbilitiesCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("updateabilities", cmdRootNs + "UpdateAllUserAbilitiesCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("level", cmdRootNs + "LevelCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("runninginputs", cmdRootNs + "NumRunningInputsCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("pressedinputs", cmdRootNs + "ListPressedInputsCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("vcontroller", cmdRootNs + "VirtualControllerCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("userinfo", cmdRootNs + "UserInfoCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("help", cmdRootNs + "ListCmdsCommand", userPerm, true, true, CommandCategories.User, "User,Input"),
                new CommandData("adminhelp", cmdRootNs + "ListCmdsCommand", userPerm, true, true, CommandCategories.User, "Admin"),
                new CommandData("gamelist", cmdRootNs + "ListCmdsCommand", userPerm, true, true, CommandCategories.User, "Games"),
                new CommandData("routines", cmdRootNs + "ListRoutinesCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("controllercount", cmdRootNs + "ControllerCountCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("credits", cmdRootNs + "CreditsCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("creditlead", cmdRootNs + "UserStatLeaderboardCommand", userPerm, true, true, CommandCategories.Games, nameof(UserStats.Credits)),
                new CommandData("inputlead", cmdRootNs + "UserStatLeaderboardCommand", userPerm, true, true, CommandCategories.Games, nameof(UserStats.ValidInputCount)),
                new CommandData("messagelead", cmdRootNs + "UserStatLeaderboardCommand", userPerm, true, true, CommandCategories.Games, nameof(UserStats.TotalMessageCount)),
                new CommandData("uptime", cmdRootNs + "UptimeCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("chat", cmdRootNs + "ChatbotCommand", userPerm, true, false, CommandCategories.Games),
                new CommandData("port", cmdRootNs + "ControllerPortCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("length", cmdRootNs + "InputLengthCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("clearstats", cmdRootNs + "ClearUserStatsCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("calculate", cmdRootNs + "CalculateCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("listresinputs", cmdRootNs + "ListRestrictedInputsCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("bet", cmdRootNs + "BetCreditsCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("transfer", cmdRootNs + "TransferCreditsCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("groupbet", cmdRootNs + "EnterGroupBetCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("exitgroupbet", cmdRootNs + "LeaveGroupBetCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("defaultinputdur", cmdRootNs + "DefaultInputDurCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("maxinputdur", cmdRootNs + "MaxInputDurCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("numlogs", cmdRootNs + "NumGameLogsCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("reverseparse", cmdRootNs + "ReverseParseInputCommand", userPerm, true, true, CommandCategories.Input),
                new CommandData("cmdinfo", cmdRootNs + "CmdInfoCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("routineinfo", cmdRootNs + "RoutineInfoCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("duel", cmdRootNs + "DuelCommand", userPerm, true, true, CommandCategories.Games),
                //By default, exclude the common savestate inputs from input exercises
                new CommandData("exercise", cmdRootNs + "InputExerciseCommand", userPerm, true, true, CommandCategories.Games, "ss,incs,decs,ss1,ss2,ss3,ss4,ss5,ss6,ls1,ls2,ls3,ls4,ls5,ls6"),
                new CommandData("inputperms", cmdRootNs + "GlobalInputPermissionsCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("midinputdelay", cmdRootNs + "MidInputDelayCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("listsyn", cmdRootNs + "ListInputSynonymsCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("teamsmode", cmdRootNs + "GetSetTeamsModeCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("teamsmaxport", cmdRootNs + "GetSetTeamsModeMaxPortCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("periodicinput", cmdRootNs + "TogglePeriodicInputCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("periodicinputport", cmdRootNs + "GetSetPeriodicInputPortCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("periodicinputseq", cmdRootNs + "GetSetPeriodicInputSequenceCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("periodicinputtime", cmdRootNs + "GetSetPeriodicInputTimeCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("periodicinputmsg", cmdRootNs + "TogglePeriodicInputMessageCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("invalidcombo", cmdRootNs + "ListInvalidInputComboCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("recentinput", cmdRootNs + "ListUserRecentInputsCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("recentinputcount", cmdRootNs + "GetSetMaxUserRecentInputsCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("inputmode", cmdRootNs + "GetSetInputModeCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("dresmode", cmdRootNs + "GetSetDemocracyResModeCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("dvotetime", cmdRootNs + "GetSetDemocracyVoteTimeCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("vote", cmdRootNs + "VoteForInputModeCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("votetime", cmdRootNs + "GetSetInputModeVoteTimeCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("votecooldown", cmdRootNs + "GetSetInputModeCooldownCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("userdefaultinputdur", cmdRootNs + "UserDefaultInputDurCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("usermaxinputdur", cmdRootNs + "UserMaxInputDurCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("listsilenced", cmdRootNs + "ListSilencedUsersCommand", userPerm, true, true, CommandCategories.Admin),
                new CommandData("simulate", cmdRootNs + "UserSimulateCommand", userPerm, true, true, CommandCategories.Games),
                new CommandData("simulatemng", cmdRootNs + "UserSimulateManageCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("custommessagetype", cmdRootNs + "GetSetCustomMessageSendTypeCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("managedata", cmdRootNs + "UserManageDataConsentCommand", userPerm, true, true, CommandCategories.User),
                new CommandData("listbuttons", cmdRootNs + "ListInputsCommand", userPerm, true, true, CommandCategories.Admin, InputTypes.Button.ToString()),
                new CommandData("listaxes", cmdRootNs + "ListInputsCommand", userPerm, true, true, CommandCategories.Admin, InputTypes.Axis.ToString()),

                new CommandData("addlog", cmdRootNs + "AddGameLogCommand", whitelistedPerm, true, true, CommandCategories.User),
                new CommandData("addsyn", cmdRootNs + "AddInputSynonymCommand", whitelistedPerm, true, true, CommandCategories.User),
                new CommandData("removesyn", cmdRootNs + "RemoveInputSynonymCommand", whitelistedPerm, true, true, CommandCategories.User),
                new CommandData("viewmultilogs", cmdRootNs + "ViewMultipleGameLogsCommand", whitelistedPerm, true, true, CommandCategories.User),

                new CommandData("setmessage", cmdRootNs + "SetGameMessageCommand", vipPerm, true, true, CommandCategories.User, SettingsConstants.GAME_MESSAGE_PATH),

                new CommandData("reload", cmdRootNs + "ReloadCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("setlevel", cmdRootNs + "SetUserLevelCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("toggleability", cmdRootNs + "UpdateUserAbilityCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("restrictinput", cmdRootNs + "AddRestrictedInputCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("unrestrictinput", cmdRootNs + "RemoveRestrictedInputCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("addinvalidcombo", cmdRootNs + "AddInvalidInputComboCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("removeinvalidcombo", cmdRootNs + "RemoveInvalidInputComboCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("invalidcombolevel", cmdRootNs + "GetSetInvalidInputComboLevelCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("userabilitylvloverride", cmdRootNs + "GetSetUserAbilityLvlOverrideCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("silence", cmdRootNs + "SilenceUserCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("unsilence", cmdRootNs + "UnsilenceUserCommand", modPerm, true, true, CommandCategories.Admin),
                new CommandData("togglecustommessages", cmdRootNs + "ToggleCustomMessagesCommand", modPerm, true, true, CommandCategories.Admin),

                new CommandData("addcmd", cmdRootNs + "AddCmdCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("removecmd", cmdRootNs + "RemoveCmdCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("togglecmd", cmdRootNs + "ToggleCmdCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("cmdcategory", cmdRootNs + "GetSetCmdCategoryCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("addroutine", cmdRootNs + "AddRoutineCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("removeroutine", cmdRootNs + "RemoveRoutineCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("toggleroutine", cmdRootNs + "ToggleRoutineCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("addconsole", cmdRootNs + "AddConsoleCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("removeconsole", cmdRootNs + "RemoveConsoleCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("addblank", cmdRootNs + "AddBlankInputCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("addbutton", cmdRootNs + "AddButtonInputCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("addaxis", cmdRootNs + "AddAxisInputCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("removeinput", cmdRootNs + "RemoveInputCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("setinputlevel", cmdRootNs + "SetInputLevelCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("toggleinput", cmdRootNs + "SetInputEnabledCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("updateeveryoneabilities", cmdRootNs + "UpdateEveryoneAbilitiesCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("getdbsetting", cmdRootNs + "GetDBSettingCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("setdbsetting", cmdRootNs + "SetDBSettingCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("removedbsetting", cmdRootNs + "RemoveDBSettingCommand", adminPerm, true, true, CommandCategories.Admin),
                new CommandData("mockuser", cmdRootNs + "MockUserCommand", adminPerm, true, true, CommandCategories.Admin),

                new CommandData("exec", cmdRootNs + "ExecCommand", superAdminPerm, false, true, CommandCategories.Admin),
                new CommandData("execcmdstxtorfile", cmdRootNs + "ExecMultiCommandsFromTextOrFileCommand", superAdminPerm, true, true, CommandCategories.Admin),
                new CommandData("exportbotdata", cmdRootNs + "ExportBotDataCommand", superAdminPerm, true, true, CommandCategories.Admin),
                new CommandData("exportdataset", cmdRootNs + "ExportDatasetToTextCommand", superAdminPerm, true, true, CommandCategories.Admin),
                new CommandData("cleardataset", cmdRootNs + "ClearDatasetCommand", superAdminPerm, false, true, CommandCategories.Admin),
                new CommandData("forceinitdefaults", cmdRootNs + "ForceInitDataCommand", superAdminPerm, true, true, CommandCategories.Admin),
            };

            return defaultCommands;
        }

        #endregion

        #region Game Consoles

        /// <summary>
        /// Returns a list of default consoles.
        /// </summary>
        /// <returns>A list of GameConsoles.</returns>
        public List<GameConsole> GetDefaultConsoles()
        {
            List<GameConsole> defaultConsoles = new List<GameConsole>()
            {
                new NESConsole(),
                new SNESConsole(),
                new GenesisConsole(),
                new N64Console(), new GBCConsole(),
                new PS2Console(), new GCConsole(), new GBAConsole(), new DCConsole(),
                new WiiConsole(), new Xbox360Console(),
                new PS4Console(),
                new SwitchConsole(),
                new PCConsole(),
            };

            return defaultConsoles;
        }

        #endregion

        #region Permission Abilities

        /// <summary>
        /// Returns a list of default permission abilities.
        /// </summary>
        /// <returns>A list of PermissionAbilities.</returns>
        public List<PermissionAbility> GetDefaultPermAbilities()
        {
            List<PermissionAbility> defaultPermissions = new List<PermissionAbility>()
            {
                new PermissionAbility(BET_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(DUEL_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(GROUP_BET_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(INPUT_EXERCISE_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(CALCULATE_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(CHATBOT_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(TRANSFER_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(VOTE_INPUT_MODE_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(ADD_INPUT_MACRO_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(REMOVE_INPUT_MACRO_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(ADD_MEME_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(REMOVE_MEME_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(ADD_INPUT_SYNONYM_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(REMOVE_INPUT_SYNONYM_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(STOP_ALL_INPUTS_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),
                new PermissionAbility(SIMULATE_ABILITY, PermissionLevels.User, PermissionLevels.Moderator),

                new PermissionAbility(SET_GAME_MESSAGE_ABILITY, PermissionLevels.VIP, PermissionLevels.VIP),

                new PermissionAbility(SET_CONSOLE_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_DEFAULT_INPUT_DUR_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_MAX_INPUT_DUR_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_MID_INPUT_DELAY_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_TEAMS_MODE_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_TEAMS_MODE_MAX_PORT_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_PERIODIC_INPUT_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_PERIODIC_INPUT_PORT_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_PERIODIC_INPUT_TIME_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_PERIODIC_INPUT_SEQUENCE_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_PERIODIC_INPUT_MESSAGE_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(SET_MAX_USER_RECENT_INPUTS_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),
                new PermissionAbility(START_VOTE_INPUT_MODE_ABILITY, PermissionLevels.Moderator, PermissionLevels.Moderator),

                new PermissionAbility(UPDATE_OTHER_USER_ABILITES, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_GLOBAL_INPUT_LEVEL_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_VCONTROLLER_TYPE_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_VCONTROLLER_COUNT_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_DEMOCRACY_VOTE_TIME_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_DEMOCRACY_RESOLUTION_MODE_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_INPUT_MODE_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_INPUT_MODE_VOTE_TIME_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_INPUT_MODE_CHANGE_COOLDOWN_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),
                new PermissionAbility(SET_CUSTOM_MESSAGE_SEND_TYPE_ABILITY, PermissionLevels.Admin, PermissionLevels.Admin),

                PermissionAbility.CreateWithMinLvlGrant(SILENCED_ABILITY, PermissionLevels.Moderator),
                PermissionAbility.CreateWithMinLvlGrant(USER_DEFAULT_INPUT_DUR_ABILITY, PermissionLevels.Moderator),
                PermissionAbility.CreateWithMinLvlGrant(USER_MAX_INPUT_DUR_ABILITY, PermissionLevels.Moderator),
                PermissionAbility.CreateWithMinLvlGrant(USER_MID_INPUT_DELAY_ABILITY, PermissionLevels.User),
            };

            return defaultPermissions;
        }

        #endregion

        #region Routine Data

        /// <summary>
        /// Returns a list of RoutineData objects containing recommended default values.
        /// </summary>
        /// <returns>A list of RoutineData objects with their default values.</returns>
        public List<RoutineData> GetDefaultRoutines()
        {
            List<RoutineData> defaultRoutines = new List<RoutineData>()
            {
                new RoutineData("creditsgive", "TRBot.Routines.CreditsGiveRoutine", true, false),
                new RoutineData("periodicmessage", "TRBot.Routines.PeriodicMessageRoutine", true, false),
            };

            return defaultRoutines;
        }

        #endregion

        #region Services

        /// <summary>
        /// Returns a list of default client service data to connect to.
        /// </summary>
        /// <returns>A list of client service data.</returns>
        public List<ServiceData> GetDefaultServices()
        {
            return new List<ServiceData>()
            {
                new ServiceData(ClientServiceNames.TERMINAL_SERVICE, true, false, string.Empty),
                new ServiceData(ClientServiceNames.TWITCH_SERVICE, true, true, string.Empty),
                new ServiceData(ClientServiceNames.WEBSOCKET_SERVICE, false, true, string.Empty),
                new ServiceData(ClientServiceNames.IRC_SERVICE, false, true, string.Empty),
                new ServiceData(ClientServiceNames.XMPP_SERVICE, false, true, string.Empty),
                new ServiceData(ClientServiceNames.MATRIX_SERVICE, false, true, string.Empty),
            };
        }

        #endregion

        #region User Display Ranks

        public List<DisplayRank> GetDefaultDisplayRanks()
        {
            //Start with a simple set of ranks by default
            string[] labels = new string[5] { "Beginner", "Novice", "Intermediate", "Expert", "Legendary" };

            List<DisplayRank> defaultDisplayRanks = new List<DisplayRank>(20);
            int sameRankLabel = defaultDisplayRanks.Capacity / labels.Length;

            long[] expReqs = new long[20]
            {
                0, 25, 75, 100,
                200, 350, 500, 750,
                1000, 1500, 2000, 2500,
                3000, 5000, 7500, 10000,
                25000, 50000, 75000, 100000
            };

            for (int i = 0; i < defaultDisplayRanks.Capacity; i++)
            {
                long rankNum = i + 1;
                int labelIndex = (i / sameRankLabel);
                long expReq = expReqs[i];

                defaultDisplayRanks.Add(new DisplayRank(rankNum, expReq, labels[labelIndex]));
            }

            return defaultDisplayRanks;
        }

        #endregion

        #region Custom Messages

        /// <summary>
        /// Returns a list of default custom messages.
        /// </summary>
        /// <returns>A list of custom messages.</returns>
        public List<CustomMessage> GetDefaultCustomMessages()
        {
            return new List<CustomMessage>()
            {
                new CustomMessage("Hey {0}! Control the game by typing inputs into chat. See all valid inputs with \"!inputs\". Hang tight, as I'll be giving tips as you play to help you up your game! See \"!tutorial\" or \"!syntax\" if you want the whole rundown or \"!managedata optout tutorial\" to stop seeing these messages :D",
                    2),
                new CustomMessage("Delay inputs by a certain amount of time with \"ms\" or \"s\" at the end. \"up500ms\" holds 'up' for 500ms and \"up1s\" holds it for 1 second. \"up1.573s\" is also valid :) Remember that 1000ms = 1 second!",
                    5),
                new CustomMessage("You're doing great, {0}! Here's another tip: perform inputs one after another by typing them sequentially. \"up300ms right\" will first press 'up' for 300ms then press 'right'.",
                    10),
                new CustomMessage("Keep it up {0} :D Tip #4: press inputs at the *same time* by adding \"+\" in between them. \"up+right\" will press both 'up' and 'right' at the exact same time.",
                    15),
                new CustomMessage("Hey, hey {0}! Tip #5 for you: delay inputs by a certain amount of time using a *blank* input, which is either \"#\" or \".\". \"up300ms #200ms right\" waits 200 milliseconds before pressing 'right'.",
                    30),
                new CustomMessage("Ever wanted to keep an input held, {0}? Add \"_\" before the input. \"_up #300ms right\" will hold 'up' until 'right' is pressed after 300ms.",
                    40),
                new CustomMessage("Oh right, release inputs early by adding \"-\" before the input. \"_up #300ms -up right\" will let go of 'up' before pressing 'right'.",
                    55),
                new CustomMessage("Finding repeating inputs tedious, {0}? Wrap your input sequence in \"[]\" and add \"*#\" at the end to specify how many times to repeat it. \"[up300ms #34ms]*10\" presses 'up' for 300ms and waits 34ms ten times.",
                    75),
                new CustomMessage("Ready for the next level, {0}? Use \"!macros\" to list macros, which are input sequences others have made and saved. Use them by simply typing them in chat, view them with \"!show\", and add them with \"!addmacro\". The sky is the limit :D",
                    100),
                new CustomMessage("Last tip, {0}! *Dynamic* macros take the form of \"#macroname(*,*)\". Substitute \"*\" with inputs to use the macro with those inputs. Ex. \"#mash(b)\" uses 'b' and \"#mash(y)\" uses 'y'. When creating a dynamic macro, specify what to replace in each slot with \"<0>\", \"<1>\", and so on (Ex. \"<0>500ms <1>+down\"). Now get out there and give it all you've got :D !",
                    125),
            };
        }

        #endregion
    }
}