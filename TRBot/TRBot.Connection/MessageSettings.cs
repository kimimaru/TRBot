﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection
{
    /// <summary>
    /// Settings for client services.
    /// </summary>
    public class MessageSettings : IMessageSettings
    {
        /// <summary>
        /// The string to prepend to each message.
        /// </summary>
        public string MessagePrefix { get; private set; } = string.Empty;

        /// <summary>
        /// Whether to also log sent messages to the logger.
        /// </summary>
        public bool LogToLogger { get; private set; } = false;

        public MessageSettings(string messagePrefix, in bool logToLogger)
        {
            SetMessagePrefix(messagePrefix);
            SetLogToLogger(logToLogger);
        }

        /// <summary>
        /// Sets the message prefix.
        /// </summary>
        /// <param name="messagePrefix">A string representing the message prefix.</param>
        public void SetMessagePrefix(string messagePrefix)
        {
            MessagePrefix = messagePrefix;
        }

        /// <summary>
        /// Sets the setting to log sent messages to the logger.
        /// </summary>
        /// <param name="logToLogger">A bool determining whether to log sent messages to the logger.</param>
        public void SetLogToLogger(in bool logToLogger)
        {
            LogToLogger = logToLogger;
        }
    }
}
