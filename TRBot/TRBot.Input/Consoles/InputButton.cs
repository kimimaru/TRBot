﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Input.Consoles
{
    /// <summary>
    /// Represents an input button.
    /// </summary>
    public struct InputButton
    {
        /// <summary>
        /// The value of the button.
        /// </summary>
        public uint ButtonVal;

        public InputButton(in uint buttonVal)
        {
            ButtonVal = buttonVal;
        }

        public override bool Equals(object obj)
        {
            if (obj is InputButton inputBtn)
            {
                return (this == inputBtn);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 13;
                hash = (hash * 17) + ButtonVal.GetHashCode();
                return hash;
            }
        }

        public static bool operator==(InputButton a, InputButton b)
        {
            return (a.ButtonVal == b.ButtonVal);
        }

        public static bool operator!=(InputButton a, InputButton b)
        {
            return !(a == b);
        }
    }
}
