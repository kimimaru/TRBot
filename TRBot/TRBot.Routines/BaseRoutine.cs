﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities.Logging;
using TRBot.Utilities;
using TRBot.WebSocket;
using TRBot.Events;
using TRBot.Input;
using TRBot.Input.VirtualControllers;

namespace TRBot.Routines
{
    /// <summary>
    /// The base class for bot routines.
    /// </summary>
    public abstract class BaseRoutine
    {
        public bool Enabled = true;
        public string ClassName = string.Empty;
        public bool ResetOnReload = false;
        public string ValueStr = string.Empty;

        /// <summary>
        /// Tells if the given routine is not part of the database.
        /// These routines should not be considered for removal until their conditions expire.
        /// <para>For example, this would be true on a routine to reconnect a given service or conduct a vote.</para>
        /// </summary>
        public readonly bool IsRuntimeRoutine = false;

        protected IRoutineHandler RoutineHandler = null;
        protected ITRBotLogger Logger = null;
        protected IFolderPathResolver FolderPathResolver = null;
        protected IDatabaseManager<BotDBContext> DatabaseMngr = null;
        protected IClientServiceManager ClientServiceMngr = null;
        protected IVirtualControllerContainer VControllerContainer = null;
        protected IEventDispatcher EvtDispatcher = null;
        protected IWebSocketManager WebSocketMngr = null;
        protected IInputHandler InputHndlr = null;

        public BaseRoutine()
        {

        }

        protected BaseRoutine(bool isRuntimeRoutine)
        {
            IsRuntimeRoutine = isRuntimeRoutine;
        }

        /// <summary>
        /// Sets data required for many routines to function.
        /// </summary>
        public void SetRequiredData(IRoutineHandler routineHandler, ITRBotLogger logger,
            IFolderPathResolver folderPathResolver, IDatabaseManager<BotDBContext> databaseMngr,
            IClientServiceManager clientServiceMngr,
            IVirtualControllerContainer vControllerContainer, IEventDispatcher evtDispatcher,
            IWebSocketManager webSocketMngr, IInputHandler inputHandler)
        {
            RoutineHandler = routineHandler;
            Logger = logger;
            FolderPathResolver = folderPathResolver;
            DatabaseMngr = databaseMngr;
            ClientServiceMngr = clientServiceMngr;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHandler;
        }

        public virtual void Initialize()
        {

        }

        public virtual void CleanUp()
        {
            RoutineHandler = null;
        }

        public abstract void UpdateRoutine(in DateTime currentTimeUTC);
    }
}
