﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;

namespace TRBot.Connection
{
    /// <summary>
    /// The names of supported client services.
    /// </summary>
    public static class ClientServiceNames
    {
        public const string TERMINAL_SERVICE = "terminal";
        public const string TWITCH_SERVICE = "twitch";
        public const string WEBSOCKET_SERVICE = "websocket";
        public const string IRC_SERVICE = "irc";
        public const string XMPP_SERVICE = "xmpp";
        public const string MATRIX_SERVICE = "matrix";

        private static readonly string[] ServiceNamesArr = new string[]
        {
            TERMINAL_SERVICE,
            TWITCH_SERVICE,
            WEBSOCKET_SERVICE,
            IRC_SERVICE,
            XMPP_SERVICE,
            MATRIX_SERVICE
        };

        public static ReadOnlySpan<string> AllServiceNames => new ReadOnlySpan<string>(ServiceNamesArr);
    }
}
