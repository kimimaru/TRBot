﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection.XMPP
{
    /// <summary>
    /// Connection settings for XMPP.
    /// </summary>
    public sealed class XMPPLoginSettings
    {
        public XMPPRoomTypes RoomType = XMPPRoomTypes.MUC;
        public XMPPConnectionTypes ConnectionType = XMPPConnectionTypes.TCP;
        public string Username = string.Empty;
        public string Password = string.Empty;
        public string Domain = string.Empty;
        public string Resource = string.Empty;
        public string RoomName = string.Empty;
        public XMPPMUCSettings MUCSettings = new XMPPMUCSettings();
        public XMPPWebSocketSettings WebSocketSettings = new XMPPWebSocketSettings();
    }
}
