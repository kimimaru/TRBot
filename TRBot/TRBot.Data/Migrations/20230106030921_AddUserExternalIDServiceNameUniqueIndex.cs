﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    public partial class AddUserExternalIDServiceNameUniqueIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_Name",
                schema: "users",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "ExternalID",
                schema: "users",
                table: "Users",
                type: "TEXT",
                nullable: true,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ServiceName",
                schema: "users",
                table: "Users",
                type: "TEXT",
                nullable: true,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ExternalID_ServiceName_Name",
                schema: "users",
                table: "Users",
                columns: new[] { "ExternalID", "ServiceName", "Name" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_ExternalID_ServiceName_Name",
                schema: "users",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ExternalID",
                schema: "users",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ServiceName",
                schema: "users",
                table: "Users");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Name",
                schema: "users",
                table: "Users",
                column: "Name");
        }
    }
}
