﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Concurrent;
using TRBot.Events;
using TRBot.Utilities.Logging;

namespace TRBot.Connection
{
    /// <summary>
    /// Manages multiple client services.
    /// </summary>
    public class ClientServiceManager : IClientServiceManager
    {
        /// <summary>
        /// The number of client services in the manager.
        /// </summary>
        public int ServiceCount => ClientServices.Count;

        private ConcurrentDictionary<string, IClientService> ClientServices = new ConcurrentDictionary<string, IClientService>();
        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        public ClientServiceManager(ITRBotLogger logger, IEventDispatcher evtDispatcher)
        {
            Logger = logger;
            EvtDispatcher = evtDispatcher;
        }

        /// <summary>
        /// Initializes the manager.
        /// </summary>
        public void Initialize()
        {
            EvtDispatcher.RegisterEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE);
            EvtDispatcher.AddListener<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE, HandleBroadcastMessage);

            EvtDispatcher.RegisterEvent<EvtBotMessageArgs>(ClientServiceEventNames.BOT_MESSAGE);
            EvtDispatcher.AddListener<EvtBotMessageArgs>(ClientServiceEventNames.BOT_MESSAGE, HandleBotMessage);
        }

        public void Dispose()
        {
            foreach (IClientService service in ClientServices.Values)
            {
                service.Dispose();
            }

            ClientServices.Clear();

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.BROADCAST_MESSAGE);
            EvtDispatcher.RemoveListener<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE, HandleBroadcastMessage);

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.BOT_MESSAGE);
            EvtDispatcher.RemoveListener<EvtBotMessageArgs>(ClientServiceEventNames.BOT_MESSAGE, HandleBotMessage);
        }

        /// <summary>
        /// Adds a client service to the manager.
        /// </summary>
        /// <param name="serviceName">The name of the client service.</param>
        /// <param name="clientService">The client service to add.</param>
        /// <param name="autoConnect">If true, connects the client service immediately.
        /// If false, the caller is responsible for connecting the client service manually.</param>
        public void AddClientService(string serviceName, IClientService clientService, in bool autoConnect)
        {
            if (string.IsNullOrEmpty(serviceName) == true)
            {
                throw new ArgumentException("Service name must not be a null or empty string.", nameof(serviceName));
            }

            if (clientService == null)
            {
                throw new ArgumentNullException(nameof(clientService));
            }

            if (ClientServices.TryGetValue(serviceName, out IClientService service) == true)
            {
                throw new InvalidOperationException($"Service \"{serviceName}\" is already active! Remove it first before adding it again.");
            }

            ClientServices.TryAdd(serviceName, clientService);

            clientService.Initialize();

            if (autoConnect == true)
            {
                try
                {
                    clientService.Connect();
                }
                catch (Exception e)
                {
                    Logger.Error($"Unable to connect to service \"{serviceName}\". {e.Message}\n{e.StackTrace}");
                }
            }
        }

        /// <summary>
        /// Removes a client service from the manager.
        /// </summary>
        /// <param name="serviceName">The name of the client service to remove.</param>
        public void RemoveClientService(string serviceName)
        {
            if (string.IsNullOrEmpty(serviceName) == true)
            {
                throw new ArgumentException("Service name must not be a null or empty string.", nameof(serviceName));
            }

            ClientServices.TryRemove(serviceName, out IClientService service);

            service?.Dispose();
        }
        
        /// <summary>
        /// Retrieves a client service by name.
        /// </summary>
        /// <param name="serviceName">The name of the client service.</param>
        public IClientService GetClientService(string serviceName)
        {
            if (ClientServices.TryGetValue(serviceName, out IClientService service) == false)
            {
                return null;
            }

            return service;
        }

        /// <summary>
        /// Retrieves the names of all client services being managed.
        /// </summary>
        public string[] GetAllServiceNames()
        {
            if (ClientServices.Count == 0)
            {
                return Array.Empty<string>();
            }

            string[] serviceNames = new string[ClientServices.Count];
            ClientServices.Keys.CopyTo(serviceNames, 0);

            return serviceNames;
        }

        /// <summary>
        /// Retrieves all client services being managed.
        /// </summary>
        public IClientService[] GetAllServices()
        {
            if (ClientServices.Count == 0)
            {
                return Array.Empty<IClientService>();
            }

            IClientService[] services = new IClientService[ClientServices.Count];
            ClientServices.Values.CopyTo(services, 0);

            return services;
        }

        private void HandleBroadcastMessage(EvtBroadcastMessageArgs e)
        {
            foreach (IClientService service in ClientServices.Values)
            {
                service.MsgHandler.QueueMessage(e.Message, logLevel: e.LogLevel);
            }
        }

        private void HandleBotMessage(EvtBotMessageArgs e)
        {
            IClientService service = GetClientService(e.ServiceName);
            service?.MsgHandler?.QueueMessage(e.Message, logLevel: e.LogLevel);
        }
    }
}
