﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Input.Consoles;
using Microsoft.CodeAnalysis.Scripting;

namespace TRBot.Commands
{
    /// <summary>
    /// Execute arbitrary C# code.
    /// <para>
    /// It's highly recommended to have this accessible ONLY to the host.
    /// This has potential to corrupt, delete, or modify data and is provided as a convenience
    /// for carrying out whatever cannot be done normally through TRBot while the application is running.
    /// You are responsible for entrusting use of this command to others.
    /// </para>
    /// </summary>
    public class ExecCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"C# code\"";

        public ExecCommand()
        {

        }

        public override void Initialize()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            string code = args.Command.ArgumentsAsString;

            if (string.IsNullOrEmpty(code) == true)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            ExecuteCSharpScript(code, args);
        }

        protected async void ExecuteCSharpScript(string code, EvtChatCommandArgs args)
        {
            try
            {
                ExecScriptDataContainer execContainer = new ExecScriptDataContainer(this, args);

                await CustomCodeHandler.ExecuteCode(code, execContainer, typeof(ExecScriptDataContainer));
            }
            catch (CompilationErrorException exception)
            {
                QueueMessage(args.ServiceName, $"Compiler error: {exception.Message}", Serilog.Events.LogEventLevel.Warning);
            }
            catch (Exception otherExc)
            {
                QueueMessage(args.ServiceName, $"Exec runtime error: {otherExc.Message}", Serilog.Events.LogEventLevel.Warning);
                Logger.Warning($"Exec runtime error: {otherExc.Message} - at\n{otherExc.StackTrace}");
            }
        }

        /// <summary>
        /// A container class holding global data accessible to arbitrary code.
        /// </summary>
        public class ExecScriptDataContainer
        {
            public BaseCommand ThisCmd = null;
            public EvtChatCommandArgs Args = null;

            public ExecScriptDataContainer(BaseCommand thisCmd, EvtChatCommandArgs args)
            {
                ThisCmd = thisCmd;
                Args = args;
            }
        }
    }
}
