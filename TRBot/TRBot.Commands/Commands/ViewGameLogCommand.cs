﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Views a game log.
    /// </summary>
    public class ViewGameLogCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"recent game log number (optional)\"";

        public ViewGameLogCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int logNum = 0;

            List<GameLog> gameLogs = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Order by ascending for most recent
                gameLogs = context.GameLogs.OrderBy(log => log.LogDateTime).ToList();
            }

            //If no log number was specified, use the most recent one
            if (arguments.Count == 0)
            {
                logNum = gameLogs.Count - 1;
            }
            else if (arguments.Count == 1)
            {
                string num = arguments[0];
                if (StringNumParser.TryParseInt(num, out logNum) == false)
                {
                    QueueMessage(args.ServiceName, "Invalid game log number!");
                    return;
                }

                //Go from back to front (Ex. "!viewlog 1" shows the most recent log)
                logNum = gameLogs.Count - logNum;
            }
            else
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (logNum < 0 || logNum >= gameLogs.Count)
            {
                QueueMessage(args.ServiceName, $"No game log found at number {logNum}!");
                return;
            }

            GameLog gameLog = gameLogs[logNum];

            User logUser = DatabaseMngr.GetUserByExternalID(gameLog.UserExternalID);

            PrintLog(args, gameLog,
                (logUser == null) ? string.Empty : logUser.Name,
                (logUser == null) ? false : logUser.HasConsentToDataOption(UserDataConsentOptions.Stats));
        }

        protected void PrintLog(EvtChatCommandArgs args, GameLog gameLog, string logUserName, in bool optedIntoStats)
        {
            //Display username if they opted into stats and the name is valid
            if (optedIntoStats == true && string.IsNullOrEmpty(logUserName) == false)
            {
                QueueMessage(args.ServiceName, $"{gameLog.LogDateTime} (UTC) --> {logUserName} : {gameLog.LogMessage}");
            }
            else
            {
                QueueMessage(args.ServiceName, $"{gameLog.LogDateTime} (UTC) --> {gameLog.LogMessage}");
            }
        }
    }
}
