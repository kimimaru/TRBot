﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Data;
using TRBot.Utilities;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Allows a user to bet credits for a chance to win.
    /// </summary>
    public class BetCreditsCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"bet amount (int)\"";
        private Random Rand = new Random();

        public BetCreditsCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            string creditsName = DatabaseMngr.GetCreditsName();

            string userName = args.Command.UserMessage.Username;
            string userExternalID = args.Command.UserMessage.UserExternalID;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User bettingUser = context.Users
                    .AsNoTrackingWithIdentityResolution()
                    .Include(u => u.Stats)
                    .Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                if (bettingUser.HasEnabledAbility(PermissionConstants.BET_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to bet!");
                    return;
                }

                if (bettingUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "You cannot bet while opted out of stats.");
                    return;
                }
            }

            if (arguments.Count != 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (long.TryParse(arguments[0], out long creditBet) == false)
            {
                QueueMessage(args.ServiceName, "Please enter a valid bet amount!");
                return;
            }

            if (creditBet <= 0)
            {
                QueueMessage(args.ServiceName, "Bet amount must be greater than 0!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User bettingUser = context.Users
                .AsNoTrackingWithIdentityResolution()
                .Include(u => u.Stats)
                .FirstOrDefault(u => u.ExternalID == userExternalID);

                if (creditBet > bettingUser.Stats.Credits)
                {
                    QueueMessage(args.ServiceName, $"Bet amount is greater than {creditsName.Pluralize(0)}!");
                    return;
                }
            }

            //Make it a 50/50 chance
            bool success = (Rand.Next(0, 2) == 0);
            string message = string.Empty;
                
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User bettingUser = context.GetUserByExternalID(userExternalID);
                
                //Add or subtract credits based on the bet result
                if (success)
                {
                    bettingUser.Stats.Credits += creditBet;
                    message = $"{bettingUser.Name} won {creditBet} {creditsName.Pluralize(creditBet)} PogChamp";
                }
                else
                {
                    bettingUser.Stats.Credits -= creditBet;
                    message = $"{bettingUser.Name} lost {creditBet} {creditsName.Pluralize(creditBet)} BibleThump";
                }

                context.SaveChanges();
            }

            (int, int) leaderBoard = LeaderboardHelper.GetPositionOnNumericStatLeaderboard(DatabaseMngr, userExternalID, nameof(UserStats.Credits));

            message += $" and is rank {leaderBoard.Item1}/{leaderBoard.Item2} on the leaderboard!";

            QueueMessage(args.ServiceName, message);
        }
    }
}
