# Building from source
TRBot is built with C# and the .NET 7 SDK. Follow the steps below to set up your environment for compiling the code.

## Installation and building
0. Open a terminal, or PowerShell if on Windows
1. Clone the repo with `git clone https://codeberg.org/kimimaru/TRBot.git`
2. Enter the directory with `cd TRBot`
3. Initialize + update submodules with `git submodule update --init --recursive`
4. [Install the .NET 7.0 SDK and Runtime](https://dotnet.microsoft.com/download/dotnet/7.0)
  * Before installing, set the `DOTNET_CLI_TELEMETRY_OPTOUT` environment variable to 1 if you don't want dotnet CLI commands sending telemetry.
  * Ubuntu 22.04+: `sudo apt install dotnet-sdk-7.0`
  * Arch Linux: `sudo pacman -Syu dotnet-sdk` (or `dotnet-sdk-7.0`)

Once you have all the requirements, build TRBot using the provided .sln or through the CLI (instructions below). You can also use any code editor or IDE supporting .NET 7, such as VSCodium.

The root of the repository contains build scripts that compile versions of the TRBot.Main project for both GNU/Linux and Windows. Run **build.sh** on GNU/Linux and **build.bat** on Windows. On Windows, make sure dotnet is installed in your `PATH` before running the script.

The .NET SDK version the project is built with is determined by the [`global.json`](../TRBot/global.json) file at the base directory containing all the TRBot projects.

For manual builds, follow the instructions below.

Command line:
* Main directory: `cd TRBot.Main`
* Building: `dotnet build`
* Publishing: `dotnet publish -c config -o dir --self-contained --runtime RID`
  * config = "Debug" or "Release"
  * dir = output directory
  * [RID](https://raw.githubusercontent.com/dotnet/runtime/main/src/libraries/Microsoft.NETCore.Platforms/src/runtime.json) = "linux-x64" or "win-x64". See the link for a full list of runtime identifiers.
  * Example: `dotnet publish -c Debug -o ~/Desktop/TRBot --self-contained --runtime linux-x64`

**Note: Virtual controller input works only on GNU/Linux (uinput) and Windows (vJoy) as virtual controllers are platform-specific. The virtual controller API is abstracted into an `IVirtualController` interface, making it possible to add new implementations. Please file an issue or submit a PR if your platform isn't supported.**

To build any other TRBot applications, run the publish command in the application's directory or build its project in your IDE of choice.

## System-wide Compilation
System-wide installations change the paths of folders to conform to the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html). One use of this is to distribute TRBot as a package through a package manager, such as `apt` or `pacman`. The changed paths will work on every platform TRBot runs on.

To compile TRBot as system-wide by default, define the `SYSTEM_WIDE_INSTALL` preprocessor directive in "TRBot.Main.csproj". This can be done by adding it to the **DefineConstants** property in one of the configuration property groups. For example, to make Release builds system-wide, do the following:
```
<PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|AnyCPU'">
  <DefineConstants>TRACE;SYSTEM_WIDE_INSTALL</DefineConstants>
</PropertyGroup>
```

To make the build portable again by default, remove `SYSTEM_WIDE_INSTALL` from **DefineConstants**.

**Alternatively**, build with more granularity using `dotnet msbuild` to specify the preprocessor directive without changing the project file:
- Example: `dotnet msbuild ~/Desktop/TRBot -target:Publish -restore:True -property:Configuration=Release -property:DefineConstants=SYSTEM_WIDE_INSTALL -property:SelfContained=True -property:OutDir=~/Desktop/TRBot2`

## Native code
Some virtual controllers require native code. These will be in the ["Native" folder of the `TRBot.Input.VirtualControllers` project](../TRBot/TRBot.Input/VirtualControllers/Native). More information about them can be found below. Note that these components are pre-compiled in source and binary releases of TRBot.

Note: The pre-built uinput virtual controllers were compiled for **Debian-based systems** and may not work out of the box with your distribution.

### uinput (GNU/Linux)
The uinput virtual controller is implemented via [libevdev](https://www.freedesktop.org/wiki/Software/libevdev/). As a result, libevdev will need to be installed on your system before you can compile the code.

Prerequisite packages:
- libevdev-dev
- libudev-dev

For Debian-based systems (Debian/Ubuntu/Linux Mint):
```
sudo apt update
sudo apt install libevdev-dev libudev-dev
```

For Arch-based systems:
```
sudo pacman -Syu libevdev
```

These package names may be different depending on your distribution.

After installing libevdev, the uinput virtual controller code, `SetupVController.c`, can be compiled with **gcc** as a shared library with:

```
gcc -fPIC -shared SetupVController.c $(pkg-config --cflags --libs libevdev) -o ../SetupVController.so
```

Use the newly compiled file in place of the old one. **The `pkg-config` section is very important**; make sure to include it. If it's absent, it won't include all the libevdev dependencies, causing the code to fail during compilation or runtime.

If you'd like to adjust the virtual controller's axes values and deadzones, modify the constants at the top of `SetupVController.h` and recompile. 

### vJoy Wrapper (Windows)
The vJoy C# wrapper code can be found on the [most up-to-date repository](https://github.com/jshafer817/vJoy/tree/master/apps/common/vJoyInterfaceCS/vJoyInterfaceWrap). While unconfirmed, you should be able to compile it with `make`.

Unfortunately, compiling the vJoy driver itself isn't very clear, but 64-bit versions of Windows will not load the driver unless it is signed. Doing this is out-of-scope for this guide, so in short, it's recommended to use the already-signed drivers provided through the official download.

## Migrations
TRBot uses a SQLite database with Entity Framework Core to store and manage its data.
- First install the `dotnet-ef` tool by going to the **TRBot.Data** folder and running `dotnet tool restore`. This will add the Entity Framework Core tool required to manage the database, which includes handling migrations.

If you make code changes to any entities or contexts that affects the database, such as adding/deleting/renaming a column, adding/removing a DbSet, or modifying entity relationships in `OnModelCreating`, you will need to add a new migration:

* Go to the **TRBot.Data** project and run `dotnet ef migrations list` to list all migrations. Take note of the furthest one down, which is the most recent. If needed, you can verify the date prepended to the name of each migration.
* Run `dotnet ef migrations add (migrationhere)`, where "(migrationhere)" is the name of the migration.
  * Example: `dotnet ef migrations add NewUserPermissions`

Afterwards, simply run TRBot to apply the new migrations and update the database. If you encounter an unforeseen error, please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new).

## Running Tests
All unit tests are in the **TRBot.Tests** project. Some of these tests include parser and input handler tests to validate correct behavior.

Simply run `dotnet test` inside this directory to run the tests. Please add new tests for code if you submit a PR. If a test fails, please fix it! Test are very important as they validate that the code functions as intended.

## Build Information
The **TRBot.Build** project uses information from Git to obtain version information. It achieves this via the [GitInfo](https://github.com/devlooped/GitInfo) library. Most of the options are the defaults; for instance, it assumes the remote is named `origin`.

If you change certain information local to your Git repository, such as the remote name, you will need to specify this new information. This can be achieved via a `Directory.Build.props` file in the root **TRBot.Build** folder. This file is in TRBot's `.gitignore`, so there's no worry of accidentally pushing it.

As an example, if one changed the remote name for their TRBot repository in Git from `origin` to `codeberg`, they can add a `Directory.Build.props` file that looks like this:

```
<Project>
  <PropertyGroup>
    <GitRemote>codeberg</GitRemote>
  </PropertyGroup>
</Project>
```

The available build properties can be found [here](https://github.com/devlooped/GitInfo#details).

### Building without Git
It's recommended to build with Git, but in certain circumstances it may not be possible. Here are the instructions to do that.

- Open [`TRBot.Build.csproj`](../TRBot/TRBot.Build/TRBot.Build.csproj) and remove the `ItemGroup` containing `GitInfo` as well as the `PopulateInfo` `Target`.
- Go to [`BuildInfo.cs`](../TRBot/TRBot.Build/BuildInfo.cs).
  - Remove any code involving `ThisAssembly`.
  - Return the values you want in `GitBaseTag`, `GitCommitHash`, `GitCommitDate`, and `GitRepositoryURL`. The actual version number is `GitBaseTag` and is in the form of `X.Y.Z` (Ex. "2.5.1"). Since `GitInfo` isn't available, you are manually supplying the build information here.
- Open [`.gitmodules`](../.gitmodules) in the root directory and manually download the repositories linked there into the [`Submodules`](../Submodules/) folder.

# Contributing
If you find any problems with TRBot, please file an [issue](https://codeberg.org/kimimaru/TRBot/issues/new). [Pull requests](https://codeberg.org/kimimaru/TRBot/pulls) are encouraged if you'd like to make contributions.

TRBot is free software; as such, you can run, study, modify, and distribute it for any purpose. See the [License](../LICENSE) for more information.
