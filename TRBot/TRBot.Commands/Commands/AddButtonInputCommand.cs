﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that adds a button input to a console.
    /// </summary>
    public sealed class AddButtonInputCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"console name\", \"button name\", \"buttonVal (int)\"";

        public AddButtonInputCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount != 3)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string consoleStr = arguments[0].ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }
            }

            string inputName = arguments[1].ToLowerInvariant();

            string buttonStr = arguments[2].ToLowerInvariant();

            if (StringNumParser.TryParseInt(buttonStr, out int buttonVal) == false)
            {
                QueueMessage(args.ServiceName, "Invalid button value.");
                return;
            }

            InputData existingInput = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                //Check if the input exists
                existingInput = console.InputList.FirstOrDefault((inpData) => inpData.Name == inputName);

                //Update if found
                if (existingInput != null)
                {
                    //Switch the input type and button value
                    existingInput.InputType = InputTypes.Button;
                    existingInput.ButtonValue = buttonVal;
                }
                //Otherwise, create the input and add it to the console
                else
                {
                    InputData inputData = new InputData(inputName, buttonVal, 0, InputTypes.Button,
                        0.5d, 1d, 0.5d, (long)PermissionLevels.User);
                    console.InputList.Add(inputData);
                }

                //Save database changes
                context.SaveChanges();
            }

            string message = string.Empty;
            if (existingInput == null)
            {
                message = $"Added new button \"{inputName}\" to console \"{consoleStr}\"!";
            }
            else
            {
                message = $"Updated button \"{inputName}\" on console \"{consoleStr}\"!";
            }

            QueueMessage(args.ServiceName, message);
        }
    }
}
