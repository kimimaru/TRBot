﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection
{
    /// <summary>
    /// Settings for client service messages.
    /// </summary>
    public interface IMessageSettings
    {
        /// <summary>
        /// The string to prepend to each message.
        /// </summary>
        string MessagePrefix { get; }

        /// <summary>
        /// Whether to also log sent messages to the logger.
        /// </summary>
        bool LogToLogger { get; }

        /// <summary>
        /// Sets the message prefix.
        /// </summary>
        /// <param name="messagePrefix">A string representing the message prefix.</param>
        void SetMessagePrefix(string messagePrefix);

        /// <summary>
        /// Sets the setting to log sent messages to the logger.
        /// </summary>
        /// <param name="logToLogger">A bool determining whether to log sent messages to the logger.</param>
        void SetLogToLogger(in bool logToLogger);
    }
}
