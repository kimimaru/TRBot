﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection
{
    /// <summary>
    /// Shared events for client services.
    /// </summary>
    public static class ClientServiceEventNames
    {
        public const string MESSAGE = "message";
        public const string INPUT = "input";
        public const string COMMAND = "command";

        public const string CONNECTED = "connected";
        public const string RECONNECTED = "reconnected";
        public const string DISCONNECTED = "disconnected";
        public const string CONNECTION_ERROR = "connection_error";

        public const string BROADCAST_MESSAGE = "broadcast_message";
        public const string BOT_MESSAGE = "bot_message";
        public const string OUTGOING_BOT_MESSAGE = "outgoing_bot_message";
    }
}
