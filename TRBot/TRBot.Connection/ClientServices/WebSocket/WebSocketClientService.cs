﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using TRBot.Utilities.Logging;
using TRBot.Events;
using WebSocketSharp;
using Newtonsoft.Json;

namespace TRBot.Connection.WebSocket
{
    /// <summary>
    /// Read inputs through a WebSocket.
    /// </summary>
    public class WebSocketClientService : IClientService
    {
        private const string LOG_PREFIX_STRING = "[" + ClientServiceNames.WEBSOCKET_SERVICE + "] ";

        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        public bool IsInitialized => Initialized;

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        public bool IsConnected => (Socket != null && Socket.ReadyState == WebSocketState.Open && Socket.IsAlive == true);

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        public bool CanSendMessages => (IsConnected == true && EstablishedConnection == true);

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        public IBotMessageHandler MsgHandler { get; private set; } = null;

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        public IMessageSettings MsgSettings { get; private set; } = null;

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        public ICommandSettings CmdSettings { get; private set; } = null;

        private bool EstablishedConnection = false;
        
        private bool Initialized = false;

        private string ConnectURL = string.Empty;
        private string BotName = string.Empty;

        private WebSocketSharp.WebSocket Socket = null;

        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        private JsonSerializerSettings SerializerSettings = null;
        private readonly string WebSocketServiceName = ClientServiceNames.WEBSOCKET_SERVICE;

        public WebSocketClientService(string connectURL, string botName,
            ITRBotLogger logger, IEventDispatcher evtDispatcher,
            IBotMessageHandler msgHandler, IMessageSettings msgSettings, ICommandSettings cmdSettings)
        {
            ConnectURL = connectURL;
            BotName = botName;
            Logger = logger;
            EvtDispatcher = evtDispatcher;
            MsgHandler = msgHandler;
            MsgSettings = msgSettings;
            CmdSettings = cmdSettings;
        }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        public void Initialize()
        {
            Logger.Information($"Opening WebSocket at {ConnectURL}");

            Socket = new WebSocketSharp.WebSocket(ConnectURL);

            //Specify SSL configuration on a secure WebSocket
            if (Socket.IsSecure == true)
            {
                Socket.SslConfiguration.EnabledSslProtocols = SslProtocols.Tls12;

                //Subscribe to validate SSL certificates for secure websockets
                ServicePointManager.ServerCertificateValidationCallback -= WebSocketSSLCertValidation;
                ServicePointManager.ServerCertificateValidationCallback += WebSocketSSLCertValidation;
            }

            Socket.OnOpen -= OnSocketOpened;
            Socket.OnOpen += OnSocketOpened;

            Socket.OnClose -= OnSocketClosed;
            Socket.OnClose += OnSocketClosed;

            Socket.OnError -= OnSocketError;
            Socket.OnError += OnSocketError;

            Socket.OnMessage -= OnSocketMessage;
            Socket.OnMessage += OnSocketMessage;

            SerializerSettings = new JsonSerializerSettings();
            SerializerSettings.StringEscapeHandling = StringEscapeHandling.Default;
            SerializerSettings.ConstructorHandling = ConstructorHandling.Default;
            SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            SerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;

            MsgHandler.SetClientService(this);

            EvtDispatcher.RegisterEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.RegisterEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.RegisterEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.RegisterEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND);

            EvtDispatcher.RegisterEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.RegisterEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR);

            Initialized = true;
        }

        /// <summary>
        /// Connects the client.
        /// </summary>
        public void Connect()
        {
            if (IsConnected == true)
            {
                Logger.Warning("Attempting to connect while already connected!");
                return;
            }

            Logger.Information($"Connecting to WebSocket at {ConnectURL}");
            Socket.Connect();
        }

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        public void Disconnect()
        {
            if (IsConnected == false)
            {
                Logger.Warning("Attempting to disconnect while not connected!");
                return;
            }

            Socket.Close(CloseStatusCode.Normal);
            EstablishedConnection = false;
        }

        /// <summary>
        /// Send a message through the client.
        /// </summary>
        public void ProcessAndSendMessage(IServiceMessage serviceMessage)
        {
            if (string.IsNullOrEmpty(serviceMessage.Message) == true)
            {
                Logger.Warning($"Empty message in {nameof(ProcessAndSendMessage)}!");
                return;
            }

            string sentMessage = serviceMessage.Message;

            if (serviceMessage.OverridePrefix == true)
            {
                sentMessage = serviceMessage.CustomPrefix + serviceMessage.Message;
            }
            else if (string.IsNullOrEmpty(MsgSettings.MessagePrefix) == false)
            {
                sentMessage = MsgSettings.MessagePrefix + serviceMessage.Message;
            }

            Socket.Send(sentMessage);

            if (MsgSettings.LogToLogger == true)
            {
                Logger.Log(serviceMessage.LogLevel, LOG_PREFIX_STRING + sentMessage);
            }

            var outgoingArgs = new EvtOutgoingBotMessageArgs()
            {
                ServiceName = WebSocketServiceName,
                UserMessage = new EvtUserMsgData(string.Empty, BotName, sentMessage)
            };

            EvtDispatcher.DispatchEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE, outgoingArgs);
        }

        public void Dispose()
        {
            Socket.Close(CloseStatusCode.Normal);

            Socket.OnOpen -= OnSocketOpened;
            Socket.OnClose -= OnSocketClosed;
            Socket.OnError -= OnSocketError;
            Socket.OnMessage -= OnSocketMessage;

            EstablishedConnection = false;

            MsgHandler.CleanUp();

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.COMMAND);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTION_ERROR);

            ServicePointManager.ServerCertificateValidationCallback -= WebSocketSSLCertValidation;

            Socket = null;
        }

        private void OnSocketOpened(object sender, EventArgs e)
        {
            EvtConnectedArgs connectedArgs = new EvtConnectedArgs()
            {
                ServiceName = WebSocketServiceName,
                BotUsername = BotName
            };

            EstablishedConnection = true;

            EvtDispatcher.DispatchEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, connectedArgs);
        }

        private void OnSocketClosed(object sender, CloseEventArgs e)
        {
            EvtDisconnectedArgs disconnectedArgs = new EvtDisconnectedArgs()
            {
                ServiceName = WebSocketServiceName
            };

            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disconnectedArgs);
        }

        private void OnSocketError(object sender, ErrorEventArgs e)
        {
            EvtErrorData errorData = new EvtErrorData($"{e.Message} | {e.Exception.Message}\n{e.Exception.StackTrace}");
            EvtConnectionErrorArgs errorArgs = new EvtConnectionErrorArgs()
            {
                Error = errorData,
                ServiceName = WebSocketServiceName,
                BotUsername = BotName
            };

            EvtDispatcher.DispatchEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, errorArgs);
        }
        
        private void OnSocketMessage(object sender, MessageEventArgs e)
        {
            string json = e.Data;

            //Logger.Information($"Received: {json}");

            WebSocketResponse response = null;

            try
            {
                response = JsonConvert.DeserializeObject<WebSocketResponse>(json, SerializerSettings);
            }
            catch (Exception exc)
            {
                Logger.Error($"Invalid JSON received as a response. {exc.Message}");
                return;
            }

            if (response.Message == null)
            {
                Logger.Error("Message JSON object is invalid as it couldn't be parsed.");
                return;
            }

            if (string.IsNullOrEmpty(response.Message.Text) == true)
            {
                Logger.Warning("null or empty message string from WebSocket.");
                return;
            }

            string msgWSReplaced = Utilities.Helpers.ReplaceAllWhitespaceWithSpace(response.Message.Text);

            string userExternalID = string.Empty;
            string userName = string.Empty;

            if (response.User != null)
            {
                userExternalID = response.User.ExternalID;
                userName = response.User.Username;
            }

            //Send message event
            EvtUserMessageArgs umArgs = new EvtUserMessageArgs()
            {
                ServiceName = WebSocketServiceName,
                UserMessage = new EvtUserMsgData(userExternalID, userName, msgWSReplaced)
            };
            
            EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, umArgs);

            //Parse command
            EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(msgWSReplaced,
                CmdSettings.CommandIdentifier, new EvtUserMsgData(userExternalID, userName, msgWSReplaced));
            
            if (chatCmdData != null)
            {
                EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                {
                    ServiceName = WebSocketServiceName,
                    Command = chatCmdData
                };

                EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
            }
        }

        private bool WebSocketSSLCertValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //Since this can connect to any server, simply return true if there are no SSL errors
            return sslPolicyErrors == SslPolicyErrors.None;

            //If you intend to use this with a specific set of servers, modify this
            //There are many ways to do this - one option is to validate against a set of certificate hashes
            //that can either be hardcoded or read from disk
        }
    }
}
