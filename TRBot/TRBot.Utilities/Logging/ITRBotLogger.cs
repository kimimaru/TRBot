﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using Serilog.Events;

namespace TRBot.Utilities.Logging
{
    /// <summary>
    /// Handles logging.
    /// </summary>
    public interface ITRBotLogger : IDisposable
    {
        /// <summary>
        /// The minimum logging level for the logger.
        /// </summary>
        LogEventLevel MinLoggingLevel { get; set; }

        /// <summary>
        /// Logs a verbose log.
        /// </summary>
        void Verbose(string message);

        /// <summary>
        /// Logs a debug log.
        /// </summary>
        void Debug(string message);

        /// <summary>
        /// Logs an information log.
        /// </summary>
        void Information(string message);

        /// <summary>
        /// Logs a warning.
        /// </summary>
        void Warning(string message);

        /// <summary>
        /// Logs an error.
        /// </summary>
        void Error(string message);

        /// <summary>
        /// Logs a fatal log.
        /// </summary>
        void Fatal(string message);

        /// <summary>
        /// Writes a log with a given log level.
        /// </summary>
        void Log(LogEventLevel logEventLevel, string message);
    }
}
