﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that adds a command.
    /// </summary>
    public sealed class AddCmdCommand : BaseCommand
    {
        private const string NULL_ARG = "null";

        private string UsageMessage = $"Usage - \"command name\", \"type w/namespace\" (can be \"null\"), \"argString - optional\" (can be \"null\"), \"level (string/int)\", \"enabled (bool)\", \"displayInHelp (bool)\", \"category (\"User\"/\"Admin\"/\"Games\") (string)\"";

        public AddCmdCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with too few arguments
            if (argCount < 6)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            int minArgCount = argCount - 4;

            string commandName = arguments[0].ToLowerInvariant();
            string className = arguments[1];
            string valueStr = string.Empty;
            string levelStr = arguments[minArgCount].ToLowerInvariant();
            string enabledStr = arguments[minArgCount + 1];
            string displayInHelpStr = arguments[minArgCount + 2];
            string categoryStr = arguments[minArgCount + 3];

            //Set the class name to null if it's the null argument
            if (className == NULL_ARG)
            {
                className = string.Empty;
            }

            //Combine all the arguments in between as the value string
            for (int i = 2; i < minArgCount; i++)
            {
                valueStr += arguments[i];
                if (i < (minArgCount - 1))
                {
                    valueStr += " ";
                }
            }

            //Set value string to null if it's the null argument
            if (valueStr == NULL_ARG)
            {
                valueStr = string.Empty;
            }

            //Parse the level
            if (PermissionHelpers.TryParsePermissionLevel(levelStr, out PermissionLevels permLevel) == false)
            {
                QueueMessage(args.ServiceName, "Invalid level specified.");
                return;
            }

            long levelNum = (long)permLevel;

            if (bool.TryParse(enabledStr, out bool cmdEnabled) == false)
            {
                QueueMessage(args.ServiceName, "Incorrect command enabled state specified.");
                return;
            }

            if (bool.TryParse(displayInHelpStr, out bool displayInHelp) == false)
            {
                QueueMessage(args.ServiceName, "Incorrect command displayInHelp state specified.");
                return;
            }

            if (EnumUtility.TryParseEnumValue(categoryStr, out CommandCategories cmdCategory) == false)
            {
                QueueMessage(args.ServiceName, "Invalid command category specified.");
                return;
            }

            bool added = CmdHandler.AddCommand(commandName, className, valueStr, levelNum, cmdEnabled, displayInHelp, cmdCategory);

            if (added == true)
            {
                //Add this command to the database
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //If this command already exists, remove it so it can be replaced with the new data
                    CommandData cmdData = context.Commands.FirstOrDefault((cmd) => cmd.Name == commandName);
                    if (cmdData != null)
                    {
                        //Remove command
                        context.Commands.Remove(cmdData);
                    }

                    //Add the new one
                    context.Commands.Add(new CommandData(commandName, className, levelNum,
                        cmdEnabled, displayInHelp, cmdCategory, valueStr));
                    
                    context.SaveChanges();
                }

                QueueMessage(args.ServiceName, $"Successfully added command \"{commandName}\"!");
            }
            else
            {
                QueueMessage(args.ServiceName, $"Failed to add command \"{commandName}\".");
            }
        }
    }
}
