﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Utilities.Logging;
using TRBot.WebSocket;
using Newtonsoft.Json;

namespace TRBot.Events
{
    /// <summary>
    /// Dispatches events through a WebSocket.
    /// </summary>
    public sealed class WebSocketEventDispatcher : EventDispatcher
    {
        public string SocketPath { get; private set; } = "/evt";
        private IWebSocketManager WebSocketMngr = null;

        public WebSocketEventDispatcher(string socketPath, IWebSocketManager webSocketMngr, ITRBotLogger logger)
            : base(logger)
        {
            WebSocketMngr = webSocketMngr;

            SocketPath = socketPath;

            WebSocketMngr.AddServerService<EventDispatcherSocketService>(SocketPath, WebSocketServiceInitializer);
        }

        public override void Dispose()
        {
            base.Dispose();

            WebSocketMngr.RemoveServerService(SocketPath);
        }

        public override void EnableExtraFeatures()
        {
            if (ExtraFeaturesEnabled == true)
            {
                Logger.Information($"Cannot enable extra features for {nameof(WebSocketEventDispatcher)} since they're already enabled.");
                return;
            }

            ExtraFeaturesEnabled = WebSocketMngr.AddServerService<EventDispatcherSocketService>(SocketPath, WebSocketServiceInitializer);
        }

        public override void DisableExtraFeatures()
        {
            if (ExtraFeaturesEnabled == false)
            {
                Logger.Information($"Cannot disable extra features for {nameof(WebSocketEventDispatcher)} since they're already disabled.");
                return;
            }

            ExtraFeaturesEnabled = !WebSocketMngr.RemoveServerService(SocketPath);
        }

        public override void DispatchEvent<T>(string eventName, T eventData)
        {
            base.DispatchEvent<T>(eventName, eventData);

            //Dispatch the WebSocket portion only if extra features are enabled and the server is listening
            if (ExtraFeaturesEnabled == false || WebSocketMngr.IsServerListening == false)
            {
                return;
            }

            try
            {
                EventResponse<T> evtResponse = new EventResponse<T>(eventName, eventData);

                string json = JsonConvert.SerializeObject(evtResponse, Formatting.None);

                //Send the data over the websocket
                WebSocketMngr.ServerBroadcastAsync(SocketPath, json, null, null);

                Logger.Debug($"Dispatched WebSocket event \"{eventName}\"");
            }
            catch (Exception e)
            {
                Logger.Warning($"Issue broadcasting WebSocket message for {nameof(WebSocketEventDispatcher)}. {e.Message}");
            }
        }

        private void WebSocketServiceInitializer(EventDispatcherSocketService service)
        {
            service.Initialize(this, Logger);
        }
    }
}
