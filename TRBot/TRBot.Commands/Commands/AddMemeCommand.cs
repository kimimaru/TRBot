﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Parsing;
using TRBot.Permissions;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Adds a meme.
    /// </summary>
    public sealed class AddMemeCommand : BaseCommand
    {
        /// <summary>
        /// The max length for memes.
        /// </summary>
        public const int MAX_MEME_NAME_LENGTH = 50;

        private string UsageMessage = "Usage: \"memename (enclose in \" quotes for multi-word)\" \"memevalue\"";

        public AddMemeCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.ADD_MEME_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to add memes.");
                    return;
                }
            }

            //Validate and add the meme
            ValidateAndAddMeme(args, arguments[0], arguments[1]);
        }

        private void ValidateAndAddMeme(EvtChatCommandArgs args, string memeName, string memeValue)
        {
            if (string.IsNullOrEmpty(memeName) == true || string.IsNullOrEmpty(memeValue) == true)
            {
                QueueMessage(args.ServiceName, "Empty meme or meme name!");
                return;
            }

            if (ConnectionHelpers.IsServiceChatCommand(args.ServiceName, memeName) == true
                || ConnectionHelpers.IsServiceChatCommand(args.ServiceName, memeValue) == true)
            {
                QueueMessage(args.ServiceName, "Memes cannot start with chat commands!");
                return;
            }

            if (memeName.StartsWith(InputMacroPreparser.DEFAULT_MACRO_START) == true)
            {
                QueueMessage(args.ServiceName, $"Memes cannot start with \"{InputMacroPreparser.DEFAULT_MACRO_START}\".");
                return;
            }

            string commandIdentifier = DatabaseMngr.GetCommandIdentifier();

            if (ConnectionHelpers.IsCommand(memeName, commandIdentifier) == true)
            {
                QueueMessage(args.ServiceName, $"Memes cannot start with \"{commandIdentifier}\".");
                return;
            }

            if (memeName.Length > MAX_MEME_NAME_LENGTH)
            {
                QueueMessage(args.ServiceName, $"Memes may have up to a max of {MAX_MEME_NAME_LENGTH} characters in their name!");
                return;
            }

            string memeToLower = memeName.ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Meme meme = context.Memes.FirstOrDefault(m => m.MemeName == memeToLower);

                if (meme != null)
                {
                    meme.MemeValue = memeValue;

                    QueueMessage(args.ServiceName, $"Meme \"{memeToLower}\" overwritten!");
                }
                else
                {
                    Meme newMeme = new Meme(memeToLower, memeValue);
                    context.Memes.Add(newMeme);

                    QueueMessage(args.ServiceName, $"Added meme \"{memeToLower}\"!");
                }

                context.SaveChanges();
            }
        }
    }
}
