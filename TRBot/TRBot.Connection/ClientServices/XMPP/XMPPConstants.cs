﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection.XMPP
{
    /// <summary>
    /// Constants regarding XMPP.
    /// </summary>
    public static class XMPPConstants
    {
        /// <summary>
        /// The file storing XMPP login settings.
        /// </summary>
        public const string LOGIN_SETTINGS_FILENAME = "XMPPLoginSettings.txt";

        /// <summary>
        /// The attribute name for the message type.
        /// </summary>
        public const string MSG_TYPE_ATTRIBUTE_NAME = "type";

        /// <summary>
        /// The attribute for private messages.
        /// </summary>
        public const string PRIVATE_CHAT_MSG_ATTRIBUTE = "chat";

        /// <summary>
        /// The attribute value for group chats.
        /// </summary>
        public const string GROUP_CHAT_MSG_ATTRIBUTE = "groupchat";
    }
}
