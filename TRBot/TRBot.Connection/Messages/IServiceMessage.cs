﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection
{
    /// <summary>
    /// Shared interface for all types of messages sent from services.
    /// </summary>
    public interface IServiceMessage
    {
        /// <summary>
        /// The content of the message.
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Whether to override the message prefix.
        /// </summary>
        bool OverridePrefix { get; }

        /// <summary>
        /// The message prefix used if the prefix is overridden.
        /// </summary>
        string CustomPrefix { get; }

        /// <summary>
        /// The log level of the message if logged.
        /// </summary>
        Serilog.Events.LogEventLevel LogLevel { get; }

        /// <summary>
        /// Clones the service message with different message content.
        /// </summary>
        IServiceMessage CloneWithDiffMsg(string messageContent);
    }
}
