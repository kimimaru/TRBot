﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TRBot.Data.Migrations
{
    public partial class ServiceData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "services");

            migrationBuilder.AlterColumn<double>(
                name: "MinAxisVal",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL",
                oldDefaultValue: 0.5);

            migrationBuilder.AlterColumn<double>(
                name: "MaxAxisVal",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL",
                oldDefaultValue: 1.0);

            migrationBuilder.CreateTable(
                name: "Services",
                schema: "services",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ServiceName = table.Column<string>(type: "TEXT", nullable: true, defaultValue: ""),
                    Enabled = table.Column<long>(type: "INTEGER", nullable: false),
                    LogToLogger = table.Column<long>(type: "INTEGER", nullable: false),
                    ConfigFile = table.Column<string>(type: "TEXT", nullable: true, defaultValue: "")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServiceName",
                schema: "services",
                table: "Services",
                column: "ServiceName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Services",
                schema: "services");

            migrationBuilder.AlterColumn<double>(
                name: "MinAxisVal",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                defaultValue: 0.5,
                oldClrType: typeof(double),
                oldType: "REAL");

            migrationBuilder.AlterColumn<double>(
                name: "MaxAxisVal",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                defaultValue: 1.0,
                oldClrType: typeof(double),
                oldType: "REAL");
        }
    }
}
