﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using static TRBot.Input.Consoles.ConsoleConstants;

namespace TRBot.Input.Consoles
{
    /// <summary>
    /// Helpers regarding consoles.
    /// </summary>
    public static class ConsoleHelpers
    {
        public static InputData CreateBlankInput(string name)
        {
            InputData blankInput = new InputData();
            blankInput.Name = name;
            blankInput.InputType = InputTypes.Blank;

            return blankInput;
        }

        public static InputData CreateButtonInput(string name, in int buttonValue)
        {
            InputData btnData = new InputData();
            btnData.Name = name;
            btnData.ButtonValue = buttonValue;
            btnData.InputType = InputTypes.Button;

            return btnData;
        }

        public static InputData CreateAxisInput(string name, in int axisValue, in double minAxisVal, in double maxAxisVal,
            in double defaultAxisVal)
        {
            InputData btnData = new InputData();
            btnData.Name = name;
            btnData.AxisValue = axisValue;
            btnData.InputType = InputTypes.Axis;
            btnData.MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);

            return btnData;
        }

        public static InputData CreateAxis(string name, in int axisValue, in double minAxisVal, in double maxAxisVal,
            in double defaultAxisVal)
        {
            InputData btnData = new InputData();
            btnData.Name = name;
            btnData.AxisValue = axisValue;
            btnData.InputType = InputTypes.Axis;
            btnData.MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            btnData.DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);

            return btnData;
        }
    }
}
