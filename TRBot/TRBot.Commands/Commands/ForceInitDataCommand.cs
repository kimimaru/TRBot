﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection;
using TRBot.Data;
using static TRBot.Data.DataEventObjects;

namespace TRBot.Commands
{
    /// <summary>
    /// Force initializes missing default data in the database.
    /// </summary>
    public sealed class ForceInitDataCommand : BaseCommand
    {
        public ForceInitDataCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            EvtDispatcher.RegisterEvent<EvtInitDataArgs>(DataEventNames.INIT_DEFAULT_DATA);
        }

        public override void CleanUp()
        {
            EvtDispatcher.UnregisterEvent(DataEventNames.INIT_DEFAULT_DATA);

            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            var initDataArgs = new EvtInitDataArgs(args.ServiceName);

            //Simply dispatch the event to initialize data, and the application should handle it however it deems appropriate
            EvtDispatcher.DispatchEvent<EvtInitDataArgs>(DataEventNames.INIT_DEFAULT_DATA, initDataArgs);
        }
    }
}
