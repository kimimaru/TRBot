﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;

namespace TRBot.Input.Consoles
{
    /// <summary>
    /// Represents an invalid input combo.
    /// </summary> 
    public class InvalidInputCombo
    {
        /// <summary>
        /// The ID of the invalid input combo.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The console ID the invalid input combo belongs to.
        /// </summary>
        public int ConsoleID { get; set; } = 0;

        /// <summary>
        /// The name of the invalid input combo.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Whether the invalid input combo is enabled or not.
        /// </summary>
        public long Enabled { get; set; } = 1L;

        /// <summary>
        /// The access level (and above) that the invalid input combo doesn't affect.
        /// This acts as a threshold: at or above this level, the invalid input combo is not invalid.
        /// </summary>
        public long ValidLevelThreshold { get; set; } = long.MaxValue;

        /// <summary>
        /// The GameConsole associated with this invalid input combo.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        public virtual GameConsole Console { get; set; } = null;

        /// <summary>
        /// A list of InputData containing the combination of inputs that be pressed together.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        public virtual List<InputData> InvalidInputs { get; set; } = null;

        /// <summary>
        /// A helper property to get the invalid input combo's enabled state.
        /// </summary>
        public bool IsEnabled => (Enabled > 0L);

        public InvalidInputCombo()
        {

        }

        public InvalidInputCombo(string name, List<InputData> inputList)
        {
            Name = name;
            InvalidInputs = inputList;
        }

        public InvalidInputCombo(string name, List<InputData> inputList, long validFor)
            : this(name, inputList)
        {
            ValidLevelThreshold = validFor;
        }

        public override string ToString()
        {
            string comboStr = "(";
            
            int i = 0;
            foreach (InputData inputData in InvalidInputs)
            {
                comboStr += inputData.Name;

                if (i < InvalidInputs.Count - 1)
                {
                    comboStr += ", ";
                }

                i++;
            }

            comboStr += ")";

            return $"Name: \"{Name}\" | ConsoleID: \"{ConsoleID}\" | Enabled: {IsEnabled} | Combo: {comboStr} | ValidFor: {ValidLevelThreshold}";
        }
    }
}
