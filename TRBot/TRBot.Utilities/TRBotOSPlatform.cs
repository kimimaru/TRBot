﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Runtime.InteropServices;

namespace TRBot.Utilities
{
    /// <summary>
    /// Operating systems.
    /// </summary>
    public static class TRBotOSPlatform
    {
        /// <summary>
        /// Operating systems supported by TRBot.
        /// </summary>
        public enum OS
        {
            Windows = 1,
            GNULinux = 2,
            FreeBSD = 3,
            macOS = 4,
            Other = 10
        }

        /// <summary>
        /// The type of operating system TRBot is currently running on.
        /// </summary>
        public static readonly OS CurrentOS = GetOSPlatform();

        private static OS GetOSPlatform()
        {
            //Retrieve the operating system type
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows) == true)
            {
                return OS.Windows;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) == true)
            {
                return OS.GNULinux;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.FreeBSD) == true)
            {
                return OS.FreeBSD;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX) == true)
            {
                return OS.macOS;
            }
            else
            {
                return OS.Other;
            }
        }
    }
}
