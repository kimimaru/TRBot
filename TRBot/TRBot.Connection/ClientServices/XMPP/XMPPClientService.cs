﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Threading.Tasks;
using TRBot.Utilities.Logging;
using TRBot.Events;
using SharpXMPP;
using SharpXMPP.XMPP;
using SharpXMPP.XMPP.Client.Elements;
using SharpXMPP.XMPP.Client.MUC.Bookmarks.Elements;

namespace TRBot.Connection.XMPP
{
    /// <summary>
    /// Read inputs from an XMPP channel.
    /// </summary>
    public class XMPPClientService : IClientService
    {
        private const string LOG_PREFIX_STRING = "[" + ClientServiceNames.XMPP_SERVICE + "] ";

        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        public bool IsInitialized => Initialized;

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        public bool IsConnected => Client != null && ConnectedToServer == true;

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        public bool CanSendMessages => IsConnected == true;

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        public IBotMessageHandler MsgHandler { get; private set; } = null;

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        public IMessageSettings MsgSettings { get; private set; } = null;

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        public ICommandSettings CmdSettings { get; private set; } = null;

        private bool Initialized = false;
        private bool Connecting = false;
        private bool ConnectedToServer = false;

        private XMPPLoginSettings LoginSettings = null;

        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        private XmppConnection Client = null;

        private readonly string XmppServiceName = ClientServiceNames.XMPP_SERVICE;

        public XMPPClientService(XMPPLoginSettings loginSettings,
            ITRBotLogger logger, IEventDispatcher evtDispatcher, IBotMessageHandler msgHandler,
            IMessageSettings msgSettings, ICommandSettings cmdSettings)
        {
            LoginSettings = loginSettings;

            Logger = logger;
            EvtDispatcher = evtDispatcher;
            MsgHandler = msgHandler;
            MsgSettings = msgSettings;
            CmdSettings = cmdSettings;
        }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        public void Initialize()
        {
            InitClient();

            MsgHandler.SetClientService(this);

            EvtDispatcher.RegisterEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.RegisterEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.RegisterEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.RegisterEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.RegisterEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.RegisterEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND);

            EvtDispatcher.AddListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);

            Initialized = true;
        }

        /// <summary>
        /// Connects the client.
        /// </summary>
        public void Connect()
        {
            AwaitConnected().ConfigureAwait(false);
        }

        private async Task AwaitConnected()
        {
            if (Connecting == true)
            {
                return;
            }

            Connecting = true;
            
            await Client.ConnectAsync();
            
            Connecting = false;

            Logger.Information($"Connected to {LoginSettings.Domain}!");
        }

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        public void Disconnect()
        {
            if (Client == null || IsConnected == false)
            {
                Logger.Error("Attempting to disconnect from XMPP while not connected!");
                return;
            }

            //Close the client
            Client.Disconnect();
        }

        /// <summary>
        /// Send a message through the client.
        /// </summary>
        public void ProcessAndSendMessage(IServiceMessage serviceMessage)
        {
            if (string.IsNullOrEmpty(serviceMessage.Message) == true)
            {
                Logger.Warning($"Empty message in {nameof(ProcessAndSendMessage)}!");
                return;
            }

            string sentMessage = serviceMessage.Message;

            if (serviceMessage.OverridePrefix == true)
            {
                sentMessage = serviceMessage.CustomPrefix + serviceMessage.Message;
            }
            else if (string.IsNullOrEmpty(MsgSettings.MessagePrefix) == false)
            {
                sentMessage = MsgSettings.MessagePrefix + serviceMessage.Message;
            }

            //Send to the given room by default
            XMPPMessage message = new XMPPMessage()
                {
                    To = new JID(LoginSettings.RoomName),
                    Text = sentMessage,
                };

            //Send to a specific user if it's a whisper message
            //The recipient must be the bare or full JID in the group chat
            if (serviceMessage is WhisperMessage whisperMessage)
            {
                message.To = new JID(whisperMessage.RecipientName);

                //Set private chat attribute if in multi-user chat
                message.SetAttributeValue(XMPPConstants.MSG_TYPE_ATTRIBUTE_NAME, XMPPConstants.PRIVATE_CHAT_MSG_ATTRIBUTE);
            }
            else
            {
                if (LoginSettings.RoomType == XMPPRoomTypes.MUC)
                {
                    //Set group chat attribute if in multi-user chat
                    message.SetAttributeValue(XMPPConstants.MSG_TYPE_ATTRIBUTE_NAME, XMPPConstants.GROUP_CHAT_MSG_ATTRIBUTE);
                }
                else
                {
                    //Set private chat attribute if not in multi-user chat
                    message.SetAttributeValue(XMPPConstants.MSG_TYPE_ATTRIBUTE_NAME, XMPPConstants.PRIVATE_CHAT_MSG_ATTRIBUTE);
                }
            }

            //Send the message
            Client.Send(message);

            if (MsgSettings.LogToLogger == true)
            {
                Logger.Log(serviceMessage.LogLevel, LOG_PREFIX_STRING + sentMessage);
            }

            var outgoingArgs = new EvtOutgoingBotMessageArgs()
            {
                ServiceName = XmppServiceName,
                UserMessage = new EvtUserMsgData(string.Empty, Client.Jid.User, sentMessage)
            };

            EvtDispatcher.DispatchEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE, outgoingArgs);
        }

        public void Dispose()
        {
            Disconnect();

            MsgHandler.CleanUp();

            DisposeClient();

            ConnectedToServer = false;

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.COMMAND);

            EvtDispatcher.RemoveListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);
        }

        private void InitClient()
        {
            string jidStr = XMPPHelpers.GetJIDString(LoginSettings.Username, LoginSettings.Domain, LoginSettings.Resource);
            
            if (LoginSettings.ConnectionType == XMPPConnectionTypes.TCP)
            {
                Client = new XmppClient(new JID(jidStr), LoginSettings.Password, false);
            }
            else
            {
                Client = new XmppWebSocketConnection(new JID(jidStr), LoginSettings.Password,
                    LoginSettings.WebSocketSettings.WebsocketUri, false);
            }

            Client.SignedIn -= OnSignedIn;
            Client.SignedIn += OnSignedIn;

            Client.Message -= OnMessage;
            Client.Message += OnMessage;

            Client.ConnectionFailed -= OnConnectionFailed;
            Client.ConnectionFailed += OnConnectionFailed;

            Client.ConnectionClosed -= OnConnectionClosed;
            Client.ConnectionClosed += OnConnectionClosed;
        }

        private void DisposeClient()
        {
            if (Client != null)
            {
                Client.SignedIn -= OnSignedIn;
                Client.Message -= OnMessage;
                Client.ConnectionFailed -= OnConnectionFailed;

                Client.Dispose();
            }

            Client = null;
        }

        private void OnSignedIn(object sender, SignedInArgs args)
        {
            Logger.Information($"Signed into {LoginSettings.Domain} as {args.Jid}!");
            
            ConnectedToServer = true;

            if (LoginSettings.RoomType == XMPPRoomTypes.MUC)
            {
                //Password must be null to specify no password
                BookmarkedConference bookmarkedConference = new BookmarkedConference()
                    {
                        JID = new JID(LoginSettings.RoomName),
                        Nick = LoginSettings.Username,
                        Password = string.IsNullOrEmpty(LoginSettings.MUCSettings.RoomPassword) == true
                            ? null : LoginSettings.MUCSettings.RoomPassword
                    };

                Logger.Information($"Attempting to join MUC room {bookmarkedConference.JID}");

                Client.BookmarkManager.Join(bookmarkedConference);
            }

            EvtConnectedArgs connectedArgs = new EvtConnectedArgs()
            {
                ServiceName = XmppServiceName,
                BotUsername = LoginSettings.Username
            };

            EvtDispatcher.DispatchEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, connectedArgs);
        }

        private void OnMessage(XmppConnection sender, XMPPMessage msg)
        {
            //Changes in presence status send messages with empty text content on some clients
            //This includes changes in typing status (typing, paused typing, focused on conversation, etc.)
            //If there's no text content, it should be safe to presume it's a presence message
            if (string.IsNullOrWhiteSpace(msg.Text) == true)
            {
                return;
            }

            string userName = msg.From.User;

            //For multi-user chats, the nickname of the user is in the resource
            //Non-anonymous multi-user chats do have the full JID but in a different element
            if (LoginSettings.RoomType == XMPPRoomTypes.MUC)
            {
                userName = msg.From.Resource;
            }

            //Ignore the bot itself
            if (userName == Client.Jid.User)
            {
                Logger.Debug($"Username is the same as the bot's name - ignoring message");
                return;
            }

            //This can happen for room history being pulled up in multi-user chats
            //It has no resource, as the message is being sent by the JID of the room itself!
            if (string.IsNullOrEmpty(userName) == true)
            {
                Logger.Debug("Username null (prior room history message) - ignoring");
                return;
            }

            try
            {
                string lineWSReplaced = Utilities.Helpers.ReplaceAllWhitespaceWithSpace(msg.Text);

                //Send message event
                EvtUserMessageArgs umArgs = new EvtUserMessageArgs()
                {
                    ServiceName = XmppServiceName,
                    UserMessage = new EvtUserMsgData(msg.From.FullJid, userName, lineWSReplaced)
                };

                EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, umArgs);

                //Parse command
                EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(lineWSReplaced,
                    CmdSettings.CommandIdentifier, new EvtUserMsgData(msg.From.FullJid, userName, lineWSReplaced));
                
                if (chatCmdData != null)
                {
                    EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                    {
                        ServiceName = XmppServiceName,
                        Command = chatCmdData
                    };

                    EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
                }
            }
            catch (Exception e)
            {
                Logger.Error($"Error encountered in XMPP service. {e.Message}");
            }
        }

        //This event is fired in all cases, even on a normal disconnect
        private void OnConnectionFailed(object sender, ConnFailedArgs args)
        {
            Logger.Information($"{nameof(OnConnectionFailed)} called with message: \"{args.Message}\"");

            if (ConnectedToServer == false)
            {
                EvtConnectionErrorArgs evtConnectionError = new EvtConnectionErrorArgs()
                {
                    Error = new EvtErrorData(args.Message),
                    ServiceName = XmppServiceName,
                    BotUsername = LoginSettings.Username
                };

                EvtDispatcher.DispatchEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, evtConnectionError);
            }
            else
            {
                Disconnect();
            }
        }

        private void OnConnectionClosed(XmppConnection sender)
        {
            ConnectedToServer = false;

            EvtDisconnectedArgs disconnectedArgs = new EvtDisconnectedArgs()
            {
                ServiceName = XmppServiceName
            };
            
            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disconnectedArgs);
        }

        private void OnConnected(EvtConnectedArgs e)
        {
            if (e.ServiceName != XmppServiceName)
            {
                return;
            }

            Logger.Information($"Connected to XMPP server {LoginSettings.Domain} over {LoginSettings.ConnectionType}!");

            ConnectedToServer = true;
        }
    }
}
