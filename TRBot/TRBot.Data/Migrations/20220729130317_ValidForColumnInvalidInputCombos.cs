﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    public partial class ValidForColumnInvalidInputCombos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ValidFor",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ValidFor",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos");
        }
    }
}
