﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using TRBot.Permissions;
using TRBot.Input.Consoles;
using TRBot.Utilities.Logging;
using TRBot.Build;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Data
{
    /// <summary>
    /// Extensions for <see cref="IDatabaseManager{T}"/>.
    /// </summary>
    public static class IDatabaseManagerExtensions
    {
        /// <summary>
        /// Obtains a setting from the database.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="settingName">The name of the setting to retrieve.</param>
        /// <returns>A Settings object corresponding to settingName. If the setting is not found, null.</returns>
        public static Settings GetSetting(this IDatabaseManager<BotDBContext> databaseMngr, string settingName)
        {
            using (BotDBContext dbContext = databaseMngr.OpenContext())
            {
                return dbContext.SettingCollection.AsNoTracking().FirstOrDefault((set) => set.Key == settingName);
            }
        }

        /// <summary>
        /// Obtains a setting integer value from the database.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="settingName">The name of the setting to retrieve.</param>
        /// <param name="defaultVal">The default value to fallback to if not found.</param>
        /// <returns>An integer for settingName. If the setting is not found, the default value.</returns>
        public static long GetSettingInt(this IDatabaseManager<BotDBContext> databaseMngr,
            string settingName, in long defaultVal)
        {
            Settings setting = databaseMngr.GetSetting(settingName);

            return setting != null ? setting.ValueInt : defaultVal;
        }

        /// <summary>
        /// Obtains a setting string value from the database.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="settingName">The name of the setting to retrieve.</param>
        /// <param name="defaultVal">The default value to fallback to if not found.</param>
        /// <returns>A string value for settingName. If the setting is not found, the default value.</returns>
        public static string GetSettingString(this IDatabaseManager<BotDBContext> databaseMngr,
            string settingName, string defaultVal)
        {
            Settings setting = databaseMngr.GetSetting(settingName);

            return setting != null ? setting.ValueStr : defaultVal;
        }

        /// <summary>
        /// A helper method to obtain the command identifier.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <returns>The string value of the command identifier.</returns>
        public static string GetCommandIdentifier(this IDatabaseManager<BotDBContext> databaseMngr)
        {
            return databaseMngr.GetSettingString(SettingsConstants.COMMAND_IDENTIFIER, "!");
        }

        /// <summary>
        /// A helper method to obtain the name of the bot credits.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <returns>The name of the bot credits.</returns>
        public static string GetCreditsName(this IDatabaseManager<BotDBContext> databaseMngr)
        {
            return databaseMngr.GetSettingString(SettingsConstants.CREDITS_NAME, "Credits");
        }

        /// <summary>
        /// Obtains a user's external ID given a username and service name.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userName">The name of the user.</param>
        /// <param name="serviceName">The name of the service.</param>
        /// <returns>An external ID associated with the username and service. An empty string if not found.</returns>
        public static string GetUserExternalIDFromNameAndService(this IDatabaseManager<BotDBContext> databaseMngr,
            string userName, string serviceName)
        {
            if (string.IsNullOrEmpty(userName) == true || string.IsNullOrEmpty(serviceName) == true)
            {
                return string.Empty;
            }

            using (BotDBContext context = databaseMngr.OpenContext())
            {
                //Lower both the user's actual name and the input username for simpler comparison
                User user = context.Users
                    .AsNoTracking()
                    .FirstOrDefault(u => u.ServiceName == serviceName && u.Name.ToLower() == userName.ToLower());
                
                if (user != null)
                {
                    return user.ExternalID;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Obtains a user object by their ExternalID from the database.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalId">The ID of the user.</param>
        /// <returns>A user object with the given ExternalID. null if not found.</returns>
        public static User GetUserByExternalID(this IDatabaseManager<BotDBContext> databaseMngr, string userExternalId)
        {
            if (string.IsNullOrEmpty(userExternalId) == true)
            {
                return null;
            }

            using (BotDBContext context = databaseMngr.OpenContext())
            {
                return context.Users.AsNoTracking().FirstOrDefault(u => u.ExternalID == userExternalId);
            }
        }

        /// <summary>
        /// Retrieves a user's overridden default input duration, or if they don't have one, the global default input duration.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The ID of the user.</param>
        /// <returns>The user-overridden or global default input duration.</returns>
        public static long GetUserOrGlobalDefaultInputDur(this IDatabaseManager<BotDBContext> databaseMngr, string userExternalID)
        {
            if (string.IsNullOrEmpty(userExternalID) == false)
            {
                using (BotDBContext context = databaseMngr.OpenContext())
                {
                    User user = context.Users
                        .AsNoTrackingWithIdentityResolution()
                        .Include(u => u.UserAbilities).ThenInclude(ua => ua.PermAbility).FirstOrDefault(u => u.ExternalID == userExternalID);

                    //Check for a user-overridden default input duration
                    if (user != null && user.TryGetAbility(PermissionConstants.USER_DEFAULT_INPUT_DUR_ABILITY, out UserAbility defaultDurAbility) == true
                        && defaultDurAbility.IsEnabled == true)
                    {
                        return defaultDurAbility.ValueInt;
                    }
                }
            }

            //Use global max input duration
            return databaseMngr.GetSettingInt(SettingsConstants.DEFAULT_INPUT_DURATION, 200L);
        }

        /// <summary>
        /// Retrieves a user's overridden max input duration, or if they don't have one, the global max input duration.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The ID of the user.</param>
        /// <returns>The user-overridden or global max input duration.</returns>
        public static long GetUserOrGlobalMaxInputDur(this IDatabaseManager<BotDBContext> databaseMngr, string userExternalID)
        {
            if (string.IsNullOrEmpty(userExternalID) == false)
            {
                using (BotDBContext context = databaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;

                    User user = context.Users
                    .AsNoTrackingWithIdentityResolution()
                    .Include(u => u.UserAbilities).ThenInclude(ua => ua.PermAbility).FirstOrDefault(u => u.ExternalID == userExternalID);

                    //Check for a user-overridden max input duration
                    if (user != null && user.TryGetAbility(PermissionConstants.USER_MAX_INPUT_DUR_ABILITY, out UserAbility maxDurAbility) == true
                        && maxDurAbility.IsEnabled == true)
                    {
                        return maxDurAbility.ValueInt;
                    }
                }
            }

            //Use global max input duration
            return databaseMngr.GetSettingInt(SettingsConstants.MAX_INPUT_DURATION, 60000L);
        }

        /// <summary>
        /// Retrieves a user's overridden mid input delay, or if they don't have one, the global mid input delay, if it's enabled.
        /// If the found mid input delay value is 0 or lower, this returns false and returns 0 as the delay.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The external ID of the user.</param>
        /// <param name="midInputDelay">The returned mid input delay.</param>
        /// <returns>true if there is a user-defined or global mid input delay greater than 0, otherwise false.</returns>
        public static bool GetUserOrGlobalMidInputDelay(this IDatabaseManager<BotDBContext> databaseMngr, string userExternalID, out long midInputDelay)
        {
            if (string.IsNullOrEmpty(userExternalID) == false)
            {
                using (BotDBContext context = databaseMngr.OpenContext())
                {
                    User user = context.Users
                        .AsNoTrackingWithIdentityResolution()
                        .Include(u => u.UserAbilities)
                        .ThenInclude(ua => ua.PermAbility)
                        .FirstOrDefault(u => u.ExternalID == userExternalID);

                    //Check for a user-overridden mid input delay
                    if (user != null && user.TryGetAbility(PermissionConstants.USER_MID_INPUT_DELAY_ABILITY, out UserAbility midInputDelayAbility) == true
                        && midInputDelayAbility.IsEnabled == true)
                    {
                        //Get the user's overridden mid input delay
                        midInputDelay = midInputDelayAbility.ValueInt;

                        //If the mid input delay is less than 1, return false and 0 for the delay
                        if (midInputDelay < 1)
                        {
                            midInputDelay = 0L;
                            return false;
                        }

                        return true;
                    }
                }
            }

            //Try to get the global mid input delay
            //Check if the global mid input delay is enabled
            long midDelayEnabled = databaseMngr.GetSettingInt(SettingsConstants.GLOBAL_MID_INPUT_DELAY_ENABLED, 0L);
            if (midDelayEnabled <= 0)
            {
                midInputDelay = 0L;
                return false;
            }

            //Get the value
            midInputDelay = databaseMngr.GetSettingInt(SettingsConstants.GLOBAL_MID_INPUT_DELAY_TIME, 0L);

            //If it's less than 1, return false and 0 for the delay
            if (midInputDelay < 1)
            {
                midInputDelay = 0L;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Obtains a PermissionAbility from the database.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="abilityName">The name of the PermissionAbility.</param>
        /// <returns>A PermissionAbility object with the given <paramref name="abilityName">. null if not found.</returns>
        public static PermissionAbility GetPermissionAbility(this IDatabaseManager<BotDBContext> databaseMngr, string abilityName)
        {
            using (BotDBContext context = databaseMngr.OpenContext())
            {
                return context.PermAbilities.AsNoTracking().FirstOrDefault(p => p.Name == abilityName);
            }
        }

        public static DisplayRank GetUserDisplayRankAtValue(this IDatabaseManager<BotDBContext> databaseMngr, long expValue)
        {
            using (BotDBContext context = databaseMngr.OpenContext())
            {
                return context.DisplayRanks
                    .AsNoTracking()
                    .Where(dr => dr.Enabled > 0 && dr.ExpRequirement <= expValue)
                    .OrderByDescending(dr => dr.ExpRequirement)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// Obtains a copy of the current GameConsole in use.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <returns>A copy of the current GameConsole. null if there is no GameConsole.</returns>
        public static GameConsole GetCurrentConsole(this IDatabaseManager<BotDBContext> databaseMngr)
        {
            long lastConsoleID = databaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);
            GameConsole usedConsole = null;

            using (BotDBContext context = databaseMngr.OpenContext())
            {
                GameConsole lastConsole = context.Consoles.AsNoTracking().Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos).ThenInclude(ic => ic.InvalidInputs).FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            return usedConsole;
        }

        /// <summary>
        /// Fully updates a user's available abilities based on their current level.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The ID of the user to fetch.</param>
        /// <param name="newLevel">The new level the user will be set to.</param>
        /// <param name="context">The open database context.</param>
        public static void UpdateUserAutoGrantAbilities(this IDatabaseManager<BotDBContext> databaseMngr, string userExternalID)
        {
            long userPerm = (long)PermissionLevels.User;

            using (BotDBContext context = databaseMngr.OpenContext())
            {
                User user = context.Users
                    .Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                long originalLevel = user.Level;

                //First, disable all auto grant abilities the user has
                //Don't disable abilities that were given by a higher level
                //This prevents users from removing constraints imposed by moderators and such
                IEnumerable<UserAbility> abilities = user.UserAbilities.Where(p => (long)p.PermAbility.AutoGrantOnLevel >= userPerm
                        && p.GrantedByLevel <= originalLevel);

                foreach (UserAbility ability in abilities)
                {
                    ability.SetEnabledState(false);
                    ability.Expiration = null;
                    ability.GrantedByLevel = -1;
                }

                //Get all auto grant abilities up to the user's level
                IEnumerable<PermissionAbility> permAbilities =
                    context.PermAbilities.Where(p => (long)p.AutoGrantOnLevel >= userPerm
                        && (long)p.AutoGrantOnLevel <= originalLevel);

                //Console.WriteLine($"Found {permAbilities.Count()} autogrant up to level {originalLevel}");

                //Enable all of those abilities
                foreach (PermissionAbility permAbility in permAbilities)
                {
                    user.EnableAbility(permAbility);
                }

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Updates user abilities upon changing the user's level.
        /// This also changes the user's level to the new level.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The ExternalID of the user.</param>
        /// <param name="newLevel">The new level the user will be set to.</param>
        public static void AdjustUserLvlAndAbilitiesOnLevel(this IDatabaseManager<BotDBContext> databaseMngr, string userExternalID, long newLevel)
        {
            using (BotDBContext context = databaseMngr.OpenContext())
            {
                User user = context.Users.Include(u => u.UserAbilities).ThenInclude(ua => ua.PermAbility).FirstOrDefault(u => u.ExternalID == userExternalID);

                long originalLevel = user.Level;

                //Nothing to do here if the levels are the same
                if (originalLevel == newLevel)
                {
                    return;
                }

                //Disable all abilities down to the new level
                if (originalLevel > newLevel)
                {
                    //Look for all auto grant abilities that are less than or equal to the original level
                    //and greater than the new level, and disable them
                    IEnumerable<UserAbility> abilities = user.UserAbilities.Where(p => p.PermAbility.AutoGrantOnLevel >= 0
                        && (long)p.PermAbility.AutoGrantOnLevel <= originalLevel
                        && (long)p.PermAbility.AutoGrantOnLevel > newLevel);

                    foreach (UserAbility ability in abilities)
                    {
                        ability.SetEnabledState(false);
                        ability.Expiration = null;
                        ability.GrantedByLevel = -1;
                    }
                }
                //Enable all abilities up to the new level
                else if (originalLevel < newLevel)
                {
                    //Look for all auto grant abilities that are greater than the original level
                    //and less than or equal to the new level
                    IEnumerable<PermissionAbility> permAbilities =
                        context.PermAbilities.Where(p => (long)p.AutoGrantOnLevel >= 0
                            && (long)p.AutoGrantOnLevel > originalLevel && (long)p.AutoGrantOnLevel <= newLevel);

                    //Add all these abilities
                    foreach (PermissionAbility pAbility in permAbilities)
                    {
                        user.EnableAbility(pAbility);
                    }
                }

                //Set to the new level
                user.Level = newLevel;

                //Save changes
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Obtains the string representation of a DateTime object in database form.
        /// </summary>
        /// <param name="dateTime">The DateTime object to convert to a string.</param>
        /// <returns>A string from a DateTime stored in the database.</returns>
        public static string GetStrFromDateTime(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
