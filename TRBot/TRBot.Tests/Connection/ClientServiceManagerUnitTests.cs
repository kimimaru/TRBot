/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using NUnit.Framework;
using NSubstitute;
using TRBot.Connection;
using TRBot.Events;
using TRBot.Utilities.Logging;

namespace TRBot.Tests.Connection
{
    [TestFixture]
    public class ClientServiceManagerUnitTests
    {
        private IEventDispatcher EvtDispatcher = null;
        private ITRBotLogger Logger = null;

        public ClientServiceManagerUnitTests()
        {
            EvtDispatcher = Substitute.For<IEventDispatcher>();
            Logger = Substitute.For<ITRBotLogger>();
        }

        public ClientServiceManager CreateManager()
        {
            return new ClientServiceManager(Logger, EvtDispatcher);
        }

        [Test]
        public void ManagerAddsService()
        {
            var manager = CreateManager();

            IClientService service = Substitute.For<IClientService>();
            
            manager.AddClientService("test", service, false);

            Assert.That(manager.ServiceCount, Is.EqualTo(1));
        }

        [Test]
        public void ManagerRemovesService()
        {
            var manager = CreateManager();

            IClientService service = Substitute.For<IClientService>();
            
            manager.AddClientService("test", service, false);
            manager.RemoveClientService("test");

            Assert.That(manager.ServiceCount, Is.EqualTo(0));
            service.Received(1).Dispose();
        }

        [Test]
        public void ManagerGetsService()
        {
            var manager = CreateManager();

            IClientService service = Substitute.For<IClientService>();
            
            manager.AddClientService("test", service, false);
            IClientService addedService = manager.GetClientService("test");

            Assert.That(addedService, Is.EqualTo(service).And.Not.EqualTo(null));
        }

        [Test]
        public void ManagerDisposesProperly()
        {
            var manager = CreateManager();

            IClientService service = Substitute.For<IClientService>();
            IClientService service2 = Substitute.For<IClientService>();

            manager.AddClientService("test", service, false);
            manager.AddClientService("test2", service2, false);

            manager.Dispose();

            Assert.That(manager.ServiceCount, Is.EqualTo(0));
            service.Received(1).Dispose();
            service2.Received(1).Dispose();
        }

        [Test]
        public void ManagerAddThrowsOnInvalidName()
        {
            var manager = CreateManager();

            IClientService service = Substitute.For<IClientService>();
            
            Assert.Throws<ArgumentException>(() => manager.AddClientService(null, service, false));
            Assert.Throws<ArgumentException>(() => manager.AddClientService(string.Empty, service, false));
        }

        [Test]
        public void ManagerAddThrowsOnNullService()
        {
            var manager = CreateManager();

            Assert.Throws<ArgumentNullException>(() => manager.AddClientService("test", null, false));
        }

        [Test]
        public void ManagerAddThrowsOnExistingService()
        {
            var manager = CreateManager();

            IClientService service = Substitute.For<IClientService>();

            manager.AddClientService("test", service, false);

            Assert.Throws<InvalidOperationException>(() => manager.AddClientService("test", service, false)); 
        }

        [Test]
        public void ManagerRemoveThrowsOnInvalidName()
        {
            var manager = CreateManager();

            Assert.Throws<ArgumentException>(() => manager.RemoveClientService(null));
            Assert.Throws<ArgumentException>(() => manager.RemoveClientService(string.Empty));
        }

        [TestCase("service1", "service2", "service3")]
        [TestCase("Test1", "test7", "randomwordsforservicename")]
        public void ManagerReturnsCorrectServicesAndNames(params string[] serviceNames)
        {
            var manager = CreateManager();

            IClientService[] services = new IClientService[serviceNames.Length];

            for (int i = 0; i < services.Length; i++)
            {
                services[i] = Substitute.For<IClientService>();

                manager.AddClientService(serviceNames[i], services[i], false);
            }

            string[] returnedNames = manager.GetAllServiceNames();
            IClientService[] returnedServices = manager.GetAllServices();

            Assert.That(returnedNames.Length, Is.EqualTo(serviceNames.Length));
            Assert.That(returnedServices.Length, Is.EqualTo(services.Length));

            //Orders of the returned objects are not guaranteed to be the same, so check for contents
            for (int i = 0; i < serviceNames.Length; i++)
            {
                int exists = Array.IndexOf(serviceNames, returnedNames[i]);
                Assert.That(exists, Is.Not.EqualTo(-1));
            }

            for (int i = 0; i < services.Length; i++)
            {
                int exists = Array.IndexOf(services, returnedServices[i]);
                Assert.That(exists, Is.Not.EqualTo(-1));
            }
        }
    }
}
