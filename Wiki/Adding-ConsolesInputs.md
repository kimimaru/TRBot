# Adding, Removing, Modifying Consoles and Inputs
TRBot utilizes a `GameConsole` model that defines the valid inputs available for users to type to control the game. There are several [pre-configured consoles](../TRBot/TRBot.Main/ConsoleDefinitions) added to the database upon first starting up TRBot, which you can use for any game you want.

What if you want to add a new console specific to a single game, add a new input (Ex. a "toggleaudio" input for 2 linked-game GBA instances), or remove some inputs? This guide will cover all three!

## Adding a new console
First let's start with adding a new console. All we need for a new console is a name, so for this example, let's go with the name "newconsole".

We'll use TRBot's [`AddConsoleCommand`](../TRBot/TRBot.Commands/Commands/AddConsoleCommand.cs) ("!addconsole" by default) to add a new console to the database.

Type the following without quotes: "!addconsole newconsole"

Now, we should have a new game console named "newconsole". We can verify this through the [`GetSetConsoleCommand`](../TRBot/TRBot.Commands/Commands/GetSetConsoleCommand.cs) ("!console" by default), which will list all available consoles. Simply type "!console" without quotes and we can see that "newconsole" is in the list. Type "!console newconsole" to switch the active console to our new console.

After switching, use the [`InputInfoCommand`](../TRBot/TRBot.Commands/Commands/GetSetConsoleCommand.cs) ("!inputs" by default) without any arguments to see...uh oh, there are no inputs! Newly created consoles do not come with any inputs - think of a game console without a controller. We now have to define the buttons on the controller so we can actually play the console.

## Adding new inputs
Let's add inputs to the "newconsole" console. It's possible to add new inputs directly through the database, but we'll use some commands to greatly simplify the process, as they automatically link input to the console (adding directly to the database requires looking up the console ID and specifying it in the record in the **Inputs** table).

There are a few commands to help add the types of inputs you want:
- [`AddButtonInputCommand`](../TRBot/TRBot.Commands/Commands/AddButtonInputCommand.cs) (default: "!addbutton") for button inputs.
- [`AddAxisInputCommand`](../TRBot/TRBot.Commands/Commands/AddAxisInputCommand.cs) (default: "!addaxis") for axes inputs.
- [`AddBlankInputCommand`](../TRBot/TRBot.Commands/Commands/AddBlankInputCommand.cs) (default: "!addblank") for blank inputs.

Let's add our first button to this console by typing: "!addbutton newconsole a 0"

Here we added an "a" button with a button value of 0. The button value represents the value pressed on the virtual controller. So if you type "a" now, it'll press button 0!

Let's change the button value of the "a" button. We don't need to do anything different aside from supply a different value.

"!addbutton newconsole a 1"

That's right, the `AddButtonInputCommand` and will update an input if it already exists for the given console. You can modify inputs the same way as you add them! The same is true for `AddAxisInputCommand`! 

Speaking of which, let's use `AddAxisInputCommand` to add a full analog stick to this console. The X axis will be axis value 0, and the Y axis will be axis value 1. This time, we'll need to specify a little more information about each axis, including:
- The minimum axis value
- The maximum axis value
- The default axis value

Why these three? Great question - there are different types of axes out there, including control sticks and triggers. Control sticks can move in a circle, whereas triggers are linear, so these values determine how the axis behaves.

Let's go through an example.

### X axis (Left/Right)
"!addaxis newconsole left 0 0.5 0 0.5"
"!addaxis newconsole right 0 0.5 1 0.5"

Typing axis input will cause it to move between the minAxis and the maxAxis. In the case of "left", it starts at 0.5, which is in the middle of our control stick. Typing "left" will move it to 0, so in a different direction.

On the other hand, "right" shares the same axis value and will move the axis from 0.5 to 1.

Our horizontal axes are done. Next is to add the vertical axes for a full control stick.

### Y axis (Up/Down)
"!addaxis newconsole up 1 0.5 0 0.5
"!addaxis newconsole down 1 0.5 1 0.5

If you noticed, the only difference between the opposing directions on each axis of a control stick is the maximum axis value. If a user inputs "up50%", the value will be 0.25 (1/4 up), whereas "down50%" will have a of 0.75 (3/4 down). Since most game controllers work this way, it's highly recommended to follow this model for defining axes.

Now let's cover the default axis, which defines which value the axis goes to at rest - in other words, once the axis is released. In our axes above, the default axis for all inputs was 0.5, meaning they rest at the center.

Let's demonstrate a trigger input now to see how it differs:

"!addaxis newconsole lt 2 0 1 0"

The "lt" trigger on the other hand is very linear. It only goes from 0 to 1 with a rest state of 0. This makes sense, as the trigger can only move in one direction. When it's fully pressed down, the value will be 1, and when it's released, the value goes back down to 0.

### Shortcuts
While the above allows for precise configuration, most setups may not need it. `AddAxisInputCommand` includes shortcuts for common axis setups! Let's see how much simpler our axis definitions would be with them:
- "!addaxis newconsole left 0 negative"
- "!addaxis newconsole right 0 positive"
- "!addaxis newconsole up 1 negative"
- "!addaxis newconsole down 1 positive"
- "!addaxis newconsole lt 2 absolute"

The valid arguments are "negative" (left & up), "positive" (right & down), and "absolute" (triggers).

## Removing inputs
Our "newconsole" is coming along! It's got an "a" button, a full analog stick with "up", "down", "left", and "right", and an "lt" trigger. Say we decided on a different set of buttons and no longer want the "a" button. How do we remove it?

Simply use the [`RemoveInputCommand`](../TRBot/TRBot.Commands/Commands/RemoveInputCommand.cs) ("!removeinput" by default).

Type the following: "!removeinput newconsole a"

That's it! The `RemoveInputCommand` simply takes in the console and input name as arguments. If you view all inputs with "!inputs", the "a" button should now be removed.

# Uses
There are many uses for custom consoles and inputs:

* Add a new console catered to a specific game you want to play. This is quite common with PC games!
* Create buttons for hotkeys, such as a "toggleaudio" input that mutes/unmutes audio on an emulator.
* Implement a "celebrate" input, used in a script through software such as AntiMicroX or AutoHotkey, that plays a video on the stream.
* ...and much more! Keep exploring and share your findings!
