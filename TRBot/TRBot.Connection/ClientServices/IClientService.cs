﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Connection
{
    /// <summary>
    /// Handles client interaction with a service.
    /// </summary>
    /// <remarks>Examples of such services are the local machine or a WebSocket.
    /// The goal of this interface is to allow integration with any service.
    /// </remarks>
    public interface IClientService : IDisposable
    {
        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        bool IsInitialized { get; }

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        bool CanSendMessages { get; }

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        IBotMessageHandler MsgHandler { get; }

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        IMessageSettings MsgSettings { get; }

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        ICommandSettings CmdSettings { get; }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Connects the client.
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Processes a message and sends it through the client.
        /// </summary>
        /// <param name="serviceMessage">The message to process and send.</param>
        void ProcessAndSendMessage(IServiceMessage serviceMessage);
    }
}
