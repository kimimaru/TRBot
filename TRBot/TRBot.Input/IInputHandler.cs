﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using TRBot.Input.Parsing;
using TRBot.Input.Consoles;
using TRBot.Input.VirtualControllers;

namespace TRBot.Input
{
    /// <summary>
    /// Interface for input handlers that carry out input sequences.
    /// </summary>
    public interface IInputHandler
    {
        /// <summary>
        /// An event invoked when input handling is enabled or disabled.
        /// </summary>
        event OnInputHandlingToggled InputHandlingToggledEvent;

        /// <summary>
        /// An event invoked when all inputs are halted.
        /// </summary>
        event OnInputsHalted InputsHaltedEvent;

        /// <summary>
        /// Whether inputs are currently enabled.
        /// </summary>
        bool InputsEnabled { get; }

        /// <summary>
        /// Whether inputs are currently halted.
        /// </summary>
        bool InputsHalted { get; }

        /// <summary>
        /// The current number of running input threads.
        /// </summary>
        int RunningInputCount { get; }

        /// <summary>
        /// Toggles input handling on the input handler. If turned off, the input handler should not process inputs.
        /// </summary>
        /// <param name="enabled">If true, enables input handling, otherwise disables it.</param>
        void ToggleInputHandling(bool enabled);

        /// <summary>
        /// Allows new inputs to be processed.
        /// </summary>
        void ResumeRunningInputs();

        /// <summary>
        /// Stops all ongoing inputs, waiting until all inputs are completely stopped, then keeps inputs halted.
        /// The caller is responsible for calling <see cref="ResumeRunningInputs" /> to resume inputs.
        /// </summary>
        Task StopAndHaltAllInputs();

        /// <summary>
        /// Stops all ongoing inputs, waiting until all inputs are completely stopped, then resumes them.
        /// </summary>
        Task StopThenResumeAllInputs();

        /// <summary>
        /// Carries out a set of inputs.
        /// </summary>
        /// <param name="inputList">A list of lists of inputs to execute.</param>
        Task CarryOutInput(List<List<ParsedInput>> inputList, IGameConsole currentConsole, IVirtualControllerManager vcManager);
    }
}
