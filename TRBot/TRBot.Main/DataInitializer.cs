﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using TRBot.Build;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Input.Consoles;
using TRBot.Utilities.Logging;
using TRBot.Permissions;

namespace TRBot.Main
{
    /// <summary>
    /// Interface for initializing data.
    /// </summary>
    public class DataInitializer : IDataInitializer
    {
        private IDatabaseManager<BotDBContext> DatabaseMngr;
        private ITRBotLogger Logger;

        public DataInitializer(IDatabaseManager<BotDBContext> databaseManager, ITRBotLogger logger)
        {
            DatabaseMngr = databaseManager;
            Logger = logger;
        }

        /// <summary>
        /// Initializes default values for data into the database.
        /// </summary>
        /// <param name="defaultDataProvider">The default data provider.</param>
        /// <returns>An int representing the number of entries added into the database context.</returns>
        public int InitDefaultData(IDefaultDataProvider defaultDataProvider)
        {
            int entriesAdded = 0;

            /* First check if the defaults should actually be initialized
             * This depends on the force init setting: initialize defaults if it's either missing or true
             * If the data version is less than the bot version, then set force init to true
             */
            string dataVersionStr = string.Empty;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check data version
                Settings dataVersionSetting = context.GetSetting(SettingsConstants.DATA_VERSION_NUM);

                //Add the version to the lowest number if the entry doesn't exist
                //This will force an init
                if (dataVersionSetting == null)
                {
                    dataVersionSetting = new Settings(SettingsConstants.DATA_VERSION_NUM, "0.0.0", 0L);
                    context.SettingCollection.Add(dataVersionSetting);

                    context.SaveChanges();

                    entriesAdded++;
                    Logger.Information($"Data version setting \"{SettingsConstants.DATA_VERSION_NUM}\" not found in database - adding.");
                }

                dataVersionStr = dataVersionSetting.ValueStr;
            }

            //Compare versions
            Version dataVersion = new Version(dataVersionStr);
            Version curVersion = new Version(BuildInfo.GitBaseTag);

            int versionResult = dataVersion.CompareTo(curVersion);
            long forceInit = 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings forceInitSetting = context.GetSetting(SettingsConstants.FORCE_INIT_DEFAULTS);
                if (forceInitSetting == null)
                {
                    forceInitSetting = new Settings(SettingsConstants.FORCE_INIT_DEFAULTS, string.Empty, 1L);
                    context.SettingCollection.Add(forceInitSetting);
                    
                    context.SaveChanges();

                    entriesAdded++;
                    Logger.Information($"Force initialize setting \"{SettingsConstants.FORCE_INIT_DEFAULTS}\" not found in database; adding.");
                }

                forceInit = forceInitSetting.ValueInt;
            }

            string newDataVersion = dataVersionStr;

            //The bot version is greater, so update the data version number and set it to force init
            if (versionResult < 0)
            {
                Logger.Information($"Data version {dataVersionStr} is less than bot version {BuildInfo.GitBaseTag}. Updating version number and forcing database initialization for missing entries.");
                Logger.Information($"IMPORTANT: Run the UpdateEveryoneAbilitiesCommand (default: \"!updateeveryoneabilities\") to update all user abilities! Failure to do so may result in users being unable to perform actions that now require new permissions.");
                newDataVersion = BuildInfo.GitBaseTag;

                forceInit = 1L;
            }
            //If the data version is greater than the bot, we should let them know
            else if (versionResult > 0)
            {
                Logger.Information($"Data version {dataVersionStr} is greater than bot version {BuildInfo.GitBaseTag}. Ensure you're running the correct version of TRBot to avoid potential issues.");
            }

            //Initialize if we're told to
            if (forceInit > 0)
            {
                Logger.Information($"{SettingsConstants.FORCE_INIT_DEFAULTS} is true; initializing missing defaults in database.");

                //Tell it to no longer force initializing
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings forceInitSetting = context.GetSetting(SettingsConstants.FORCE_INIT_DEFAULTS);
                    forceInitSetting.ValueInt = 0;

                    context.SaveChanges();
                }

                //Check all settings with the defaults
                List<Settings> settings = defaultDataProvider.GetDefaultSettings();
                for (int i = 0; i < settings.Count; i++)
                {
                    Settings setting = settings[i];
                        
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        //See if the setting exists
                        Settings foundSetting = context.SettingCollection.FirstOrDefault((set) => set.Key == setting.Key);

                        if (foundSetting == null)
                        {
                            //Default setting does not exist, so add it
                            context.SettingCollection.Add(setting);

                            context.SaveChanges();

                            entriesAdded++;
                        }
                    }
                }

                List<CommandData> cmdData = defaultDataProvider.GetDefaultCommands();
                for (int i = 0; i < cmdData.Count; i++)
                {
                    CommandData commandData = cmdData[i];
                        
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        //See if the command data exists
                        CommandData foundCommand = context.Commands.FirstOrDefault((cmd) => cmd.Name == commandData.Name);

                        if (foundCommand == null)
                        {
                            //Default command does not exist, so add it
                            context.Commands.Add(commandData);

                            context.SaveChanges();

                            entriesAdded++;
                        }
                    }
                }

                List<PermissionAbility> permAbilities = defaultDataProvider.GetDefaultPermAbilities();
                for (int i = 0; i < permAbilities.Count; i++)
                {
                    PermissionAbility permAbility = permAbilities[i];
                        
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        //See if the command data exists
                        PermissionAbility foundPerm = context.PermAbilities.FirstOrDefault((pAb) => pAb.Name == permAbility.Name);

                        if (foundPerm == null)
                        {
                            //Default permission ability does not exist, so add it
                            context.PermAbilities.Add(permAbility);

                            context.SaveChanges();

                            entriesAdded++;
                        }
                    }
                }

                List<RoutineData> routData = defaultDataProvider.GetDefaultRoutines();
                for (int i = 0; i < routData.Count; i++)
                {
                    RoutineData routineData = routData[i];
                        
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        //See if the command data exists
                        RoutineData foundRoutine = context.Routines.FirstOrDefault((rout) => rout.Name == routineData.Name);

                        if (foundRoutine == null)
                        {
                            //Default command does not exist, so add it
                            context.Routines.Add(routineData);

                            context.SaveChanges();

                            entriesAdded++;
                        }
                    }
                }

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //Add services if there are none
                    if (context.Services.Count() == 0)
                    {
                        List<ServiceData> serviceData = defaultDataProvider.GetDefaultServices();
                        for (int i = 0; i < serviceData.Count; i++)
                        {
                            context.Services.Add(serviceData[i]);
                            
                            entriesAdded++;
                        }

                        context.SaveChanges();
                    }
                }

                long firstLaunchVal = 0L;

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings firstLaunchSetting = context.GetSetting(SettingsConstants.FIRST_LAUNCH);
                    if (firstLaunchSetting == null)
                    {
                        firstLaunchSetting = new Settings(SettingsConstants.FIRST_LAUNCH, string.Empty, 1L);
                        context.SettingCollection.Add(firstLaunchSetting);

                        context.SaveChanges();

                        entriesAdded++;
                    }

                    firstLaunchVal = firstLaunchSetting.ValueInt;
                }

                //Populate default custom messages if there are none
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    if (context.CustomMessages.Count() == 0)
                    {
                        List<CustomMessage> customMessages = defaultDataProvider.GetDefaultCustomMessages();
                        for (int i = 0; i < customMessages.Count; i++)
                        {
                            context.CustomMessages.Add(customMessages[i]);

                            entriesAdded++;
                        }

                        context.SaveChanges();
                    }
                }

                //Do these things upon first launching the bot
                if (firstLaunchVal > 0)
                {
                    //Populate default consoles - this will also populate inputs
                    List<GameConsole> consoleData = defaultDataProvider.GetDefaultConsoles();
                    
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        bool addedOne = false;

                        for (int i = 0; i < consoleData.Count; i++)
                        {
                            GameConsole console = consoleData[i];

                            //See if the console exists
                            GameConsole foundConsole = context.Consoles.FirstOrDefault((c) => c.Name == console.Name);
                            
                            if (foundConsole == null)
                            {
                                //This console isn't in the database, so add it
                                context.Consoles.Add(console);
                                
                                addedOne = true;

                                entriesAdded++;
                            }
                        }

                        if (addedOne == true)
                        {
                            context.SaveChanges();
                        }
                    }

                    //Populate default user display ranks if empty
                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        if (context.DisplayRanks.Count() == 0)
                        {
                            List<DisplayRank> displayRankData = defaultDataProvider.GetDefaultDisplayRanks();

                            for (int i = 0; i < displayRankData.Count; i++)
                            {
                                context.DisplayRanks.Add(displayRankData[i]);

                                entriesAdded++;
                            }

                            context.SaveChanges();
                        }
                    }

                    using (BotDBContext context = DatabaseMngr.OpenContext())
                    {
                        Settings firstLaunchSetting = context.GetSetting(SettingsConstants.FIRST_LAUNCH);

                        //Set first launch to 0
                        firstLaunchSetting.ValueInt = 0;

                        context.SaveChanges();
                    }
                }

                //Apply version-specific database changes when migrating from an older version
                if (versionResult < 0 && dataVersion.Major > 0)
                {
                    Logger.Information($"Applying version-specific database adjustments...");

                    MakeVersionSpecificDBAdjustments(dataVersion, curVersion, defaultDataProvider);
                }
            }

            //Update the data version string if it's different
            if (dataVersionStr != newDataVersion)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    Settings dataVersionSetting = context.GetSetting(SettingsConstants.DATA_VERSION_NUM);
                    dataVersionSetting.ValueStr = newDataVersion;

                    context.SaveChanges();

                    Logger.Information($"Successfully updated data version from \"{dataVersionStr}\" to \"{newDataVersion}\"!");
                }
            }

            return entriesAdded;
        }

        private void MakeVersionSpecificDBAdjustments(Version oldTRBotVersion, Version newTRBotVersion,
            IDefaultDataProvider defaultDataProvider)
        {
            if (newTRBotVersion.Major >= 2 && newTRBotVersion.Minor >= 5)
            {
                MakeVersion2_5DBAdjustments(defaultDataProvider);
            }

            if (newTRBotVersion.Major >= 2 && newTRBotVersion.Minor >= 7)
            {
                MakeVersion2_7DBAdjustments(defaultDataProvider);
            }

            if (newTRBotVersion.Major >= 2 && newTRBotVersion.Minor >= 8)
            {
                MakeVersion2_8DBAdjustments(defaultDataProvider);
            }
        }

        private void MakeVersion2_8DBAdjustments(IDefaultDataProvider defaultDataProvider)
        {
            Logger.Information($"Adjusting data after upgrading to v2.8 or greater.");

            const string nameSpaceHeader = "TRBot.Commands.";

            //Remove commands that no longer exist
            string[] removedCommands = new string[]
            {
                $"{nameSpaceHeader}AddInputCommand",
            };

            using (var context = DatabaseMngr.OpenContext())
            {
                for (int i = 0; i < removedCommands.Length; i++)
                {
                    string commandClassName = removedCommands[i];
                    CommandData remCmd = context.Commands.FirstOrDefault(c => c.ClassName == commandClassName);
                    if (remCmd != null)
                    {
                        string truncatedClassName = remCmd.ClassName.Remove(0, nameSpaceHeader.Length);
                        Logger.Information($"Removed \"{remCmd.Name}\" command from the database because \"{truncatedClassName}\" has been removed from this version of TRBot.");

                        context.Remove(remCmd);
                    }
                }

                context.SaveChanges();
            }

            Logger.Information($"Finished v2.8 data adjustments!");
        }

        private void MakeVersion2_7DBAdjustments(IDefaultDataProvider defaultDataProvider)
        {
            Logger.Information($"Adjusting data after upgrading to v2.7 or greater.");

            const string nameSpaceHeader = "TRBot.Commands.";

            //Remove commands that no longer exist
            string[] removedCommands = new string[]
            {
                $"{nameSpaceHeader}AverageCreditsCommand",
                $"{nameSpaceHeader}OptStatsCommand",
                $"{nameSpaceHeader}IgnoreMemesCommand",
                $"{nameSpaceHeader}HighFiveCommand",
                $"{nameSpaceHeader}InspirationCommand",
                $"{nameSpaceHeader}MedianCreditsCommand",
                $"{nameSpaceHeader}RandomNumberCommand",
                $"{nameSpaceHeader}ReverseCommand",
                $"{nameSpaceHeader}SayCommand",
                $"{nameSpaceHeader}SlotsCommand",
                $"{nameSpaceHeader}TimeCommand",
            };

            using (var context = DatabaseMngr.OpenContext())
            {
                for (int i = 0; i < removedCommands.Length; i++)
                {
                    string commandClassName = removedCommands[i];
                    CommandData remCmd = context.Commands.FirstOrDefault(c => c.ClassName == commandClassName);
                    if (remCmd != null)
                    {
                        string truncatedClassName = remCmd.ClassName.Remove(0, nameSpaceHeader.Length);
                        Logger.Information($"Removed \"{remCmd.Name}\" command from the database because \"{truncatedClassName}\" has been removed from this version of TRBot.");

                        context.Remove(remCmd);
                    }
                }

                context.SaveChanges();
            }

            //Apply all command categories to existing commands that correspond with the defaults
            List<CommandData> defaultCmds = defaultDataProvider.GetDefaultCommands();
            var defaultCommandDict = defaultCmds
                .ToDictionary(c => c.Name, c => c.Category);
            
            using (var context = DatabaseMngr.OpenContext())
            {
                foreach (CommandData cmdData in context.Commands)
                {
                    if (defaultCommandDict.TryGetValue(cmdData.Name, out CommandCategories categories) == true)
                    {
                        Logger.Information($"Setting the category of \"{cmdData.Name}\" to \"{categories}\"");
                        cmdData.Category = categories;
                    }
                }

                context.SaveChanges();
            }

            //Set the ValueStr of the default command list command to the value in the defaults
            using (var context = DatabaseMngr.OpenContext())
            {
                //Make sure it's still the same type of command as the default
                CommandData defaultHelpCmd = defaultCmds.FirstOrDefault(c => c.Name == "help");

                //This can be null if the default commands changed in a later version
                if (defaultHelpCmd != null)
                {
                    CommandData helpCmd = context.Commands
                        .FirstOrDefault(c => c.ClassName == defaultHelpCmd.ClassName && string.IsNullOrEmpty(c.ValueStr) == true);

                    if (helpCmd != null && defaultHelpCmd != null)
                    {
                        helpCmd.ValueStr = defaultHelpCmd.ValueStr;
                        context.SaveChanges();
                        Logger.Information($"Set the {nameof(CommandData.ValueStr)} of the \"{helpCmd.Name}\" command to \"{defaultHelpCmd.ValueStr}\"");
                    }
                }
            }

            //Remove settings that are no longer relevant
            string[] removedSettings = new string[]
            {
                "slots_blank_emote",
                "slots_cherry_emote",
                "slots_plum_emote",
                "slots_watermelon_emote",
                "slots_orange_emote",
                "slots_lemon_emote",
                "slots_bar_emote",
            };

            Logger.Information($"Checking for removing redundant settings...");

            using (var context = DatabaseMngr.OpenContext())
            {
                for (int i = 0; i < removedSettings.Length; i++)
                {
                    string settingKey = removedSettings[i];
                    Settings setting = context.SettingCollection.FirstOrDefault(s => s.Key == settingKey);
                    if (setting != null)
                    {
                        Logger.Information($"Removed setting \"{setting.Key}\" from the database because it has been removed from this version of TRBot.");
                        
                        context.SettingCollection.Remove(setting);
                    }
                }

                context.SaveChanges();
            }

            using (var context = DatabaseMngr.OpenContext())
            {
                //Remove the slots ability since the game no longer exists
                PermissionAbility slotsAbility = context.PermAbilities.FirstOrDefault(pa => pa.Name == "slots");
                if (slotsAbility != null)
                {
                    Logger.Information($"Removed permission ability \"{slotsAbility.Name}\" from the database because the slots game has been removed from this version of TRBot.");

                    context.SaveChanges();
                }
            }

            Logger.Information($"Finished v2.7 data adjustments!");
        }

        private void MakeVersion2_5DBAdjustments(IDefaultDataProvider defaultDataProvider)
        {
            Logger.Information($"Adjusting data after upgrading to v2.5 or greater.");

            //Remove the bingo command since it no longer exists
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                CommandData bingoCmd = context.Commands.FirstOrDefault(c => c.ClassName == "TRBot.Commands.BingoCommand");

                if (bingoCmd != null)
                {
                    Logger.Information($"Removed \"{bingoCmd.Name}\" command from the database. \"BingoCommand\" has been removed from this version of TRBot.");

                    context.Remove(bingoCmd);
                    context.SaveChanges();
                }
            }

            //Remove the "reconnect" routine since reconnect routines are now created dynamically
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Remove only if it has the exact name and class name for the previous default
                RoutineData reconnectRoutineData = context.Routines.FirstOrDefault(r => r.Name == "reconnect");
                if (reconnectRoutineData != null
                    && reconnectRoutineData.ClassName == "TRBot.Routines.ReconnectRoutine")
                {
                    Logger.Information($"Removed \"reconnect\" routine from the database. Reconnect routines are dynamically added for each service as of v2.5.");

                    context.Remove(reconnectRoutineData);
                    context.SaveChanges();
                }
            }

            //Copy the value of event dispatcher settings to the new ones
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                const string evtDispatcherEnabledStr = "event_dispatcher_enabled";

                Settings evtDispatchEnabledSetting = context.GetSetting(evtDispatcherEnabledStr);
                if (evtDispatchEnabledSetting != null)
                {
                    Settings evtDispatchExtraFeaturesSetting = context.GetSetting(SettingsConstants.EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED);
                    if (evtDispatchExtraFeaturesSetting != null)
                    {
                        evtDispatchExtraFeaturesSetting.ValueInt = evtDispatchEnabledSetting.ValueInt;

                        context.SaveChanges();

                        Logger.Information($"Applied the value of the deprecated \"{evtDispatcherEnabledStr}\" setting to the new \"{SettingsConstants.EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED}\" setting.");
                    }
                }
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                const string evtDispatcherPathStr = "event_dispatcher_path";

                Settings evtDispatchPathSetting = context.GetSetting(evtDispatcherPathStr);
                if (evtDispatchPathSetting != null)
                {
                    Settings evtDispatchWSPathSetting = context.GetSetting(SettingsConstants.EVENT_DISPATCHER_WEBSOCKET_PATH);
                    if (evtDispatchWSPathSetting != null)
                    {
                        evtDispatchWSPathSetting.ValueStr = evtDispatchPathSetting.ValueStr;
                        
                        context.SaveChanges();

                        Logger.Information($"Applied the value of the deprecated \"{evtDispatcherPathStr}\" setting to the new \"{SettingsConstants.EVENT_DISPATCHER_WEBSOCKET_PATH}\" setting.");
                    }
                }
            }

            //Enable only the service defined by the legacy client service setting if present
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                const string clientServiceTypeStr = "client_service_type";

                Settings csTypeSetting = context.GetSetting(clientServiceTypeStr);
                string csTypeName = string.Empty;
                
                //Get the name of the service
                if (csTypeSetting != null)
                {
                    if (csTypeSetting.ValueInt == 1)
                    {
                        csTypeName = ClientServiceNames.TWITCH_SERVICE;
                    }
                    else if (csTypeSetting.ValueInt == 2)
                    {
                        csTypeName = ClientServiceNames.WEBSOCKET_SERVICE;
                    }
                    else
                    {
                        csTypeName = ClientServiceNames.TERMINAL_SERVICE;
                    }
                }
    
                if (string.IsNullOrEmpty(csTypeName) == false)
                {
                    foreach (ServiceData serveData in context.Services)
                    {
                        //Check if the service name matches the client service setting and enable only that service
                        //This will be removed along with the client service setting in a future release
                        if (serveData.ServiceName == csTypeName)
                        {
                            Logger.Information($"Found deprecated \"{clientServiceTypeStr}\" setting matching \"{serveData.ServiceName}\" - enabling only this service and deactivating all others.");
                            serveData.Enabled = 1L;
                        }
                        else
                        {
                            serveData.Enabled = 0L;
                        }
                    }

                    context.SaveChanges();
                }
            }

            Logger.Information($"Finished v2.5 data adjustments!");
        }
    }
}
