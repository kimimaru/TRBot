﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Routines
{
    /// <summary>
    /// A routine that grants users credits for participating in chat after some time.
    /// </summary>
    public class CreditsGiveRoutine : BaseRoutine
    {
        private DateTime CurCreditsTime;

        private readonly Dictionary<string, bool> UsersTalkedExternalIDs = new Dictionary<string, bool>();

        public CreditsGiveRoutine()
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            EvtDispatcher.AddListener<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, MessageReceived);

            CurCreditsTime = DateTime.UtcNow;
        }

        public override void CleanUp()
        {
            EvtDispatcher.RemoveListener<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, MessageReceived);

            base.CleanUp();
        }

        public override void UpdateRoutine(in DateTime currentTimeUTC)
        {
            TimeSpan creditsDiff = currentTimeUTC - CurCreditsTime;

            long creditsTimeMS = DatabaseMngr.GetSettingInt(SettingsConstants.CREDITS_GIVE_TIME, -1L);

            //Don't do anything if the credits time is less than 0
            if (creditsTimeMS < 0L)
            {
                CurCreditsTime = currentTimeUTC;
                return;
            }

            long creditsGiveAmount = DatabaseMngr.GetSettingInt(SettingsConstants.CREDITS_GIVE_AMOUNT, 100L);

            //Check if the time is surpassed
            if (creditsDiff.TotalMilliseconds >= creditsTimeMS)
            {
                string[] talkedExternalIDs = UsersTalkedExternalIDs.Keys.ToArray();
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    for (int i = 0; i < talkedExternalIDs.Length; i++)
                    {
                        //Add to each user's credits
                        User user = context.GetUserByExternalID(talkedExternalIDs[i]);

                        //This null check is here in case the user is no longer in the database (Ex. manually deleted)
                        if (user != null)
                        {
                            user.Stats.Credits += creditsGiveAmount;
                        }
                    }

                    context.SaveChanges();
                }

                UsersTalkedExternalIDs.Clear();

                CurCreditsTime = currentTimeUTC;
            }
        }

        private void MessageReceived(EvtUserMessageArgs e)
        {
            string userExternalID = e.UserMessage.UserExternalID;

            //Check if the user talked before
            if (UsersTalkedExternalIDs.ContainsKey(userExternalID) == true)
            {
                return;
            }
            
            long creditsTimeMS = DatabaseMngr.GetSettingInt(SettingsConstants.CREDITS_GIVE_TIME, -1L);

            //Don't do anything if the credits time is less than 0
            if (creditsTimeMS < 0L)
            {
                return;
            }

            //If so, check if they're in the database and opted into stats, then add them for gaining credits
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.Users
                    .AsNoTracking()
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);

                if (user != null && user.HasConsentToDataOption(UserDataConsentOptions.Stats) == true)
                {
                    UsersTalkedExternalIDs.Add(userExternalID, true);
                }
            }
        }
    }
}
