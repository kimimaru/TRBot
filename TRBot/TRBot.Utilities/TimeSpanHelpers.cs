﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Text.RegularExpressions;

namespace TRBot.Utilities
{
    /// <summary>
    /// Various utilities and helper methods regarding TimeSpans.
    /// </summary>
    public static class TimeSpanHelpers
    {
        /// <summary>
        /// Attempts to construct a TimeSpan given a string containing values and modifiers.
        /// Examples include "30ms", "72s", "15m", "24h", "365d", and "2d40h30m2s1500ms"
        /// </summary>
        /// <param name="timeStr">A string representing the time.</param>
        /// <param name="timeSpan">A returned TimeSpan object.</param>
        /// <returns>true if the TimeSpan is successfully created from the given information, otherwise false.</returns>
        public static bool TryParseTimeModifierFromStr(string timeStr, out TimeSpan timeSpan)
        {
            const string durGroup = @"dur";
            const string modGroup = @"mod";

            //Capture all duration
            const string regexPattern = @"(?<" + durGroup + @">\d+)(?<" + modGroup + @">(d|h|ms|m|s){1})";

            MatchCollection matches = Regex.Matches(timeStr, regexPattern,
                RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

            timeSpan = TimeSpan.Zero;

            foreach (Match m in matches)
            {
                if (m.Success == false)
                {
                    timeSpan = TimeSpan.Zero;
                    return false;
                }

                //Parse number from duration group
                if (int.TryParse(m.Groups["dur"].Value, out int timeVal) == false)
                {
                    timeSpan = TimeSpan.Zero;
                    return false;
                }
                
                //Parse modifier
                TimeReference? timeRef = GetTimeReferenceFromString(m.Groups["mod"].Value);

                if (timeRef == null)
                {
                    timeSpan = TimeSpan.Zero;
                    return false;
                }

                TimeSpan newTimeSpan = GetTimeSpanFromTimeReference(timeVal, timeRef.Value);
                timeSpan += newTimeSpan;
            }

            return true;
        }

        /// <summary>
        /// Returns a TimeReference given a string representing a time modifier.
        /// </summary>
        /// <param name="modifier">The string modifier, in the form of "d", "h", "m", "s", or "ms".</param>
        /// <returns>A TimeReference based on the given modifier. null if the modifier isn't valid.</returns>
        public static TimeReference? GetTimeReferenceFromString(string modifier)
        {
            switch (modifier)
            {
                case "ms": return TimeReference.Millisecond;
                case "s": return TimeReference.Second;
                case "m": return TimeReference.Minute;
                case "h": return TimeReference.Hour;
                case "d": return TimeReference.Day;
                default: return null;
            }
        }

        /// <summary>
        /// Returns a TimeSpan given a time value and time reference.
        /// </summary>
        /// <param name="timeVal">A value representing the amount of time.</param>
        /// <param name="timeReference">A string representing the time reference.</param>
        /// <returns>A returned TimeSpan object.</returns>
        public static TimeSpan GetTimeSpanFromTimeReference(in long timeVal, in TimeReference timeReference)
        {
            switch (timeReference)
            {
                case TimeReference.Day: return TimeSpan.FromDays(timeVal);
                case TimeReference.Hour: return TimeSpan.FromHours(timeVal);
                case TimeReference.Minute: return TimeSpan.FromMinutes(timeVal);
                case TimeReference.Second: return TimeSpan.FromSeconds(timeVal);
                case TimeReference.Millisecond: return TimeSpan.FromMilliseconds(timeVal);
                default: throw new ArgumentOutOfRangeException(nameof(timeReference), "Time reference is an invalid value.");
            }
        }
    }
}
