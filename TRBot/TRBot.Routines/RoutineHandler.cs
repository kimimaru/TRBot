﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Reflection;
using System.Linq;
using TRBot.Connection;
using TRBot.CustomCode;
using TRBot.Data;
using TRBot.Utilities.Logging;
using TRBot.WebSocket;
using TRBot.Events;
using TRBot.Input;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities;
using static TRBot.Data.DataEventObjects;

namespace TRBot.Routines
{
    /// <summary>
    /// Handles bot routines.
    /// </summary>
    public class RoutineHandler : IRoutineHandler
    {
        private readonly ConcurrentDictionary<string, BaseRoutine> BotRoutines = new ConcurrentDictionary<string, BaseRoutine>(Environment.ProcessorCount * 2, 8);

        private ITRBotLogger Logger = null;
        private IFolderPathResolver FolderPathResolver = null;
        private ICustomCodeHandler CustomCodeHandler = null;
        private IDatabaseManager<BotDBContext> DatabaseMngr = null;
        private IClientServiceManager ClientServiceMngr = null;
        private IVirtualControllerContainer VControllerContainer = null;
        private IEventDispatcher EvtDispatcher = null;
        private IWebSocketManager WebSocketMngr = null;
        private IInputHandler InputHndlr = null;

        /// <summary>
        /// Additional assemblies to look in when adding routines.
        /// This is useful if the routine's Type is outside this assembly.
        /// </summary>
        private Assembly[] AdditionalAssemblies = Array.Empty<Assembly>();

        public RoutineHandler(IDatabaseManager<BotDBContext> databaseMngr, ITRBotLogger logger,
            IFolderPathResolver folderPathResolver, ICustomCodeHandler customCodeHandler,
            IClientServiceManager clientServiceMngr, IVirtualControllerContainer vControllerContainer,
            IEventDispatcher evtDispatcher, IWebSocketManager webSocketMngr, IInputHandler inputHandler,
            Assembly[] additionalAssemblies)
        {
            DatabaseMngr = databaseMngr;
            Logger = logger;
            FolderPathResolver = folderPathResolver;
            CustomCodeHandler = customCodeHandler;
            ClientServiceMngr = clientServiceMngr;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHandler;
            AdditionalAssemblies = additionalAssemblies;
        }

        public void Initialize()
        {
            EvtDispatcher.RegisterEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA);
            EvtDispatcher.AddListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);

            PopulateRoutinesFromDB();
        }

        public void CleanUp()
        {
            EvtDispatcher.UnregisterEvent(DataEventNames.RELOAD_DATA);
            EvtDispatcher.RemoveListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);

            CleanUpRoutines();
        }

        private void CleanUpRoutines()
        {
            foreach (KeyValuePair<string, BaseRoutine> routine in BotRoutines)
            {
                routine.Value.CleanUp();
            }
        }

        private void PopulateRoutinesFromDB()
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                foreach (RoutineData routineData in context.Routines)
                {
                    AddRoutine(routineData.Name, routineData.ClassName, routineData.ValueStr, routineData.Enabled > 0, routineData.ResetOnReload > 0);
                }
            }
        }

        private void OnDataReloaded(EvtDataReloadedArgs e)
        {
            if (e.HardReload == false)
            {
                UpdateRoutinesFromDB();
            }
            else
            {
                //Clean up routines only if they hard reload
                KeyValuePair<string, BaseRoutine>[] allRoutines = BotRoutines.ToArray();
                
                for (int i = 0; i < allRoutines.Length; i++)
                {
                    KeyValuePair<string, BaseRoutine> keyValue = allRoutines[i];
                    BaseRoutine baseRoutine = keyValue.Value;

                    if (baseRoutine == null || baseRoutine.ResetOnReload == false || baseRoutine.IsRuntimeRoutine == true)
                    {
                        continue;
                    }

                    RemoveRoutine(keyValue.Key);
                }

                UpdateRoutinesFromDB();
            }
        }

        private void UpdateRoutinesFromDB()
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                List<string> encounteredRoutines = new List<string>(context.Routines.Count());

                foreach (RoutineData routineData in context.Routines)
                {
                    string routineName = routineData.Name;
                    if (BotRoutines.TryGetValue(routineName, out BaseRoutine baseRoutine) == true)
                    {
                        //Remove this routine if the type name is different so we can reconstruct it
                        if (baseRoutine.ClassName != routineData.ClassName)
                        {
                            RemoveRoutine(routineName);

                            baseRoutine = null;
                        }
                    }

                    //Add this routine if it doesn't exist and should
                    if (baseRoutine == null)
                    {
                        //Add this routine
                        AddRoutine(routineName, routineData.ClassName, routineData.ValueStr,
                            routineData.Enabled > 0, routineData.ResetOnReload > 0 );
                    }
                    else
                    {
                        baseRoutine.Enabled = routineData.Enabled > 0;
                        baseRoutine.ClassName = routineData.ClassName;
                        baseRoutine.ResetOnReload = routineData.ResetOnReload > 0;
                        baseRoutine.ValueStr = routineData.ValueStr;
                    }

                    encounteredRoutines.Add(routineName);
                }

                //Remove non-runtime routines that are no longer in the database
                foreach (var routineKVPair in BotRoutines)
                {
                    if (routineKVPair.Value.IsRuntimeRoutine == true)
                    {
                        continue;
                    }

                    if (encounteredRoutines.Contains(routineKVPair.Key) == false)
                    {
                        RemoveRoutine(routineKVPair.Key);
                    }
                }
            }
        }

        public void Update(in DateTime nowUTC)
        {
            //Update routines
            foreach (BaseRoutine routine in BotRoutines.Values)
            {
                if (routine == null || routine.Enabled == false)
                {
                    continue;
                }

                routine.UpdateRoutine(nowUTC);
            }
        }

        public bool AddRoutine(string routineName, string routineTypeName, string valueStr,
            in bool routineEnabled, in bool resetOnReload)
        {
            //Custom routine - load it
            if (string.IsNullOrEmpty(routineTypeName) == true)
            {
                BaseRoutine customRoutine = LoadCustomRoutine(routineName, valueStr);
                if (customRoutine == null)
                {
                    EvtDispatcher.BroadcastMessage($"Unable to add routine \"{routineName}\": Custom code failed.");
                    return false;
                }
                else
                {
                    Logger.Information($"Successfully compiled custom routine \"{routineName}\"");
                }

                customRoutine.Enabled = routineEnabled;
                customRoutine.ClassName = routineTypeName;
                customRoutine.ResetOnReload = resetOnReload;
                customRoutine.ValueStr = valueStr;

                return AddRoutine(routineName, customRoutine);
            }

            Type routineType = Type.GetType(routineTypeName, false, true);
            if (routineType == null && AdditionalAssemblies?.Length > 0)
            {
                //Look for the type in our other assemblies
                for (int i = 0; i < AdditionalAssemblies.Length; i++)
                {
                    Assembly asm = AdditionalAssemblies[i];

                    routineType = asm.GetType(routineTypeName, false, true);
                    
                    if (routineType != null)
                    {
                        Logger.Debug($"Found \"{routineTypeName}\" in assembly \"{asm.GetName()}\"!");
                        break;
                    }
                }                
            }

            if (routineType == null)
            {
                EvtDispatcher.DispatchEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE,
                        new EvtBroadcastMessageArgs($"Cannot find routine type \"{routineTypeName}\" for routine \"{routineName}\" in all provided assemblies."));
                return false;
            }

            BaseRoutine routine = null;

            //Try to create an instance
            try
            {
                routine = (BaseRoutine)Activator.CreateInstance(routineType, Array.Empty<object>());
                routine.Enabled = routineEnabled;
                routine.ClassName = routineTypeName;
                routine.ResetOnReload = resetOnReload;
                routine.ValueStr = valueStr;
            }
            catch (Exception e)
            {
                EvtDispatcher.DispatchEvent<EvtBroadcastMessageArgs>(ClientServiceEventNames.BROADCAST_MESSAGE,
                        new EvtBroadcastMessageArgs($"Unable to add routine \"{routineName}\": \"{e.Message}\""));
            }

            return AddRoutine(routineName, routine);
        }

        public bool AddRoutine(string routineName, BaseRoutine routine)
        {
            if (routine == null)
            {
                Logger.Warning("Cannot add null routine.");
                return false;
            }

            //Clean up the existing routine before overwriting it with the new value
            if (BotRoutines.TryGetValue(routineName, out BaseRoutine existingRoutine) == true)
            {
                existingRoutine.CleanUp();
            }

            BotRoutines[routineName] = routine;
            routine.SetRequiredData(this, Logger, FolderPathResolver, DatabaseMngr, ClientServiceMngr,
                VControllerContainer, EvtDispatcher, WebSocketMngr, InputHndlr);
            routine.Initialize();

            return true;
        }

        public bool RemoveRoutine(string routineName)
        {
            bool removed = BotRoutines.Remove(routineName, out BaseRoutine routine);

            if (removed == true)
            {
                routine?.CleanUp();
            }

            return removed;
        }

        public bool RemoveRoutine(BaseRoutine routine)
        {
            string routineName = string.Empty;
            bool found = false;

            //Look for the reference
            foreach (KeyValuePair<string, BaseRoutine> kvPair in BotRoutines)
            {
                if (kvPair.Value == routine)
                {
                    routineName = kvPair.Key;
                    found = true;
                    break;
                }
            }

            //The routine was found, so remove it
            if (found == true)
            {
                return RemoveRoutine(routineName);
            }

            return false;
        }

        public BaseRoutine FindRoutine(string name)
        {
            BotRoutines.TryGetValue(name, out BaseRoutine routine);
            
            return routine;
        }

        public string[] GetRunningRoutineNames()
        {
            //Disabled routines are not running
            return BotRoutines.Where(brkv => brkv.Value.Enabled).Select(brkv => brkv.Key).ToArray();
        }

        private BaseRoutine LoadCustomRoutine(string routineName, string filePath)
        {
            //First try to read the file as an absolute path
            string codeText = FileHelpers.ReadFromTextFile(filePath);

            //If that wasn't found, try a relative path
            if (string.IsNullOrEmpty(codeText) == true)
            {
                codeText = FileHelpers.ReadFromTextFile(FolderPathResolver.DataFolderPath, filePath);
            }

            if (string.IsNullOrEmpty(codeText) == true)
            {
                EvtDispatcher.BroadcastMessage("Invalid source file for custom routine. Double check its location.", Serilog.Events.LogEventLevel.Warning);
                return null;
            }

            Logger.Information($"Compiling custom routine \"{routineName}\" from code file \"{filePath}\"...");

            CustomCodeCreationData<BaseRoutine> cstmRoutCData = new CustomCodeCreationData<BaseRoutine>(null, false, string.Empty);

            try
            {
                //Find a way to do this async down the road so it doesn't halt the thread
                var cstmRoutCTask = CustomCodeHandler.CompileCodeAndReturnObject<BaseRoutine>(codeText);
                cstmRoutCTask.Wait();

                cstmRoutCData = cstmRoutCTask.Result;

                //Print error message
                if (cstmRoutCData.Success == false)
                {
                    EvtDispatcher.BroadcastMessage(cstmRoutCData.ErrorMessage, Serilog.Events.LogEventLevel.Warning);
                    return null;
                }
            }
            catch (CompilationErrorException e)
            {
                EvtDispatcher.BroadcastMessage($"Compiler error on custom routine \"{routineName}\". {e.Message}", Serilog.Events.LogEventLevel.Warning);
                return null;
            }
            catch (Exception e)
            {
                EvtDispatcher.BroadcastMessage($"Error with custom routine \"{routineName}\". {e.Message}", Serilog.Events.LogEventLevel.Warning);
                return null;
            }

            //Return the routine
            return cstmRoutCData.NewObject;
        }
    }
}
