﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    /// <inheritdoc />
    public partial class InvalidInputComboReplaceValidForWithValidLevelThreshold : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ValidFor",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos");

            migrationBuilder.AddColumn<long>(
                name: "ValidLevelThreshold",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                type: "INTEGER",
                nullable: false,
                defaultValue: 9223372036854775807L);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ValidLevelThreshold",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos");

            migrationBuilder.AddColumn<string>(
                name: "ValidFor",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                type: "TEXT",
                nullable: true);
        }
    }
}
