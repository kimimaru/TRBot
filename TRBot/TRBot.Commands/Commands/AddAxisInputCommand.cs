﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;
using TRBot.Permissions;
using static TRBot.Input.Consoles.ConsoleConstants;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that adds an axis input to a console.
    /// </summary>
    public sealed class AddAxisInputCommand : BaseCommand
    {
        private const string NEGATIVE_AXIS_ARG = "negative";
        private const string POSITIVE_AXIS_ARG = "positive";
        private const string ABSOLUTE_AXIS_ARG = "absolute";

        private string UsageMessage = "Usage - \"console name\", \"axis name\", \"axisVal (int)\", \"negative/positive/absolute\""
            + " OR \"console name\", \"axis name\", \"axisVal (int)\", \"minAxis (0.0 to 1.0)\", \"maxAxis (0.0 to 1.0)\", \"defaultAxis (0.0 to 1.0)\"";

        public AddAxisInputCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount != 4 && argCount != 6)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string consoleStr = arguments[0].ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }
            }

            string inputName = arguments[1].ToLowerInvariant();
            string axisValStr = arguments[2].ToLowerInvariant();

            if (StringNumParser.TryParseInt(axisValStr, out int axisVal) == false)
            {
                QueueMessage(args.ServiceName, "Invalid axis value.");
                return;
            }

            (double, double, double) axisValues = default;

            if (argCount == 4)
            {
                axisValues = GetAxisValuesFromName(arguments[3].ToLowerInvariant());
            }
            else
            {
                axisValues = GetAxisValuesFromArgs(arguments[3].ToLowerInvariant(), arguments[4].ToLowerInvariant(), arguments[5].ToLowerInvariant());
            }

            double minAxisVal = axisValues.Item1;
            double maxAxisVal = axisValues.Item2;
            double defaultAxisVal = axisValues.Item3;

            if (minAxisVal < MIN_NORMALIZED_AXIS_VAL || minAxisVal > MAX_NORMALIZED_AXIS_VAL)
            {
                QueueMessage(args.ServiceName, "Invalid minimum axis value or another argument is invalid.");
                return;
            }

            //Ensure that the minimum axis value has at most 1 decimal point
            double minAxisDecimals = minAxisVal * 10d;
            if (((int)minAxisDecimals) != minAxisDecimals)
            {
                QueueMessage(args.ServiceName, "Minimum axis value has more than 1 decimal point.");
                return;
            }

            if (maxAxisVal < ConsoleConstants.MIN_NORMALIZED_AXIS_VAL || maxAxisVal > MAX_NORMALIZED_AXIS_VAL)
            {
                QueueMessage(args.ServiceName, "Invalid maximum axis value.");
                return;
            }

            //Ensure that the maximum axis value has at most 1 decimal point
            double maxAxisDecimals = maxAxisVal * 10d;
            if (((int)maxAxisDecimals) != maxAxisDecimals)
            {
                QueueMessage(args.ServiceName, "Maximum axis value has more than 1 decimal point.");
                return;
            }

            if (defaultAxisVal < MIN_NORMALIZED_AXIS_VAL || defaultAxisVal > MAX_NORMALIZED_AXIS_VAL)
            {
                QueueMessage(args.ServiceName, "Invalid default axis value.");
                return;
            }

            //Ensure that the default axis value has at most 1 decimal point
            double defaultAxisDecimals = defaultAxisVal * 10d;
            if (((int)defaultAxisDecimals) != defaultAxisDecimals)
            {
                QueueMessage(args.ServiceName, "Default axis value has more than 1 decimal point.");
                return;
            }

            InputData existingInput = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                //Check if the input exists
                existingInput = console.InputList.FirstOrDefault((inpData) => inpData.Name == inputName);

                //Update if found
                if (existingInput != null)
                {
                    //Switch the input type and values
                    existingInput.InputType = InputTypes.Axis;
                    existingInput.AxisValue = axisVal;
                    existingInput.MinAxisVal = minAxisVal;
                    existingInput.MaxAxisVal = maxAxisVal;
                    existingInput.DefaultAxisVal = defaultAxisVal;
                }
                //Otherwise, create the input and add it to the console
                else
                {
                    InputData inputData = new InputData(inputName, 0, axisVal, InputTypes.Axis,
                        minAxisVal, maxAxisVal, defaultAxisVal,(long)PermissionLevels.User);
                    console.InputList.Add(inputData);
                }

                //Save database changes
                context.SaveChanges();
            }

            string message = string.Empty;
            if (existingInput == null)
            {
                message = $"Added new axis \"{inputName}\" to console \"{consoleStr}\"!";
            }
            else
            {
                message = $"Updated axis \"{inputName}\" on console \"{consoleStr}\"!";
            }

            QueueMessage(args.ServiceName, message);
        }

        private (double, double, double) GetAxisValuesFromName(string axisName)
        {
            if (axisName == NEGATIVE_AXIS_ARG)
            {
                return (0.5d, 0d, 0.5d);
            }
            
            if (axisName == POSITIVE_AXIS_ARG)
            {
                return (0.5d, 1d, 0.5d);
            }

            if (axisName == ABSOLUTE_AXIS_ARG)
            {
                return (0d, 1d, 0d);
            }

            return (-999d, -999d, -999d);
        }

        private (double, double, double) GetAxisValuesFromArgs(string minAxisStr, string maxAxisStr, string defaultAxisStr)
        {
            (double, double, double) axisVals = (0d, 0d, 0d);

            if (StringNumParser.TryParseDouble(minAxisStr, out axisVals.Item1) == false
                || StringNumParser.TryParseDouble(maxAxisStr, out axisVals.Item2) == false
                || StringNumParser.TryParseDouble(defaultAxisStr, out axisVals.Item3) == false)
            {
                axisVals.Item1 = -999d;
                axisVals.Item2 = -999d;
                axisVals.Item3 = -999d;
            }

            return axisVals;
        }
    }
}
