﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that changes the periodic input interval.
    /// </summary>
    public sealed class GetSetPeriodicInputTimeCommand : BaseCommand
    {
        private const int MIN_INTERVAL_VAL = 1;
        private const int RECOMMENDED_MIN_VAL = 15000;

        private string UsageMessage = $"Usage - no arguments (get value) or \"interval in milliseconds (int)\"";

        public GetSetPeriodicInputTimeCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //Ignore with too few arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (arguments.Count == 0)
            {
                long periodicInputTime = DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_TIME, 0L);

                QueueMessage(args.ServiceName, $"The interval for periodic inputs is {periodicInputTime} milliseconds. To change the periodic input interval, a value, in milliseconds, as an argument."); 
                return;
            }

            //Check for sufficient permissions
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
                if (user == null || user.HasEnabledAbility(PermissionConstants.SET_PERIODIC_INPUT_TIME_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the default periodic input interval!");
                    return;
                }
            }

            string intervalValStr = arguments[0].ToLowerInvariant();

            if (StringNumParser.TryParseInt(intervalValStr, out int newInterval) == false)
            {
                QueueMessage(args.ServiceName, "That's not a valid number!");
                return;
            }

            if (newInterval < MIN_INTERVAL_VAL)
            {
                string timePluralized = "milliseconds";
                timePluralized = timePluralized.Pluralize(MIN_INTERVAL_VAL);

                QueueMessage(args.ServiceName, $"The interval cannot be below {MIN_INTERVAL_VAL} {timePluralized}!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings periodicInputTime = context.GetSetting(SettingsConstants.PERIODIC_INPUT_TIME);
                if (periodicInputTime == null)
                {
                    periodicInputTime = new Settings(SettingsConstants.PERIODIC_INPUT_TIME, string.Empty, 0L);
                    context.SettingCollection.Add(periodicInputTime);
                }

                periodicInputTime.ValueInt = newInterval;

                context.SaveChanges();
            }

            string intervalPluralized = "milliseconds";
            intervalPluralized = intervalPluralized.Pluralize(newInterval);

            QueueMessage(args.ServiceName, $"Set the periodic input interval to {newInterval} {intervalPluralized}!");
            
            //If the interval is less than a given amount, recommend a higher value so the message doesn't spam chat and interfere with inputs
            if (newInterval < RECOMMENDED_MIN_VAL)
            {
                QueueMessage(args.ServiceName, $"The set interval is fairly low. Keep in mind that it may spam chat and/or interfere with player inputs."); 
            }
        }
    }
}
