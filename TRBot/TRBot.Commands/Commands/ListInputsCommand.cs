﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using System.Text;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// Shows information about all inputs in a specific category for a console.
    /// </summary>
    public sealed class ListInputsCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"console name (optional)\" \"input type (Button/Axis/Blank)\"";

        public ListInputsCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            bool arg1FoundConsole = false;
            long lastConsole = DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);
            StringBuilder strBuilder = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = null;

                switch (arguments.Count)
                {
                    //Allow 0 arguments because the ValueStr could be the input type
                    case 0:
                        console = context.Consoles.FirstOrDefault(console => console.ID == lastConsole);
                        break;
                    case 1:
                        console = context.Consoles.FirstOrDefault(console => console.Name == arguments[0].ToLowerInvariant());
                        
                        if (console != null)
                        {
                            arg1FoundConsole = true;
                            break;
                        }

                        //Argument 0 should be the input type if the ValueStr isn't set or the console isn't found, so use the last console
                        goto case 0;
                    case 2:
                        console = context.Consoles.FirstOrDefault(console => console.Name == arguments[0].ToLowerInvariant());
                        break;
                }

                //Invalid console
                if (console == null || console.InputList.Count == 0)
                {
                    QueueMessage(args.ServiceName, "The current or given console is invalid or has no inputs.");
                    return;
                }

                string inputTypeStr = string.Empty;

                if (arguments.Count == 0 || arg1FoundConsole == true)
                {
                    inputTypeStr = ValueStr?.ToLowerInvariant() ?? string.Empty;
                }
                else if (arguments.Count == 1)
                {
                    inputTypeStr = arguments[0].ToLowerInvariant();
                }
                else if (arguments.Count == 2)
                {
                    inputTypeStr = arguments[1].ToLowerInvariant();
                }

                if (EnumUtility.TryParseEnumValue(inputTypeStr, out InputTypes inputType) == false)
                {
                    string str = EnumUtility.GetNames<InputTypes>.EnumNames.GetStringRepresentation();
                    QueueMessage(args.ServiceName, $"Please enter one of the following input types: {str}");

                    return;
                }

                var matchingInputs = console.InputList
                    .Where(inp => inp.InputType == inputType)
                    .OrderBy(inp => inputType == InputTypes.Button ? inp.ButtonValue : inp.AxisValue)
                    .ToArray();

                if (matchingInputs.Length == 0)
                {
                    QueueMessage(args.ServiceName, $"No inputs with input type {inputType} found.");
                    return;
                }

                //List all inputs of this type for this console
                strBuilder = new StringBuilder(300);

                for (int i = 0; i < matchingInputs.Length; i++)
                {
                    InputData inputData = matchingInputs[i];

                    strBuilder.Append(inputData.Name);

                    if (inputData.InputType == InputTypes.Button)
                    {
                        strBuilder.Append(": Val = ").Append(inputData.ButtonValue);
                    }
                    else if (inputData.InputType == InputTypes.Axis)
                    {
                        strBuilder.Append(": Val = ").Append(inputData.AxisValue)
                            .Append(", Min = ").Append(inputData.MinAxisVal)
                            .Append(", Max = ").Append(inputData.MaxAxisVal)
                            .Append(", Def = ").Append(inputData.DefaultAxisVal);
                    }

                    //Note if the input is disabled
                    if (inputData.Enabled == 0)
                    {
                        strBuilder.Append(" (disabled)");
                    }

                    if (i < (matchingInputs.Length - 1))
                    {
                        strBuilder.Append(" | ");
                    }
                }
            }
            
            QueueMessage(args.ServiceName, strBuilder.ToString(), "| ");
        }
    }
}
