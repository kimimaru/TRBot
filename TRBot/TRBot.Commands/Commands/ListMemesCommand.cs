﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Linq;
using System.Text;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Lists all available memes.
    /// </summary>
    public sealed class ListMemesCommand : BaseCommand
    {
        public ListMemesCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            StringBuilder strBuilder = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                int memeCount = context.Memes.Count();

                if (memeCount == 0)
                {
                    QueueMessage(args.ServiceName, "There are no memes!");
                    return;
                }

                //The capacity is the estimated average number of characters for each meme multiplied by the number of memes
                strBuilder = new StringBuilder(memeCount * 20);

                foreach (Meme meme in context.Memes)
                {
                    strBuilder.Append('"').Append(meme.MemeName).Append('"').Append(',').Append(' ');
                }
            }

            strBuilder.Remove(strBuilder.Length - 2, 2);
            
            QueueMessage(args.ServiceName, strBuilder.ToString(), ", ");
        }
    }
}
