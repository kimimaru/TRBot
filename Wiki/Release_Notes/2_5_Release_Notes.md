TRBot v2.5.0, featuring simultaneous service support, IRC support, and custom messages.

Summary of changes:
- It's now possible to run TRBot over multiple services at once, including Twitch, Terminal, and a WebSocket simultaneously! Services can even be dynamically added and removed at runtime. See the updated [setup documentation](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Setup-Init.md) for more information.
- Added IRC support! Run TRBot over your favorite IRC channel!
- The input handler has been refactored to use Tasks over separate threads. In practice, this means it's able to handle many incoming inputs more efficiently.
- The event dispatcher has been refactored to consolidate events. Now many of the events used in the application are [also available to external applications over a WebSocket](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Event-Dispatcher.md)!
- [GitInfo](https://github.com/devlooped/GitInfo) has been added under a new `TRBot.Build` project, allowing access to more detailed build information for debugging purposes.
- A new [custom messages](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Custom-Messages.md) feature has been added, allowing the host to send messages to users based on their message count. The default set of messages includes a set of tips to ease newer players into the syntax. This feature is controlled by the new [custom_messages_enabled](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Settings-Documentation.md#custom_messages_enabled) setting.
- Further improved the testability of the codebase, and added more unit tests for components such as the InputHandler.
- Handled version-specific database adjustments starting from this version. For example, this version removes the "reconnect" routine from the database upon upgrading, as the routine is now added at runtime for each connected service upon disconnecting.
- The Terminal service no longer accepts empty usernames or usernames consisting entirely of whitespace for consistency with the other services. If it detects one of these, it'll fall back to the default username.

Woodpecker CI (Continuous Integration) has been added to the project! The CI currently checks for successful builds and tests on both GNU/Linux and Windows. The configuration can be found in the [.woodpecker](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/.woodpecker) folder at the root of the project. See the CI status for the `develop` branch on the README.

**IMPORTANT for those upgrading from previous 2.0+ releases:**
- There have been breaking changes to custom commands and routines as a result of the client service refactoring. Consult the [2.5.0 migration guide](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Custom-Cmd-Routine-Changes-2_5.md) for how to proceed. The [custom commands](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Custom-Commands.md) and [custom routines](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Custom-Routines.md) documentation has also been updated accordingly.

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.5.0/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
