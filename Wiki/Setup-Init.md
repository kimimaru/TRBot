# Getting TRBot
* To get started immediately, download the latest release on the [releases page](https://codeberg.org/kimimaru/TRBot/releases). Keep in mind that there is no schedule for releases, so they may be behind the latest on the [`develop` branch](https://codeberg.org/kimimaru/TRBot/src/branch/develop).
* To build from source, please follow the [building guide](./Building.md).

# Setting up TRBot
If you installed a pre-built binary, run `TRBot.Main` (Ex. `TRBot.Main.exe` on Windows, `./TRBot.Main` in a terminal on GNU/Linux). If you built the project, use either `dotnet run` or open the native executable depending on whether the runtime is self-contained or not.

After running TRBot once, it will create a **Data** folder in the same folder you ran it from along with a **TRBotData.db** database file, which holds all your settings. It's highly recommended to first go through the [tutorial on managing TRBot's data](./Managing-Data.md) to learn how to view and modify this data before continuing this guide.

### System-wide Install
If TRBot is run as a system-wide install, then the database and other files will be found in the user local application folder (Ex. `~/.local/share/TRBot` on GNU/Linux or `%LOCALAPPDATA%/TRBot/Data` on Windows).

TRBot can be compiled to run as a system-wide install by default with the `SYSTEM_WIDE_INSTALL` preprocessor directive, but the releases in the repository by run TRBot as a portable install by default. You can manually run as a system-wide install by passing `-s` or `--system-wide` as a command line argument when running TRBot. Conversely, `-p` or `--portable` will force TRBot to run as portable. Use `--help` to list all command line arguments. 

## Permissions
For security reasons, no user is a Superadmin in TRBot by default. **You will have to set yourself to a Superadmin to change important settings and use specific commands, like changing the console.** To set a user as an Admin or Superadmin, open up the **TRBotData.db** file in SQLite or a database viewer, select the **Browse Data** tab, find the user under the **Users** table, and manually change their level to 40 (Admin) or 50 (Superadmin), then apply and save your changes.

**IMPORTANT**: After setting your user's level in the database, run the `UpdateAllUserAbilitiesCommand` with "!updateabilities" to update your permissions. This step is necessary since permissions are not updated when a user's level is changed directly in the database.

# Connecting
The **Services** table of the **TRBotData.db** file determines which services TRBot connects to.

TRBot's supported services have the following `ServiceNames`:
- `terminal` - Local terminal
- `twitch` - Use TRBot over Twitch
- `websocket` - Use TRBot over a WebSocket
- `irc` - Use TRBot over an IRC network and channel of your choosing
- `xmpp`- Use TRBot over XMPP, either via TCP or WebSocket, on either one-to-one or multi-user chats
- `matrix` - Connect TRBot over the Matrix protocol

The service is active if `Enabled` has a value of 1. Similarly, the service is inactive if `Enabled` has a value of 0.

**Multiple services can be active at once**, allowing inputs from all of them to influence the game. Services can be dynamically enabled and disabled while TRBot is running through a data reload via the [`ReloadCommand`](../TRBot/TRBot.Commands/Commands/ReloadCommand.cs) (default: "!reload") after making changes in the **Services** table. If there are no services available after reloading, TRBot will fallback to the Terminal service to stay running. However, TRBot **requires at least one enabled service at startup** to function.

By default, TRBot connects over both the Terminal and Twitch services.

Please read the relevant section(s) pertaining to the platforms you'd like to run TRBot on:
- [Connecting via Terminal](./Setup-Terminal.md)
- [Connecting via Twitch](./Setup-Twitch.md)
- [Connecting via WebSocket](./Setup-WebSocket.md)
- [Connecting via IRC](./Setup-IRC.md)
- [Connecting via XMPP](./Setup-XMPP.md)
- [Connecting via Matrix](./Setup-Matrix.md)

## Other Service Information
The `LogToLogger` row in the `Services` table controls if messages sent from the service are logged to the console and log files. It's recommended to keep this on for all services except `terminal` to help identify issues that arise. To turn it off, change the value from 1 to 0.

# Migrating Data from older releases
If you're upgrading TRBot from an older release, please see the [data migration guide](./Migrating-Data.md).

# Next Step - Setting up virtual controllers
Done with this step? Next, [configure the virtual controllers](./Setup-VController.md) so your viewers can play through chat!
