﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that adds an input to an invalid input combo for a console.
    /// If the invalid input combo doesn't exist, it'll be created.
    /// </summary>
    public sealed class AddInvalidInputComboCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"console name (string)\", \"invalid input combo name (string)\", \"input name (string)\"";

        public AddInvalidInputComboCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount != 3)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string consoleStr = arguments[0].ToLowerInvariant();
            string invalidInputComboName = arguments[1].ToLowerInvariant();
            string inputName = arguments[2].ToLowerInvariant();

            bool createdNewInvalidCombo = false;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }

                //Check if the input exists in the console
                InputData existingInput = console.InputList.FirstOrDefault((inpData) => inpData.Name == inputName);

                if (existingInput == null)
                {
                    QueueMessage(args.ServiceName, $"No input named \"{inputName}\" exists for the \"{console.Name}\" console!");
                    return;
                }

                InvalidInputCombo existingCombo = console.InvalidInputCombos.FirstOrDefault(combo => combo.Name == invalidInputComboName);

                //Create a new one if so
                if (existingCombo == null)
                {
                    createdNewInvalidCombo = true;
                    existingCombo = new InvalidInputCombo(invalidInputComboName, new List<InputData>());
                }

                //Check if the input is already in the existing invalid combo
                bool inputExistsInCombo = existingCombo.InvalidInputs.Exists(inpData => inpData.ID == existingInput.ID);

                if (inputExistsInCombo == true)
                {
                    QueueMessage(args.ServiceName, $"Input \"{inputName}\" is already part of the invalid input combo \"{invalidInputComboName}\" for the \"{console.Name}\" console!");
                    return;
                }

                //Add the input
                existingCombo.InvalidInputs.Add(existingInput);

                //If a new invalid combo was created, add it
                if (createdNewInvalidCombo == true)
                {
                    console.InvalidInputCombos.Add(existingCombo);
                }

                context.SaveChanges();
            }

            if (createdNewInvalidCombo == true)
            {
                QueueMessage(args.ServiceName, $"Added new invalid input combo \"{invalidInputComboName}\" with the \"{inputName}\" input for the \"{consoleStr}\" console!");
            }
            else
            {
                QueueMessage(args.ServiceName, $"Successfully added input \"{inputName}\" to the invalid input combo \"{invalidInputComboName}\" for the \"{consoleStr}\" console!");
            }
        }
    }
}
