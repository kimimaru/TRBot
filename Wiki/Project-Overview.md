TRBot contains several different projects with the purpose of isolating functionality for a more modular approach. This approach allows TRBot to scale more elegantly over time, and it allows each project to use only the parts it needs instead of requiring the entire package.

# Project Structure
## Core Components
* **TRBot.Build** - Contains build information, including the Git commit hash.
* **TRBot.Commands** - Contains code for all commands entered through chat.
* **TRBot.Connection** - Handles sending and receiving events to and from client services and TRBot. You'll find the service integrations here.
* **TRBot.CustomCode** - Provides systems for compiling and executing custom code. For example, this provides the backbone of custom commands and custom routines.
* **TRBot.Data** - Contains everything relating to data and the SQLite database, including the database context.
* **TRBot.Events** - Contains the core of the built-in event system, including the event dispatcher that external applications can listen to.
* **TRBot.Input** - Handles anything input-related, including the parser to transform text into inputs, virtual controllers with native code implementations, and the console infrastructure holding input definitions.
* **TRBot.Integrations** - Integrations with third-party applications. This code is strictly separated from the other non-application projects.
* **TRBot.Permissions** - Code for the access levels and other moderation features.
* **TRBot.Routines** - Deals with bot routines, or pieces of code that run regularly.
* **TRBot.Utilities** - Contains various utilities, including a crash handler, logging, operating system detection, methods for reading/writing files, algorithms, and other mathematical operations.
* **TRBot.WebSocket** - Includes the WebSocket manager for handling concurrent WebSocket servers.

## Applications
* **TRBot.Main** - The main application that utilizes the core components together for a full-fledged bot. This is what you're running with each TRBot release.
* **TRBot.Tests** - Unit tests and benchmarks for TRBot.

## Other
* **Submodules** - Versioned code repositories that TRBot depends on.
* **Logo** - Resources for the TRBot logo.
* **Supplementary** - Contains resources and code not directly related to TRBot, such as a sample WebSocket server implementation and a ChatterBot instance that users can interact with through TRBot.
* **Wiki** - What you're reading right now. This is the TRBot documentation.
* **.woodpecker** - Configuration for [Woodpecker](https://woodpecker-ci.org/), the CI used by TRBot on Codeberg.
