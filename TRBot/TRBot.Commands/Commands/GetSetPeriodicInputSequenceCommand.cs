﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Input;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that changes the periodic input sequence.
    /// </summary>
    public sealed class GetSetPeriodicInputSequenceCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - no arguments (get value) or \"input sequence (string)\"";

        public GetSetPeriodicInputSequenceCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            string userExternalID = args.Command.UserMessage.UserExternalID;
            string userName = args.Command.UserMessage.Username;
            string argInputSequence = args.Command.ArgumentsAsString;

            if (string.IsNullOrEmpty(argInputSequence) == true)
            {
                string periodicInputSequence = DatabaseMngr.GetSettingString(SettingsConstants.PERIODIC_INPUT_VALUE, string.Empty);
                if (string.IsNullOrEmpty(periodicInputSequence) == true)
                {
                    QueueMessage(args.ServiceName, "The current periodic input sequence is not defined! You can define one by providing an input sequence as an argument."); 
                }

                QueueMessage(args.ServiceName, $"The current periodic input sequence is: {periodicInputSequence}", string.Empty); 
                return;
            }

            //Replace all non-space whitespace in the argument with space for readability
            //Some platforms will do this by default (Ex. Twitch), but we can't rely on them all being consistent here
            argInputSequence = Helpers.ReplaceAllWhitespaceWithSpace(argInputSequence);

            long globalInputPermLevel = DatabaseMngr.GetSettingInt(SettingsConstants.GLOBAL_INPUT_LEVEL, 0L);
            
            //Consider the user's level so they cannot use this to perform inputs normally unavailable to them
            long userLevel = 0L;
            
            HashSet<string> userRestrictedInputs = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(userExternalID);
                
                //Check for permissions
                if (user == null || user.HasEnabledAbility(PermissionConstants.SET_PERIODIC_INPUT_SEQUENCE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the periodic input sequence!");
                    return;
                }

                //Check if the user is silenced
                if (user.HasEnabledAbility(PermissionConstants.SILENCED_ABILITY) == true)
                {
                    QueueMessage(args.ServiceName, "Nice try, but you can't set the periodic input sequence while silenced!");
                    return;
                }
            
                //Ignore based on user level and permissions
                if (user.Level < globalInputPermLevel)
                {
                    QueueMessage(args.ServiceName, $"Inputs are restricted to levels {(PermissionLevels)globalInputPermLevel} and above, so you can't set the periodic input sequence.");
                    return;
                }

                userLevel = user.Level;
                userRestrictedInputs = user.GetRestrictedInputs();
            }

            //Parse the input sequence
            //Set up the console
            GameConsole usedConsole = null;

            int lastConsoleID = (int)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole lastConsole = context.Consoles.FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            //If there are no valid inputs, don't attempt to parse
            if (usedConsole == null)
            {
                QueueMessage(args.ServiceName, "The current console does not point to valid data. Please set a different console to use, or if none are available, add one.");
                return;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current console, \"{usedConsole.Name}\", does not have any available inputs.");
                return;
            }

            int defaultDur = (int)DatabaseMngr.GetUserOrGlobalDefaultInputDur(string.Empty);
            int maxDur = (int)DatabaseMngr.GetUserOrGlobalMaxInputDur(string.Empty);
            int defaultPeriodicInpPort = (int)DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_PORT, 0L);

            ParsedInputSequence inputSequence = default;

            try
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //Get input synonyms for this console
                    IQueryable<InputSynonym> synonyms = context.InputSynonyms.Where(syn => syn.ConsoleID == lastConsoleID);

                    StandardParserValidator validator = new StandardParserValidator(userRestrictedInputs,
                        userLevel, usedConsole, VControllerContainer.VControllerMngr, Logger);

                    //Parse inputs to get our parsed input sequence
                    IParser standardParser = new StandardParserFactory(context.Macros, synonyms,
                            usedConsole.GetInputNames(InputNameFiltering.Enabled), defaultPeriodicInpPort, VControllerContainer.VControllerMngr.ControllerCount - 1,
                            defaultDur, maxDur, true, validator)
                        .Create();
                    
                    inputSequence = standardParser.ParseInputs(argInputSequence);
                }
            }
            catch (Exception exception)
            {
                string excMsg = exception.Message;

                //Handle parsing exceptions
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;

                QueueMessage(args.ServiceName, $"Couldn't parse periodic input sequence: {excMsg}");
                return;
            }

            //Check for non-valid messages
            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                string message = inputSequence.Error;
                if (string.IsNullOrEmpty(message) == true)
                {
                    message = "Input is invalid";
                }

                QueueMessage(args.ServiceName, $"Cannot set periodic input sequence. {message}");
                return;
            }

            //Defer input combo validation to when it's about to be be executed
            //This way, the state of the virtual controllers now shouldn't
            //affect an otherwise valid combo from being accepted

            string newPeriodicInpSequence = ReverseParser.ReverseParse(inputSequence, usedConsole,
                new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowNonDefaultPorts,
                    defaultPeriodicInpPort, ReverseParser.ShowDurationTypes.ShowNonDefaultDurations, defaultDur));

            /*
             * Finally, after everything is good, set the input sequence!
            */
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings periodicInputSeqSetting = context.GetSetting(SettingsConstants.PERIODIC_INPUT_VALUE);
                periodicInputSeqSetting.ValueStr = newPeriodicInpSequence;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, "Successfully set the periodic input sequence!");
        }
    }
}
