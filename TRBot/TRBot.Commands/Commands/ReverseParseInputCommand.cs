﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Linq;
using TRBot.Connection;
using TRBot.Input;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;
using TRBot.Data;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Reverse parses an input sequence to print it in natural language.
    /// </summary>
    public sealed class ReverseParseInputCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"input sequence\"";

        public ReverseParseInputCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            string input = args.Command.ArgumentsAsString;

            if (string.IsNullOrEmpty(input) == true)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            GameConsole usedConsole = null;

            int lastConsoleID = (int)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole lastConsole = context.Consoles.AsNoTracking().Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos).ThenInclude(ic => ic.InvalidInputs).FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            //If there are no valid inputs, don't attempt to parse
            if (usedConsole == null)
            {
                QueueMessage(args.ServiceName, $"The current console does not point to valid data. Please set a different console to use, or if none are available, add one.");
                return;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current console, \"{usedConsole.Name}\", does not have any available inputs.");
                return;
            }

            ParsedInputSequence inputSequence = default;
            int defaultPort = 0;

            try
            {
                string userExternalID = args.Command.UserMessage.UserExternalID;

                //Get default and max input durations
                //Use user overrides if they exist, otherwise use the global values
                User user = DatabaseMngr.GetUserByExternalID(userExternalID);
                
                if (user != null)
                {
                    defaultPort = (int)user.ControllerPort;
                }

                int defaultDur = (int)DatabaseMngr.GetUserOrGlobalDefaultInputDur(userExternalID);
                int maxDur = (int)DatabaseMngr.GetUserOrGlobalMaxInputDur(userExternalID);

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //Get input synonyms for this console
                    IQueryable<InputSynonym> synonyms = context.InputSynonyms.Where(syn => syn.ConsoleID == lastConsoleID);

                    IParser standardParser = new StandardParserFactory(context.Macros, synonyms,
                            usedConsole.GetInputNames(InputNameFiltering.Enabled), defaultPort, int.MaxValue,
                            defaultDur, maxDur, true, null)
                        .Create();
                    
                    //Parse inputs to get our parsed input sequence
                    inputSequence = standardParser.ParseInputs(input);
                }
            }
            catch (Exception exception)
            {
                string excMsg = exception.Message;

                //Handle parsing exceptions
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;

                QueueMessage(args.ServiceName, $"Invalid input. {excMsg}");
                return;
            }

            //Check for invalid inputs
            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                if (inputSequence.ParsedInputResult == ParsedInputResults.NormalMsg
                    || string.IsNullOrEmpty(inputSequence.Error) == true)
                {
                    QueueMessage(args.ServiceName, $"Invalid input.");
                }
                else
                {
                    QueueMessage(args.ServiceName, $"Invalid input. {inputSequence.Error}");
                }
                
                return;
            }

            //Reverse parse the string
            string reverseParsed = ReverseParser.ReverseParseNatural(inputSequence, usedConsole,
                new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.ShowNonDefaultPorts, defaultPort));

            QueueMessage(args.ServiceName, reverseParsed, ", ");
        }
    }
}
