﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Data;
using TRBot.Utilities;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Allows a user to transfer credits to another user.
    /// </summary>
    public class TransferCreditsCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"username (string)\" \"transfer amount (int)\"";

        public TransferCreditsCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string creditsName = DatabaseMngr.GetCreditsName();
            string giverName = args.Command.UserMessage.Username;
            string giverExternalID = args.Command.UserMessage.UserExternalID;
            long giverCredits = 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User giverUser = context.Users
                    .AsNoTrackingWithIdentityResolution()
                    .Include(u => u.Stats)
                    .Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility)
                    .FirstOrDefault(u => u.ExternalID == giverExternalID);

                if (giverUser.HasEnabledAbility(PermissionConstants.TRANSFER_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, $"You do not have the ability to transfer !");
                    return;
                }
    
                if (giverUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "You cannot transfer while opted out of stats.");
                    return;
                }

                giverCredits = giverUser.Stats.Credits;
            }

            string receiverName = arguments[0];

            string receiverExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(receiverName, args.ServiceName);

            if (giverExternalID == receiverExternalID)
            {
                QueueMessage(args.ServiceName, $"You cannot transfer {creditsName.Pluralize(0)} to yourself!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User receiverUser = context.Users
                    .AsNoTrackingWithIdentityResolution()
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == receiverExternalID);

                if (receiverUser == null)
                {
                    QueueMessage(args.ServiceName, "This user does not exist in the database!");
                    return;
                }

                if (receiverUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "This user is opted out of stats, so you can't transfer to them!");
                    return;
                }
            }

            string transferAmountStr = arguments[1];

            if (long.TryParse(transferAmountStr, out long transferAmount) == false)
            {
                QueueMessage(args.ServiceName, "Please enter a valid transfer amount!");
                return;
            }

            if (transferAmount <= 0)
            {
                QueueMessage(args.ServiceName, "Transfer amount must be greater than 0!");
                return;
            }

            if (transferAmount > giverCredits)
            {
                QueueMessage(args.ServiceName, $"Transfer amount is greater than {creditsName.Pluralize(0)}!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User giverUser = context.GetUserByExternalID(giverExternalID);
                User receiverUser = context.GetUserByExternalID(receiverExternalID);

                //Transfer credits and save
                giverUser.Stats.Credits -= transferAmount;
                receiverUser.Stats.Credits += transferAmount;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"{giverName} has transferred {transferAmount} {creditsName.Pluralize(transferAmount)} to {receiverName} :D !");
        }
    }
}
