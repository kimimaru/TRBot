# Custom Commands
TRBot supports running custom commands written in C#. If you provide an **empty** `ClassName` and a path to a C# source file as the `ValueStr` of a command, it'll read the file contents as code and compile it to be invoked upon receiving the command. The `ValueStr` supports both absolute paths and files relative to the **Data** folder.

This tutorial will be using a command called "testcmd" with a source file called "TestCmd.cs".

## Setting up the custom command
First we have to set up the custom command in the database. If you don't know how to view the database, please read [managing data](./Managing-Data.md).

1. Open the database and navigate to the **CommandData** table.
2. Create a new record and fill out the following information:
  - Set the `Name` of the command to be something descriptive that mentions what it does. In our case, the name should be "testcmd".
  - Set the `ClassName` of the command to be *completely empty*.
  - Set the `Level` value of the command to be the access level you want. For this tutorial, we want this command accessible to everyone, so set it to 0 (User level).
  - Set the `Enabled` value of the command to be 1 so we can actually use it.
  - Set the `DisplayInList` value of the command to 1. We can still use it if it's 0, but unless you don't want users seeing it in the help list for `ListCmdsCommand`, set it to 1.
  - Set the `ValueStr` to the path of the file we're going to use, "TestCmd.cs", without quotes.
  - Set the `Category` to 2 to mark it as a "User" command, thus making it visible in the default help command.
3. Write your changes to the database.

## Writing a custom command
Right now the database has a "testcmd" command that will execute custom code from a file called "TestCmd.cs" that's in our **Data** folder. Go ahead and create a text file named "TestCmd.cs" in your **Data** folder. Make sure file extensions are fully visible in your operating system, otherwise the file might actually be named "TestCmd.cs.txt".

Open up that file and input the following lines:

```cs
using System;
using TRBot.Commands;

public class TestCmd : BaseCommand
{
    public TestCmd()
    {
        
    }

    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        QueueMessage(args.ServiceName, "This is a test cmd!");
        
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            User user = context.GetUserByExternalID("terminaluser");
            
            QueueMessage(args.ServiceName, $"{user.Name} has {user.Stats.Credits} credits!!!!!!!");
        }
        
        QueueMessage(args.ServiceName, $"Cmd Level: {Level} | Enabled: {Enabled}");

        QueueMessage(args.ServiceName, $"Evt chat command args: {args.Command.ArgumentsAsString}");
    }
}

return new TestCmd();
```

Save the file, load up TRBot (or hard reload with "!reload hard" if it's already running), then type "!testcmd a b cd" to invoke the command.

If all went well, you should see the following messages in order:
1. "This is a test cmd!"
2. "terminaluser has x credits!!!!!!!" ("x" being the number of credits for "terminaluser")
3. "Cmd Level: 0 | Enabled: True"
4. "Evt chat command args: a b cd"

In basic terms, what this command did was print a message, load a user object from the database and print their credit count. Then, it printed data about the command itself and finally printed the arguments given to the command.

If things didn't go so well, you should see an error message in your console. If this happens, double-check your database settings and make sure the code doesn't have any syntax errors.

## Return Value
All custom commands **must** return an instance of a class deriving from `BaseCommand`.

The example above achieves this by defining a new `TestCmd` class deriving from `BaseCommand`, then returning a new instance of `TestCmd` after the class definition. This is necessary since the `CommandHandler` that manages the in-memory command instances will then be able to initialize and execute your custom code just like any other command. It can't do this if it doesn't know which command to use!

## Important things to note
- Custom commands have access to the same fields and properties that other commands do.
- Use `QueueMessage` over `Console.WriteLine` to send messages through the client service. The name of the service is the first argument to `QueueMessage` and can be obtained as from the `EvtChatCommandArgs`. On top of sending the messages to the correct destination, `QueueMessage` respects the rate-limiting settings for TRBot.
- Custom commands have most other TRBot projects and several common namespaces imported (such as `System`). Anything else will need their full type names, or you will need to explicitly include the namespace. For example, `StringBuilder` needs to be referenced as `System.Text.StringBuilder`, but if you want just `StringBuilder`, you will need to add `using System.Text;` at the top of your custom command's source file.
- Custom commands utilize C#'s scripting API, which may consume significant RAM. Monitor RAM usage, especially if you have many custom commands.
- Since custom commands directly utilize TRBot code, and TRBot directly executes the custom commands, [custom commands are subject to the same licensing terms as TRBot itself](../LICENSE). Keep this in mind if you intend to include any sensitive data in your custom commands.

## Custom code through ExecCommand
The [`ExecCommand`](../TRBot/TRBot.Commands/Commands/ExecCommand.cs) (default: "!exec") can be used to execute custom code at any time when enabled. This can be useful for debugging issues while TRBot is running. Code run through `ExecCommand` is different from custom commands:

* While custom commands derive from `BaseCommand`, code run through `ExecCommand` is not.
* To access command fields and methods, use the provided `ThisCmd` field.
* Access command argument data with the `Args` field.
* There is no persistence. For example, if a field (Ex. `object a = new object()`) is declared, it won't be the same instance the next time `ExecCommand` is called.
