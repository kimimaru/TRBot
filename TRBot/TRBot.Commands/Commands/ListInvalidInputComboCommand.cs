﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using System.Text;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that lists all invalid input combos for a console.
    /// </summary>
    public sealed class ListInvalidInputComboCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"console name (string)\"";

        public ListInvalidInputComboCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount != 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string consoleStr = arguments[0].ToLowerInvariant();
            StringBuilder strBuilder = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                IGameConsole console = context.Consoles.AsNoTracking()
                    .Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos)
                    .ThenInclude(ic => ic.InvalidInputs)
                    .FirstOrDefault(c => c.Name == consoleStr);

                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }

                List<InvalidInputCombo> invalidComboList = console.InvalidInputCombos;
                if (invalidComboList.Count == 0)
                {
                    QueueMessage(args.ServiceName, $"Console \"{consoleStr}\" does not have any invalid input combos.");
                    return;
                }

                strBuilder = new StringBuilder(128);
                for (int i = 0; i < invalidComboList.Count; i++)
                {
                    strBuilder.Append("Invalid combo \"").Append(invalidComboList[i].Name).Append("\" = (");

                    List<InputData> invalidInputsInCombo = invalidComboList[i].InvalidInputs;
                    foreach (InputData inputData in invalidInputsInCombo)
                    {
                        strBuilder.Append('"').Append(inputData.Name).Append('"');
                        strBuilder.Append(',').Append(' ');
                    }

                    if (invalidInputsInCombo.Count > 0)
                    {
                        strBuilder.Remove(strBuilder.Length - 2, 2);
                    }

                    if (i < (invalidComboList.Count - 1))
                    {
                        strBuilder.Append(')').Append(' ').Append('|').Append(' ');
                    }
                }

                strBuilder.Append("). Neither of these input combinations are allowed to be pressed at once on the same controller port.");
            }

            QueueMessage(args.ServiceName, strBuilder.ToString(), "| ");
        }
    }
}
