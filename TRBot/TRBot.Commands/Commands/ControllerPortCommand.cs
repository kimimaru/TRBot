﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Displays or changes which controller port a user is on.
    /// </summary>
    public sealed class ControllerPortCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"controllerPort (int - starting from 1) (optional)\"";

        public ControllerPortCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            User user = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

            //Display controller port with no arguments
            if (arguments.Count == 0)
            {
                if (user == null)
                {
                    QueueMessage(args.ServiceName, "You're not in the database, so I can't display your controller port.", Serilog.Events.LogEventLevel.Error);
                }
                else
                {
                    QueueMessage(args.ServiceName, $"Your controller port is {user.ControllerPort + 1}!");
                }

                return;
            }

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (StringNumParser.TryParseInt(arguments[0], out int portNum) == false)
            {
                QueueMessage(args.ServiceName, "That is not a valid number!");
                return;
            }

            int controllerCount = VControllerContainer.VControllerMngr.ControllerCount;

            if (portNum <= 0 || portNum > controllerCount)
            {
                QueueMessage(args.ServiceName, $"Please specify a number in the range of 1 through the current controller count ({controllerCount}).");
                return;
            }

            //Change to zero-based index for referencing
            int controllerNum = portNum - 1;

            if (user == null)
            {
                QueueMessage(args.ServiceName, "Somehow, you're an invalid user not in the database, so I can't change your controller port.", Serilog.Events.LogEventLevel.Error);
                return;
            }

            if (user.ControllerPort == controllerNum)
            {
                QueueMessage(args.ServiceName, "You're already on this controller port!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                //Change port and save data
                user.ControllerPort = controllerNum;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"{user.Name} changed their controller port to {portNum}!");
        }
    }
}
