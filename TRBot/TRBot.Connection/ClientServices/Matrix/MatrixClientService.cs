﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Matrix_CS_SDK;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using TRBot.Events;
using TRBot.Utilities.Logging;

namespace TRBot.Connection.Matrix
{
    /// <summary>
    /// Connection settings for Matrix.
    /// </summary>
    public class MatrixClientService : IClientService
    {
        private const string LOG_PREFIX_STRING = "[" + ClientServiceNames.MATRIX_SERVICE + "] ";

        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        public bool IsInitialized => IsConnected;

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        public bool IsConnected => Client?.IsLoggedIn == true && Client?.IsSyncing == true;

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        public bool CanSendMessages => IsConnected;

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        public IBotMessageHandler MsgHandler { get; private set; } = null;

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        public IMessageSettings MsgSettings { get; private set; } = null;

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        public ICommandSettings CmdSettings { get; private set; } = null;

        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        private bool Connecting = false;

        private MatrixLoginSettings LoginSettings = null;
        private IMatrixClient Client = null;

        private string BotDisplayName = string.Empty;
        private string NextBatch = null;

        private readonly string MatrixServiceName = ClientServiceNames.MATRIX_SERVICE;

        public MatrixClientService(MatrixLoginSettings loginSettings, ITRBotLogger logger, IEventDispatcher evtDispatcher,
            IBotMessageHandler msgHandler, IMessageSettings msgSettings, ICommandSettings cmdSettings)
        {
            LoginSettings = loginSettings;
            Logger = logger;
            EvtDispatcher = evtDispatcher;
            MsgHandler = msgHandler;
            MsgSettings = msgSettings;
            CmdSettings = cmdSettings;
        }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        public void Initialize()
        {
            Client = new MatrixClient(new MatrixHttpBackend(new HttpClient() { Timeout = TimeSpan.FromSeconds(40) }),
                LoginSettings.ServerAddress);
            
            Client.SyncInterval = 500;
            Client.SyncTimeout = 30000;

            Client.OnReceivedMessages -= HandleReceivedMessages;
            Client.OnReceivedMessages += HandleReceivedMessages;

            Client.OnSyncFailed -= HandleSyncFailed;
            Client.OnSyncFailed += HandleSyncFailed;

            Client.OnLoggedOut -= HandleLoggedOut;
            Client.OnLoggedOut += HandleLoggedOut;

            MsgHandler.SetClientService(this);

            EvtDispatcher.RegisterEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.RegisterEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.RegisterEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.RegisterEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.RegisterEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.RegisterEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND);

            EvtDispatcher.AddListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);
        }

        /// <summary>
        /// Connects the client.
        /// </summary>
        public void Connect()
        {
            AwaitConnected().ConfigureAwait(false);
        }

        private async Task AwaitConnected()
        {
            if (Connecting == true)
            {
                return;
            }

            Connecting = true;
            
            try
            {
                await Client.LoginWithPassword(LoginSettings.BotMatrixID,
                    LoginSettings.BotPassword, LoginSettings.DeviceID);
            }
            catch (Exception e)
            {
                Logger.Error($"Exception connecting to Matrix: {e.Message}");

                EvtConnectionErrorArgs connErrorArgs = new EvtConnectionErrorArgs()
                {
                    Error = new EvtErrorData(e.Message),
                    ServiceName = MatrixServiceName,
                    BotUsername = Client.UserID
                };

                EvtDispatcher.DispatchEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, connErrorArgs);

                return;
            }
            finally
            {
                Connecting = false;
            }

            BotDisplayName = MatrixHelpers.GetLocalPartFromMXID(Client.UserID);

            EvtConnectedArgs connectedArgs = new EvtConnectedArgs()
            {
                ServiceName = MatrixServiceName,
                BotUsername = Client.UserID
            };

            EvtDispatcher.DispatchEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, connectedArgs);

            Client.StartSyncing(NextBatch);
        }

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        public void Disconnect()
        {
            if (IsConnected == false)
            {
                Logger.Error("Invalid attempt to disconnect from Matrix while not connected.");
                return;
            }

            if (Client != null)
            {
                Client.StopSyncing();
                HandleLogout();
            }
        }

        private async void HandleLogout()
        {
            try
            {
                await Client.Logout();
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Processes a message and sends it through the client.
        /// </summary>
        /// <param name="serviceMessage">The message to process and send.</param>
        public async void ProcessAndSendMessage(IServiceMessage serviceMessage)
        {
            if (string.IsNullOrEmpty(serviceMessage.Message) == true)
            {
                Logger.Warning($"Empty message in {nameof(MatrixClientService)}.{nameof(ProcessAndSendMessage)}");
                return;
            }

            string sentMessage = serviceMessage.Message;

            if (serviceMessage.OverridePrefix == true)
            {
                sentMessage = serviceMessage.CustomPrefix + serviceMessage.Message;
            }
            else if (string.IsNullOrEmpty(MsgSettings.MessagePrefix) == false)
            {
                sentMessage = MsgSettings.MessagePrefix + serviceMessage.Message;
            }
            
            string roomID = LoginSettings.RoomID;

            //Send in a different room if it's a whisper message
            //The recipient name must be a room
            if (serviceMessage is WhisperMessage whisperMessage)
            {
                roomID = whisperMessage.RecipientName;
            }

            try
            {
                await Client.SendMessage(roomID, sentMessage);
            }
            catch (Exception exc)
            {
                Logger.Error($"Failed to send Matrix message: {exc.Message}");
                return;
            }

            var outgoingArgs = new EvtOutgoingBotMessageArgs()
            {
                ServiceName = MatrixServiceName,
                UserMessage = new EvtUserMsgData(Client.UserID, BotDisplayName, sentMessage)
            };

            EvtDispatcher.DispatchEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE, outgoingArgs);

            if (MsgSettings.LogToLogger == true)
            {
                Logger.Log(serviceMessage.LogLevel, LOG_PREFIX_STRING + sentMessage);
            }
        }

        public void Dispose()
        {
            Disconnect();

            MsgHandler.CleanUp();

            if (Client != null)
            {
                Client.OnReceivedMessages -= HandleReceivedMessages;
                Client.OnSyncFailed -= HandleSyncFailed;
                Client.OnLoggedOut -= HandleLoggedOut;    

                Client.Dispose();
                Client = null;
            }

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.COMMAND);

            EvtDispatcher.RemoveListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);
        }

        private void HandleReceivedMessages(object sender, ReceivedMessagesArgs e)
        {
            //Skip first sync since it doesn't contain updates
            if (NextBatch == null)
            {
                Logger.Debug("Skipping first Matrix room sync since it doesn't return deltas.");

                NextBatch = e.NextBatch;
                return;
            }

            NextBatch = e.NextBatch;

            for (int i = 0; i < e.ReceivedMessages.Length; i++)
            {
                ReceivedMessageData messageData = e.ReceivedMessages[i];
                
                //Check only for updates from the given room
                if (messageData.RoomID != LoginSettings.RoomID)
                {
                    continue;
                }

                if (string.IsNullOrEmpty(messageData.MessageContent.Body) == true)
                {
                    continue;
                }

                string externalID = messageData.Sender;

                //Matrix display names need to be fetched differently and ideally should be handled in the library
                //For the time being, get the name from the localpart of their Matrix ID
                //Ex. "@localpart:domain.org" - discard "domain.org" and trim the "@" to get "localpart" as the localpart
                int indexOfSeparator = externalID.IndexOf(':');
                ReadOnlySpan<char> usernameSpan = messageData.Sender.AsSpan(1, indexOfSeparator - 1);
                string usernameStr = usernameSpan.ToString();

                string lineWSReplaced = Utilities.Helpers.ReplaceAllWhitespaceWithSpace(messageData.MessageContent.Body);

                EvtUserMessageArgs umArgs = new EvtUserMessageArgs()
                {
                    ServiceName = MatrixServiceName,
                    UserMessage = new EvtUserMsgData(externalID, usernameStr, lineWSReplaced)
                };

                EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, umArgs);

                //Parse command
                //Username is currently the same as external ID since Matrix display names need to be fetched differently
                EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(lineWSReplaced,
                    CmdSettings.CommandIdentifier, new EvtUserMsgData(externalID, usernameStr, lineWSReplaced));
                
                if (chatCmdData != null)
                {
                    EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                    {
                        ServiceName = MatrixServiceName,
                        Command = chatCmdData
                    };

                    EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
                }
            }
        }

        private void HandleLoggedOut(object sender, EventArgs e)
        {
            EvtDisconnectedArgs disconnectedArgs = new EvtDisconnectedArgs()
            {
                ServiceName = MatrixServiceName
            };

            Connecting = false;

            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disconnectedArgs);
        }

        private void HandleSyncFailed(object sender, SyncFailedArgs e)
        {
            Logger.Warning($"Matrix sync failed with {e.MatrixErrCode} - {e.ErrorMessage}");

            NextBatch = e.NextBatch;
        }

        private void OnConnected(EvtConnectedArgs e)
        {
            if (e.ServiceName != MatrixServiceName)
            {
                return;
            }

            Logger.Information($"Connected to Matrix homeserver {LoginSettings.ServerAddress} as {Client.UserID}!");
        }
    }
}
