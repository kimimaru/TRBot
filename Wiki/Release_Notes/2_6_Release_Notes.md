TRBot v2.6.0, featuring XMPP support and support for incoming external events.

Summary of changes:
- XMPP support is in! Run TRBot over your favorite XMPP server, either in one-to-one or multi-user chats. XMPP WebSockets are supported too!
- [TwitchLib](https://github.com/TwitchLib/TwitchLib) has been upgraded to fix problems connecting to Twitch. In addition, there is improved support for rejoining Twitch channels on a disconnect.
- It's now possible to **define multiple invalid input combos** per console! In addition, each invalid input combo supports a level threshold to make the combination valid for certain access levels (Ex. Moderators and above)!
- Convenient commands are available to add/modify different types of inputs. These are [`AddAxisInputCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/TRBot/TRBot.Commands/Commands/AddAxisInputCommand.cs) (default: "!addaxis"), [`AddButtonInputCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/TRBot/TRBot.Commands/Commands/AddButtonInputCommand.cs) (default: "!addbutton"), and [`AddBlankInputCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/TRBot/TRBot.Commands/Commands/AddBlankInputCommand.cs) (default: "!addblank").
- A new [`ExecMultiCommandsFromTextOrFileCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/TRBot/TRBot.Commands/Commands/ExecMultiCommandsFromTextOrFileCommand.cs) command has been added to invoke any number of commands sequentially. This is especially useful for hosts with custom consoles to define said consoles in text files.
- Display ranks and custom messages now have an `Enabled` state in the database that determines if they're shown.
- In addition, custom messages can be sent just to the intended user instead of always to the entire room through the [`custom_message_send_type`](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/Wiki/Settings-Documentation.md#custom_message_send_type) setting.
- It's now possible to send events to TRBot with external applications. Please see the [event dispatcher](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/Wiki/Event-Dispatcher.md) documentation for more details.
- Fixed disabled inputs being passed through the parser.
- Fixed a rare crash in `CreditsGiveRoutine` if a user was deleted before the credits are given out.
- Time modifier parsing has been improved, allowing more accurate time periods (Ex. "30m15s" instead of only "30m").
- Fixed failing to obtain build information on newly cloned repositories. This affects only developers.
- Upgraded all projects to .NET 6. CI images have been updated as well.
- Deprecated settings have been removed in code and documentation. Regarding database upgrades in code, the settings have been replaced with hardcoded strings to still allow upgrading older versions of TRBot.

In addition, the more advanced [Syntax-Tutorial](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/Wiki/Syntax-Tutorial.md) has been updated, and all example GIFs are now hosted in the repository. Give it a look!

**IMPORTANT for those upgrading from previous 2.0+ releases:**
- There are breaking changes regarding invalid input combos. Since invalid input combos have a completely different database structure, and a join table with input IDs, all existing input combos are removed. It is little work for hosts to re-add them. The default consoles in this release have updated soft reset combos.

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.6.0/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
