TRBot v2.3.1, featuring custom routines, critical bug fixes, and numerous refinements.

Summary of changes:
- Fixed insecure WebSockets failing to connect.
- Fixed macros being case-sensitive.
- uinput controllers on GNU/Linux have been rewritten in evdev. The default raw minimum axes values on the controllers have also been changed from -32767 to 0.
- Added configuring routines via a new `RoutineData` table in the database. Custom routines are supported as well, similarly to custom commands.
- Virtual controller axes handling is fixed for vJoy and axes handling in general has been made consistent across the board.
- Supported decimal values for the `MinAxisVal` and `MaxAxisVal` columns in the `Inputs` table in the database. These values are now restricted in the range of 0.0 to 1.0.
- Added a `DefaultAxisVal` column in the `Inputs` table. This determines the at rest value of the axis when it's released and allows for better adapting each axis for its intended purpose.
- A new `message_prefix` setting is available in the database. If not empty, the bot will prepend the `ValueStr` of this prefix to each message it sends.
- Added a new `UserSimulateCommand` to simulate a user. This utilizes a markov chain text generator to create a sentence based on what the user typed in chat, excluding inputs and commands. This is an opt-in feature via the `UserSimulateManageCommand`.
- Improved the performance of the `UpdateEveryoneAbilitiesCommand` by roughly **50x**!
- Custom commands and routines can be written in a more robust and efficient way that uses far less memory.
- ChatterBot integration has been adjusted to be cross-platform.
- Fixed many outstanding issues with input exercises. They no longer fail on input sequences that look correct.
- Added an `InputLeaderboardCommand` to show a leaderboard regarding users' valid input count.
- All predefined GameConsoles have had their inputs revised for consistency, making it seamless to switch among them.

To run the main application, locate the **TRBot.Main** binary for your platform and execute it.

Please read the [wiki](https://codeberg.org/kimimaru/TRBot/src/tag/2.3.1/Wiki/Home.md) for your use-case.

**IMPORTANT for those upgrading from 2.3:**
- All predefined GameConsoles now use different default button and axes values. The easiest way to use these is to close TRBot, delete all predefined consoles in the database, set the `first_launch` and `force_init_default` settings to 1, then restart TRBot.
- Changes in the `MinAxisVal` and `MaxAxisVal` ranges may cause your axes to behave incorrectly. You will need to change these values on each axis to fix the problem. Please consult the [documentation here](https://codeberg.org/kimimaru/TRBot/src/tag/2.3.1/Wiki/Adding-ConsolesInputs.md) to learn how to define your axes values.
- Consult the updated [custom commands documentation](https://codeberg.org/kimimaru/TRBot/src/tag/2.3.1/Wiki/Custom-Commands.md) to learn how to rewrite your custom commands in the more robust, efficient way.

**IMPORTANT for those upgrading from 1.x releases:** Please read the [migration guide](https://codeberg.org/kimimaru/TRBot/src/tag/2.3.1/Wiki/Migrating-Data.md) and download the **TRBotDataMigrationTool** if you're upgrading from 1.8!

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.3.1/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
