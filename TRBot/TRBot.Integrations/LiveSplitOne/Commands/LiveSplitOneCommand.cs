﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using TRBot.Data;
using TRBot.Commands;
using TRBot.Connection;

namespace TRBot.Integrations.LiveSplitOne
{
    /// <summary>
    /// A command that controls a local LiveSplitOne instance through a WebSocket.
    /// </summary>
    public class LiveSplitOneCommand : BaseCommand
    {
        private const string DEFAULT_SOCKET_PATH = "/";

        /// <summary>
        /// The available LiveSplitOne commands.
        /// </summary>
        private readonly Dictionary<string, int> AvailableCommands = new Dictionary<string, int>()
        {
            { "start", 0 },
            { "split", 0 },
            { "splitorstart", 0 },
            { "reset", 0 },
            { "togglepause", 0 },
            { "undo", 0 },
            { "skip", 0 },
            { "initgametime", 0 },
            { "setgametime", 1 },
            { "setloadingtimes", 1 },
            { "pausegametime", 0 },
            { "resumegametime", 0 },
        };

        //Configuration for the WebSocket
        private string SocketPath = DEFAULT_SOCKET_PATH;

        private string CachedCmdsStr = string.Empty;

        public LiveSplitOneCommand()
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            InitializeCmdMessage();

            //Get database value for the path
            SocketPath = DatabaseMngr.GetSettingString(LiveSplitOneSettingsConstants.LIVESPLITONE_WEBSOCKET_PATH, DEFAULT_SOCKET_PATH);

            WebSocketMngr.AddServerService<LiveSplitOneSocketService>(SocketPath, null);
        }

        public override void CleanUp()
        {
            WebSocketMngr.RemoveServerService(SocketPath);

            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            if (WebSocketMngr.IsServerListening == false)
            {
                QueueMessage(args.ServiceName, "ERROR: Cannot send command because the WebSocket server isn't running!", Serilog.Events.LogEventLevel.Warning);
                return;
            }

            //Validate arguments
            List<string> argList = args.Command.ArgumentsAsList;

            if (argList.Count == 0)
            {
                QueueMessage(args.ServiceName, $"Please specify a command: {CachedCmdsStr}");
                return;
            }

            string cmdArg = argList[0].ToLowerInvariant();

            //Invalid command
            if (AvailableCommands.TryGetValue(cmdArg, out int argCount) == false)
            {
                QueueMessage(args.ServiceName, $"\"{cmdArg}\" is an invalid command. Please specify a valid command: {CachedCmdsStr}");
                return;
            }

            //Invalid number of arguments for this LiveSplitOne command
            if ((argList.Count - 1) != argCount)
            {
                if (argCount <= 0)
                {
                    QueueMessage(args.ServiceName, "\"{cmdArg}\" doesn't take any arguments!");
                }
                else
                {
                    QueueMessage(args.ServiceName, $"\"{cmdArg}\" requires {argCount} argument(s)!");
                }

                return;
            }

            //Send the data over the websocket
            WebSocketMngr.ServerBroadcastAsync(SocketPath, args.Command.ArgumentsAsString.ToLowerInvariant(),
                null, (Exception e) => 
                {
                    QueueMessage(args.ServiceName, $"Issue broadcasting WebSocket message: {e.Message}", Serilog.Events.LogEventLevel.Warning);        
                });
        }

        private void InitializeCmdMessage()
        {
            //Build the message
            StringBuilder strBuilder = new StringBuilder(256);

            foreach (KeyValuePair<string, int> kvPair in AvailableCommands)
            {
                strBuilder.Append('"').Append(kvPair.Key).Append('"').Append(',').Append(' ');
            }

            if (strBuilder.Length > 1)
            {
                strBuilder.Remove(strBuilder.Length - 2, 2);

                CachedCmdsStr = strBuilder.ToString();
            }
        }
    }
}
