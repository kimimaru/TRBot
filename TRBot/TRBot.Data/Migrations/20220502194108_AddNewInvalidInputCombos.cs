﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TRBot.Data.Migrations
{
    public partial class AddNewInvalidInputCombos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "invalidinputcombos");

            migrationBuilder.CreateTable(
                name: "InvalidInputCombos",
                schema: "invalidinputcombos",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ConsoleID = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Enabled = table.Column<long>(type: "INTEGER", nullable: false, defaultValue: 1L)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvalidInputCombos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InvalidInputCombos_Consoles_ConsoleID",
                        column: x => x.ConsoleID,
                        principalSchema: "consoles",
                        principalTable: "Consoles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InvalidInputCombosInputs",
                columns: table => new
                {
                    InvalidCombosID = table.Column<int>(type: "INTEGER", nullable: false),
                    InvalidInputsID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvalidInputCombosInputs", x => new { x.InvalidCombosID, x.InvalidInputsID });
                    table.ForeignKey(
                        name: "FK_InvalidInputCombosInputs_Inputs_InvalidInputsID",
                        column: x => x.InvalidInputsID,
                        principalSchema: "inputs",
                        principalTable: "Inputs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvalidInputCombosInputs_InvalidInputCombos_InvalidCombosID",
                        column: x => x.InvalidCombosID,
                        principalSchema: "invalidinputcombos",
                        principalTable: "InvalidInputCombos",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvalidInputCombos_ConsoleID",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                column: "ConsoleID");

            migrationBuilder.CreateIndex(
                name: "IX_InvalidInputCombos_Name_ConsoleID",
                schema: "invalidinputcombos",
                table: "InvalidInputCombos",
                columns: new[] { "Name", "ConsoleID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InvalidInputCombosInputs_InvalidInputsID",
                table: "InvalidInputCombosInputs",
                column: "InvalidInputsID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvalidInputCombosInputs");

            migrationBuilder.DropTable(
                name: "InvalidInputCombos",
                schema: "invalidinputcombos");
        }
    }
}
