﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection
{
    /// <summary>
    /// Settings for client service commands.
    /// </summary>
    public class CommandSettings : ICommandSettings
    {
        /// <summary>
        /// The string that identifies each message as a command.
        /// </summary>
        public string CommandIdentifier { get; private set; } = "!";

        public CommandSettings(string commandIdentifier)
        {
            SetCommandIdentifier(commandIdentifier);
        }

        /// <summary>
        /// Sets the command identifier.
        /// </summary>
        /// <param name="commandIdentifier">A string representing the command identifier.</param>
        public void SetCommandIdentifier(string commandIdentifier)
        {
            CommandIdentifier = commandIdentifier;
        }
    }
}
