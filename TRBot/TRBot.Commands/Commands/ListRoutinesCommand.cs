﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Lists all routines.
    /// </summary>
    public sealed class ListRoutinesCommand : BaseCommand
    {
        private const string DISABLED_ARG = "disabled";
        private const string ALL_ARG = "all";

        private string UsageMessage = "Usage: no arguments (enabled + running routines), \"disabled (optional)\" (only disabled routines), or \"all (optional)\" (enabled, disabled, and running routines)";

        public ListRoutinesCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string arg = string.Empty;

            if (arguments.Count > 0)
            {
                arg = arguments[0].ToLowerInvariant();

                //Validate argument
                if (arg != DISABLED_ARG && arg != ALL_ARG)
                {
                    QueueMessage(args.ServiceName, UsageMessage);
                    return;
                }
            }

            StringBuilder stringBuilder = null;

            List<string> routineNameList = new List<string>();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //All routines
                if (arg == ALL_ARG)
                {
                    routineNameList.AddRange(context.Routines.Select(r => r.Enabled <= 0 ? r.Name + " (disabled)" : r.Name));
                }
                //Disabled routines only
                else if (arg == DISABLED_ARG)
                {
                    routineNameList.AddRange(context.Routines.Where(r => r.Enabled <= 0).Select(r => r.Name + " (disabled)"));
                }
                //Enabled routines
                else
                {
                    routineNameList.AddRange(context.Routines.Where(r => r.Enabled > 0).Select(r => r.Name));
                }
            }

            if (arg != DISABLED_ARG)
            {
                var runningRoutines = RoutineHandler.GetRunningRoutineNames();
                for (int i = 0; i < runningRoutines.Length; i++)
                {
                    //Don't add duplicates
                    if (routineNameList.Any(rn => rn.StartsWith(runningRoutines[i], StringComparison.Ordinal)) == false)
                    {
                        routineNameList.Add(runningRoutines[i]);
                    }
                }
            }
                
            //Sort alphabetically
            routineNameList = routineNameList.OrderBy(r => r).ToList();
            int routineCount = routineNameList.Count();

            if (routineCount == 0)
            {
                QueueMessage(args.ServiceName, "No routines found.");
                return;
            }

            //The capacity is estimated by the number of routines times the average string length of each one
            stringBuilder = new StringBuilder(routineCount * 12);

            stringBuilder.Append("Hi ").Append(args.Command.UserMessage.Username).Append(", here's the list of routines: ");

            for (int i = 0; i < routineNameList.Count; i++)
            {
                stringBuilder.Append(routineNameList[i]);
                stringBuilder.Append(',').Append(' ');
            }

            stringBuilder.Remove(stringBuilder.Length - 2, 2);

            QueueMessage(args.ServiceName, stringBuilder.ToString(), ", ");
        }
    }
}
