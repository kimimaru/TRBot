﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Data
{
    /// <summary>
    /// Constants for bot setting keys.
    /// </summary>
    public static class SettingsConstants
    {
        public const string MAIN_THREAD_SLEEP = "main_thread_sleep";
        
        public const string CREDITS_NAME = "credits_name";
        public const string CREDITS_GIVE_TIME = "credits_give_time";
        public const string CREDITS_GIVE_AMOUNT = "credits_give_amount";
        public const string DUEL_TIMEOUT = "duel_timeout";
        public const string GROUP_BET_TOTAL_TIME = "group_bet_total_time";
        public const string GROUP_BET_MIN_PARTICIPANTS = "group_bet_min_participants";

        public const string COMMAND_IDENTIFIER = "command_identifier";

        public const string CHATBOT_ENABLED = "chatbot_enabled";
        public const string CHATBOT_SOCKET_HOSTNAME = "chatbot_socket_hostname";
        public const string CHATBOT_SOCKET_PORT = "chatbot_socket_port";

        public const string LOG_LEVEL = "log_level";

        public const string AUTO_PROMOTE_ENABLED = "auto_promote_enabled";
        public const string AUTO_PROMOTE_LEVEL = "auto_promote_level";
        public const string AUTO_PROMOTE_INPUT_REQ = "auto_promote_input_req";

        public const string BOT_MESSAGES_ENABLED = "bot_messages_enabled";
        public const string BOT_MSG_CHAR_LIMIT = "bot_message_char_limit";
        public const string PERIODIC_MSG_TIME = "periodic_message_time";
        public const string PERIODIC_MSG_PREREQ_MSG_COUNT = "periodic_message_prereq_msg_count";
        public const string MESSAGE_THROTTLE_TYPE = "message_throttle_type";
        public const string MESSAGE_COOLDOWN = "message_cooldown";
        public const string MESSAGE_THROTTLE_COUNT = "message_throttle_count";
        public const string MESSAGE_PREFIX = "message_prefix";
        public const string RECONNECT_TIME = "reconnect_time";

        public const string CONNECT_MESSAGE = "connect_message";
        public const string RECONNECTED_MESSAGE = "reconnected_message";
        public const string PERIODIC_MESSAGE = "periodic_message";
        public const string AUTOPROMOTE_MESSAGE = "auto_promote_message";
        public const string NEW_USER_MESSAGE = "new_user_message";
        public const string BEING_HOSTED_MESSAGE = "being_hosted_message";
        public const string NEW_SUBSCRIBER_MESSAGE = "new_subscriber_message";
        public const string RESUBSCRIBER_MESSAGE = "resubscriber_message";
        public const string SOURCE_CODE_MESSAGE = "source_code_message";
        public const string PERIODIC_MESSAGE_ROTATION = "periodic_message_rotation";
        public const string RANK_UP_MESSAGE = "rank_up_message";

        public const string GAME_MESSAGE_PATH = "game_message_path";
        public const string GAME_MESSAGE_PATH_IS_RELATIVE = "game_message_path_is_relative";
        public const string INFO_MESSAGE = "info_message";
        public const string TUTORIAL_MESSAGE = "tutorial_message";
        public const string DOCUMENTATION_MESSAGE = "documentation_message";
        public const string DONATE_MESSAGE = "donate_message";
        
        public const string SYNTAX_REFERENCE_INPUTS_MESSAGE = "syntax_reference_input_sequence_message";
        public const string SYNTAX_REFERENCE_DURATION_MESSAGE = "syntax_reference_duration_message";
        public const string SYNTAX_REFERENCE_SIMULTANEOUS_MESSAGE = "syntax_reference_simultaneous_message";
        public const string SYNTAX_REFERENCE_DELAY_MESSAGE = "syntax_reference_delay_message";
        public const string SYNTAX_REFERENCE_HOLD_MESSAGE = "syntax_reference_hold_message";
        public const string SYNTAX_REFERENCE_RELEASE_MESSAGE = "syntax_reference_release_message";
        public const string SYNTAX_REFERENCE_REPEATED_MESSAGE = "syntax_reference_repeated_message";
        public const string SYNTAX_REFERENCE_MULTIPLAYER_MESSAGE = "syntax_reference_multiplayer_message";
        public const string SYNTAX_REFERENCE_MACROS_MESSAGE = "syntax_reference_macros_message";
        public const string SYNTAX_REFERENCE_DYNAMIC_MACROS_MESSAGE = "syntax_reference_dynamic_macros_message";
        public const string SYNTAX_REFERENCE_COMMENTS_MESSAGE = "syntax_reference_comments_message";

        public const string PERIODIC_INPUT_ENABLED = "periodic_input_enabled";
        public const string PERIODIC_INPUT_TIME = "periodic_input_time";
        public const string PERIODIC_INPUT_PORT = "periodic_input_port";
        public const string PERIODIC_INPUT_VALUE = "periodic_input_value";
        public const string PERIODIC_INPUT_MESSAGE_ENABLED = "periodic_input_message_enabled";

        public const string TEAMS_MODE_ENABLED = "teams_mode_enabled";
        public const string TEAMS_MODE_MAX_PORT = "teams_mode_max_port";
        public const string TEAMS_MODE_NEXT_PORT = "teams_mode_next_port";

        public const string DEFAULT_INPUT_DURATION = "default_input_duration";
        public const string MAX_INPUT_DURATION = "max_input_duration";
        
        public const string INPUTS_ENABLED = "inputs_enabled";

        public const string GLOBAL_MID_INPUT_DELAY_ENABLED = "global_mid_input_delay_enabled";
        public const string GLOBAL_MID_INPUT_DELAY_TIME = "global_mid_input_delay_time";

        public const string MAX_USER_RECENT_INPUTS = "max_user_recent_inputs";
        public const string MAX_USER_SIMULATE_STRING_LENGTH = "max_user_simulate_string_length";
        public const string USER_SIMULATE_CREDIT_COST = "user_simulate_credit_cost";

        public const string WEBSOCKET_SERVER_ENABLED = "websocket_server_enabled";
        public const string WEBSOCKET_SERVER_ADDRESS = "websocket_server_address";

        public const string EVENT_DISPATCHER_EXTRA_FEATURES_ENABLED = "event_dispatcher_extra_features_enabled";
        public const string EVENT_DISPATCHER_WEBSOCKET_PATH = "event_dispatcher_websocket_path";

        public const string CUSTOM_MESSAGES_ENABLED = "custom_messages_enabled";
        public const string CUSTOM_MESSAGE_SEND_TYPE = "custom_message_send_type";

        public const string DEMOCRACY_VOTE_TIME = "democracy_vote_time";
        public const string DEMOCRACY_RESOLUTION_MODE = "democracy_resolution_mode";
        public const string INPUT_MODE_VOTE_TIME = "input_mode_vote_time";
        public const string INPUT_MODE_CHANGE_COOLDOWN = "input_mode_change_cooldown";
        public const string INPUT_MODE_NEXT_VOTE_DATE = "input_mode_next_vote_date";

        public const string LAST_CONSOLE = "last_console";
        public const string LAST_VCONTROLLER_TYPE = "last_vcontroller_type";
        public const string JOYSTICK_COUNT = "joystick_count";

        public const string GLOBAL_INPUT_LEVEL = "global_input_level";
        public const string INPUT_MODE = "input_mode";

        public const string FIRST_LAUNCH = "first_launch";
        public const string FORCE_INIT_DEFAULTS = "force_init_defaults";
        public const string DATA_VERSION_NUM = "data_version";
    }
}
