﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Input;
using TRBot.Input.Parsing;
using TRBot.Input.Consoles;
using TRBot.Data;
using TRBot.Utilities;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Handles input exercises for players.
    /// </summary>
    public sealed class InputExerciseCommand : BaseCommand
    {
        private const int MIN_INPUTS = 3;
        private const int MAX_INPUTS = 8;
        private const int MIN_SUB_SEQUENCES = 2;
        private const int MAX_SUB_SEQUENCES = 4;

        private const int MIN_SECONDS_VAL = 1;
        private const int MAX_SECONDS_VAL = 26;

        private const int MIN_PERCENT_VAL = 1;
        private const int MAX_PERCENT_VAL = 100;

        private const int BASE_CREDIT_REWARD = 100;
        private const int CREDIT_REWARD_MULTIPLIER = 3;
        private const double HARD_EXERCISE_MULTIPLIER = 1.3d;

        private const int MAX_PORTS = 8;

        private const string GENERATE_NEW_ARG = "new";
        private const string EASY_DIFFICULTY_ARG = "easy";
        private const string HARD_DIFFICULTY_ARG = "hard";

        private const string NO_EXERCISE_FOUND_MSG = "No input exercise found. Generate a new one with \"" + GENERATE_NEW_ARG + "\" as an argument to this command, optionally with a difficulty level: \"" + EASY_DIFFICULTY_ARG + "\" or \"" + HARD_DIFFICULTY_ARG + "\".";

        private const char SPLIT_CHAR = ',';

        private ConcurrentDictionary<string, InputExercise> UserExercises = null;
        private string[] InvalidExerciseInputNames = Array.Empty<string>();
        private readonly Random Rand = new Random();

        //private string UsageMessage = "Usage: (\"new\" \"difficulty - easy (default) or hard\") for new exercise, or \"input sequence\" to input exercise";

        public InputExerciseCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            if (UserExercises == null)
            {
                UserExercises = new ConcurrentDictionary<string, InputExercise>(Environment.ProcessorCount * 2, 32);
            }

            //Split inputs by what's in the value string
            if (string.IsNullOrEmpty(ValueStr) == false)
            {
                InvalidExerciseInputNames = ValueStr.Split(SPLIT_CHAR, StringSplitOptions.RemoveEmptyEntries);
                
                //Trim whitespace
                for (int i = 0; i < InvalidExerciseInputNames.Length; i++)
                {
                    InvalidExerciseInputNames[i] = InvalidExerciseInputNames[i].Trim();
                }
            }
        }

        public override void CleanUp()
        {
            if (UserExercises != null)
            {
                UserExercises.Clear();
                UserExercises = null;
            }

            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;
            string userExternalID = args.Command.UserMessage.UserExternalID;
            long userLevel = 0;

            using (var context = DatabaseMngr.OpenContext())
            {
                User user = context.Users.AsNoTracking().Include(u => u.Stats).Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility).FirstOrDefault(u => u.ExternalID == userExternalID);

                if (user == null)
                {
                    QueueMessage(args.ServiceName, "You're not in the database, so you can't perform exercises. Sorry!");
                    return;
                }

                if (user.HasEnabledAbility(PermissionConstants.INPUT_EXERCISE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to use input exercises!");
                    return;
                }

                userLevel = user.Level;
            }

            //Get the last console used
            int lastConsoleID = (int)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);

            IGameConsole usedConsole = null;

            using (var context = DatabaseMngr.OpenContext())
            {
                IGameConsole lastConsole = context.Consoles.AsNoTracking().Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos).ThenInclude(ic => ic.InvalidInputs).FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            //If there are no valid inputs, don't attempt to generate or solve
            if (usedConsole == null)
            {
                QueueMessage(args.ServiceName, $"The current console does not point to valid data, making it impossible to solve or generate a new exercise. Please set a different console to use, or if none are available, add one.");
                return;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current console, \"{usedConsole.Name}\", does not have any available inputs. Cannot determine solve or generate an exercise.");
                return;
            }

            string creditsName = DatabaseMngr.GetCreditsName();

            //Since the default controller port for input exercises is port 1, use the same value for the display
            ReverseParser.ReverseParserOptions parseOptions = new ReverseParser.ReverseParserOptions(ReverseParser.ShowPortTypes.None, 0);

            //Handle no arguments
            if (arguments.Count == 0)
            {
                if (UserExercises.TryGetValue(userExternalID, out InputExercise inputExercise) == true)
                {
                    OutputInputExercise(args, inputExercise, usedConsole, creditsName, inputExercise.ParseOptions);
                }
                else
                {
                    QueueMessage(args.ServiceName, NO_EXERCISE_FOUND_MSG);
                }
                return;
            }

            //If "new" is specified, generate a new input sequence
            if ((arguments.Count == 1 || arguments.Count == 2) && arguments[0].ToLowerInvariant() == GENERATE_NEW_ARG)
            {
                //Check for a difficulty argument
                if (arguments.Count == 2)
                {
                    string difString = arguments[1].ToLowerInvariant();

                    //Check difficulty level
                    if (difString == EASY_DIFFICULTY_ARG)
                    {
                        parseOptions.ShowPortType = ReverseParser.ShowPortTypes.None;
                    }
                    else if (difString == HARD_DIFFICULTY_ARG)
                    {
                        parseOptions.ShowPortType = ReverseParser.ShowPortTypes.ShowNonDefaultPorts;
                    }
                    else
                    {
                        QueueMessage(args.ServiceName, $"Invalid difficulty level specified. Please choose either \"{EASY_DIFFICULTY_ARG}\" or \"{HARD_DIFFICULTY_ARG}\".");
                        return;
                    }
                }

                //Use the global default input duration for consistency
                int defaultInputDur = (int)DatabaseMngr.GetSettingInt(SettingsConstants.DEFAULT_INPUT_DURATION, 200L);

                //Generate the exercise 
                ParsedInputSequence newSequence = GenerateExercise(userLevel, defaultInputDur, usedConsole, parseOptions);

                //Give greater credit rewards for longer input sequences
                long creditReward = (newSequence.Inputs.Count * BASE_CREDIT_REWARD) * CREDIT_REWARD_MULTIPLIER;

                //This parser option is set when performing a hard exercise 
                if (parseOptions.ShowPortType == ReverseParser.ShowPortTypes.ShowNonDefaultPorts)
                {
                    creditReward = (long)Math.Ceiling(creditReward * HARD_EXERCISE_MULTIPLIER);
                }

                InputExercise inputExercise = new InputExercise(newSequence, parseOptions, creditReward);
                UserExercises[userExternalID] = inputExercise;

                OutputInputExercise(args, inputExercise, usedConsole, creditsName, parseOptions);      
                return;
            }

            //Make sure the user has an exercise - if not, output the same message
            if (UserExercises.TryGetValue(userExternalID, out InputExercise exercise) == false)
            {
                QueueMessage(args.ServiceName, NO_EXERCISE_FOUND_MSG);
                return;
            }

            //There's more than one argument and the user has an exercise; so it has to be the input
            //Let's validate it!
            if (ExecuteValidateInput(args, userExternalID, args.Command.ArgumentsAsString, usedConsole, parseOptions) == true)
            {
                using (var context = DatabaseMngr.OpenContext())
                {
                    User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                    //Grant credits if the user is opted into stats
                    if (user.HasConsentToDataOption(UserDataConsentOptions.Stats) == true)
                    {
                        long creditReward = exercise.CreditReward;
                        user.Stats.Credits += creditReward;
                        context.SaveChanges();

                        QueueMessage(args.ServiceName, $"Correct input! Awesome job! You've earned your {creditReward} {creditsName.Pluralize(creditReward)}!");
                    }
                    else
                    {
                        QueueMessage(args.ServiceName, "Correct input! Awesome job!");
                    }
                }

                //Remove the entry
                UserExercises.TryRemove(userExternalID, out InputExercise value);
            }
        }

        private void OutputInputExercise(EvtChatCommandArgs args, in InputExercise inputExercise, IGameConsole console,
            string creditsName, in ReverseParser.ReverseParserOptions options)
        {
            //Get the input in natural language
            string reverseSentence = ReverseParser.ReverseParseNatural(inputExercise.Sequence, console, options);
            
            reverseSentence += " | " + inputExercise.CreditReward + " " + creditsName + " reward.";

            QueueMessage(args.ServiceName, reverseSentence, ", ");

            //Add another message telling what to do
            QueueMessage(args.ServiceName, $"Put your input as an argument to this command. To generate a new exercise, pass \"{GENERATE_NEW_ARG}\" as an argument.");
        }

        private bool ExecuteValidateInput(EvtChatCommandArgs args, string userExternalID, string userInput, IGameConsole console,
            in ReverseParser.ReverseParserOptions options)
        {
            /* We don't need any parser post processing done here, as these inputs don't affect the game itself */

            //Use the global default input duration for consistency
            int defaultInputDur = (int)DatabaseMngr.GetSettingInt(SettingsConstants.DEFAULT_INPUT_DURATION, 200L);

            Logger.Debug($"USER INPUT: {userInput}");

            ParsedInputSequence inputSequence = default;

            try
            {
                using (var context = DatabaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //Parse inputs to get our parsed input sequence
                    //Ignore input synonyms and max duration
                    IParser standardParser = new StandardParserFactory(context.Macros, null,
                            console.GetInputNames(InputNameFiltering.Enabled), 0, int.MaxValue, defaultInputDur, 0, false, null)
                        .Create();

                    inputSequence = standardParser.ParseInputs(userInput);
                }
            }
            catch (Exception e)
            {
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;

                QueueMessage(args.ServiceName, $"Sorry, I couldn't parse your input: {e.Message}");
                return false;
            }

            Logger.Debug($"RESULT: {inputSequence.ParsedInputResult}");

            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                if (string.IsNullOrEmpty(inputSequence.Error) == true)
                {
                    QueueMessage(args.ServiceName, "Sorry, I couldn't parse your input.");
                }
                else
                {
                    QueueMessage(args.ServiceName, $"Sorry, I couldn't parse your input: {inputSequence.Error}");
                }
                return false;
            }

            InputExercise currentExercise = UserExercises[userExternalID];
            
            Logger.Debug($"Correct: {ReverseParser.ReverseParse(currentExercise.Sequence, console, options)}");
            
            List<List<ParsedInput>> exerciseInputs = currentExercise.Sequence.Inputs;

            List<List<ParsedInput>> userInputs = inputSequence.Inputs;

            //Compare input lengths - this has some downsides, but it's a quick check
            if (userInputs.Count != exerciseInputs.Count)
            {
                Logger.Debug($"COUNT DISPARITY {userInputs.Count} vs {exerciseInputs.Count}");

                QueueMessage(args.ServiceName, "Incorrect input! Try again!");
                return false;
            }

            for (int i = 0; i < exerciseInputs.Count; i++)
            {
                List<ParsedInput> exerciseSubInputs = exerciseInputs[i];
                List<ParsedInput> userSubInputs = userInputs[i];

                if (exerciseSubInputs.Count != userSubInputs.Count)
                {
                    Logger.Debug($"SUBINPUT COUNT DISPARITY AT {i}: {userSubInputs.Count} vs {exerciseSubInputs.Count}");

                    QueueMessage(args.ServiceName, "Incorrect input! Try again!");
                    return false;
                }

                //Now validate each input
                for (int j = 0; j < exerciseSubInputs.Count; j++)
                {
                    ParsedInput excInp = exerciseSubInputs[j];
                    ParsedInput userInp = userSubInputs[j];

                    //For simplicity when comparing, if the user put a blank input where the exercise is expecting it,
                    //use the same blank input as the exercise so it's not marked as incorrect
                    if (console.IsBlankInput(excInp) == true && console.IsBlankInput(userInp) == true)
                    {
                        userInp.Name = excInp.Name;
                    }

                    if (excInp != userInp)
                    {
                        Logger.Debug($"FAILED COMPARISON ON: {userInp.ToString()} ===== CORRECT: {excInp.ToString()}");

                        QueueMessage(args.ServiceName, "Incorrect input! Try again!");
                        return false;
                    }
                }
            }

            //No errors - the input should be correct!
            return true;
        }

        #region Exercise Generation

        private ParsedInputSequence GenerateExercise(in long userLevel, in int defaultInputDur, IGameConsole console,
            in ReverseParser.ReverseParserOptions options)
        {
            int numInputs = Rand.Next(MIN_INPUTS, MAX_INPUTS);

            ParsedInputSequence inputSequence = default;
            inputSequence.ParsedInputResult = ParsedInputResults.Valid;
            List<List<ParsedInput>> exerciseInputs = new List<List<ParsedInput>>(numInputs);
            inputSequence.Inputs = exerciseInputs;

            List<string> heldInputs = new List<string>();

            for (int i = 0; i < numInputs; i++)
            {
                bool haveSubSequence = (Rand.Next(0, 2) == 0);
                int subSequences = (haveSubSequence == false) ? 1 : Rand.Next(MIN_SUB_SEQUENCES, MAX_SUB_SEQUENCES);

                List<ParsedInput> subSequence = GenerateSubSequence(subSequences, heldInputs, defaultInputDur,
                    userLevel, console, options);
                exerciseInputs.Add(subSequence);
            }

            return inputSequence;
        }

        private List<ParsedInput> GenerateSubSequence(in int numSubSequences, List<string> heldInputs,
            in int defaultInputDur, in long userLevel, IGameConsole console, in ReverseParser.ReverseParserOptions options)
        {
            List<ParsedInput> subSequence = new List<ParsedInput>(numSubSequences);

            //Trim inputs that shouldn't be chosen in exercises
            //NOTE: To improve performance we should trim only at the highest level, not here in the subsequences
            List<InputData> validInputs = TrimInvalidExerciseInputs(userLevel, console.ConsoleInputs);

            //If there's more than one subsequence, remove blank inputs since they're redundant in this case
            if (numSubSequences > 1)
            {
                for (int i = validInputs.Count - 1; i >= 0; i--)
                {
                    if (validInputs[i].InputType == InputTypes.Blank)
                    {
                        validInputs.RemoveAt(i);
                    }
                }
            }

            for (int i = 0; i < numSubSequences; i++)
            {
                //If we run out of valid inputs (Ex. consoles with very few buttons), get out of the loop
                if (validInputs.Count == 0)
                {
                    break;
                }

                ParsedInput input = ParsedInput.Default(defaultInputDur);

                int chosenInputIndex = Rand.Next(0, validInputs.Count);
                input.Name = validInputs[chosenInputIndex].Name;

                input.Duration = Rand.Next(MIN_SECONDS_VAL, MAX_SECONDS_VAL);
                bool useMilliseconds = (Rand.Next(0, 2) == 0);

                //If using milliseconds instead of seconds, multiply by 100 for more multiples of 100
                if (useMilliseconds == true)
                {
                    input.Duration *= 100;
                    input.DurationType = InputDurationTypes.Milliseconds;
                }
                else
                {
                    input.Duration *= 1000;
                    input.DurationType = InputDurationTypes.Seconds;
                }

                //Decide whether to hold or release this input if it's not a wait input
                if (console.IsBlankInput(input) == false)
                {
                    bool holdRelease = (Rand.Next(0, 2) == 0);
                    if (holdRelease == true)
                    {
                        //If already held, release this input
                        if (heldInputs.Contains(input.Name) == true)
                        {
                            input.Release = true;
                            heldInputs.Remove(input.Name);
                        }
                        else
                        {
                            input.Hold = true;
                            heldInputs.Add(input.Name);
                        }
                    }

                    //Randomize port if we should
                    if (options.ShowPortType != ReverseParser.ShowPortTypes.None)
                    {
                        int randPort = Rand.Next(0, MAX_PORTS);
                        input.ControllerPort = randPort;
                    }
                }

                //Check for choosing a percent if the input is an axes
                if (console.IsAxis(input) == true)
                {
                    bool usePercent = (Rand.Next(0, 2) == 0);

                    if (usePercent == true)
                    {
                        input.Percent = Rand.Next(MIN_PERCENT_VAL, MAX_PERCENT_VAL);
                    }
                }

                //Remove the input in the subsequence so it can't be chosen again, as it doesn't make sense
                //This prevents something like "a600ms+b400ms+a100ms" from happening
                validInputs.RemoveAt(chosenInputIndex);

                subSequence.Add(input);
            }

            return subSequence;
        }

        private List<InputData> TrimInvalidExerciseInputs(in long userLevel, Dictionary<string, InputData> validInputs)
        {
            List<InputData> inputs = new List<InputData>(validInputs.Values);

            for (int i = inputs.Count - 1; i >= 0; i--)
            {
                InputData inp = inputs[i];

                //Remove the input if the user doesn't have access to use it or if the input is disabled
                if (userLevel < inputs[i].Level || inputs[i].IsEnabled == false)
                {
                    inputs.RemoveAt(i);
                    continue;
                }

                //Also remove any inputs with prohibited names
                for (int j = 0; j < InvalidExerciseInputNames.Length; j++)
                {
                    string invalidInputName = InvalidExerciseInputNames[j];
                    if (inp.Name == invalidInputName)
                    {
                        inputs.RemoveAt(i);
                        break;
                    }
                }
            }

            return inputs;
        }

        #endregion

        /// <summary>
        /// Represents an input exercise containing an input sequence and a credit reward for matching it.
        /// </summary>
        private struct InputExercise
        {
            public ParsedInputSequence Sequence;
            public ReverseParser.ReverseParserOptions ParseOptions;
            public long CreditReward;

            public InputExercise(in ParsedInputSequence sequence, in ReverseParser.ReverseParserOptions parseOptions, in long creditReward)
            {
                Sequence = sequence;
                ParseOptions = parseOptions;
                CreditReward = creditReward;
            }

            public override bool Equals(object obj)
            {
                if (obj is InputExercise inpExc)
                {
                    return (this == inpExc);
                }

                return false;
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hash = 11;
                    hash = (hash * 37) + Sequence.GetHashCode();
                    hash = (hash * 37) + CreditReward.GetHashCode();
                    hash = (hash * 37) + ParseOptions.GetHashCode();
                    return hash;
                }
            }

            public static bool operator==(InputExercise a, InputExercise b)
            {
                return (a.CreditReward == b.CreditReward && a.Sequence == b.Sequence && a.ParseOptions == b.ParseOptions);
            }

            public static bool operator!=(InputExercise a, InputExercise b)
            {
                return !(a == b);
            }
        }
    }
}
