﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// A command allowing a user to manage their data consent settings, opting into or out of any sort of data.
    /// For simplicity, this doesn't factor in existing processes, such as ongoing duels or group bets.
    /// </summary>
    public sealed class UserManageDataConsentCommand : BaseCommand
    {
        private string UsageMessage = string.Empty;
        
        private const string OPTIN_ARG = "optin";
        private const string OPTOUT_ARG = "optout";

        public UserManageDataConsentCommand()
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            UsageMessage = $"Usage: Either no arguments, \"username\", or (\"{OPTIN_ARG}\" or \"{OPTOUT_ARG}\")";
            
            string msgAddition = string.Empty;

            //Add all options
            string[] names = EnumUtility.GetNames<UserDataConsentOptions>.EnumNames;
            for (int i = 0; i < names.Length; i++)
            {
                if (EnumUtility.TryParseEnumValue(names[i], out UserDataConsentOptions consentOption) == true
                    && consentOption == UserDataConsentOptions.None)
                {
                    continue;
                }

                msgAddition += names[i] + ", ";
            }

            if (msgAddition.Length >= 2)
            {
                msgAddition = msgAddition.Remove(msgAddition.Length - 2, 2);
                UsageMessage += " followed by one or more of: " + msgAddition;
            }
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            string userName = args.Command.UserMessage.Username;
            string userExternalID = args.Command.UserMessage.UserExternalID;

            //Print consent options for current user or the given user, whichever is applicable
            if (arguments.Count <= 1)
            {
                if (arguments.Count == 1)
                {
                    userName = arguments[0];
                    userExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(userName, args.ServiceName);
                }

                User user = DatabaseMngr.GetUserByExternalID(userExternalID);

                if (user == null)
                {
                    QueueMessage(args.ServiceName, $"User \"{userName}\" cannot be found! - {UsageMessage}");
                    return;
                }

                QueueMessage(args.ServiceName, $"{userName} is opted into the following data: {user.ConsentOptions} - {UsageMessage}");
                return;
            }

            string optArg = arguments[0].ToLowerInvariant();

            if (optArg != OPTIN_ARG && optArg != OPTOUT_ARG)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            bool optIn = optArg == OPTIN_ARG;

            //Ensure each option is listed only once by counting
            Dictionary<UserDataConsentOptions, int> consentOptionDict = new Dictionary<UserDataConsentOptions, int>();

            Span<UserDataConsentOptions> consentOptions = stackalloc UserDataConsentOptions[arguments.Count - 1];

            //Parse all further arguments as options
            for (int i = 1; i < arguments.Count; i++)
            {
                if (EnumUtility.TryParseEnumValue<UserDataConsentOptions>(arguments[i],
                    out consentOptions[i - 1]) == false)
                {
                    QueueMessage(args.ServiceName, $"\"{arguments[i]}\" isn't recognized as a data option - {UsageMessage}");
                    return;
                }

                if (consentOptionDict.ContainsKey(consentOptions[i - 1]) == true)
                {
                    QueueMessage(args.ServiceName,
                        $"\"{consentOptions[i - 1]}\" was specified more than once. Please specify only one of each data option to opt in or out of!");
                    return;
                }

                consentOptionDict.Add(consentOptions[i - 1], 1);
            }

            consentOptionDict = null;

            string optMessage = $"Opted {(optIn == true ? "into" : "out of")}: ";

            using (var context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(userExternalID);

                for (int i = 0; i < consentOptions.Length; i++)
                {
                    user.SetDataOptionConsent(consentOptions[i], optIn);

                    optMessage += $"{consentOptions[i]}, "; 
                }

                context.SaveChanges();
            }

            optMessage = optMessage.Remove(optMessage.Length - 2, 2);

            QueueMessage(args.ServiceName, optMessage);
        }
    }
}
