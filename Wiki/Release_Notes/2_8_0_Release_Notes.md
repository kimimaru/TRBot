TRBot v2.8.0, featuring Matrix protocol support!

## Matrix Protocol Support
TRBot can now read messages coming through the [Matrix protocol](https://matrix.org/)! It currently works only for unencrypted rooms and for text messages. See more about [setting up TRBot with Matrix](https://codeberg.org/kimimaru/TRBot/src/tag/2.8.0/Wiki/Setup-Matrix.md)!

Matrix further brings many exciting new possibilities to TRBot, including reading inputs from other platforms like [Discord, Telegram, and even email through bridges](https://matrix.org/ecosystem/bridges/). We hope you enjoy exploring the vast Matrix network!

## Inputs
- For those running TRBot on GNU/Linux, the uinput virtual controller now acts like a standard game controller by expanding the axis range to negative values. The full axis range is now -32767 to 32767. This fixes some applications like RPCS3 not picking up the proper axis.
- `MaxAxisPercent` has been removed from inputs due to its niche uses and complications with maintenance going forwards. It can be replicated by having separate button and axis inputs to emulate a trigger.
- The `ValidFor` field for InvalidInputCombos has been replaced with `ValidLevelThreshold`.

## Command Changes
- The unwieldy `AddInputCommand` has been removed in favor of the specialized `AddButtonInputCommand`, `AddAxisInputCommand`, and `AddBlankInputCommand`.
- Added a new `ListInputsCommand` showing detailed information about specific input categories. Two new defaults, "!listbuttons" and "!listaxes", show button and axis information respectively for a given console.
- Users who enter a command as their first message will now properly be added to the database.

## Events
- A new `outgoing_bot_message` event is available for external applications to view messages that TRBot sends itself.
- New `init_default_data` and `reload_data` events are sent when TRBot initializes or reloads data, respectively.
- All TRBot event fields now use camelcased JSON names when reading them from external applications.
- All TRBot events now have a unique `eventId` to make it easier to differentiate them. Notably, input events now send over the `eventId` of their associated message events, making it easier to write a chat display for instance.

## Routines
- Fixed a critical bug that removed reconnection routines when reloading data, causing a service to never reconnect.

## Miscellaneous
- Logs now mention which platform each service an outgoing bot message comes from.
- CI images have been updated to more recent operating systems.
- `TRBotDataMigrationTool` has been removed. If you need to upgrade from a 1.x release, please use a build from a previous release.

## Internal Changes
TRBot v2.8.0 now uses .NET 7 for new features and improved performance.

Additionally, many parts of the codebase have been greatly cleaned up and consolidated to reduce the number of TRBot projects required. Dependencies have been completely reassessed, names should be more clear, and more code should be testable across the project.

- There's a new `TRBot.Input` project which contains everything related to parsing and processing inputs. Related projects, such as `TRBot.Parsing`, have been moved into here.
- The vaguely-named `TRBot.Misc` has been removed, with parts moved into `TRBot.Utilities`, `TRBot.Input`, and other projects.
- `TRBot.Logging` has been moved into `TRBot.Utilities.Logging`.
- `DataHelper` no longer exists. Instead, there are `IDatabaseManager` and `BotDBContext` extension methods for all the processing before.
- Similarly `MessageHelpers` has been removed and `IEventDispatcher` now contains `BroadcastMessage` as an extension method.
- All pre-defined game consoles are now in the `TRBot.Main` project.
- `DataReloader` has been replaced with the aforementioned`reload_data` event.
- Many classes that were previously static, such as `ParserPostProcess`, are now interfaces with concrete implementations, increasing the testability of code. This includes the newly-introduced `StandardPostParser`, `DataInitializer` and `DefaultDataProvider`.
- `IParserFactory` and `StandardParserFactory` replace the static `StandardParser.CreateStandard` method. This makes it easier to create many parsers with the same configuration as well as test code.
- Custom code been consolidated into a new `TRBot.CustomCode` project, reducing code duplication.

**IMPORTANT for those upgrading from previous 2.0+ releases:**
- There are breaking changes for custom commands and routines in TRBot 2.8.0 regarding accessing data, broadcasting messages, and more. Please [read here](https://codeberg.org/kimimaru/TRBot/src/tag/2.8.0/Wiki/Custom-Cmd-Routine-Changes-2_8_0.md) for how to update your own commands and routines.

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.8.0/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
