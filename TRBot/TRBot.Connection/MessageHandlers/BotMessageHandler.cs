﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TRBot.Utilities.Logging;
using TRBot.Utilities;

namespace TRBot.Connection
{
    /// <summary>
    /// Handles messages with rate limiting.
    /// </summary>
    public class BotMessageHandler : IBotMessageHandler
    {
        private static BotMessageNoThrottle NoThrottleInstance = new BotMessageNoThrottle();

        public IClientService ClientService { get; private set; } = null;

        /// <summary>
        /// Whether messages should be queued for sending or not.
        /// If disabled, the message handler won't send new messages.
        /// </summary>
        public bool MessageQueuingEnabled { get; private set; } = true;

        /// <summary>
        /// How many messages are in the queue.
        /// </summary>
        public int ClientMessageCount => ClientMessages.Count;

        /// <summary>
        /// The message throttler that restricts how often messages are sent.
        /// </summary>
        public IBotMessageThrottler MessageThrottler { get; private set; } = NoThrottleInstance;

        /// <summary>
        /// The current message throttling option.
        /// </summary>
        public MessageThrottlingOptions CurThrottleOption { get; private set; } = MessageThrottlingOptions.None;

        /// <summary>
        /// The character limit for bot messages. Bot messages longer than this value will be split into separate messages.
        /// <summary>
        public int BotMessageCharLimit { get; set; }

        /// <summary>
        /// Queued messages.
        /// </summary>
        private readonly Queue<IServiceMessage> ClientMessages = new Queue<IServiceMessage>(16);

        private ITRBotLogger Logger = null;
        
        private CancellationTokenSource CTokenSource = null;

        public BotMessageHandler(ITRBotLogger logger, in MessageThrottlingOptions msgThrottleOption,
            in MessageThrottleData messageThrottleData, in int botMessageCharLimit, in bool messageQueuingEnabled)
        {
            Logger = logger;
            SetMessageThrottling(msgThrottleOption, messageThrottleData);
            BotMessageCharLimit = botMessageCharLimit;
            ToggleMessageQueueing(messageQueuingEnabled);
        }

        public void CleanUp()
        {
            ClientService = null;
            CTokenSource?.Cancel(false);
            CTokenSource?.Dispose();
            ClientMessages.Clear();
        }

        /// <summary>
        /// Toggles messages to be queued on the message handler. If turned off, the message handler should not queue up new messages.
        /// </summary>
        /// <param name="enabled">If true, enables message queuing, otherwise disables it.</param>
        public void ToggleMessageQueueing(bool enabled)
        {
            MessageQueuingEnabled = enabled;
        }

        public void SetClientService(IClientService clientService)
        {
            ClientService = clientService;
        }

        public void SetMessageThrottling(in MessageThrottlingOptions msgThrottleOption,
            in MessageThrottleData messageThrottleData)
        {
            InstantiateOrUpdateThrottler(msgThrottleOption, messageThrottleData);
            CurThrottleOption = msgThrottleOption;
        }

        private void InstantiateOrUpdateThrottler(in MessageThrottlingOptions msgThrottleOption,
            in MessageThrottleData messageThrottleData)
        {
            //Simply set data if the throttle option hasn't changed
            if (CurThrottleOption == msgThrottleOption && MessageThrottler != null)
            {
                MessageThrottler.SetData(messageThrottleData);
            }
            else
            {
                //Instantiate based on the throttle option we have
                switch (msgThrottleOption)
                {
                    case MessageThrottlingOptions.MsgCountPerInterval:
                        MessageThrottler = new BotMessagePerIntervalThrottler(messageThrottleData);
                        break;
                    case MessageThrottlingOptions.TimeThrottled:
                        MessageThrottler = new BotMessageTimeThrottler(messageThrottleData);
                        break;
                    case MessageThrottlingOptions.None:
                    default: 
                        MessageThrottler = NoThrottleInstance;
                        break;
                }

                CTokenSource?.Cancel(false);
                CTokenSource?.Dispose();
                CTokenSource = new CancellationTokenSource();
                
                //Run the throttler on another task
                Task.Run(() => UpdateThrottler(CTokenSource.Token));
            }
        }

        private async Task UpdateThrottler(CancellationToken cancellationToken)
        {
            while (true)
            {
                if (cancellationToken.IsCancellationRequested == true)
                {
                    break;
                }

                MessageThrottler.Update(DateTime.UtcNow, this);

                await Task.Delay(1);
            }
        }

        /// <summary>
        /// Sends the next queued message through the client service. This returns false if this fails.
        /// </summary>
        /// <returns>true if the message was successfully sent. false if the client service is disconnected or the message fails to send.</returns>
        public bool SendNextQueuedMessage()
        {
            //Ensure the client service is connected and can send messages 
            if (ClientMessages.Count == 0 || ClientService.CanSendMessages == false)
            {
                return false;
            }
            
            //See the message
            IServiceMessage queuedMsg = ClientMessages.Peek();

            //There's a chance networked services could be disconnected between now and when the message is actually sent
            try
            {
                //Notify that a message is ready
                ClientService.ProcessAndSendMessage(queuedMsg);

                //Remove from queue
                ClientMessages.Dequeue();
            }
            catch (Exception e)
            {
                Logger.Error($"Could not send message: {e.Message}");
                return false;
            }

            return true;
        }

        public void QueueMessage(IServiceMessage serviceMessage, string msgSplitSeparator = "")
        {
            //Don't send messages if they're disabled
            if (MessageQueuingEnabled == false)
            {
                return;
            }

            if (serviceMessage == null)
            {
                throw new ArgumentNullException(nameof(serviceMessage), $"Null message passed to {nameof(QueueMessage)}.");
            }

            string sentMessage = Helpers.SplitStringWithinCharCount(serviceMessage.Message, BotMessageCharLimit, msgSplitSeparator,
                out List<string> textList);

            //If the text fits within the character limit, print it all out at once
            if (textList == null)
            {
                ClientMessages.Enqueue(serviceMessage);
            }
            else
            {
                //Otherwise, queue up the text in pieces, cloning the message with different content
                for (int i = 0; i < textList.Count; i++)
                {
                    ClientMessages.Enqueue(serviceMessage.CloneWithDiffMsg(textList[i]));
                }
            }
        }

        public void QueueMessage(string message, string msgSplitSeparator = "", in Serilog.Events.LogEventLevel logLevel = Serilog.Events.LogEventLevel.Information)
        {
            //Don't send messages if they're disabled
            if (MessageQueuingEnabled == false)
            {
                return;
            }

            string sentMessage = Helpers.SplitStringWithinCharCount(message, BotMessageCharLimit, msgSplitSeparator,
                out List<string> textList);

            //If the text fits within the character limit, print it all out at once
            if (textList == null)
            {
                QueueMessage(new BasicMessage(sentMessage, false, string.Empty, logLevel));
            }
            else
            {
                //Otherwise, queue up the text in pieces
                for (int i = 0; i < textList.Count; i++)
                {
                    QueueMessage(new BasicMessage(textList[i], false, string.Empty, logLevel));
                }
            }
        }
    }
}
