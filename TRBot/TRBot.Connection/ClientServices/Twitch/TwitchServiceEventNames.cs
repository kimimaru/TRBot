﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection.Twitch
{
    /// <summary>
    /// Specific events for Twitch.
    /// </summary>
    public static class TwitchServiceEventNames
    {
        public const string TWITCH_JOINED_CHANNEL = "twitch_joined_channel";
        public const string TWITCH_LEFT_CHANNEL = "twitch_left_channel";
        public const string TWITCH_WHISPER_RECEIVED = "twitch_whisper_received";
        public const string TWITCH_WHISPER_COMMAND_RECEIVED = "twitch_whisper_command_received";

        public const string TWITCH_NEWSUBSCRIBER = "twitch_new_subscriber";
        public const string TWITCH_RESUBSCRIBER = "twitch_resubscriber";

        public const string TWITCH_PUB_SUB_CONNECTED = "twitch_pubsub_connected";
        public const string TWITCH_PUB_SUB_DISCONNECTED = "twitch_pubsub_disconnected";
        public const string TWITCH_PUB_SUB_ERROR = "twitch_pubsub_error";

        public const string TWITCH_FOLLOW = "twitch_follow";
        public const string TWITCH_EMOTE_ONLY_ON = "twitch_emote_only_on";
        public const string TWITCH_EMOTE_ONLY_OFF = "twitch_emote_only_off";
    }
}
