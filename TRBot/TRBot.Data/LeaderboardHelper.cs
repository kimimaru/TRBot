/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Data
{
    /// <summary>
    /// Helps retrieve leaderboard data.
    /// </summary>
    public static class LeaderboardHelper
    {
        /// <summary>
        /// Gets a user's position in relation to all others based on a numeric stat.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The external ID of the user.</param>
        /// <param name="numericStatMemberName">The name of the member corresponding to the stat. The name must be exact and correspond to a numeric field.</param>
        /// <returns>An tuple of integers; the first represents the user's position on the leaderboard
        /// corresponding to the stat, and the second represents the number of unique counts on the leaderboard.
        /// If the user is not found, the returned user position is -1.</returns>
        public static (int, int) GetPositionOnNumericStatLeaderboard(IDatabaseManager<BotDBContext> databaseMngr,
            string userExternalID, string numericStatMemberName)
        {
            using (BotDBContext context = databaseMngr.OpenContext())
            {
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;

                User user = context.Users
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);
                
                //Get the number of scores on the leaderboard
                int leaderBoardCount = context.Users
                    .Include(u => u.Stats)
                    .Where(u => (u.ConsentOptions & UserDataConsentOptions.Stats) != 0)
                    .Count();

                if (user == null)
                {
                    return (-1, leaderBoardCount);
                }

                PropertyInfo statProp = typeof(UserStats).GetProperty(numericStatMemberName,
                    BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance);

                IComparable userStat = statProp.GetValue(user.Stats) as IComparable;

                //Get the position - take all of the values of that stat greater than or equal to the user's
                //Order them in descending order, and get the count of users greater than the current count
                //Don't include opted out users
                int position = context.Users
                    .Include(u => u.Stats)
                    .AsEnumerable()
                    .Where(u => u.HasConsentToDataOption(UserDataConsentOptions.Stats) == true)
                    .Select(u => (IComparable)statProp.GetValue(u.Stats))
                    .Where(sp => sp.CompareTo(userStat) >= 0)
                    .OrderByDescending(sp => sp, Comparer<IComparable>.Default)
                    .Count(sp => sp.CompareTo(userStat) > 0);

                //We have how many users have a greater value in this stat than this one,
                //so add 1 to get the user's actual position
                position += 1;

                return (position, leaderBoardCount);
            }
        }

        /// <summary>
        /// Gets a subset of a given stat on a leaderboard around a given user external ID.
        /// </summary>
        /// <param name="databaseMngr">The database manager.</param>
        /// <param name="userExternalID">The external ID of the user.</param>
        /// <param name="surroundingCount">The total number of entries to obtain around the given user external ID.
        /// This will be spread out as evenly as possible.
        /// <para>For example, if this is 10, it will attempt to obtain 5 above and 5 below the user's position;
        /// however, if the user is at the top of the leaderboard, it will obtain 10 below the user's position.</para>
        /// </param>
        /// <param name="numericStatMemberName">The name of the member corresponding to the stat. The name must be exact and correspond to a numeric field.</param>
        /// <returns>A SortedDictionary with integer keys and tuple values.
        /// The key is the zero-based position in the leaderboard.
        /// The tuple's first value is a numeric type representing the value of the stat the user has.
        /// The second value is a string containing the user's name.
        /// If the user is not found, the returned SortedDictionary is null.
        /// </returns>
        public static SortedDictionary<int, (T, string)> GetNumericStatLeaderboardSubset<T>(
            IDatabaseManager<BotDBContext> databaseMngr, string userExternalID,
            in int surroundingCount, string numericStatMemberName) where T : IComparable
        {
            using (BotDBContext context = databaseMngr.OpenContext())
            {
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;

                User namedUser = context.Users.Include(u => u.Stats).FirstOrDefault(u => u.ExternalID == userExternalID);

                if (namedUser == null)
                {
                    return null;
                }

                PropertyInfo statProp = typeof(UserStats).GetProperty(numericStatMemberName, BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance);
                
                T userStat = (T)statProp.GetValue(namedUser.Stats);

                //Get the position - take all values greater than or equal to the user's stat
                //Order them in descending order, and get the count of users greater than current count
                //Don't include opted out users, but always include the user we're looking for the subsets around
                var allUsers = context.Users
                    .Include(u => u.Stats)
                    .AsEnumerable()
                    .Where(u => u.HasConsentToDataOption(UserDataConsentOptions.Stats) == true)
                    .Select(u => (u, (T)statProp.GetValue(u.Stats)))
                    .OrderByDescending(sp => sp, new UserStatComparer<T>())
                    .Select(ut => ut.u)
                    .ToList();

                int userIndex = allUsers.FindIndex(0, allUsers.Count, u => u.ID == namedUser.ID);

                //Somehow, the user isn't in this list
                if (userIndex < 0)
                {
                    return null;
                }

                SortedDictionary<int, (T, string)> leaderboardDict = new SortedDictionary<int, (T, string)>();

                //Add users with credit values greater
                //Keep going until either the top of the list or we found half of the surrounding value
                for (int i = userIndex - 1, foundCount = 0; i >= 0 && foundCount < (surroundingCount / 2); i--, foundCount++)
                {
                    User user = allUsers[i];
                    leaderboardDict[i] = ((T)statProp.GetValue(user.Stats), user.Name);
                }
                
                //Add the user
                leaderboardDict[userIndex] = (userStat, namedUser.Name);

                //Add users with credit values lower
                //Keep going until either the end of the list or we found the rest of the surrounding value
                for (int i = userIndex + 1, foundCount = leaderboardDict.Count - 1; i < allUsers.Count && foundCount < surroundingCount; i++, foundCount++)
                {
                    User user = allUsers[i];
                    leaderboardDict[i] = ((T)statProp.GetValue(user.Stats), user.Name);
                }

                return leaderboardDict;
            }
        }

        /// <summary>
        /// A <see cref="Comparer"/> for a comparable user stat type.
        /// </summary>
        public class UserStatComparer<T> : Comparer<(User, T)> where T : IComparable
        {
            public UserStatComparer()
            {

            }

            public override int Compare((User, T) x, (User, T) y)
            {
                return x.Item2.CompareTo(y.Item2);
            }
        }
    }
}