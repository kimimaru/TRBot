﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that gets or sets the max number of recent inputs stored per user.
    /// </summary>
    public sealed class GetSetMaxUserRecentInputsCommand : BaseCommand
    {
        private const int MIN_VALUE = 0;

        private string UsageMessage = $"Usage - no arguments (get value) or \"max # of inputs to store (int)\"";

        public GetSetMaxUserRecentInputsCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //Ignore with too few arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (arguments.Count == 0)
            {
                long maxRecentInputCount = DatabaseMngr.GetSettingInt(SettingsConstants.MAX_USER_RECENT_INPUTS, 0L);

                QueueMessage(args.ServiceName, $"The max number of recent inputs stored per user is {maxRecentInputCount}. To change this number, pass an integer as an argument."); 
                return;
            }

            //Check for sufficient permissions
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
                if (user == null || user.HasEnabledAbility(PermissionConstants.SET_MAX_USER_RECENT_INPUTS_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the number of recent inputs stored per user!");
                    return;
                }
            }

            string recentInputCountStr = arguments[0].ToLowerInvariant();

            if (StringNumParser.TryParseInt(recentInputCountStr, out int newRecentInputCount) == false)
            {
                QueueMessage(args.ServiceName, "That's not a valid number!");
                return;
            }

            if (newRecentInputCount < MIN_VALUE)
            {
                QueueMessage(args.ServiceName, $"Value cannot be lower than {MIN_VALUE}!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings userMaxRecentInputs = context.GetSetting(SettingsConstants.MAX_USER_RECENT_INPUTS);
                if (userMaxRecentInputs == null)
                {
                    userMaxRecentInputs = new Settings(SettingsConstants.MAX_USER_RECENT_INPUTS, string.Empty, 0L);
                    context.SettingCollection.Add(userMaxRecentInputs);
                }

                userMaxRecentInputs.ValueInt = newRecentInputCount;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Set the max number of recent inputs stored per user to {newRecentInputCount}!"); 
        }
    }
}
