TRBot v1.8.

* Fixed `InputHandler` releasing already released inputs, causing problems with specific axis inputs.
* Fixed memes breaking valid inputs.
* Dynamic macros can now take in hold ("_"), release ("-"), simultaneous ("+"), and port-specific ("&") inputs as arguments.
* Fixed the macro parser list not being updated when reloading bot data.
* Made input exercises give greater credit rewards and prevented inputs the user doesn't have permission to perform from showing up in them.
* `ValidInputsCommand` no longer displays inputs a user doesn't have access to.
* Fixed being able to set a user's level to your own with `SetLevelCommand`.
* Macros are now sorted alphabetically.
* `MacrosCommand` and `MemesCommand` have been renamed to `ListMacrosCommand` and `ListMemesCommand`, respectively.
* Refactored console button and axes definitions to use `InputButton` and `InputAxes` structs, and simplified the axes code.
* No longer check for the presence of savestate buttons when attempting to view savestate information.
* The chatbot has been rewritten to use sockets and pipes. It is now much more responsive.
* A new **VIP** access level has been added between **Whitelisted** and **Moderator**.
* More commands:
  * `ReverseInputCommand` allows you to easily view inputs in a natural sentence.
  * `NumRunningInputsCommand` lets you view how many concurrent input sequences are running.
  * `CmdAccessLevelCommand` lets you view the access level required to perform another command.
  * `UptimeCommand` gets the bot's uptime.
