﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis.CSharp.Scripting;

namespace TRBot.CustomCode
{
    /// <summary>
    /// Handles compiling and executing custom code.
    /// </summary>
    public class CustomCodeHandler : ICustomCodeHandler
    {
        private readonly Assembly[] AssemblyReferences = Array.Empty<Assembly>();
        private string[] Imports = Array.Empty<string>();
        private ScriptOptions ScriptCompileOptions;

        public CustomCodeHandler(Assembly[] assemblyReferences, string[] imports)
        {
            AssemblyReferences = assemblyReferences;
            Imports = imports;

            ScriptCompileOptions = ScriptOptions.Default.WithReferences(AssemblyReferences).WithImports(Imports);
        }

        /// <summary>
        /// Compiles and executes the given C# code.
        /// </summary>
        /// <param name="code">A string representing the C# code to execute.</param>
        /// <param name="globals">An optional object instance whose members can be accessed by the code as global variables.</param>
        /// <param name="globalsType">The type of the optional global object.</param>
        /// <returns>A Task regarding the code execution.</returns>
        /// <exception cref="CompilationErrorException"></exception>
        public async Task ExecuteCode(string code, object globals, Type globalsType)
        {
            await CSharpScript.RunAsync(code, ScriptCompileOptions, globals, globalsType);
        }

        /// <summary>
        /// Compiles C# code and returns a <see cref="CustomCodeCreationData{T}"/> with the given object of type T if successful.
        /// </summary>
        /// <param name="code">The C# code to run for the script.
        /// The code must return an instance of an object of type T.</param>
        /// <returns>A Task of CustomCommandCreationData, containing the new command and an error message.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="CompilationErrorException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public async Task<CustomCodeCreationData<T>> CompileCodeAndReturnObject<T>(string code)
        {
            //Compile the code
            Script<T> command = CSharpScript.Create<T>(code, ScriptCompileOptions, null, null);
            command.Compile();

            //Run it and return the object
            ScriptState<T> newObject = await command.RunAsync();

            //No return value
            if (newObject.ReturnValue == null)
            {
                return new CustomCodeCreationData<T>(default, false, "Custom code does not return a valid object.");
            }

            return new CustomCodeCreationData<T>(newObject.ReturnValue, true, string.Empty);
        }
    }
}