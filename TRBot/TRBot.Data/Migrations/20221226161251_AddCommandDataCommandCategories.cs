﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    public partial class AddCommandDataCommandCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Category",
                schema: "commanddata",
                table: "CommandData",
                type: "INTEGER",
                nullable: false,
                defaultValue: 2L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Category",
                schema: "commanddata",
                table: "CommandData");
        }
    }
}
