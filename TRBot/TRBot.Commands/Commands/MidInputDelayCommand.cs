﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Gets or sets the global mid input delay's enabled state and duration.
    /// </summary>
    public sealed class MidInputDelayCommand : BaseCommand
    {
        private const long MIN_MID_INPUT_DELAY_DURATION = 1L;

        private string UsageMessage = "Usage: no arguments (get values), (\"enabled state (bool)\" and/or \"duration (int)\") (set values)";

        public MidInputDelayCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            long midDelayEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.GLOBAL_MID_INPUT_DELAY_ENABLED, 0L);
            long midDelayTime = DatabaseMngr.GetSettingInt(SettingsConstants.GLOBAL_MID_INPUT_DELAY_TIME, 34L);

            if (arguments.Count == 0)
            {
                string enabledStateStr = (midDelayEnabled > 0L) ? "enabled" : "disabled";

                QueueMessage(args.ServiceName, $"The global mid input delay is {enabledStateStr} with a duration of {midDelayTime} milliseconds!");
                return;
            }

            if (arguments.Count > 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has this ability
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user == null)
                {
                    QueueMessage(args.ServiceName, "Somehow, the user calling this is not in the database.");
                    return;
                }

                if (user.HasEnabledAbility(PermissionConstants.SET_MID_INPUT_DELAY_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the global mid input delay!");
                    return;
                }
            }

            string enabledStr = arguments[0];
            string durationStr = arguments[0];

            bool parsedEnabled = false;
            bool parsedDuration = false;

            //Set the duration string to the second argument if we have more arguments
            if (arguments.Count == 2)
            {
                durationStr = arguments[1];
            }

            //Parse enabled state
            parsedEnabled = bool.TryParse(enabledStr, out bool enabledState);
            
            //Parse duration
            parsedDuration = long.TryParse(durationStr, out long newMidInputDelay);

            //Failed to parse enabled state
            if (parsedEnabled == false)
            {
                //Return if we're expecting two valid arguments or one valid argument and the duration parse failed 
                if (arguments.Count == 2 || (arguments.Count == 1 && parsedDuration == false))
                {
                    QueueMessage(args.ServiceName, "Please enter \"true\" or \"false\" for the enabled state!");
                    return;
                }
            }

            if (parsedDuration == false)
            {
                //Return if we're expecting two valid arguments or one valid argument and the enabled parse failed 
                if (arguments.Count == 2 || (arguments.Count == 1 && parsedEnabled == false))
                {
                    QueueMessage(args.ServiceName, "Please enter a valid number greater than 0!");
                    return;
                }
            }

            if (parsedDuration == true && newMidInputDelay < MIN_MID_INPUT_DELAY_DURATION)
            {
                QueueMessage(args.ServiceName, $"The global mid input delay cannot be less than {MIN_MID_INPUT_DELAY_DURATION}!");
                return;
            }

            string message = "Set the global mid input delay";

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings midDelayEnabledSetting = context.GetSetting(SettingsConstants.GLOBAL_MID_INPUT_DELAY_ENABLED);
                Settings midDelayTimeSetting = context.GetSetting(SettingsConstants.GLOBAL_MID_INPUT_DELAY_TIME);

                //Modify the message based on what was successfully parsed
                if (parsedEnabled == true)
                {
                    midDelayEnabledSetting.ValueInt = (enabledState == true) ? 1L : 0L;

                    message += " enabled state to " + enabledState;
                }

                if (parsedDuration == true)
                {
                    midDelayTimeSetting.ValueInt = newMidInputDelay;

                    //The enabled state was also parsed
                    if (parsedEnabled == true)
                    {
                        message += " and duration to " + newMidInputDelay;
                    }
                    else
                    {
                        message += " duration to " + newMidInputDelay + " milliseconds";
                    }
                }

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"{message}!");
        }
    }
}
