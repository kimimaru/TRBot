/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NSubstitute;
using NUnit.Framework;
using TRBot.Connection.Matrix;

namespace TRBot.Tests.Connection
{
    [TestFixture]
    public class MatrixHelpersUnitTests
    {
        private static TestCaseData[] IsMatrixIDLocalPartValid_ValidateOutput_TestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData("example", true),
                new TestCaseData("@example:domain.org", false),
                new TestCaseData("example:", false),
                new TestCaseData("example;", false),
                new TestCaseData("capital", true),
                new TestCaseData("CAPITAL", false),
                new TestCaseData("_test_", true),
                new TestCaseData(".test.", true),
                new TestCaseData("/test/", true),
                new TestCaseData("=test=", true),
                new TestCaseData("1234567890abcdefghijklmnopqrstuvwxyz._=/", true),
                new TestCaseData("ab#rtu/", false),
                new TestCaseData("00000", true),
                //255 character localpart
                new TestCaseData(
                    "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                    true),
                //256 character localpart
                new TestCaseData(
                    "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                    false),
            };
        }

        [TestCaseSource(nameof(IsMatrixIDLocalPartValid_ValidateOutput_TestCases))]
        public void IsMatrixIDLocalPartValid_ValidateOutput(string matrixIDLocalPart, bool expectedValue)
        {
            bool value = MatrixHelpers.IsMatrixIDLocalPartValid(matrixIDLocalPart);

            Assert.That(value, Is.EqualTo(expectedValue));
        }

        private static TestCaseData[] GetLocalPartFromMXID_ValidateOutput_TestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData("@example:domain.org", "example"),
                new TestCaseData("@/testFriend_.=09234823:domain.org", "/testFriend_.=09234823"),
                new TestCaseData("@0123456789abcdefghijklmnopqrstuvwxyz_.=/:domain.org", "0123456789abcdefghijklmnopqrstuvwxyz_.=/"),
            };
        }

        [TestCaseSource(nameof(GetLocalPartFromMXID_ValidateOutput_TestCases))]
        public void GetLocalPartFromMXID_ValidateOutput(string matrixID, string expectedLocalPart)
        {
            string localPart = MatrixHelpers.GetLocalPartFromMXID(matrixID);

            Assert.That(localPart, Is.EqualTo(expectedLocalPart));
        }
    }
}
