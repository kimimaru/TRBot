﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using WebSocketSharp;
using WebSocketSharp.Server;

namespace TRBot.WebSocket
{
    /// <summary>
    /// Manages WebSocket client and server connections.
    /// </summary>
    public abstract class WebSocketService : WebSocketBehavior
    {
        protected override sealed void OnMessage(MessageEventArgs e)
        {
            OnServiceMessage(e);
        }

        protected override sealed void OnOpen()
        {
            OnServiceOpen();
        }
        
        protected override sealed void OnError(ErrorEventArgs e)
        {
            OnServiceError(e);
        }

        protected override sealed void OnClose(CloseEventArgs e)
        {
            OnServiceClose(e);
        }

        protected virtual void OnServiceOpen()
        {

        }
        
        protected virtual void OnServiceMessage(MessageEventArgs e)
        {

        }

        protected virtual void OnServiceError(ErrorEventArgs e)
        {

        }

        protected virtual void OnServiceClose(CloseEventArgs e)
        {

        }
    }
}
