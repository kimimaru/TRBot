﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Net;
using WebSocketSharp.Server;
using TRBot.Utilities.Logging;

namespace TRBot.WebSocket
{
    /// <summary>
    /// Manages WebSocket client and server connections.
    /// </summary>
    public class WebSocketManager : IWebSocketManager
    {
        private WebSocketServer SocketServer = null;

        public event WebSocketServerStarted ServerStarted = null;
        public event WebSocketServerStopped ServerStopped = null;

        public bool IsServerListening => SocketServer?.IsListening == true;
        public bool IsServerSecure => SocketServer?.IsSecure == true; 

        public IPAddress ServerIPAddress => SocketServer?.Address ?? IPAddress.None;
        public string ServerUrl { get; private set; } = string.Empty;
        public int ServerPort => SocketServer?.Port ?? 0;

        private ITRBotLogger Logger = null;

        public WebSocketManager(ITRBotLogger logger)
        {
            Logger = logger;
        }

        public void Dispose()
        {
            try
            {
                SocketServer.Stop();
            }
            catch (InvalidOperationException e)
            {
                string message = $"Issue stopping WebSocket server for {nameof(WebSocketManager)}. {e.Message}";
                if (e.InnerException != null)
                {
                    message += $" - {e.InnerException.Message}";
                }

                Logger.Error(message);
            }

            SocketServer = null;

            ServerStarted = null;
            ServerStopped = null;
        }

        public bool StartServer(string addressURL)
        {
            if (SocketServer != null)
            {
                return false;
            }

            try
            {
                SocketServer = new WebSocketServer(addressURL);

                ServerUrl = addressURL;

                //Increase wait time to give some more leniency
                SocketServer.WaitTime = TimeSpan.FromSeconds(60);

                SocketServer.Start();
            }
            catch (InvalidOperationException e)
            {
                Logger.Error($"Issue starting {(SocketServer.IsSecure == true ? "secure" : "insecure")} WebSocket server on port {SocketServer.Port}. {e.Message}");
                SocketServer = null;
                
                return false;
            }

            ServerStarted?.Invoke();

            return true;
        }

        public void StopServer()
        {
            if (SocketServer == null)
            {
                return;
            }

            try
            {
                SocketServer.Stop();
            }
            catch (InvalidOperationException e)
            {
                string message = $"Issue stopping WebSocket server for {nameof(WebSocketManager)} when closing. {e.Message}";
                if (e.InnerException != null)
                {
                    message += $" - {e.InnerException.Message}";
                }

                Logger.Error(message);

                return;
            }

            ServerStopped?.Invoke();

            ServerUrl = string.Empty;
            SocketServer = null;
        }

        public bool AddServerService<T>(string path, Action<T> serviceInitializer) where T : WebSocketService, new()
        {
            if (SocketServer == null)
            {
                return false;
            }

            try
            {
                SocketServer.AddWebSocketService<T>(path, serviceInitializer);
            }
            catch (Exception e)
            {
                Logger.Error($"Failed to add WebSocket service at \"{path}\". {e.Message}");
                return false;
            }

            return true;
        }

        public bool RemoveServerService(string path)
        {
            if (SocketServer == null)
            {
                return false;
            }

            try
            {
                SocketServer.RemoveWebSocketService(path);
            }
            catch (Exception e)
            {
                Logger.Error($"Failed to remove WebSocket service at \"{path}\". {e.Message}");
                return false;
            }

            return true;
        }

        public void ServerBroadcastAsync(string path, string data, Action onCompleted, Action<Exception> onError)
        {
            try
            {
                if (SocketServer.WebSocketServices.TryGetServiceHost(path, out WebSocketServiceHost host) == false)
                {
                    onError?.Invoke(new Exception($"WebSocket service at \"{path}\" cannot be found."));
                    return;
                }

                host.Sessions.BroadcastAsync(data, onCompleted);
            }
            catch (Exception e)
            {
                Logger.Error($"Error broadcasting data with WebSocket server at \"{path}\". {e.Message}");
                onError?.Invoke(e);
            }
        }
    }
}
