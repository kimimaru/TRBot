﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Adds a user ability.
    /// </summary>
    public sealed class UpdateUserAbilityCommand : BaseCommand
    {
        private const string NULL_EXPIRATION_ARG = "null";
        private string UsageMessage = "Usage: \"username\", \"ability name\", \"enabled state (bool)\" (\"ability value_str (string)\", \"ability value_int (int)\", \"expiration from now (Ex. 30 ms/s/m/h/d) - \"null\" for no expiration\") (optional group)";

        public UpdateUserAbilityCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 3 && arguments.Count != 6)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string abilityUserName = arguments[0];
            string abilityName = arguments[1].ToLowerInvariant();
            string abilityEnabledStr = arguments[2].ToLowerInvariant();

            string abilityUserExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(abilityUserName, args.ServiceName);

            User thisUser = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
            User abilityUser = DatabaseMngr.GetUserByExternalID(abilityUserExternalID);

            if (abilityUser == null)
            {
                QueueMessage(args.ServiceName, "This user doesn't exist in the database!");
                return;
            }

            PermissionAbility permAbility = DatabaseMngr.GetPermissionAbility(abilityName);

            if (permAbility == null)
            {
                QueueMessage(args.ServiceName, $"There is no permission ability named \"{abilityName}\".");
                return;
            }

            if (thisUser.ID != abilityUser.ID && thisUser.Level <= abilityUser.Level)
            {
                QueueMessage(args.ServiceName, "You cannot modify abilities for other users with levels greater than or equal to yours!");
                return;
            }

            if ((long)permAbility.AutoGrantOnLevel > thisUser.Level)
            {
                QueueMessage(args.ServiceName, "This ability isn't available to your level, so you cannot grant it!");
                return;
            }

            if ((long)permAbility.MinLevelToGrant > thisUser.Level)
            {
                QueueMessage(args.ServiceName, $"You need to be at least level {(long)permAbility.MinLevelToGrant}, {permAbility.MinLevelToGrant}, to grant this ability.");
                return;
            }

            if (bool.TryParse(abilityEnabledStr, out bool enabledState) == false)
            {
                QueueMessage(args.ServiceName, "Invalid enabled state.");
                return;
            }

            string valueStrArg = string.Empty;
            string valueIntArg = string.Empty;
            string expirationArg = string.Empty;
            int valueInt = 0;

            DateTime? expiration = default;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                UserAbility newUserAbility = null;

                abilityUser = context.GetUserByExternalID(abilityUserExternalID);
                abilityUser.TryGetAbility(abilityName, out newUserAbility);

                //Prevent overriding a higher-leveled ability grant
                if (newUserAbility != null && newUserAbility.GrantedByLevel > thisUser.Level)
                {
                    QueueMessage(args.ServiceName, $"A user at level {newUserAbility.GrantedByLevel}, {(PermissionLevels)newUserAbility.GrantedByLevel}, granted this ability, so you need to be at a level equal or higher to modify it.");
                    return;
                }

                bool shouldAdd = false;

                if (newUserAbility == null)
                {
                    //Logger.Information($"New ability {abilityName}");

                    newUserAbility = new UserAbility();
                    shouldAdd = true;
                }

                newUserAbility.PermabilityID = permAbility.ID;
                newUserAbility.UserID = abilityUser.ID;
                newUserAbility.GrantedByLevel = thisUser.Level;
                newUserAbility.SetEnabledState(enabledState);

                //If there are only three arguments, enable/disable the ability
                if (arguments.Count == 3)
                {
                    if (shouldAdd == true)
                    {
                        //Logger.Information($"Adding ability {abilityName}");
                        abilityUser.UserAbilities.Add(newUserAbility);
                    }

                    if (enabledState == true)
                    {
                        QueueMessage(args.ServiceName, $"Enabled the \"{abilityName}\" ability for {abilityUser.Name}!");
                    }
                    else
                    {
                        QueueMessage(args.ServiceName, $"Disabled the \"{abilityName}\" ability for {abilityUser.Name}!");
                    }

                    //Save and exit here
                    context.SaveChanges();

                    return;
                }

                valueStrArg = arguments[3];
                valueIntArg = arguments[4];
                expirationArg = arguments[5].ToLowerInvariant();

                newUserAbility.ValueStr = valueStrArg;

                //Validate arguments
                if (StringNumParser.TryParseInt(valueIntArg, out valueInt) == false)
                {
                    QueueMessage(args.ServiceName, "Invalid value_int argument.");
                    return;
                }

                newUserAbility.ValueInt = valueInt;

                if (expirationArg == NULL_EXPIRATION_ARG)
                {
                    newUserAbility.Expiration = null;
                }
                else
                {
                    if (TimeSpanHelpers.TryParseTimeModifierFromStr(expirationArg, out TimeSpan timeFromNow) == false)
                    {
                        QueueMessage(args.ServiceName, "Unable to parse expiration time from now.");
                        return;
                    }

                    //Set the time to this amount from now
                    newUserAbility.Expiration = DateTime.UtcNow + timeFromNow;

                    expiration = newUserAbility.Expiration;
                }

                //Add if we should
                if (shouldAdd == true)
                {
                    abilityUser.UserAbilities.Add(newUserAbility);
                }

                //Save changes
                context.SaveChanges();
            }

            string ending = (expiration == null) ? "!" : $" and expires on {expiration} (UTC)!";

            if (enabledState == true)
            {
                QueueMessage(args.ServiceName, $"Enabled the \"{abilityName}\" ability to {abilityUser.Name} with values ({valueStrArg}, {valueInt}){ending}");
            }
            else
            {
                QueueMessage(args.ServiceName, $"Disabled the \"{abilityName}\" ability on {abilityUser.Name} with values ({valueStrArg}, {valueInt}){ending}");
            }
        }
    }
}
