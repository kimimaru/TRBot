﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Displays a user's most recent inputs.
    /// </summary>
    public sealed class ListUserRecentInputsCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"username (optional)\" \"recent input number (int)\"";

        public ListUserRecentInputsCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count < 1 || arguments.Count > 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string inputUserExternalID = args.Command.UserMessage.UserExternalID;
            string inputUsername = args.Command.UserMessage.Username;

            if (arguments.Count == 2)
            {
                inputUserExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(arguments[0], args.ServiceName);
            }


            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User inputUser = context.GetUserByExternalID(inputUserExternalID);

                if (inputUser == null)
                {
                    QueueMessage(args.ServiceName, $"User does not exist in database!");
                    return;
                }

                inputUsername = inputUser.Name;

                if (inputUser.HasConsentToDataOption(UserDataConsentOptions.RecentInputs) == false)
                {
                    QueueMessage(args.ServiceName, $"{inputUsername} is opted out of recent inputs, so you can't see their recent inputs.");
                    return;
                }

                if (inputUser.RecentInputs.Count == 0)
                {
                    QueueMessage(args.ServiceName, $"{inputUsername} doesn't have any recent inputs!");
                    return;
                }
            }

            //Get the recent input number
            string num = (arguments.Count == 1) ? arguments[0] : arguments[1];
            
            if (StringNumParser.TryParseInt(num, out int recentInputNum) == false)
            {
                QueueMessage(args.ServiceName, "Invalid recent input number!");
                return;
            }

            string recentInputSeq = string.Empty;
            int finalRecentInputNum = recentInputNum;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User inputUser = context.GetUserByExternalID(inputUserExternalID);

                //Go from back to front (Ex. 1 shows the most recent input)
                finalRecentInputNum = inputUser.RecentInputs.Count - recentInputNum;

                if (finalRecentInputNum < 0 || finalRecentInputNum >= inputUser.RecentInputs.Count)
                {
                    QueueMessage(args.ServiceName, $"No recent input sequence found!");
                    return;
                }

                recentInputSeq = inputUser.RecentInputs[finalRecentInputNum].InputSequence;
            }

            QueueMessage(args.ServiceName, $"{inputUsername}'s #{recentInputNum} most recent input: {recentInputSeq}", " ");
        }
    }
}
