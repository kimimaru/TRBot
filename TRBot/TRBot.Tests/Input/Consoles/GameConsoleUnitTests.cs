/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using NUnit.Framework;
using TRBot.Input.Consoles;

namespace TRBot.Tests.Input.Consoles
{
    [TestFixture]
    public class GameConsoleUnitTests
    {
        private const string TEST_CONSOLE_NAME = "TestConsole";

        private static TestCaseData[] ConstructsProperlyCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(
                    new List<InputData>()
                    {
                        new InputData("a", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("b", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("c", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 0 },
                    }
                )
            };
        }

        [TestCaseSource(nameof(ConstructsProperlyCases))]
        public void GameConsoleConstructsProperly(List<InputData> inputList)
        {
            IGameConsole console = new GameConsole(TEST_CONSOLE_NAME, inputList);

            Assert.That(console.ConsoleInputs.Count, Is.EqualTo(inputList.Count));

            Dictionary<string, InputData> inpDict = new Dictionary<string, InputData>();

            for (int i = 0; i < inputList.Count; i++)
            {
                InputData inputData = inputList[i];
                inpDict[inputData.Name] = inputData;
            }

            CollectionAssert.AreEquivalent(inpDict, console.ConsoleInputs);
        }

        private static TestCaseData[] InputNamesCorrectFilteringCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(new List<InputData>()
                    {
                        new InputData("a", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("b", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("c", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 0 },
                    },
                    InputNameFiltering.All,
                    new string[] { "a", "b", "c" }),
                new TestCaseData(new List<InputData>()
                    {
                        new InputData("a", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("b", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("c", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 0 },
                    },
                    InputNameFiltering.Enabled,
                    new string[] { "a", "b" }),
                new TestCaseData(new List<InputData>()
                    {
                        new InputData("a", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("b", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 1 },
                        new InputData("c", 0, 0, InputTypes.Blank, 0d, 0d, 0d, 0) { Enabled = 0 },
                    },
                    InputNameFiltering.Disabled,
                    new string[] { "c" }),
            };
        }

        [TestCaseSource(nameof(InputNamesCorrectFilteringCases))]
        public void GetInputNamesReturnsCorrectValuesForFiltering(List<InputData> inputList,
            InputNameFiltering filter, string[] expectedInputNames)
        {
            IGameConsole console = new GameConsole(TEST_CONSOLE_NAME, inputList);

            string[] inputNames = console.GetInputNames(filter);

            CollectionAssert.AreEquivalent(expectedInputNames, inputNames);
        }
    }
}