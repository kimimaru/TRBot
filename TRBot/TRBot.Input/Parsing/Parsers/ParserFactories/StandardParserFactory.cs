/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Linq;
using System.Collections.Generic;

namespace TRBot.Input.Parsing
{
    /// <summary>
    /// A factory for <see cref="StandardParser"/>.
    /// </summary>
    public class StandardParserFactory : IParserFactory
    {
        private IQueryable<InputMacro> Macros = null;
        private IEnumerable<InputSynonym> Synonyms = null;
        private IList<string> ValidInputs = null;
        private int DefaultPortNum = 0;
        private int MaxPortNum = 0;
        private int DefaultInputDur = 0;
        private int MaxInputDur = 0;
        private bool CheckMaxDur = false;
        private IParserValidator ParserValidator = null;

        /// <summary>
        /// Creates a <see cref="StandardParserFactory" />.
        /// </summary>
        /// <param name="macros">The InputMacro data to use.</param>
        /// <param name="synonyms">The InputSynonym data to use.</param>
        /// <param name="validInputs">The available valid inputs.</param>
        /// <param name="defaultPortNum">The default controller port number.</param>
        /// <param name="maxPortNum">The maximum controller port number.</param>
        /// <param name="defaultInputDur">The default input duration for inputs, in milliseconds.</param>
        /// <param name="maxInputDur">The maximum duration of an input sequence, in milliseconds.</param>
        /// <param name="checkMaxDur">Whether to mark the input sequence as invalid if it surpasses the maximum duration.</param>
        /// <param name="parserValidator">A parser validator to validate inputs as they're parsed.</param>
        /// <returns>A <see cref="StandardParserFactory" />.</returns>
        public StandardParserFactory(IQueryable<InputMacro> macros, IEnumerable<InputSynonym> synonyms,
            IList<string> validInputs, in int defaultPortNum, in int maxPortNum, in int defaultInputDur,
            in int maxInputDur, in bool checkMaxDur, IParserValidator parserValidator)
        {
            Macros = macros;
            Synonyms = synonyms;
            ValidInputs = validInputs;
            DefaultPortNum = defaultPortNum;
            MaxPortNum = maxPortNum;
            DefaultInputDur = defaultInputDur;
            MaxInputDur = maxInputDur;
            CheckMaxDur = checkMaxDur;
            ParserValidator = parserValidator;
        }

        public List<IPreparser> GetStandardPreparsers()
        {
            return new List<IPreparser>()
            {
                new RemoveWhitespacePreparser(),
                new LowercasePreparser(),
                new RemoveCommentsPreparser(),
                new InputMacroPreparser(Macros),
                new InputSynonymPreparser(Synonyms),
                new RemoveCommentsPreparser(),
                new ExpandPreparser(),
                new RemoveWhitespacePreparser(),
                new LowercasePreparser()
            };
        }

        public List<IParserComponent> GetStandardParserComponents()
        {
            return new List<IParserComponent>()
            {
                new PortParserComponent(),
                new HoldParserComponent(),
                new ReleaseParserComponent(),
                new InputParserComponent(ValidInputs),
                new PercentParserComponent(),
                new MillisecondParserComponent(),
                new SecondParserComponent(),
                new SimultaneousParserComponent()
            };
        }

        /// <summary>
        /// Creates a parser.
        /// </summary>
        /// <returns>An <see cref="IParser"/> instance.</returns>
        public IParser Create()
        {
            List<IPreparser> preparsers = GetStandardPreparsers();
            List<IParserComponent> components = GetStandardParserComponents();

            StandardParser standardParser = new StandardParser(preparsers, components, DefaultPortNum, MaxPortNum,
                DefaultInputDur, MaxInputDur, CheckMaxDur, ParserValidator);
            
            return standardParser;
        }
    }
}