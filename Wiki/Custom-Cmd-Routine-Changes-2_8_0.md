# Custom Command and Routine changes in TRBot 2.8.0

TRBot 2.8.0 introduces numerous changes, including common methods to retrieve information from the database.

Here's what you need to know about migrating custom commands and routines to TRBot v2.8.0:

- `DataHelper` has been removed. Use extension methods from the `DatabaseMngr` for unopened contexts and `BotDBContext` for opened contexts.
- `DataHelper.GetOrAddUser` is no longer available. TRBot should add the user to the database automatically unless in exceptional circumstances (Ex. WebSocket service with no user object).
- `DataReloader` is no longer available. Instead listen for the `reload_data` event (`DataEventNames.RELOAD_DATA`) in `EvtDispatcher`.
- `MessageHelpers` has been removed. Use `EvtDispatcher.BroadcastMessage` instead.
- You will need to include `using TRBot.Commands;` or `using TRBot.Routines;` for custom commands and routines, respectively.