﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using static TRBot.Input.Consoles.ConsoleConstants;

namespace TRBot.Input.Consoles
{
    /// <summary>
    /// Represents input data.
    /// </summary>
    public class InputData
    {
        /// <summary>
        /// The ID of the input.
        /// </summary>
        public int ID { get; set; } = 0;

        /// <summary>
        /// The console ID the input belongs to.
        /// </summary>
        public int ConsoleID { get; set; } = 0;

        /// <summary>
        /// The access level of the input.
        /// </summary>
        public long Level { get; set; } = 0;

        /// <summary>
        /// The name of the input.
        /// </summary>
        public string Name { get; set; } = string.Empty;
        
        /// <summary>
        /// The button value for the input.
        /// </summary>
        public int ButtonValue { get; set; } = 0;

        /// <summary>
        /// The axis value for the input.
        /// </summary>
        public int AxisValue { get; set; } = 0;

        /// <summary>
        /// The type of input this input is. An input can be more than one type.
        /// </summary>
        public InputTypes InputType { get; set; } = InputTypes.Blank;

        /// <summary>
        /// The minimum value of the axis, normalized from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
        /// </summary>
        public double MinAxisVal { get; set; }

        /// <summary>
        /// The maximum value of the axis, normalized from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
        /// </summary>
        public double MaxAxisVal { get; set; }

        /// <summary>
        /// The default value of the axis, or its at rest state, normalized from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
        /// </summary>
        public double DefaultAxisVal { get; set; }

        /// <summary>
        /// Whether the input is enabled.
        /// </summary>
        public long Enabled { get; set; } = 1;

        /// <summary>
        /// The GameConsole associated with this input.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        public virtual GameConsole Console { get; set; } = null;

        /// <summary>
        /// A list of InvalidInputCombos this input is associated with.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        public virtual List<InvalidInputCombo> InvalidCombos { get; set; } = null;

        /// <summary>
        /// A helper property to get the input's enabled state.
        /// </summary>
        public bool IsEnabled => (Enabled > 0);

        public InputData()
        {

        }

        public InputData(string name, in int buttonValue, in int axisValue, in InputTypes inputType,
            in double minAxisVal, in double maxAxisVal, in double defaultAxisVal)
        {
            Name = name;
            ButtonValue = buttonValue;
            AxisValue = axisValue;
            InputType = inputType;
            MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
        }

        public InputData(string name, in int buttonValue, in int axisValue, in InputTypes inputType,
            in double minAxisVal, in double maxAxisVal, in double defaultAxisVal, in long inputLevel)
            : this(name, buttonValue, axisValue, inputType, minAxisVal, maxAxisVal, defaultAxisVal)
        {
            Level = inputLevel;
        }

        public void UpdateData(in InputData inputData)
        {
            Name = inputData.Name;
            ButtonValue = inputData.ButtonValue;
            AxisValue = inputData.AxisValue;
            InputType = inputData.InputType;
            MinAxisVal = inputData.MinAxisVal;
            MaxAxisVal = inputData.MaxAxisVal;
            DefaultAxisVal = inputData.DefaultAxisVal;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = (hash * 31) + ID.GetHashCode();
                hash = (hash * 31) + ConsoleID.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"Name: \"{Name}\" | {nameof(ConsoleID)}: {ConsoleID} | BtnVal: {ButtonValue} | AxisVal: {AxisValue} | InputType: {(int)InputType} ({InputType}) | MinAxis: {MinAxisVal} | MaxAxis: {MaxAxisVal} | | DefaultAxisVal: {DefaultAxisVal} | Level: {Level} | Enabled: {IsEnabled}";
        }
    }
}
