﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Linq;
using System.Text;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Lists all permission abilities.
    /// </summary>
    public sealed class ListPermissionAbilitiesCommand : BaseCommand
    {
        public ListPermissionAbilitiesCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            StringBuilder strBuilder = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //No abilities
                if (context.PermAbilities.Count() == 0)
                {
                    QueueMessage(args.ServiceName, "There are no permission abilities!");
                    return;
                }

                //Order alphabetically
                IOrderedQueryable<PermissionAbility> permAbilities = context.PermAbilities.OrderBy(p => p.Name);

                strBuilder = new StringBuilder(250);
                strBuilder.Append("Hi, ").Append(args.Command.UserMessage.Username);
                strBuilder.Append(", here's a list of all permission abilities: ");

                foreach (PermissionAbility pAbility in permAbilities)
                {
                    strBuilder.Append(pAbility.Name);
                    strBuilder.Append(',').Append(' ');
                }
            }

            strBuilder.Remove(strBuilder.Length - 2, 2);

            QueueMessage(args.ServiceName, strBuilder.ToString(), ", ");
        }
    }
}
