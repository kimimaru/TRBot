﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that removes a restricted input from a user.
    /// </summary>
    public sealed class RemoveRestrictedInputCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"username\", \"console name\", \"input name\"";

        public RemoveRestrictedInputCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount != 3)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string username = arguments[0];

            string userExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(username, args.ServiceName);

            User restrictedUser = DatabaseMngr.GetUserByExternalID(userExternalID);

            //Check for the user
            if (restrictedUser == null)
            {
                QueueMessage(args.ServiceName, "A user with this name does not exist in the database!");
                return;
            }

            //Compare this user's level with the user they're trying to restrict
            User thisUser = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

            if (thisUser == null)
            {
                QueueMessage(args.ServiceName, "Huh? The user calling this doesn't exist in the database!");
                return;
            }

            if (thisUser.Level <= restrictedUser.Level)
            {
                QueueMessage(args.ServiceName, "Cannot remove restricted inputs for users greater than or equal to you in level!");
                return;
            }

            string consoleStr = arguments[1].ToLowerInvariant();
            long consoleID = 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);
                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }

                consoleID = console.ID;
            }

            string inputName = arguments[2].ToLowerInvariant();
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                restrictedUser = context.GetUserByExternalID(userExternalID);

                //Check if the restricted input exists for this console
                RestrictedInput restrictedInput = restrictedUser.RestrictedInputs.FirstOrDefault(r => r.inputData.Name == inputName && r.inputData.ConsoleID == consoleID);

                //Not restricted
                if (restrictedInput == null)
                {
                    QueueMessage(args.ServiceName, $"{restrictedUser.Name} already has no restrictions on inputting \"{inputName}\" on the \"{consoleStr}\" console!");
                    return;
                }

                //Remove the restricted input and save
                restrictedUser.RestrictedInputs.Remove(restrictedInput);
                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Lifted the restriction for {restrictedUser.Name} on inputting \"{inputName}\" on the \"{consoleStr}\" console!");
        }
    }
}
