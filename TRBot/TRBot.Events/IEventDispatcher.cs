﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Events
{
    /// <summary>
    /// An interface for event dispatchers.
    /// </summary>
    public interface IEventDispatcher : IDisposable
    {
        /// <summary>
        /// Whether extra features from the event dispatcher are enabled.
        /// The features are implementation-dependent.
        /// </summary>
        bool ExtraFeaturesEnabled { get; }

        void EnableExtraFeatures();
        void DisableExtraFeatures();

        void RegisterEvent<T>(string eventName) where T : EventArgs;
        void UnregisterEvent(string eventName);
        bool HasRegisteredEvent(string eventName);

        void AddListener<T>(string eventName, Action<T> action) where T : EventArgs;
        void RemoveListener<T>(string eventName, Action<T> action) where T : EventArgs;
        void RemoveAllListeners(string eventName);

        void DispatchEvent<T>(string eventName, T eventData) where T : EventArgs;
        void DispatchEventWithoutExtraFeatures<T>(string eventName, T eventData) where T : EventArgs;
    }
}
