:: This batch script will build TRBot for both Windows and GNU/Linux
:: Run it in the same directory as this file
echo off

:: Make a "build" folder if it doesn't exist
mkdir build

:: Change directory to the main application
cd .\TRBot\TRBot.Main

:: TRBot GNU/Linux build
dotnet publish -c Release -o ..\..\build/TRBotLinux --self-contained --runtime linux-x64

:: TRBot Windows build
dotnet publish -c Release -o ..\..\build/TRBotWin --self-contained --runtime win-x64

echo Builds complete!

cd ..\..
