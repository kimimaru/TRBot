/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NUnit.Framework;
using System.Collections.Generic;
using TRBot.Connection;

namespace TRBot.Tests.Connection
{
    [TestFixture]
    public class ConnectionHelpersUnitTests
    {
        private static TestCaseData[] ParseCommandArgumentsWithQuotesReturnsCorrectArgumentListTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(
                    "!say",
                    new string[]
                    {
                        "!say"
                    }),
                new TestCaseData(
                    "this is a test",
                    new string[]
                    {
                        "this",
                        "is",
                        "a",
                        "test",
                    }),
                new TestCaseData(
                    "this is a test",
                    new string[]
                    {
                        "this",
                        "is",
                        "a",
                        "test",
                    }),
                new TestCaseData(
                    "hi \"user 2\" this \"is a\" test",
                    new string[]
                    {
                        "hi",
                        "user 2",
                        "this",
                        "is a",
                        "test",
                    }),
                new TestCaseData(
                    "!addmeme \"this is not a meme\" test",
                    new string[]
                    {
                        "!addmeme",
                        "this is not a meme",
                        "test",
                    }),
                new TestCaseData(
                    "this \"is \"a test\" string\"",
                    new string[]
                    {
                        "this",
                        "is ",
                        "a",
                        "test\"",
                        "string\"",
                    }),
                new TestCaseData(
                    "this \"is a test string\"",
                    new string[]
                    {
                        "this",
                        "is a test string",
                    }),
                new TestCaseData(
                    "\"this is a test string\"",
                    new string[]
                    {
                        "this is a test string",
                    }),
            };
        }

        [TestCaseSource(nameof(ParseCommandArgumentsWithQuotesReturnsCorrectArgumentListTestCases))]
        public void ParseCommandArgumentsWithQuotesReturnsCorrectArgumentList(string messageStr, string[] expectedArgs)
        {
            List<string> argList = ConnectionHelpers.ParseCommandArgumentsWithQuotes(messageStr);

            CollectionAssert.AreEqual(expectedArgs, argList);
        }
    }
}
