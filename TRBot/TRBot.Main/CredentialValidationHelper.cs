﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection.Twitch;
using TRBot.Connection.WebSocket;
using TRBot.Connection.IRC;
using TRBot.Connection.XMPP;
using TRBot.Connection.Matrix;
using TRBot.Utilities;
using TwitchLib.Client.Models;
using Newtonsoft.Json;

namespace TRBot.Main
{
    /// <summary>
    /// Contains helper methods regarding validating credentials.
    /// </summary>
    public static class CredentialValidationHelper
    {
        /// <summary>
        /// Validates if the file for Twitch credentials exists and reads it if so.
        /// If it does not exist, it creates it and returns a newly initialized object.
        /// </summary>
        /// <returns>A TwitchLoginSettings object.</returns>
        public static TwitchLoginSettings ValidateTwitchCredentialsPresent(string loginSettingsPath)
        {
            TwitchLoginSettings loginSettings = null;

            //Read from the file and create it if it doesn't exist
            string text = FileHelpers.ReadFromTextFileOrCreate(loginSettingsPath);
            
            if (string.IsNullOrEmpty(text) == true)
            {
                //Create default settings and save to the file
                loginSettings = new TwitchLoginSettings();
                string loginText = JsonConvert.SerializeObject(loginSettings, Formatting.Indented);
                FileHelpers.SaveToTextFile(loginSettingsPath, loginText);
            }
            else
            {
                //Read from the file
                loginSettings = JsonConvert.DeserializeObject<TwitchLoginSettings>(text);
            }

            return loginSettings;
        }

        /// <summary>
        /// Creates connection credentials based on twitch login settings.
        /// </summary>
        /// <returns>A ConnectionCredentials object.</returns>
        public static ConnectionCredentials MakeCredentialsFromTwitchLogin(TwitchLoginSettings twitchLoginSettings)
        {
            return new ConnectionCredentials(twitchLoginSettings.BotName, twitchLoginSettings.Password); 
        }

        /// <summary>
        /// Validates if the file for WebSocket connection credentials exists and reads it if so.
        /// If it does not exist, it creates it and returns a newly initialized object.
        /// </summary>
        /// <returns>A WebSocketConnectSettings object.</returns>
        public static WebSocketConnectSettings ValidateWebSocketCredentialsPresent(string loginSettingsPath)
        {
            WebSocketConnectSettings loginSettings = null;

            //Read from the file and create it if it doesn't exist
            string text = FileHelpers.ReadFromTextFileOrCreate(loginSettingsPath);
            
            if (string.IsNullOrEmpty(text) == true)
            {
                //Create default settings and save to the file
                loginSettings = new WebSocketConnectSettings();
                string loginText = JsonConvert.SerializeObject(loginSettings, Formatting.Indented);
                FileHelpers.SaveToTextFile(loginSettingsPath, loginText);
            }
            else
            {
                //Read from the file
                loginSettings = JsonConvert.DeserializeObject<WebSocketConnectSettings>(text);
            }

            return loginSettings;
        }

        /// <summary>
        /// Validates if the file for IRC login settings exists and reads it if so.
        /// If it does not exist, it creates it and returns a newly initialized object.
        /// </summary>
        /// <returns>An IRCLoginSettings object.</returns>
        public static IRCLoginSettings ValidateIRCCredentialsPresent(string loginSettingsPath)
        {
            IRCLoginSettings loginSettings = null;

            //Read from the file and create it if it doesn't exist
            string text = FileHelpers.ReadFromTextFileOrCreate(loginSettingsPath);
            
            if (string.IsNullOrEmpty(text) == true)
            {
                //Create default settings and save to the file
                loginSettings = new IRCLoginSettings();
                string loginText = JsonConvert.SerializeObject(loginSettings, Formatting.Indented);
                FileHelpers.SaveToTextFile(loginSettingsPath, loginText);
            }
            else
            {
                //Read from the file
                loginSettings = JsonConvert.DeserializeObject<IRCLoginSettings>(text);
            }

            return loginSettings;
        }

        /// <summary>
        /// Validates if the file for XMPP login settings exists and reads it if so.
        /// If it does not exist, it creates it and returns a newly initialized object.
        /// </summary>
        /// <returns>An XMPPLoginSettings object.</returns>
        public static XMPPLoginSettings ValidateXMPPCredentialsPresent(string loginSettingsPath)
        {
            XMPPLoginSettings loginSettings = null;

            //Read from the file and create it if it doesn't exist
            string text = FileHelpers.ReadFromTextFileOrCreate(loginSettingsPath);
            
            if (string.IsNullOrEmpty(text) == true)
            {
                //Create default settings and save to the file
                loginSettings = new XMPPLoginSettings();
                string loginText = JsonConvert.SerializeObject(loginSettings, Formatting.Indented);
                FileHelpers.SaveToTextFile(loginSettingsPath, loginText);
            }
            else
            {
                //Read from the file
                loginSettings = JsonConvert.DeserializeObject<XMPPLoginSettings>(text);
            }

            return loginSettings;
        }

        /// <summary>
        /// Validates if the file for Matrix login settings exists and reads it if so.
        /// If it does not exist, it creates it and returns a newly initialized object.
        /// </summary>
        /// <returns>A MatrixLoginSettings object.</returns>
        public static MatrixLoginSettings ValidateMatrixCredentialsPresent(string loginSettingsPath)
        {
            MatrixLoginSettings loginSettings = null;

            //Read from the file and create it if it doesn't exist
            string text = FileHelpers.ReadFromTextFileOrCreate(loginSettingsPath);
            
            if (string.IsNullOrEmpty(text) == true)
            {
                //Create default settings and save to the file
                loginSettings = new MatrixLoginSettings();
                string loginText = JsonConvert.SerializeObject(loginSettings, Formatting.Indented);
                FileHelpers.SaveToTextFile(loginSettingsPath, loginText);
            }
            else
            {
                //Read from the file
                loginSettings = JsonConvert.DeserializeObject<MatrixLoginSettings>(text);
            }

            return loginSettings;
        }
    }
}
