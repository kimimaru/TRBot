## Connecting TRBot to an arbitrary WebSocket
This continues from the [initial setup](./Setup-Init.md), if you choose to connect TRBot over a WebSocket.

The service name for WebSocket in the **Services** table of the **TRBotData.db** database is `websocket`.

The WebSocket service needs additional configuration to connect and has its own configuration file. The default template file is named "WebSocketConnectionSettings.txt" and is located in the Data folder. The file path can be changed with the `ConfigFile` row in the database.

The settings for the config file are described below:

- *BotName* = Desired display username of your bot.
- *ConnectURL* = The full WebSocket address to connect to. This starts with "ws://" for unsecured connections and "wss://" for secured connections. For example, "ws://127.0.0.1:5333", will connect to a WebSocket on localhost (your own computer) through port 5333.

Data sent to the WebSocket server itself can come from almost anything: it can be a simple console application or an input form on a website. However, the server will need to send the following JSON response for TRBot to be able to parse it:

```
{
    "user": {
        "externalId": "idhere",
        "username": "namehere"
    },
    "message": {
        "text": "text here"
    }
}
```

The `user` field is optional; if omitted, all of TRBot's user-specific features will be unavailable for that response.

TRBot internally uses [websocket-sharp](https://github.com/sta/websocket-sharp), specifically the [WebSocketSharp-netstandard fork](https://github.com/PingmanTools/websocket-sharp/) as a library to handle WebSocket connections. See the [example WebSocket server](../Supplementary/websocketexample.py) for a very simple working local WebSocket server.
