﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Sets the default input duration.
    /// </summary>
    public sealed class DefaultInputDurCommand : BaseCommand
    {
        private string UsageMessage = "Usage: no arguments (get value). \"duration (int)\" (set value)";

        public DefaultInputDurCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int defaultInputDur = (int)DatabaseMngr.GetSettingInt(SettingsConstants.DEFAULT_INPUT_DURATION, 200L);

            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The default duration of an input is {defaultInputDur} milliseconds!");
                return;
            }

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has this ability
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user == null)
                {
                    QueueMessage(args.ServiceName, "Somehow, the user calling this is not in the database.");
                    return;
                }

                if (user.HasEnabledAbility(PermissionConstants.SET_DEFAULT_INPUT_DUR_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the global default input duration!");
                    return;
                }
            }

            if (StringNumParser.TryParseInt(arguments[0], out int newDefaultDur) == false)
            {
                QueueMessage(args.ServiceName, "Please enter a valid number!");
                return;
            }

            if (newDefaultDur <= 0)
            {
                QueueMessage(args.ServiceName, "Cannot set a duration less than or equal to 0!");
                return;
            }

            if (newDefaultDur == defaultInputDur)
            {
                QueueMessage(args.ServiceName, "The duration is already this value!");
                return;
            }

            //Change the setting
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings defaultDurSetting = context.GetSetting(SettingsConstants.DEFAULT_INPUT_DURATION);

                defaultDurSetting.ValueInt = newDefaultDur;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Set the default input duration to {newDefaultDur} milliseconds!");
        }
    }
}
