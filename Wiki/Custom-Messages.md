# Custom Messages
TRBot supports custom messages that are sent when a user sends a certain number of messages. The default set offers a gradual tutorial to help newer players learn how to play.

Custom messages are enabled when the `custom_messages_enabled` setting is 1 and disabled when 0. Use the [ToggleCustomMessagesCommand](../TRBot/TRBot.Commands/Commands/ToggleCustomMessagesCommand.cs) (default: "!togglecustommessages") command to easily change this value.

## Adding a custom message
To add a custom message, we have to add it as a new row in the database. If you don't know how to view the database, please read [managing data](./Managing-Data.md).

1. Open the database and navigate to the **CustomMessages** table.
2. Create a new record and fill out the following information:
  - Set the `Message` to the message you want to output.
  - Set the `TriggerValue` to a number corresponding to the message count of the user. For example, a `TriggerValue` of 5 would output the message when the user has sent exactly 5 messages total.
3. Write your changes to the database.

## Additional Information
- The user's message count is determined by the **TotalMessageCount** row in the **UserStats** table.
- In the `Message` row, specify "{0}" to insert the user's name and "{1}" to insert the message count.
  - For example, "Hi {0}, you've sent {1} messages!" would read "Hi fred, you've sent 50 messages!" for a user named "fred" and a `TriggerValue` of 50.
