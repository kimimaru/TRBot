/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NSubstitute;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TRBot.Input.Parsing;

namespace TRBot.Tests.Input
{
    [TestFixture]
    public class StandardParserFactoryUnitTests
    {
        private IQueryable<InputMacro> Macros = null;
        IEnumerable<InputSynonym> Synonyms = null;
        IList<string> ValidInputs = null;
        IParserValidator ParserValidator = null;

        [SetUp]
        public void Setup()
        {
            Macros = Substitute.For<IQueryable<InputMacro>>();
            Synonyms = Substitute.For<IEnumerable<InputSynonym>>();
            ValidInputs = Substitute.For<IList<string>>();
            ParserValidator = Substitute.For<IParserValidator>();
        }

        [Test]
        public void StandardParserFactoryCreatesStandardParser()
        {
            StandardParserFactory spf = new StandardParserFactory(Macros, Synonyms, ValidInputs, 0, 0, 200, 60000, true, ParserValidator);
            IParser parser = spf.Create();

            Assert.That(parser.GetType(), Is.EqualTo(typeof(StandardParser)));
        }

        [Test]
        public void StandardParserFactoryReturnsCorrectStandardPreparsers()
        {
            StandardParserFactory spf = new StandardParserFactory(Macros, Synonyms, ValidInputs, 0, 0, 200, 60000, true, ParserValidator);

            var compareList = new List<IPreparser>()
            {
                new RemoveWhitespacePreparser(),
                new LowercasePreparser(),
                new RemoveCommentsPreparser(),
                new InputMacroPreparser(Macros),
                new InputSynonymPreparser(Synonyms),
                new RemoveCommentsPreparser(),
                new ExpandPreparser(),
                new RemoveWhitespacePreparser(),
                new LowercasePreparser()
            };

            var preparserList = spf.GetStandardPreparsers();

            Assert.That(preparserList.Count, Is.EqualTo(compareList.Count));
            CollectionAssert.AreEqual(compareList, preparserList, new TypeComparer());
        }

        [Test]
        public void StandardParserFactoryReturnsCorrectStandardParserComponents()
        {
            StandardParserFactory spf = new StandardParserFactory(Macros, Synonyms, ValidInputs, 0, 0, 200, 60000, true, ParserValidator);

            var compareList = new List<IParserComponent>()
            {
                new PortParserComponent(),
                new HoldParserComponent(),
                new ReleaseParserComponent(),
                new InputParserComponent(ValidInputs),
                new PercentParserComponent(),
                new MillisecondParserComponent(),
                new SecondParserComponent(),
                new SimultaneousParserComponent()
            };

            var parserComponentList = spf.GetStandardParserComponents();

            Assert.That(parserComponentList.Count, Is.EqualTo(compareList.Count));
            CollectionAssert.AreEqual(compareList, parserComponentList, new TypeComparer());
        }

        private class TypeComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                if (x == null)
                {
                    return 1;
                }

                if (y == null)
                {
                    return -1;
                }

                if (x == null && y == null)
                {
                    return 0;
                }

                if (x.GetType() == y.GetType())
                {
                    return 0;
                }

                return 1;
            }
        }
    }
}