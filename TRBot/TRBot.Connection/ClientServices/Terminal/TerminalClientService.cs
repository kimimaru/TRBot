﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TRBot.Utilities.Logging;
using TRBot.Events;
using TRBot.Utilities;

namespace TRBot.Connection
{
    /// <summary>
    /// Read inputs through a terminal.
    /// </summary>
    public class TerminalClientService : IClientService
    {
        private const string LOG_PREFIX_STRING = "[" + ClientServiceNames.TERMINAL_SERVICE + "] ";

        /// <summary>
        /// The default name for the terminal user.
        /// </summary>
        private const string DEFAULT_TERMINAL_USERNAME = "terminaluser";

        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        public bool IsInitialized => Initialized;

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        public bool IsConnected => Connected;

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        public bool CanSendMessages => Connected;

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        public IBotMessageHandler MsgHandler { get; private set; } = null;

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        public IMessageSettings MsgSettings { get; private set; } = null;

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        public ICommandSettings CmdSettings { get; private set; } = null;

        private bool Initialized = false;
        private bool Connected = false;

        private string TerminalUserExternalID = DEFAULT_TERMINAL_USERNAME;
        private string TerminalUsername = DEFAULT_TERMINAL_USERNAME;
        private volatile bool StopConsoleThread = false;

        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        private readonly string TerminalServiceName = ClientServiceNames.TERMINAL_SERVICE;

        public TerminalClientService(ITRBotLogger logger, IEventDispatcher evtDispatcher,
            IBotMessageHandler msgHandler, IMessageSettings msgSettings, ICommandSettings cmdSettings)
        {
            Logger = logger;
            EvtDispatcher = evtDispatcher;
            MsgHandler = msgHandler;
            MsgSettings = msgSettings;
            CmdSettings = cmdSettings;
        }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        public void Initialize()
        {
            MsgHandler.SetClientService(this);

            EvtDispatcher.RegisterEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.RegisterEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.RegisterEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.RegisterEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.RegisterEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND);

            Initialized = true;
        }

        /// <summary>
        /// Connects the client.
        /// </summary>
        public void Connect()
        {
            StopConsoleThread = false;

            WaitForMainInitialization();

            Connected = true;
        }

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        public void Disconnect()
        {
            Connected = false;
            StopConsoleThread = true;

            EvtDisconnectedArgs evtDisconnectedArgs = new EvtDisconnectedArgs()
            {
                ServiceName = ClientServiceNames.TERMINAL_SERVICE
            };

            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, evtDisconnectedArgs);
        }

        /// <summary>
        /// Send a message through the client.
        /// </summary>
        public void ProcessAndSendMessage(IServiceMessage serviceMessage)
        {
            if (string.IsNullOrEmpty(serviceMessage.Message) == true)
            {
                Logger.Warning($"Empty message in {nameof(ProcessAndSendMessage)}!");
                return;
            }

            string sentMessage = serviceMessage.Message;

            if (serviceMessage.OverridePrefix == true)
            {
                sentMessage = serviceMessage.CustomPrefix + serviceMessage.Message;
            }
            else if (string.IsNullOrEmpty(MsgSettings.MessagePrefix) == false)
            {
                sentMessage = MsgSettings.MessagePrefix + serviceMessage.Message;
            }

            Logger.Log(serviceMessage.LogLevel, LOG_PREFIX_STRING + sentMessage);

            var outgoingArgs = new EvtOutgoingBotMessageArgs()
            {
                ServiceName = TerminalServiceName,
                UserMessage = new EvtUserMsgData(string.Empty, TerminalUsername, sentMessage)
            };

            EvtDispatcher.DispatchEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE, outgoingArgs);
        }

        public void Dispose()
        {
            Disconnect();

            MsgHandler.CleanUp();

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.COMMAND);
        }

        private void WaitForMainInitialization()
        {
            SetupStart();

            //Run this on a new task to handle inputs through the console
            Task.Run(ReadWaitInputs);
        }

        private void SetupStart()
        {
            Logger.Information("\nPlease enter up to two space-separated arguments for the user to act as in the terminal.");
            Logger.Information("- 1 argument = username");
            Logger.Information("- 2 arguments = username, external ID");
            Logger.Information($"Skip to use \"{DEFAULT_TERMINAL_USERNAME}\" as the username and external ID.");

            string argsStr = string.Empty;
            List<string> argList = null;

            while (IsTerminalUserArgListValid(argList) == false)
            {
                argsStr = Console.ReadLine();

                argList = ConnectionHelpers.ParseCommandArgumentsWithQuotes(Helpers.ReplaceAllWhitespaceWithSpace(argsStr));
            }

            Console.WriteLine();

            //Set the fields from the arguments
            if (argList.Count > 0)
            {
                TerminalUsername = argList[0];
            }

            if (argList.Count > 1)
            {
                TerminalUserExternalID = argList[1];
            }

            EvtConnectedArgs conArgs = new EvtConnectedArgs()
            {
                ServiceName = TerminalServiceName,
                BotUsername = TerminalUsername
            };

            EvtDispatcher.DispatchEvent(ClientServiceEventNames.CONNECTED, conArgs);
        }

        private bool IsTerminalUserArgListValid(List<string> argList)
        {
            if (argList == null || argList.Count > 2)
            {
                return false;
            }

            return argList.Contains(string.Empty) == false;
        }

        private void ReadWaitInputs()
        {
            while (true)
            {
                if (StopConsoleThread == true)
                {
                    break;
                }

                try
                {
                    string line = Console.ReadLine();

                    //Ignore null or empty strings
                    if (string.IsNullOrEmpty(line) == true)
                    {
                        continue;
                    }

                    string lineWSReplaced = Utilities.Helpers.ReplaceAllWhitespaceWithSpace(line);
    
                    //Send message event
                    EvtUserMessageArgs umArgs = new EvtUserMessageArgs()
                    {
                        ServiceName = TerminalServiceName,
                        UserMessage = new EvtUserMsgData(TerminalUserExternalID, TerminalUsername, lineWSReplaced)
                    };

                    EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, umArgs);
    
                    //Parse command
                    EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(lineWSReplaced,
                        CmdSettings.CommandIdentifier, new EvtUserMsgData(TerminalUserExternalID, TerminalUsername, lineWSReplaced));
                    
                    if (chatCmdData != null)
                    {
                        EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                        {
                            ServiceName = TerminalServiceName,
                            Command = chatCmdData
                        };

                        EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error($"Error encountered in Terminal service. {e.Message}");
                }
            }
        }
    }
}
