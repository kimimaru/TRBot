TRBot v2.0, a major rewrite and refactoring of the entire codebase.

There are far too many changes to list. The most notable changes are:
- The codebase has been split into separate projects designed for specific purposes.
- The codebase has been cleaned up tremendously with better programming practices. This includes more emphasis on dependency injection and SOLID principles.
- All data is now in a SQLite database. This allows most data to be changed at runtime, makes the data far more scalable, and allows easily searching and filtering through data.
- Consoles are flexible - you can add and remove inputs from them, and even create entirely new consoles on the fly.
- Enhanced permissions system - user abilities determine who can do what. Enable or disable an ability on a user indefinitely or for a specific amount of time.
- **MANY** new bot features - periodic inputs, teams mode, custom commands, mid-input delays, and more!

To run the main application, locate the **TRBot.Main** binary for your platform and execute it.

Please read the [wiki](https://github.com/teamradish/TRTwitchPlaysBot/tree/2.0/Wiki/Home.md) for your use-case.

**IMPORTANT:** Please read the [migration guide](https://github.com/teamradish/TRTwitchPlaysBot/blob/2.0/Wiki/Migrating-Data.md) and download the **TRBotDataMigrationTool** if you're upgrading from 1.8!

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://github.com/teamradish/TRTwitchPlaysBot/blob/2.0/Wiki/Managing-Data.md).

This is TRBot's largest release yet, and with so many changes, there are bound to be issues. Please [file an issue](https://github.com/teamradish/TRTwitchPlaysBot/issues/new?assignees=&labels=bug&template=bug-report.md&title=%5BBUG%5D) so we can address it as soon as possible.
