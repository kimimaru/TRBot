/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.IO;

namespace TRBot.Utilities
{
    /// <summary>
    /// Contains utilities for debugging.
    /// </summary>
    public static class DebugUtility
    {
        /// <summary>
        /// The name of the crash log folder.
        /// </summary>
        public const string CRASH_LOG_FOLDER_NAME = "CrashLogs";

        /// <summary>
        /// The default path to the crash log folder.
        /// </summary>
        public static readonly string DefaultCrashLogPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CRASH_LOG_FOLDER_NAME);
        
        /// <summary>
        /// Gets the path for crash log files.
        /// </summary>
        /// <param name="folderPathResolver">The folder path resolver.
        ///  If null, the default crash log path will be used.</param>
        /// <returns>A string representing the crash log path.</returns>
        public static string GetCrashLogPath(IFolderPathResolver folderPathResolver)
        {
            return folderPathResolver?.CrashLogsFolderPath ?? DefaultCrashLogPath;
        }

        /// <summary>
        /// Gets the next crash log filename.
        /// </summary>
        /// <param name="folderPathResolver">The folder path resolver.
        ///  If null, the default crash log path will be used.</param>
        /// <param name="applicationVersion">A string containing the application version.</param>
        /// <returns>A string with the full name of the crash log file.</returns>
        public static string GetCrashLogFilename(IFolderPathResolver folderPathResolver, string applicationVersion)
        {
            string time = GetFileFriendlyTimeStamp();
            string path = Path.Combine(folderPathResolver?.CrashLogsFolderPath ?? DefaultCrashLogPath,
                $"{nameof(TRBot)} {applicationVersion} Crash Log - {time}.txt");
            return path;
        }
        
        /// <summary>
        /// Returns an OS-agnostic file time stamp of the current time.
        /// </summary>
        /// <returns>A string representing the current time.</returns>
        public static string GetFileFriendlyTimeStamp()
        {
            return GetFileFriendlyTimeStamp(DateTime.Now);
        }
        
        /// <summary>
        /// Returns an OS-agnostic file time stamp of a given time.
        /// </summary>
        /// <param name="dateTime">The time stamp.</param>
        /// <returns>A string representing the current time.</returns>
        public static string GetFileFriendlyTimeStamp(DateTime dateTime)
        {
            string time = dateTime.ToUniversalTime().ToString();
            time = time.Replace(':', '-');
            time = time.Replace('/', '-');
            return time;
        }
    }
}