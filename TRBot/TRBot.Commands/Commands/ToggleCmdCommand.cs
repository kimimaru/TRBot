﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that toggles a command's enabled state.
    /// </summary>
    public sealed class ToggleCmdCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"command name\", (\"true\" or \"false\")";

        public ToggleCmdCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with the incorrect number of arguments
            if (argCount != 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string commandName = arguments[0].ToLowerInvariant();
            string enabledStr = arguments[1];
            bool cmdEnabled = false;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Find the command
                CommandData cmdData = context.Commands.FirstOrDefault(c => c.Name == commandName);

                if (cmdData == null)
                {
                    QueueMessage(args.ServiceName, $"Cannot find a command named \"{commandName}\".");
                    return;
                }

                if (bool.TryParse(enabledStr, out cmdEnabled) == false)
                {
                    QueueMessage(args.ServiceName, "Incorrect command enabled state specified.");
                    return;
                }

                cmdData.Enabled = (cmdEnabled == true) ? 1 : 0;

                context.SaveChanges();
            }

            BaseCommand baseCmd = CmdHandler.GetCommand(commandName);
            baseCmd.Enabled = cmdEnabled;

            QueueMessage(args.ServiceName, $"Successfully set the enabled state of command \"{commandName}\" to {cmdEnabled}!");
        }
    }
}
