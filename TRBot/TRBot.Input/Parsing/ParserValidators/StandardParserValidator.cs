﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Text;
using TRBot.Input.Consoles;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities.Logging;

namespace TRBot.Input.Parsing
{
    /// <summary>
    /// A parser validator for the <see cref="StandardParser"/>.
    /// </summary>
    public class StandardParserValidator : IParserValidator
    {
        private HashSet<string> RestrictedInputs = null;
        private long UserLevel = 0L;
        private IGameConsole UsedConsole = null;
        private IVirtualControllerManager VControllerMngr = null;

        private Dictionary<string, InputData> InputPermissionLevels = null;
        private List<InvalidInputCombo> InvalidCombos = null;

        private StringBuilder ComboStrBuilder = new StringBuilder(128);
        private Dictionary<int, HashSet<string>> CurrentPressedInputsDict = null;

        private List<HashSet<string>> InvalidCombosAsNames = new List<HashSet<string>>();
        private HashSet<string> InputNamesAcrossAllInvalidCombos = new HashSet<string>();

        private ITRBotLogger Logger = null;

        private int MinLengthCombo = int.MaxValue;

        public StandardParserValidator(HashSet<string> restrictedInputs,
            in long userLevel, IGameConsole usedConsole, IVirtualControllerManager vControllerMngr,
            ITRBotLogger logger)
        {
            RestrictedInputs = restrictedInputs;
            UserLevel = userLevel;
            UsedConsole = usedConsole;
            VControllerMngr = vControllerMngr;

            InputPermissionLevels = UsedConsole.ConsoleInputs;
            InvalidCombos = UsedConsole.InvalidInputCombos;

            Logger = logger;

            Initialize();
        }

        private void Initialize()
        {
            int controllerCount = VControllerMngr.ControllerCount;

            //Add all input names in the invalid combos
            for (int i = 0; i < InvalidCombos.Count; i++)
            {
                InvalidInputCombo invalidCombo = InvalidCombos[i];

                //Skip if there are no inputs
                if (invalidCombo.InvalidInputs.Count == 0)
                {
                    continue;
                }

                //Ignore if this invalid combo is valid for this user
                //It's valid if the user is at or above the designated level
                if (UserLevel >= invalidCombo.ValidLevelThreshold)
                {
                    continue;
                }

                //Optimize - store the minimum length combo
                if (invalidCombo.InvalidInputs.Count < MinLengthCombo)
                {
                    MinLengthCombo = invalidCombo.InvalidInputs.Count;
                }

                //Fetch only the names of the invalid combos for more reliable comparison
                HashSet<string> stringHashSet = new HashSet<string>(invalidCombo.InvalidInputs.Count);
                InvalidCombosAsNames.Add(stringHashSet);

                foreach (InputData inputData in invalidCombo.InvalidInputs)
                {
                    InputNamesAcrossAllInvalidCombos.Add(inputData.Name);
                    stringHashSet.Add(inputData.Name);
                }
            }

            //The dictionary is for each controller port
            CurrentPressedInputsDict = new Dictionary<int, HashSet<string>>(controllerCount);
            
            //Add pressed inputs from each controller
            for (int i = 0; i < controllerCount; i++)
            {
                IVirtualController controller = VControllerMngr.GetController(i); 
                if (controller.IsAcquired == false)
                {
                    Logger.Warning($"Controller at index {i} is not acquired!");
                    continue;
                }

                //Add already pressed inputs from all controllers
                foreach (string inputName in InputNamesAcrossAllInvalidCombos)
                {
                    //Check if the button exists and is pressed
                    if (UsedConsole.GetButtonValue(inputName, out InputButton inputBtn) == true)
                    {
                        if (controller.GetButtonState(inputBtn.ButtonVal) == ButtonStates.Pressed)
                        {
                            if (CurrentPressedInputsDict.TryGetValue(i, out HashSet<string> pressedCombosOnPort) == false)
                            {
                                pressedCombosOnPort = new HashSet<string>(InputNamesAcrossAllInvalidCombos.Count);
                                CurrentPressedInputsDict[i] = pressedCombosOnPort;
                            }

                            pressedCombosOnPort.Add(inputName);
                        }
                    }
                    //Check if the axis exists and is pressed in any capacity
                    else if (UsedConsole.GetAxisValue(inputName, out InputAxis inputAxis) == true)
                    {
                        if (controller.GetAxisState(inputAxis.AxisVal, inputAxis.DefaultAxisVal) != inputAxis.DefaultAxisVal)
                        {
                            if (CurrentPressedInputsDict.TryGetValue(i, out HashSet<string> pressedCombosOnPort) == false)
                            {
                                pressedCombosOnPort = new HashSet<string>(InputNamesAcrossAllInvalidCombos.Count);
                                CurrentPressedInputsDict[i] = pressedCombosOnPort;
                            }

                            pressedCombosOnPort.Add(inputName);
                        }
                    }
                    else
                    {
                        Logger.Warning($"\"{inputName}\" is part of an invalid input combo but doesn't exist in the \"{UsedConsole.Name}\" console.");
                    }
                }
            }
        }

        public ParserValidationData ValidateInput(in ParsedInput parsedInput)
        {
            //Check for a valid controller port
            ParserValidationData validationData = ValidateControllerPort(parsedInput.ControllerPort);
            if (validationData.ParserValidationType == ParserValidationTypes.Failed)
            {
                return validationData;
            }

            //Check if this input is restricted
            validationData = ValidateRestrictedInputs(parsedInput.Name);
            if (validationData.ParserValidationType == ParserValidationTypes.Failed)
            {
                return validationData;
            }

            //Check permission level
            validationData = ValidatePermissionLevels(parsedInput.Name);
            if (validationData.ParserValidationType == ParserValidationTypes.Failed)
            {
                return validationData;
            }

            //End with validating input combos
            return ValidateInputCombos(parsedInput);
        }

        public ParserValidationData ValidateControllerPort(int controllerPort)
        {
            //Check for a valid controller port
            if (controllerPort >= 0 && controllerPort < VControllerMngr.ControllerCount)
            {
                //Check if the controller is acquired
                IVirtualController controller = VControllerMngr.GetController(controllerPort);
                if (controller.IsAcquired == false)
                {
                    return new ParserValidationData(ParserValidationTypes.Failed, $"(ERROR) Joystick number {controllerPort + 1} with controller ID of {controller.ControllerID} has not been acquired! Ensure you, the host, have a virtual controller set up at this ID (double check permissions)");
                }
            }
            //Invalid port
            else
            {
                return new ParserValidationData(ParserValidationTypes.Failed, $"Invalid joystick number {controllerPort + 1}. Joystick count = {VControllerMngr.ControllerCount}. Please change yours or your input's controller port to a valid number to perform inputs");
            }

            return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
        }

        public ParserValidationData ValidateRestrictedInputs(string parsedInputName)
        {
            if (RestrictedInputs != null && RestrictedInputs.Contains(parsedInputName) == true)
            {
                return new ParserValidationData(ParserValidationTypes.Failed, $"You're restricted from using input \"{parsedInputName}\"");
            }

            return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
        }

        public ParserValidationData ValidatePermissionLevels(string parsedInputName)
        {
            if (InputPermissionLevels != null
                && InputPermissionLevels.TryGetValue(parsedInputName, out InputData inputData) == true
                && UserLevel < inputData.Level)
            {
                return new ParserValidationData(ParserValidationTypes.Failed,
                    $"No permission to use input \"{parsedInputName}\", which requires at least level {inputData.Level}");
            }

            return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
        }

        public ParserValidationData ValidateInputCombos(in ParsedInput parsedInput)
        {
            //If all inputs in an invalid combo are somehow pressed already, any other inputs will result in a failure
            //However, returning false here would automatically prevent any further inputs from ever working
            //Give a chance to check releasing inputs in the combo

            //Check if there are no invalid combos
            if (InvalidCombosAsNames.Count == 0)
            {
                return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
            }

            //Get controller port and initialize
            int port = parsedInput.ControllerPort;
            
            //Ensure a currentcombo entry is available for this port
            if (CurrentPressedInputsDict.TryGetValue(port, out HashSet<string> currentCombo) == false)
            {
                currentCombo = new HashSet<string>(InputNamesAcrossAllInvalidCombos.Count);
                CurrentPressedInputsDict[port] = currentCombo;
            }

            //Don't bother adding/removing if the input isn't in any invalid combo
            //This will essentially check just the existing combo
            if (InputNamesAcrossAllInvalidCombos.Contains(parsedInput.Name) == true)
            {
                //Add if not a release
                if (parsedInput.Release == false)
                {
                    currentCombo.Add(parsedInput.Name);
                }
                //Remove if released
                else
                {
                    currentCombo.Remove(parsedInput.Name);
                }
            }

            //Check for an invalid combo once the current combo reaches the shortest invalid combo
            if (currentCombo.Count >= MinLengthCombo)
            {
                //Check all inputs
                for (int i = 0; i < InvalidCombosAsNames.Count; i++)
                {
                    HashSet<string> invalidComboAsName = InvalidCombosAsNames[i];
                    
                    //All inputs in this are input combo are pressed
                    if (invalidComboAsName.IsSubsetOf(currentCombo) == true)
                    {
                        ComboStrBuilder.Clear();

                        //Make the message mention which inputs aren't allowed
                        ComboStrBuilder.Append("Inputs (");

                        foreach (string inputName in invalidComboAsName)
                        {
                            ComboStrBuilder.Append('"').Append(inputName).Append('"');
                            ComboStrBuilder.Append(',').Append(' ');
                        }

                        ComboStrBuilder.Remove(ComboStrBuilder.Length - 2, 2);

                        ComboStrBuilder.Append(") are not allowed to be pressed at the same time");

                        return new ParserValidationData(ParserValidationTypes.Failed, ComboStrBuilder.ToString());
                    }
                }
            }

            return new ParserValidationData(ParserValidationTypes.Passed, string.Empty);
        }
    }
}
