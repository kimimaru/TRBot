﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TRBot.Input.Parsing;

namespace TRBot.Input.Consoles
{
    /// <summary>
    /// The interface for game consoles.
    /// </summary>
    public interface IGameConsole
    {
        /// <summary>
        /// The id of the console.
        /// </summary>
        int ID { get; } 

        /// <summary>
        /// The name of the console.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// All the input data for this console.
        /// </summary>
        Dictionary<string, InputData> ConsoleInputs { get; }

        /// <summary>
        /// The input list for the console.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        List<InputData> InputList { get; }

        /// <summary>
        /// The invalid input combos for the console.
        /// This is used by the database and should not be assigned or modified manually.
        /// </summary>
        List<InvalidInputCombo> InvalidInputCombos { get; }

        void SetConsoleInputs(Dictionary<string, InputData> consoleInputs);

        void SetInputsFromList(List<InputData> inputList);

        /// <summary>
        /// Adds an input to the console. If the input already exists, it will be updated with the new value.
        /// This updates the input regex if the input did not previously exist.
        /// </summary>
        /// <param name="inputName">The name of the input to add.</param>
        /// <param name="inputData">The data corresponding to the input.</param>
        /// <returns>true if the input was added, otherwise false.</returns>
        bool AddInput(string inputName, InputData inputData);

        /// <summary>
        /// Removes an input from the console.
        /// This updates the input regex if removed.
        /// </summary>
        /// <param name="inputName">The name of the input to remove.</param>
        /// <returns>true if the input was removed, otherwise false.</returns>
        bool RemoveInput(string inputName);

        /// <summary>
        /// Tells if a given input exists for this console.
        /// </summary>
        /// <param name="inputName">The name of the input.</param>
        /// <returns>true if the input name is a valid input, otherwise false.</returns>
        bool DoesInputExist(string inputName);

        /// <summary>
        /// Tells if a given input exists and is enabled for this console.
        /// </summary>
        /// <param name="inputName">The name of the input.</param>
        /// <returns>true if the input name is a valid input and the input is enabled, otherwise false.</returns>
        bool IsInputEnabled(string inputName);

        /// <summary>
        /// Tells if a given axis value exists and returns it if so.
        /// </summary>
        /// <param name="axisName">The name of the axis to get the value for.</param>
        /// <param name="inputAxis">The returned InputAxis if found.</param>
        /// <returns>true if an enabled axis with the given name exists, otherwise false.</returns>
        bool GetAxisValue(string axisName, out InputAxis inputAxis);

        /// <summary>
        /// Tells if a given button value exists and returns it if so.
        /// </summary>
        /// <param name="buttonName">The name of the button to get the value for.</param>
        /// <param name="inputButton">The returned InputButton if found.</param>
        /// <returns>true if an enabled button with the given name exists, otherwise false.</returns>
        bool GetButtonValue(string buttonName, out InputButton inputButton);

        /// <summary>
        /// A more efficient version of telling whether an input is an axis.
        /// Returns the axis if found to save a dictionary lookup if one is needed afterwards.
        /// </summary>
        /// <param name="input">The input to check.</param>
        /// <param name="axis">The InputAxis value that is assigned. If no axis is found, the default value.</param>
        /// <returns>true if the input is an enabled axis, otherwise false.</returns>
        bool GetAxis(in ParsedInput input, out InputAxis axis);

        /// <summary>
        /// Tells whether an input is an axis or not.
        /// </summary>
        /// <param name="input">The input to check.</param>
        /// <returns>true if the input is an enabled axis, otherwise false.</returns>
        bool IsAxis(in ParsedInput input);

        /// <summary>
        /// Tells whether the input is a button.
        /// </summary>
        /// <param name="input">The input to check.</param>
        /// <returns>true if the input is an enabled button, otherwise false.</returns>
        bool IsButton(in ParsedInput input);

        /// <summary>
        /// Tells whether the input is a blank input, an input without any specially defined function.
        /// </summary>
        /// <param name="input">The input to check.</param>
        /// <returns>true if the input is enabled and is not a button or axes, otherwise false.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        bool IsBlankInput(in ParsedInput input);

        /// <summary>
        /// Returns the names of all inputs for the console in a new array that align with the given filter.
        /// </summary>
        /// <param name="filter">Filtering for the returned input names.</param>
        /// <returns>A new array containing all the input names for the console associated with the filter.</returns>
        string[] GetInputNames(in InputNameFiltering filter);

        /// <summary>
        /// Gets the first available blank input usable by a user,
        /// considering the user's level and their restricted inputs.
        /// </summary>
        /// <param name="userLevel">The level of the user.</param>
        /// <param name="userRestrictedInputs">The user's restricted inputs.</param>
        /// <returns>InputData corresponding to a usable blank input. If none are found nor usable by the user, then null.</returns>
        InputData GetAvailableBlankInput(in long userLevel, HashSet<string> userRestrictedInputs);
    }
}
