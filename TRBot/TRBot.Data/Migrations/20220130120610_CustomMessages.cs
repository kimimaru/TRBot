﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TRBot.Data.Migrations
{
    public partial class CustomMessages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "custommessages");

            migrationBuilder.CreateTable(
                name: "CustomMessages",
                schema: "custommessages",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Message = table.Column<string>(type: "TEXT", nullable: true),
                    TriggerValue = table.Column<long>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomMessages", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomMessages_TriggerValue",
                schema: "custommessages",
                table: "CustomMessages",
                column: "TriggerValue");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomMessages",
                schema: "custommessages");
        }
    }
}
