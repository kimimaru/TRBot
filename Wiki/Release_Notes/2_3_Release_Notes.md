TRBot v2.3, featuring WebSocket support, LibreTranslate integration, and vastly improved macro parsing.

Summary of changes:
- The macro preparser has been rewritten from scratch to fix bugs and improve performance. Recursive macros are now parsed in-place, yielding better accuracy. Performance has been improved by **roughly 2x**, memory usage has been reduced by **roughly 2x**, and there are no longer limits on dynamic macro arguments!
- A new WebSocket client service is available by setting the `ValueInt` of the `client_service_type` setting to **2**. TRBot can now connect to a WebSocket server and read inputs from its responses! This also supports secure WebSockets.
- [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate) integration! Translate text through TRBot!
- Fixed integer overflows when parsing inputs, causing them to have negative durations.
- Fixed the total duration of input sequences being calculated incorrectly.
- Addd a separate permission for starting input mode votes.
- Updated Json.Net from 12.0.3 to 13.0.1.

To run the main application, locate the **TRBot.Main** binary for your platform and execute it.

Please read the [wiki](https://codeberg.org/kimimaru/TRBot/src/tag/2.3/Wiki/Home.md) for your use-case.

**IMPORTANT:** Please read the [migration guide](https://codeberg.org/kimimaru/TRBot/src/tag/2.3/Wiki/Migrating-Data.md) and download the **TRBotDataMigrationTool** if you're upgrading from 1.8!

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.3/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
