## Connecting TRBot to a local Terminal
This continues from the [initial setup](./Setup-Init.md), if you choose to connect TRBot over a local terminal.

The service name for the terminal in the **Services** table of the **TRBotData.db** database is `terminal`.

When running TRBot through the terminal, no login settings are required or used. Instead, it prompts you for a username or a username and External ID to use while the bot is running. This can be an existing user in the database. If no data is specified, it defaults to a user with a name and External ID of "terminaluser".

In this mode, TRBot will read all lines input to the terminal window. Simply press Enter/Return after typing what you want to process it. TRBot is 100% functional in this mode, allowing one to perform commands and inputs just like the other modes.
