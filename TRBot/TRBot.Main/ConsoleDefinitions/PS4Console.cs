﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Input.VirtualControllers;
using TRBot.Input.Consoles;

namespace TRBot.Main
{
    /// <summary>
    /// The PlayStation 4.
    /// </summary>
    public sealed class PS4Console : GameConsole
    {
        public PS4Console()
        {
            Name = "ps4";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(39)
            {
                { "left",       ConsoleHelpers.CreateAxisInput("left", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "right",      ConsoleHelpers.CreateAxisInput("right", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "up",         ConsoleHelpers.CreateAxisInput("up", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "down",       ConsoleHelpers.CreateAxisInput("down", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },
                { "rleft",      ConsoleHelpers.CreateAxisInput("rleft", (int)GlobalAxisVals.AXIS_RX, 0.5d, 0d, 0.5d) },
                { "rright",     ConsoleHelpers.CreateAxisInput("rright", (int)GlobalAxisVals.AXIS_RX, 0.5d, 1d, 0.5d) },
                { "rup",        ConsoleHelpers.CreateAxisInput("rup", (int)GlobalAxisVals.AXIS_RY, 0.5d, 0d, 0.5d) },
                { "rdown",      ConsoleHelpers.CreateAxisInput("rdown", (int)GlobalAxisVals.AXIS_RY, 0.5d, 1d, 0.5d) },
                { "swipeleft",  ConsoleHelpers.CreateAxisInput("swipeleft", (int)GlobalAxisVals.AXIS_M1, 0.5d, 0d, 0.5d) },
                { "swiperight", ConsoleHelpers.CreateAxisInput("swiperight", (int)GlobalAxisVals.AXIS_M1, 0.5d, 1d, 0.5d) },
                { "swipeup",    ConsoleHelpers.CreateAxisInput("swipeup", (int)GlobalAxisVals.AXIS_M2, 0.5d, 0d, 0.5d) },
                { "swipedown",  ConsoleHelpers.CreateAxisInput("swipedown", (int)GlobalAxisVals.AXIS_M2, 0.5d, 1d, 0.5d) },

                { "dleft",      ConsoleHelpers.CreateButtonInput("dleft", (int)GlobalButtonVals.BTN1) },
                { "dright",     ConsoleHelpers.CreateButtonInput("dright", (int)GlobalButtonVals.BTN2) },
                { "dup",        ConsoleHelpers.CreateButtonInput("dup", (int)GlobalButtonVals.BTN3) },
                { "ddown",      ConsoleHelpers.CreateButtonInput("ddown", (int)GlobalButtonVals.BTN4) },
                { "square",     ConsoleHelpers.CreateButtonInput("square", (int)GlobalButtonVals.BTN9) },
                { "triangle",   ConsoleHelpers.CreateButtonInput("triangle", (int)GlobalButtonVals.BTN10) },
                { "circle",     ConsoleHelpers.CreateButtonInput("circle", (int)GlobalButtonVals.BTN11) },
                { "cross",      ConsoleHelpers.CreateButtonInput("cross", (int)GlobalButtonVals.BTN12) },
                { "share",      ConsoleHelpers.CreateButtonInput("share", (int)GlobalButtonVals.BTN13) },
                { "options",    ConsoleHelpers.CreateButtonInput("options", (int)GlobalButtonVals.BTN14) },
                { "l1",         ConsoleHelpers.CreateButtonInput("l1", (int)GlobalButtonVals.BTN15) },
                { "r1",         ConsoleHelpers.CreateButtonInput("r1", (int)GlobalButtonVals.BTN16) },
                { "l2",         ConsoleHelpers.CreateButtonInput("l2", (int)GlobalButtonVals.BTN17) },
                { "r2",         ConsoleHelpers.CreateButtonInput("r2", (int)GlobalButtonVals.BTN18) },
                { "l3",         ConsoleHelpers.CreateButtonInput("l3", (int)GlobalButtonVals.BTN19) },
                { "r3",         ConsoleHelpers.CreateButtonInput("r3", (int)GlobalButtonVals.BTN20) },
                { "touchclick", ConsoleHelpers.CreateButtonInput("touchclick", (int)GlobalButtonVals.BTN21) },
                { "#",          ConsoleHelpers.CreateBlankInput("#") },
                { ".",          ConsoleHelpers.CreateBlankInput(".") },

                //Spare buttons
                { "sb1",        ConsoleHelpers.CreateButtonInput("sb1", (int)GlobalButtonVals.BTN24) },
                { "sb2",        ConsoleHelpers.CreateButtonInput("sb2", (int)GlobalButtonVals.BTN25) },
                { "sb3",        ConsoleHelpers.CreateButtonInput("sb3", (int)GlobalButtonVals.BTN26) },
                { "sb4",        ConsoleHelpers.CreateButtonInput("sb4", (int)GlobalButtonVals.BTN27) },
                { "sb5",        ConsoleHelpers.CreateButtonInput("sb5", (int)GlobalButtonVals.BTN28) },
                { "sb6",        ConsoleHelpers.CreateButtonInput("sb6", (int)GlobalButtonVals.BTN29) },
                { "sb7",        ConsoleHelpers.CreateButtonInput("sb7", (int)GlobalButtonVals.BTN30) },
                { "sb8",        ConsoleHelpers.CreateButtonInput("sb8", (int)GlobalButtonVals.BTN31) },
            });

            InvalidInputCombos = new List<InvalidInputCombo>();
        }
    }
}
