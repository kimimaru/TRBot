Welcome! This is a quick guide to TRBot's data and how to manage it. It's highly recommended to read through this at least once before setting up TRBot.

If you already know SQLite, you can skip directly to the sections most relevant to you.

# Intro
TRBot uses a SQLite database to manage data, such as inputs, consoles, and numerous settings.

Here's some background on why TRBot chose to use a database for its 2.x+ releases:
* Databases are highly scalable. They can fit many different types of data, neatly organized into tables and columns.
* Databases can search and filter data **very fast**!
* Databases are surprisingly simple to manage and use. There are numerous visual database viewers out there to make this even easier.
* Database changes are reflected in real-time.
* SQLite in particular can be run locally and requires very little maintenance. SQLite uses a single database file for all of its transactions, making it easy to backup and restore data.

In contrast, the 1.x releases of TRBot used several JSON files for handling data, and it was extremely hard to manage and scale. Furthermore, any reads/writes to the data involved loading the *entire JSON file*, which was very slow.

Many features in the 2.x+ releases, such as dynamic GameConsoles and inputs, are possible thanks to the use of the SQLite database. SQLite essentially solved all of TRBot's data problems, hence why it was chosen.

# Getting Started
First, run TRBot at least once to generate a **TRBotData.db** database file. This is the SQLite database file that TRBot regularly reads and writes to.

## Data Folder and Database Location

The **TRBotData.db** file is stored in a **Data** folder, the location of which varies depending on whether TRBot was installed as system-wide (Ex. a package manager) or portable. 
- Portable: a sub-directory of the executable (Ex. `~/Desktop/TRBot/Data`)
- System-wide: the user's local application folder. This is `~/.local/share/TRBot/Data` on GNU/Linux and `%LOCALAPPDATA%/TRBot/Data` on Windows.

If you downloaded a release from a code repository, chances are the install will be portable. A message is logged to the console on startup if TRBot is being run as portable or system-wide.

Whenever you make a change to the data, whether inside or outside of TRBot, it will be reflected in TRBot immediately (outside of some exceptions: see below). This allows you to change your data without having to restart TRBot. Some uses for this include adding new commands, changing user data, and modifying messages.

# Viewing data
It's recommended to use a SQLite database viewer such as [SQLiteBrowser](https://sqlitebrowser.org/) or [SQLiteStudio](https://github.com/pawelsalawa/sqlitestudio). It's also possible to use the SQLite CLI to view and modify data, but that is out of scope for this tutorial. This tutorial will presume that you are using a database viewer, specifically SQLiteBrowser.

## Opening the database
Open SQLiteBrowser and click File > Open Database, then load in your **TRBotData.db** file. You should see a view similar to this one:

![TRBot database tables in SQLiteBrowser](Images/Setup/TRBot_DBTables.png)

## Tables
The tables you see in the database are, in short, the categories of data TRBot stores. Below are brief descriptions of each.

- **CommandData** - Holds all data on commands, such as command names, permission levels, and the actual code they link to.
- **Consoles** - The available game consoles, each of which determines the available inputs that can be put into chat.
- **CustomMessages** - Messages sent to users when they type a specific number of messages in chat.
- **GameLogs** - Stores time-stamped logs that users can add on stream. Useful for tracking game progress.
- **InputSynonyms** - Stores console-specific synonyms correlating to inputs.
- **Inputs** - Contains all available inputs. Inputs are console-specific and have button and axes values.
- **InvalidInputCombos** - Collections of console-specific input combinations that cannot be pressed at once. This can include soft reset button combinations.
- **InvalidInputCombosInputs** - A join table associating **InvalidInputCombos** with **Inputs** and vice versa. **Auto-generated - do not modify.**
- **Macros** - Contains all available input macros.
- **Memes** - Contains all available memes.
- **PermissionAbilities** - Contains all available abilities that can be granted or revoked to users. Typically, this does not change much over time.
- **RecentInputs** - Contains a specific number of recent inputs made by each player if opted into bot stats.
- **RestrictedInputs** - References inputs that each user is restricted from performing.
- **RoutineData** - Holds all data on bot routines, such as routine names, enabled states, and the actual code they link to.
- **Services** - Contains data containing all the services TRBot should connect to for handling interaction.
- **Settings** - Contains a myriad of bot settings.
- **UserAbilities** - Contains all granted abilities and which users they belong to.
- **UserDisplayRanks** - Cosmetic ranks visible to users based on their input count.
- **UserStats** - Contains all user-specific stats, such as valid input and message count.
- **Users** - Contains all user data, including name, level, and controller port.

The **sqlite_sequence** and **__EFMigrationsHistory** tables are automatically generated and **should not be modified**.

The **Indices** likewise **should not be modified** and exist as a means to improve performance.

## Browsing data
While the database is open, click on the **Browse Data** tab in SQLiteBrowser. You can select the table you want to browse, which will show all the rows in that table. This is how you can view all of TRBot's data!

You can select a column by clicking on it. If the Edit Database Cell menu isn't up, open it with View > Edit Database Cell. You can modify the data for the selected column and apply it. Note that the change hasn't been saved yet! To save changes, hit CTRL + S or click on File > Write Changes. In SQLiteBrowser, you can hold CTRL + Shift and click on a column to jump to its relation, if it has one. This is useful for seeing which User a UserStats row belongs to, for instance.

If you want to revert the changes you've applied but have not yet saved, click on File > Revert Changes.

# Notable Settings
Unless noted otherwise, all of the following settings are under the "Settings" table in the database.

- [`main_thread_sleep`](./Settings-Documentation.md#main_thread_sleep) - Indicates how much time, in milliseconds, to sleep the main thread. This is used to throttle TRBot's main loop so it doesn't use up as much CPU time on your machine. Values too high may noticeably delay the execution of bot routines and messages.
- [`first_launch`](./Settings-Documentation.md#first_launch) - Indicates the first ever launch of TRBot. This sets up all the default game consoles. Starts at 0 then gets set to 1.
- [`force_init_defaults`](./Settings-Documentation.md#force_init_defaults) - If 1, initializes all default values, including default commands, permissions, and settings, if they don't already exist.
- [`data_version`](./Settings-Documentation.md#data_version) - The version for TRBot's data. If this is behind the bot version being used, it will automatically set force_init_defaults to 1 and add missing data.
- [`joystick_count`](./Settings-Documentation.md#joystick_count) - The number of virtual controllers to use.
- [`last_vcontroller_type`](./Settings-Documentation.md#last_vcontroller_type) - The type of virtual controller to use. If the one specified is not available on your platform, it will be switched to the default available one automatically.
- [`last_console`](./Settings-Documentation.md#last_console) - The game console to use.
- [`message_cooldown`](./Settings-Documentation.md#message_cooldown) - Indicates how much time, in milliseconds, each message can be sent in max. This acts as a message throttler for platforms with rate-limiting on bots.

For more information on settings, view the [settings documentation](./Settings-Documentation.md).

# Reloading data
TRBot comes with a [`ReloadCommand`](../TRBot/TRBot.Commands/Commands/ReloadCommand.cs) (default: "!reload") to reload data. Some data cannot be applied to TRBot until it is reloaded for technical reasons. There are two types of reloads: soft and hard. Hard reloads typically destroy in-memory objects then recreate them using data from the database, while soft reloads typically apply only the changes. Pass "soft" or "hard" as an argument to this command to choose how to reload (Ex. "!reload hard").

# Data requiring reloads/restarts

## CommandData
All commands require code to function and thus are stored in memory as separate objects using information from the CommandData table. If TRBot is running, you will need to reload to apply any changes made directly to the CommandData table.

## RoutineData
Like with commands, all routines are stored as separate objects using information from the RoutineData table. The same reload rules apply.

## ServiceData
Reloading is required to apply changes to which services are active, in addition to adding or removing newly active or inactive services.
