﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Utilities;
using TRBot.Permissions;
using TRBot.Data;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Simulates a user.
    /// </summary>
    public sealed class UserSimulateCommand : BaseCommand
    {
        /// <summary>
        /// The number of words to use for each prefix.
        /// </summary>
        private const int PREFIX_COUNT = 2;

        /// <summary>
        /// The minimum number of prefixes required to generate a sentence.
        /// </summary>
        private const int MIN_PREFIX_GEN_COUNT = 5;

        private string UsageMessage = "Usage: \"username (optional)\"";

        private string UserDataConsentCmdName = null;
        private string ManageCmdUsage = string.Empty;

        public UserSimulateCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void CleanUp()
        {
            UserDataConsentCmdName = null;
            ManageCmdUsage = string.Empty;

            base.CleanUp();
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            CacheManageCommandMessage();

            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string thisUserName = args.Command.UserMessage.Username;
            string thisUserExternalID = args.Command.UserMessage.UserExternalID;
            string simulatedUserName = thisUserName;
            string simulatedUserExternalID = thisUserExternalID;

            if (arguments.Count > 0)
            {
                simulatedUserName = arguments[0];
                simulatedUserExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(simulatedUserName, args.ServiceName);
            }

            //Check if the user has enough credits
            long creditCost = DatabaseMngr.GetSettingInt(SettingsConstants.USER_SIMULATE_CREDIT_COST, 1000L);

            long thisUserCredits = 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;

                User thisUser = context.Users
                    .Include(u => u.Stats)
                    .Include(u => u.UserAbilities)
                    .ThenInclude(ua => ua.PermAbility)
                    .FirstOrDefault(u => u.ExternalID == thisUserExternalID);

                if (thisUser == null || thisUser.HasEnabledAbility(PermissionConstants.SIMULATE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to use simulate!");
                    return;
                }

                //It's possible to simulate other users while opted out of simulate data
                //However, if simulate costs credits, the user can't use it if opted out of bot stats
                if (creditCost > 0L && thisUser.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, "You're not opted into bot stats, so you cannot use simulate!");
                    return;
                }

                //Fetch available credits
                thisUserCredits = thisUser.Stats.Credits;
            }

            string simulateData = string.Empty;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;

                User simulatedUser = context.Users
                    .Include(u => u.Stats).FirstOrDefault(u => u.ExternalID == simulatedUserExternalID);

                if (simulatedUser == null)
                {
                    QueueMessage(args.ServiceName, $"User not found in database.");
                    return;
                }

                if (simulatedUser.HasConsentToDataOption(UserDataConsentOptions.Simulate) == false)
                {
                    QueueMessage(args.ServiceName, $"\"{simulatedUserName}\" cannot be simulated because they're not opted into simulate data! {ManageCmdUsage}");
                    return;
                }

                simulateData = simulatedUser.Stats.SimulateHistory;
            }

            string creditsName = DatabaseMngr.GetCreditsName();

            if (creditCost > 0L && thisUserCredits < creditCost)
            {
                QueueMessage(args.ServiceName, $"You need at least {creditCost} {creditsName.Pluralize(creditCost)} to use simulate!");
                return;
            }

            //Generate a sentence no longer than the bot character limit
            int botCharLimit = (int)DatabaseMngr.GetSettingInt(SettingsConstants.BOT_MSG_CHAR_LIMIT, 500L);

            MarkovChainTextGenerator textGenerator = new MarkovChainTextGenerator();
            textGenerator.PrefixWordCount = PREFIX_COUNT;
            textGenerator.MinPrefixGenCount = MIN_PREFIX_GEN_COUNT;
            textGenerator.CharacterLimit = botCharLimit;

            string simulateSentence = textGenerator.GenerateText(simulateData);

            if (string.IsNullOrEmpty(simulateSentence) == true)
            {
                QueueMessage(args.ServiceName, "No simulation could be generated for this user. There may not be enough data available. Simply talk in chat to generate simulation data.");
                return;
            }

            QueueMessage(args.ServiceName, simulateSentence);

            //Subtract credits if there is a cost
            if (creditCost > 0L)
            {
                long remainingCredits = 0L;

                //Subtract the credits from this user
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    User thisUser = context.GetUserByExternalID(thisUserExternalID);

                    thisUser.Stats.Credits -= creditCost;

                    context.SaveChanges();

                    remainingCredits = thisUser.Stats.Credits;
                }

                QueueMessage(args.ServiceName, $"Spent {creditCost} {creditsName.Pluralize(creditCost)} to simulate! You now have {remainingCredits} {creditsName.Pluralize(remainingCredits)} remaining!");
            }
        }
        
        private void CacheManageCommandMessage()
        {
            //Check specifically for null, which means the message hasn't been cached at all
            //We can't do it in Initialize because the other command may not have been added to the command handler yet
            if (UserDataConsentCmdName != null)
            {
                return;
            }

            //Get the name of the command that manages user data
            if (CmdHandler.GetCommand<UserManageDataConsentCommand>(out UserDataConsentCmdName) == false)
            {
                UserDataConsentCmdName = string.Empty;
                return;
            }

            //Set the message to tell users how to opt in
            ManageCmdUsage = $"To change your simulate opt status, use the \"{UserDataConsentCmdName}\" command.";
        }
    }
}
