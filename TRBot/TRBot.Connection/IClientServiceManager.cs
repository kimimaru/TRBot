﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Connection
{
    /// <summary>
    /// Manages multiple client services.
    /// </summary>
    public interface IClientServiceManager : IDisposable
    {
        /// <summary>
        /// The number of client services in the manager.
        /// </summary>
        int ServiceCount { get; }

        /// <summary>
        /// Initializes the manager.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Adds a client service to the manager.
        /// </summary>
        /// <param name="serviceName">The name of the client service.</param>
        /// <param name="clientService">The client service to add.</param>
        /// <param name="autoConnect">If true, connects the client service immediately.
        /// If false, the caller is responsible for connecting the client service manually.</param>
        void AddClientService(string serviceName, IClientService clientService, in bool autoConnect);

        /// <summary>
        /// Removes a client service from the manager.
        /// </summary>
        /// <param name="serviceName">The name of the client service to remove.</param>
        void RemoveClientService(string serviceName);
        
        /// <summary>
        /// Retrieves a client service by name.
        /// </summary>
        /// <param name="serviceName">The name of the client service.</param>
        /// <returns>A client service associated with the given name. null if the client service wasn't found.</returns>
        IClientService GetClientService(string serviceName);

        /// <summary>
        /// Retrieves the names of all client services being managed.
        /// </summary>
        string[] GetAllServiceNames();

        /// <summary>
        /// Retrieves all client services being managed.
        /// </summary>
        IClientService[] GetAllServices();
    }
}
