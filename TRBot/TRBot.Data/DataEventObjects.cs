/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Newtonsoft.Json;
using TRBot.Events;

namespace TRBot.Data
{
    /// <summary>
    /// Shared events for data.
    /// </summary>
    public static class DataEventObjects
    {
        public class EvtInitDataArgs : EvtBaseEventArgs
        {
            [JsonProperty("serviceName")]
            public string ServiceName { get; init; } = string.Empty;

            public EvtInitDataArgs(string serviceName)
            {
                ServiceName = serviceName;
            }
        }

        public class EvtDataReloadedArgs : EvtBaseEventArgs
        {
            [JsonProperty("hardReload")]
            public bool HardReload { get; init; } = false;

            [JsonProperty("serviceName")]
            public string ServiceName { get; init; } = string.Empty;

            public EvtDataReloadedArgs()
            {

            }

            public EvtDataReloadedArgs(in bool hardReload, string serviceName)
            {
                HardReload = hardReload;
                ServiceName = serviceName;
            }
        }
    }
}