#!/usr/bin/env python

# This is a Python source file that sets up a test WebSocket, which you can connect to via TRBot
# Run this program separately with "python websocketexample.py" or "python3 websocketexample.py" depending on your platform
#
# Overview: This sets up a WebSocket on localhost (127.0.0.1) on port 8765 by default
# It then asynchronously reads console input on another task, which the WebSocket server reads and to the WebSocket client as JSON
# The user is "websocketuser"
#
# This requires Python 3+ and the websockets and aioconsole packages to be installed
# Install the package with "python -m pip install websockets aioconsole" or "python3 -m pip install websockets aioconsole" depending on your platform
#
# The websockets package is licensed under BSD 3-Clause: https://github.com/aaugustin/websockets/blob/main/LICENSE
# The aioconsole package is licensed under GPLv3: https://github.com/vxgmichel/aioconsole
# This specific file is licensed under BSD 3-Clause

import asyncio
import websockets
import json
import aioconsole
from aioconsole import ainput

input_txt = ""
set_input = False

async def input_handler():
    global input_txt
    global set_input    

    set_input = False

    while True:
        # Await here so it can switch between this and the websocket_handler tasks without any blocking
        await asyncio.sleep(.01)

        if set_input == False:
            # Get input from the console asynchronously
            input_txt = await ainput()
            set_input = True

async def websocket_handler(websocket, path):
    global input_txt
    global set_input    

    print("Called websocket_handler!")

    set_input = False

    # Send console inputs over the socket
    while True:
        # Wait for a response from the websocket we're connecting to and timeout early
        try:
            await asyncio.wait_for(websocket.recv(), timeout=0.1)
        # Connection closed fine, no issues
        except websockets.ConnectionClosedOK as excOk:
            print("Socket closed ok with code: " + str(excOk.code))
            await websocket.close()
            return
        # Error closing socket
        except websockets.ConnectionClosedError as exc:
            print("Error closing socket: " + str(exc.code))
            await websocket.close()
            return
        # The operation timed out - this is normal
        except asyncio.TimeoutError:
            pass

        # Wait until input is retrieved from the console
        if set_input == False:
            continue

        obj = {
            "user": {
                "externalId": "websocketuser",
                "username": "websocketuser"
            },
            "message": {
                "text": input_txt
            }
        }

        # Convert the JSON to string
        objJSON = json.dumps(obj)

        print(objJSON)

        set_input = False

        # If configured correctly, this should cause TRBot to press inputs on its virtual controllers if the input was valid
        # Commands and other text will work just the same!
        try:
            await websocket.send(objJSON)
        except:
            print("Something wrong happened with the socket!")
            await websocket.close()
            return

# Main Code

# To connect to this WebSocket via TRBot, add a new row in the Services table with a ServiceName of "websocket" and Enabled set to 1. Then, open WebSocketConnectionSettings.txt (if not available, run TRBot once to generate it)
# Set the ConnectURL to "ws://(hosthere):(porthere)/" - for instance, the default should be "ws://127.0.0.1:8765/"
host = "127.0.0.1"
port = 8765

start_server = websockets.serve(websocket_handler, host, port)

print("Started WebSocket server at host " + host + " at port " + str(port))

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.ensure_future(input_handler())
asyncio.get_event_loop().run_forever()

