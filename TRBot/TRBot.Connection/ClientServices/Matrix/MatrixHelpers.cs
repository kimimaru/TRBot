﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Text.RegularExpressions;

namespace TRBot.Connection.Matrix
{
    /// <summary>
    /// Helper methods regarding Matrix.
    /// </summary>
    public static class MatrixHelpers
    {
        /// <summary>
        /// Gets the localpart of a Matrix user ID (MXID).
        /// </summary>
        /// <param name="matrixUserID">The Matrix user ID.</param>
        /// <returns>A string representing the local part of a Matrix user ID.</returns>
        /// <remarks>
        /// An example of a full MXID is "@example:domain.org".
        /// "example" is the localpart and "domain.org" is the homeserver.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static string GetLocalPartFromMXID(string matrixUserID)
        {
            int indexOfSeparator = matrixUserID.IndexOf(':');
            string localPart = matrixUserID.Substring(1, indexOfSeparator - 1);

            return localPart;
        }

        /// <summary>
        /// Tells if the localpart of Matrix user ID (MXID) is valid.
        /// </summary>
        /// <remarks>https://spec.matrix.org/latest/appendices/#user-identifiers</remarks>
        /// <param name="matrixIDLocalPart">The localpart of the Matrix user ID.</param>
        /// <param name="domain">The domain associated with the JID.</param>
        /// <param name="resource">The resource associated with the JID.</param>
        /// <returns>A bool telling if the given localpart of the Matrix user ID is valid or not.</returns>
        public static bool IsMatrixIDLocalPartValid(string matrixIDLocalPart)
        {
            if (string.IsNullOrEmpty(matrixIDLocalPart) == true)
            {
                return false;
            }

            if (matrixIDLocalPart.Length > MatrixConstants.MAX_MXID_LOCALPART_LENGTH)
            {
                return false;
            }

            var match = Regex.Match(matrixIDLocalPart, @"([a-z]|[0-9]|\.|_|=|\/)+");

            return (match.Success && match.Length == matrixIDLocalPart.Length);
        }
    }
}
