TRBot v2.7.0, focusing on quality of life improvements for both hosts and players using TRBot.

Summary of changes:
- User data has been revamped to be more friendly when TRBot is run over multiple services simultaneously. Users are now accessed by an `ExternalID` that is retrieved from the service they're coming from. This means that usernames are accurate in the database for each service!
 - A compatibility layer has been implemented that automatically associates a username with a given `ExternalID` when that user sends a message on a service if they don't already have this association.
  - Additionally, user data consent for opting in/out of various types of data, such as bot stats, simulate data, memes, and more, has been standardized. Use the new [`UserManageDataConsentCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.7.0/TRBot/TRBot.Commands/Commands/UserManageDataConsentCommand.cs) (default: "!managedata") for all your needs here.
- The `bot_message_char_limit` setting is now respected for **all** bot messages.
- Added command line arguments to allow running TRBot as a portable or system-wide install without recompiling. Run TRBot with the `--help` argument or see [here](https://codeberg.org/kimimaru/TRBot/src/tag/2.7.0/Wiki/Coding-Information.md#command-line-arguments) for all the options.
- Commands now have a `Category` that determines if they show up when listed (User, Input, Admin, Games commands). This was done to show only relevant commands when listed with `ListCmdsCommand`. In addition, new "adminhelp" and "gamelist" commands have been added to list Admin and Games commands, respectively.
- Command parsing has been standardized across all services, and it's now possible to have arguments with whitespace count as a single argument. For example, `hello there` is considered two arguments while `"hello there"` is considered one argument.
- It's now possible to set a custom command identifier through the `command_identifier` setting, which defaults to `!`. Keep in mind that command identifiers with whitespace may not work correctly due to technical constraints and thus are not officially supported.
- New `inputs_enabled` and `bot_messages_enabled` settings have been added to toggle input handling and bot messages, respectively.
- Added a new `SyntaxCommand` (default: "!syntax") which can be used as a quick and effective reference to the various pieces of TRBot's syntax. The messages can be customized via settings starting with "syntax_reference_" for their respective parts of the syntax.
- More information is presented when TRBot starts, such as the current input mode, the command identifier, and more. This should help diagnose issues such as unresponsive inputs that are in reality due to the input mode being set to Democracy.
- Rarely used and marginally useful commands have been removed, along with their settings, if any. The following commands have been removed:
  - AverageCreditsCommand
  - OptStatsCommand
  - IgnoreMemesCommand
  - HighFiveCommand
  - InspirationCommand
  - MedianCreditsCommand
  - RandomNumberCommand
  - ReverseCommand
  - SayCommand
  - SlotsCommand
  - TimeCommand
- Updated SDL2 controller configs for GNU/Linux and added udev controller configs for RetroArch.
- CI images have been updated to more recent operating systems.

**IMPORTANT for those upgrading from previous 2.0+ releases:**
- There are breaking changes for custom commands and routines in TRBot 2.7 regarding accessing user objects. Please [read here](https://codeberg.org/kimimaru/TRBot/src/tag/2.7.0/Wiki/Custom-Cmd-Routine-Changes-2_7.md) for how to update your own commands and routines.

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.7.0/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
