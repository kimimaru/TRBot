﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;
using TRBot.Data;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Shows the length of an input sequence.
    /// </summary>
    public sealed class InputLengthCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"input sequence\"";

        public InputLengthCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            string input = args.Command.ArgumentsAsString;

            if (string.IsNullOrEmpty(input) == true)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            GameConsole usedConsole = null;

            int lastConsoleID = 1;

            lastConsoleID = (int)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole lastConsole = context.Consoles.AsNoTracking().Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos).ThenInclude(ic => ic.InvalidInputs).FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            //If there are no valid inputs, don't attempt to parse
            if (usedConsole == null)
            {
                QueueMessage(args.ServiceName, $"The current console does not point to valid data. Please set a different console to use, or if none are available, add one.");
                return;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current console, \"{usedConsole.Name}\", does not have any available inputs. Cannot determine length.");
                return;
            }

            ParsedInputSequence inputSequence = default;

            try
            {
                string userExternalID = args.Command.UserMessage.UserExternalID;

                //Get default and max input durations
                //Use user overrides if they exist, otherwise use the global values
                int defaultDur = (int)DatabaseMngr.GetUserOrGlobalDefaultInputDur(userExternalID);
                int maxDur = (int)DatabaseMngr.GetUserOrGlobalMaxInputDur(userExternalID);
            
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //Get input synonyms for this console
                    IQueryable<InputSynonym> synonyms = context.InputSynonyms.Where(syn => syn.ConsoleID == lastConsoleID);

                    //Parse inputs to get our parsed input sequence
                    IParser standardParser = new StandardParserFactory(context.Macros, synonyms,
                        usedConsole.GetInputNames(InputNameFiltering.Enabled), 0,
                        VControllerContainer.VControllerMngr.ControllerCount - 1, defaultDur, maxDur, true, null)
                    .Create();

                    inputSequence = standardParser.ParseInputs(input);
                }
            }
            catch (Exception exception)
            {
                string excMsg = exception.Message;

                //Handle parsing exceptions
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;

                QueueMessage(args.ServiceName, $"Invalid input. {excMsg}", Serilog.Events.LogEventLevel.Warning);
                return;
            }

            //Check for non-valid messages
            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                const string dyMacroLenErrorMsg = "Note that length cannot be determined for dynamic macros without inputs filled in.";

                if (inputSequence.ParsedInputResult == ParsedInputResults.NormalMsg
                    || string.IsNullOrEmpty(inputSequence.Error) == true)
                {
                    QueueMessage(args.ServiceName, $"Invalid input. {dyMacroLenErrorMsg}");
                }
                else
                {
                    QueueMessage(args.ServiceName, $"Invalid input. {inputSequence.Error} {dyMacroLenErrorMsg}");
                }
                
                return;
            }

            QueueMessage(args.ServiceName, $"Total length: {inputSequence.TotalDuration}ms");
        }
    }
}
