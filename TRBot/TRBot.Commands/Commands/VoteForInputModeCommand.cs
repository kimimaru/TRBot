﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;
using TRBot.Permissions;
using TRBot.Routines;

namespace TRBot.Commands
{
    /// <summary>
    /// Votes for a new input mode to take over.
    /// </summary>
    public sealed class VoteForInputModeCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"input mode (string/int)\"";
        private string CachedInputModesStr = string.Empty;
        
        public VoteForInputModeCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            //Show all input modes
            InputModes[] inputModeArr = EnumUtility.GetValues<InputModes>.EnumValues;

            for (int i = 0; i < inputModeArr.Length; i++)
            {
                InputModes inputMode = inputModeArr[i];

                CachedInputModesStr += inputMode.ToString();

                if (i < (inputModeArr.Length - 1))
                {
                    CachedInputModesStr += ", ";
                }
            }
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            long curInputMode = DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE, 0L);
            InputModes inpMode = (InputModes)curInputMode;

            //Check the number of votes
            if (arguments.Count == 0)
            {
                InputModeVoteRoutine inpModeVoteRoutine = RoutineHandler.FindRoutine(RoutineConstants.INPUT_MODE_VOTE_ROUTINE_NAME) as InputModeVoteRoutine;

                if (inpModeVoteRoutine == null)
                {
                    QueueMessage(args.ServiceName, $"There is no voting in progress for a new input mode. To start one up, pass a mode as an argument: {CachedInputModesStr}");
                }
                else
                {
                    StringBuilder stringBuilder = new StringBuilder(128);

                    //Show the number of votes for each mode
                    Dictionary<InputModes, long> curVotes = inpModeVoteRoutine.GetVotesPerMode();
                    foreach (KeyValuePair<InputModes, long> kvPair in curVotes)
                    {
                        stringBuilder.Append(kvPair.Key.ToString()).Append(' ').Append('=');
                        stringBuilder.Append(' ').Append(kvPair.Value).Append(' ').Append('|').Append(' ');
                    }

                    stringBuilder.Remove(stringBuilder.Length - 3, 3);
                    stringBuilder.Append(". To vote for a new input mode, pass one as an argument: ").Append(CachedInputModesStr);

                    QueueMessage(args.ServiceName, stringBuilder.ToString(), "| ");
                }

                return;
            }

            //Invalid number of arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string userExternalID = args.Command.UserMessage.UserExternalID;
            bool canStartInputModeVote = false;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has the ability to vote
                User user = context.GetUserByExternalID(userExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.VOTE_INPUT_MODE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You don't have the ability to vote for a new input mode!");
                    return;
                }

                //Check if the user can start a vote if the need arises
                canStartInputModeVote = (user != null && user.HasEnabledAbility(PermissionConstants.START_VOTE_INPUT_MODE_ABILITY));
            }

            //Check if the cooldown is up
            DateTime nowUTC = DateTime.UtcNow;

            //Check the last completed time
            string lastComplete = DatabaseMngr.GetSettingString(SettingsConstants.INPUT_MODE_NEXT_VOTE_DATE, DateTime.UnixEpoch.ToConsistentString());
            if (DateTime.TryParse(lastComplete, out DateTime lastCompleteDate) == false)
            {
                lastCompleteDate = DateTime.UnixEpoch;
                Logger.Warning($"Failed to parse DateTime: {lastCompleteDate.ToConsistentString()}");
            }

            if (nowUTC < lastCompleteDate)
            {
                QueueMessage(args.ServiceName, $"Input mode voting is on cooldown until {lastCompleteDate}");
                return;
            }

            string inputModeStr = arguments[0];

            //Parse
            if (EnumUtility.TryParseEnumValue(inputModeStr, out InputModes parsedInputMode) == false)
            {
                QueueMessage(args.ServiceName, $"Please enter a valid input mode: {CachedInputModesStr}");
                return;
            }

            bool commencedNewVote = false;

            //Get the routine
            InputModeVoteRoutine inputModeVoteRoutine = RoutineHandler.FindRoutine(RoutineConstants.INPUT_MODE_VOTE_ROUTINE_NAME) as InputModeVoteRoutine;
            
            //Add the routine if it doesn't exist
            if (inputModeVoteRoutine == null)
            {
                //Deny if the user doesn't have permission to start it up
                if (canStartInputModeVote == false)
                {
                    QueueMessage(args.ServiceName, "You don't have the ability to start a vote for a new input mode!");
                    return;
                }

                long voteDur = DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE_VOTE_TIME, 60000L);
                inputModeVoteRoutine = new InputModeVoteRoutine(RoutineConstants.INPUT_MODE_VOTE_ROUTINE_NAME, voteDur);
                RoutineHandler.AddRoutine(RoutineConstants.INPUT_MODE_VOTE_ROUTINE_NAME, inputModeVoteRoutine);

                commencedNewVote = true;
            }
            
            //Check for tallying
            if (inputModeVoteRoutine.TallyingCommenced == true)
            {
                QueueMessage(args.ServiceName, "Too late! Voting has ended and tallying has already begun!");
                return;
            }

            string userName = args.Command.UserMessage.Username;

            //Add the vote
            inputModeVoteRoutine.AddModeVote(userExternalID, userName, args.ServiceName, parsedInputMode, out bool voteChanged);

            if (commencedNewVote == false)
            {
                if (voteChanged == true)
                {
                    QueueMessage(args.ServiceName, $"{userName} changed their vote to {parsedInputMode}!");
                }
                else
                {
                    QueueMessage(args.ServiceName, $"{userName} voted for {parsedInputMode}!");
                }
            }
            else
            {
                QueueMessage(args.ServiceName, $"Voting for changing the input mode has begun! {userName} voted for {parsedInputMode}!");
            }
        }
    }
}
