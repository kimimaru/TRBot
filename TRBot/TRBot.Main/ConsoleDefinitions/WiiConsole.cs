﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Input.VirtualControllers;
using TRBot.Input.Consoles;

namespace TRBot.Main
{
    /// <summary>
    /// The Nintendo Wii.
    /// </summary>
    public sealed class WiiConsole : GameConsole
    {
        /// <summary>
        /// The various extensions for the Wii.
        /// </summary>
        /// <remarks>Some peripherals, such as the Wii Balance Board, currently cannot be emulated at all (see Dolphin wiki).</remarks>
        public enum WiiInputExtensions
        {
            None = 0,
            Nunchuk = 1,
            ClassicController = 2,
            Guitar = 3,
            DrumKit = 4,
            DJTurntable = 5,
            uDrawGameTablet = 6,
            DrawsomeTablet = 7,
            TaikoDrum = 8
        }

        /// <summary>
        /// The current input mode for the Wii.
        /// </summary>
        public WiiInputExtensions InputExtension = WiiInputExtensions.Nunchuk;

        public WiiConsole()
        {
            Name = "wii";

            Initialize();
        }

        private void Initialize()
        {
            SetConsoleInputs(new Dictionary<string, InputData>(40) {
                { "left",       ConsoleHelpers.CreateAxisInput("left", (int)GlobalAxisVals.AXIS_X, 0.5d, 0d, 0.5d) },
                { "right",      ConsoleHelpers.CreateAxisInput("right", (int)GlobalAxisVals.AXIS_X, 0.5d, 1d, 0.5d) },
                { "up",         ConsoleHelpers.CreateAxisInput("up", (int)GlobalAxisVals.AXIS_Y, 0.5d, 0d, 0.5d) },
                { "down",       ConsoleHelpers.CreateAxisInput("down", (int)GlobalAxisVals.AXIS_Y, 0.5d, 1d, 0.5d) },
                { "tleft",      ConsoleHelpers.CreateAxisInput("tleft", (int)GlobalAxisVals.AXIS_RX, 0.5d, 0d, 0.5d) },
                { "tright",     ConsoleHelpers.CreateAxisInput("tright", (int)GlobalAxisVals.AXIS_RX, 0.5d, 1d, 0.5d) },
                { "tforward",   ConsoleHelpers.CreateAxisInput("tforward", (int)GlobalAxisVals.AXIS_RY, 0.5d, 0d, 0.5d) },
                { "tback",      ConsoleHelpers.CreateAxisInput("tback", (int)GlobalAxisVals.AXIS_RY, 0.5d, 1d, 0.5d) },
                { "pleft",      ConsoleHelpers.CreateAxisInput("pleft", (int)GlobalAxisVals.AXIS_RZ, 0.5d, 0d, 0.5d) },
                { "pright",     ConsoleHelpers.CreateAxisInput("pright", (int)GlobalAxisVals.AXIS_RZ, 0.5d, 1d, 0.5d) },
                { "pup",        ConsoleHelpers.CreateAxisInput("pup", (int)GlobalAxisVals.AXIS_Z, 0.5d, 0d, 0.5d) },
                { "pdown",      ConsoleHelpers.CreateAxisInput("pdown", (int)GlobalAxisVals.AXIS_Z, 0.5d, 1d, 0.5d) },
                
                { "dleft",      ConsoleHelpers.CreateButtonInput("dleft", (int)GlobalButtonVals.BTN1) },
                { "dright",     ConsoleHelpers.CreateButtonInput("dright", (int)GlobalButtonVals.BTN2) },
                { "dup",        ConsoleHelpers.CreateButtonInput("dup", (int)GlobalButtonVals.BTN3) },
                { "ddown",      ConsoleHelpers.CreateButtonInput("ddown", (int)GlobalButtonVals.BTN4) },              
                { "a",          ConsoleHelpers.CreateButtonInput("a", (int)GlobalButtonVals.BTN9) },
                { "b",          ConsoleHelpers.CreateButtonInput("b", (int)GlobalButtonVals.BTN10) },
                { "one",        ConsoleHelpers.CreateButtonInput("one", (int)GlobalButtonVals.BTN11) },
                { "two",        ConsoleHelpers.CreateButtonInput("two", (int)GlobalButtonVals.BTN12) },
                { "minus",      ConsoleHelpers.CreateButtonInput("minus", (int)GlobalButtonVals.BTN13) },
                { "plus",       ConsoleHelpers.CreateButtonInput("plus", (int)GlobalButtonVals.BTN14) },
                { "c",          ConsoleHelpers.CreateButtonInput("c", (int)GlobalButtonVals.BTN15) },
                { "z",          ConsoleHelpers.CreateButtonInput("z", (int)GlobalButtonVals.BTN16) },  
                { "shake",      ConsoleHelpers.CreateButtonInput("shake", (int)GlobalButtonVals.BTN17) },
                { "point",      ConsoleHelpers.CreateButtonInput("point", (int)GlobalButtonVals.BTN18) },
                { "ss1",        ConsoleHelpers.CreateButtonInput("ss1", (int)GlobalButtonVals.BTN21) },
                { "ss2",        ConsoleHelpers.CreateButtonInput("ss2", (int)GlobalButtonVals.BTN22) },
                { "ss3",        ConsoleHelpers.CreateButtonInput("ss3", (int)GlobalButtonVals.BTN23) },
                { "ss4",        ConsoleHelpers.CreateButtonInput("ss4", (int)GlobalButtonVals.BTN24) },
                { "ss5",        ConsoleHelpers.CreateButtonInput("ss5", (int)GlobalButtonVals.BTN25) },
                { "ss6",        ConsoleHelpers.CreateButtonInput("ss6", (int)GlobalButtonVals.BTN26) },
                { "ls1",        ConsoleHelpers.CreateButtonInput("ls1", (int)GlobalButtonVals.BTN27) },
                { "ls2",        ConsoleHelpers.CreateButtonInput("ls2", (int)GlobalButtonVals.BTN28) },
                { "ls3",        ConsoleHelpers.CreateButtonInput("ls3", (int)GlobalButtonVals.BTN29) },
                { "ls4",        ConsoleHelpers.CreateButtonInput("ls4", (int)GlobalButtonVals.BTN30) },
                { "ls5",        ConsoleHelpers.CreateButtonInput("ls5", (int)GlobalButtonVals.BTN31) },
                { "ls6",        ConsoleHelpers.CreateButtonInput("ls6", (int)GlobalButtonVals.BTN32) },
                { "#",          ConsoleHelpers.CreateBlankInput("#") },
                { ".",          ConsoleHelpers.CreateBlankInput(".") }
            });

            InvalidInputCombos = new List<InvalidInputCombo>();
        }
    }
}
