﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Sets the maximum input duration.
    /// </summary>
    public sealed class MaxInputDurCommand : BaseCommand
    {
        private string UsageMessage = "Usage: no arguments (get value). \"duration (int)\" (set value)";

        public MaxInputDurCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int maxInputDur = (int)DatabaseMngr.GetSettingInt(SettingsConstants.MAX_INPUT_DURATION, 60000L);

            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The maximum duration of an input sequence is {maxInputDur} milliseconds!");
                return;
            }

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has this ability
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user == null)
                {
                    QueueMessage(args.ServiceName, "Somehow, the user calling this is not in the database.");
                    return;
                }

                if (user.HasEnabledAbility(PermissionConstants.SET_MAX_INPUT_DUR_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the global max input duration!");
                    return;
                }
            }

            if (StringNumParser.TryParseInt(arguments[0], out int newMaxDur) == false)
            {
                QueueMessage(args.ServiceName, "Please enter a valid number!");
                return;
            }

            if (newMaxDur < 0)
            {
                QueueMessage(args.ServiceName, "Cannot set a negative duration!");
                return;
            }

            if (newMaxDur == maxInputDur)
            {
                QueueMessage(args.ServiceName, "The duration is already this value!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings maxDurSetting = context.GetSetting(SettingsConstants.MAX_INPUT_DURATION);
                maxDurSetting.ValueInt = newMaxDur;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Set the maximum input sequence duration to {newMaxDur} milliseconds!");
        }
    }
}
