﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// Enables or disables teams mode.
    /// </summary>
    public sealed class GetSetTeamsModeCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"true\" or \"false\"";

        public GetSetTeamsModeCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (arguments.Count == 0)
            {
                long teamsModeVal = DatabaseMngr.GetSettingInt(SettingsConstants.TEAMS_MODE_ENABLED, 0L);
                string enabledStr = (teamsModeVal <= 0) ? "disabled" : "enabled";

                QueueMessage(args.ServiceName, $"Teams mode is currently {enabledStr}. To change the teams mode state, pass either \"true\" or \"false\" as an argument."); 
                return;
            }

            //Check for sufficient permissions
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
                if (user == null || user.HasEnabledAbility(PermissionConstants.SET_TEAMS_MODE_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set teams mode!");
                    return;
                }
            }

            string newStateStr = arguments[0].ToLowerInvariant();

            if (bool.TryParse(newStateStr, out bool newState) == false)
            {
                QueueMessage(args.ServiceName, "Invalid state argument. To change the teams mode state, pass either \"true\" or \"false\" as an argument.");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings teamsMode = context.GetSetting(SettingsConstants.TEAMS_MODE_ENABLED);
                if (teamsMode == null)
                {
                    teamsMode = new Settings(SettingsConstants.TEAMS_MODE_ENABLED, string.Empty, 0L);
                    context.SettingCollection.Add(teamsMode);
                }

                teamsMode.ValueInt = (newState == true) ? 1L : 0L;

                context.SaveChanges();
            }

            if (newState == true)
            {
                QueueMessage(args.ServiceName, $"Enabled teams mode! New users will be assigned different controller ports based on the \"{SettingsConstants.TEAMS_MODE_NEXT_PORT}\" and \"{SettingsConstants.TEAMS_MODE_MAX_PORT}\" settings.");
            }
            else
            {
                QueueMessage(args.ServiceName, "Disabled teams mode. New users will be assigned controller port 1.");
            }
        }
    }
}
