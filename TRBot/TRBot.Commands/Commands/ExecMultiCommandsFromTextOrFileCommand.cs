﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Connection;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that allows executing multiple commands from text or a file with each line as a separate command.
    /// </summary>
    public sealed class ExecMultiCommandsFromTextOrFileCommand : BaseCommand
    {
        public ExecMultiCommandsFromTextOrFileCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            if (string.IsNullOrEmpty(args.Command.ArgumentsAsString) == false)
            {
                QueueMessage(args.ServiceName, $"This command takes no arguments. The text or file to read must be defined in the {nameof(ValueStr)} for this command for security reasons.");
                return;
            }

            string filePath = ValueStr;

            //Make sure the value is valid
            if (string.IsNullOrWhiteSpace(filePath) == true)
            {
                QueueMessage(args.ServiceName, $"{nameof(ValueStr)} is null, empty, or only whitespace. Define a value with the commands to execute in separate lines, or a path to a file.");
                return;
            }

            //First try to read the file as an absolute path
            string[] cmdLines = FileHelpers.ReadLinesFromTextFile(filePath);
            
            bool absoluteFileFound = cmdLines.Length != 0;

            //If that wasn't found, try a relative path
            if (absoluteFileFound == false)
            {
                cmdLines = FileHelpers.ReadLinesFromTextFile(FolderPathResolver.DataFolderPath, filePath);
            }

            bool relativeFileFound = cmdLines.Length != 0;

            //No file was found, so split the string by newline
            if (relativeFileFound == false)
            {
                cmdLines = filePath.Split(System.Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

                if (cmdLines.Length == 0)
                {
                    QueueMessage(args.ServiceName, $"File not found or unreadable at the specified path, or no command text is defined by the {nameof(ValueStr)} of this command. Double check the path or value of {nameof(ValueStr)}.");
                    return;
                }
            }

            //For each line, dispatch an event to execute a command
            for (int i = 0; i < cmdLines.Length; i++)
            {
                string line = cmdLines[i];
                string cmdLineWsAsSpace = Helpers.ReplaceAllWhitespaceWithSpace(line);

                //Empty line; ignore
                if (string.IsNullOrWhiteSpace(cmdLineWsAsSpace) == true)
                {
                    continue;
                }

                //Construct command event data using information of the user who invoked this command 
                EvtUserMsgData existingMsgData = args.Command.UserMessage;
                EvtUserMsgData newMsgData = new EvtUserMsgData(existingMsgData.UserExternalID,
                    existingMsgData.Username, cmdLineWsAsSpace);

                EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(cmdLineWsAsSpace,
                    args.Command.CommandIdentifier, newMsgData);

                //Failed to parse, move onto the next line
                if (chatCmdData == null)
                {
                    Logger.Information($"Line {i} - \"{cmdLineWsAsSpace}\" could not be parsed as a valid command.");
                    continue;
                }

                //Construct the command event to be dispatched
                EvtChatCommandArgs evtChatCommandArgs = new EvtChatCommandArgs();
                evtChatCommandArgs.ServiceName = args.ServiceName;
                evtChatCommandArgs.Command = chatCmdData;

                EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, evtChatCommandArgs);
            }
        }
    }
}
