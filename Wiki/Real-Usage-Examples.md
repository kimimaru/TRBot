TRBot is being used by various streamers:
- [Type2Play](https://www.twitch.tv/type2play) (stream by the developers of TRBot)
- [TwitchPlaysSpeedruns](https://www.twitch.tv/twitchplaysspeedruns)
- [Nickadabra](https://www.twitch.tv/nickaabra)
- [TheChatPlaysGames](https://www.twitch.tv/thechatplaysgames)
- [BraceYourselfGames](https://www.twitch.tv/braceyourselfgames/) (the developers behind Crypt of the NecroDancer!)
- [f1ashko](https://www.twitch.tv/f1ashko)

Here are some feats performed with TRBot:

* **Twitch Plays Crypt of the NecroDancer**
  * https://www.twitch.tv/braceyourselfgames/clip/SmoggyTangibleChoughEleGiggle
* **Completing Puzzle (Maze Burrow)**
  * https://www.twitch.tv/kimimaru4000/clip/SavoryBraveSandwichJebaited
* **Gnorc Cove Big Skip (Spyro the Dragon)**
  * https://www.twitch.tv/twitchplaysspeedruns/clip/FurtiveIcySwallowLitty
* **Prison 2 Speedrun Sub-57 (Katana ZERO)**
  * https://clips.twitch.tv/ExquisiteDeterminedBubbleteaKeyboardCat-X_nurQac_VsYqfjG
* **Boba Skip (Metal Gear Solid)**
  * https://youtube.com/watch?v=rdOSNKHhvBQ
* **New Guard Manipulation Glitch (Metal Gear Solid)**
  * https://twitter.com/MGSrunners/status/1431503754256388098#m
* **City Escape Skip (Sonic Adventure 2: Battle)**
  * https://www.twitch.tv/type2play/clip/BashfulGlutenFreeYogurtMau5-RSOmyobdkY3Sy8Py
* **Fast Egg Golem Fight (Sonic Adventure 2: Battle)**
  * https://clips.twitch.tv/LovelyAmazingSwanNerfRedBlaster-t3isGJyk3prN0EA6
* **Allegro Clip (Rayman)**
  * https://www.twitch.tv/type2play/clip/ResourcefulPrettiestGnatCopyThis-oROCSMTT0OqTpZVi

Have an awesome clip or way you've seen TRBot used? Comment [here](https://codeberg.org/kimimaru/TRBot/issues/89) or submit a PR to add to this list!
