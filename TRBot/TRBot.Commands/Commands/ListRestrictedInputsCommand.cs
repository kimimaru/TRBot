﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using System.Text;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that lists a user's restricted inputs.
    /// </summary>
    public sealed class ListRestrictedInputsCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"username (optional)\"";

        public ListRestrictedInputsCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            //Get this user if an argument wasn't given, otherwise get the user specified
            string userExternalID = args.Command.UserMessage.UserExternalID;

            if (argCount > 0)
            {
                userExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(arguments[0], args.ServiceName);
            }

            StringBuilder strBuilder = null;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User restrictedUser = context.GetUserByExternalID(userExternalID);

                //Check for the user
                if (restrictedUser == null)
                {
                    QueueMessage(args.ServiceName, "A user with this name does not exist in the database!");
                    return;
                }

                //Find all unexpired restricted inputs
                IEnumerable<RestrictedInput> restrictedInputs = restrictedUser.RestrictedInputs.Where(r => r.HasExpired == false);

                int restrictedInputCount = restrictedInputs.Count();

                //Check for no restricted inputs
                if (restrictedInputCount == 0)
                {
                    QueueMessage(args.ServiceName, $"{restrictedUser.Name} has no restricted inputs!");
                    return;
                }

                strBuilder = new StringBuilder(restrictedInputCount * 8);
                strBuilder.Append("Restricted inputs for ").Append(restrictedUser.Name).Append(':').Append(' ');

                foreach(RestrictedInput resInp in restrictedInputs)
                {
                    strBuilder.Append(resInp.inputData.Name).Append(" (\"").Append(resInp.inputData.Console.Name).Append("\" console)");

                    if (resInp.HasExpiration == true)
                    {
                        strBuilder.Append(" (exp: ").Append(resInp.Expiration.Value.ToString()).Append(" UTC)");
                    }

                    strBuilder.Append(',').Append(' ');
                }
            }

            strBuilder.Remove(strBuilder.Length - 2, 2);

            QueueMessage(args.ServiceName, strBuilder.ToString(), ", ");
        }
    }
}
