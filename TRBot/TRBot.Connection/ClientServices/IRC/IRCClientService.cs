﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Threading.Tasks;
using TRBot.Utilities.Logging;
using TRBot.Events;
using NetIRC;
using NetIRC.Connection;
using NetIRC.Messages;

namespace TRBot.Connection.IRC
{
    /// <summary>
    /// Read inputs from an IRC channel.
    /// </summary>
    public class IRCClientService : IClientService
    {
        private const string LOG_PREFIX_STRING = "[" + ClientServiceNames.IRC_SERVICE + "] ";

        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        public bool IsInitialized => Initialized;

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        public bool IsConnected => IrcClient != null && ConnectedToServer == true;

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        public bool CanSendMessages => IsConnected == true && IrcClient.Channels?.Count > 0;

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        public IBotMessageHandler MsgHandler { get; private set; } = null;

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        public IMessageSettings MsgSettings { get; private set; } = null;

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        public ICommandSettings CmdSettings { get; private set; } = null;

        private bool Initialized = false;
        private bool Connecting = false;
        private bool ConnectedToServer = false;

        private IRCLoginSettings LoginSettings = null;

        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        private NetIRC.User IrcUser = null;
        private Client IrcClient = null;
        private TcpClientConnection TcpConnection = null;

        private readonly string IrcServiceName = ClientServiceNames.IRC_SERVICE;

        public IRCClientService(IRCLoginSettings loginSettings,
            ITRBotLogger logger, IEventDispatcher evtDispatcher, IBotMessageHandler msgHandler,
            IMessageSettings msgSettings, ICommandSettings cmdSettings)
        {
            LoginSettings = loginSettings;

            Logger = logger;
            EvtDispatcher = evtDispatcher;
            MsgHandler = msgHandler;
            MsgSettings = msgSettings;
            CmdSettings = cmdSettings;
        }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        public void Initialize()
        {
            IrcUser = new NetIRC.User(LoginSettings.BotName, LoginSettings.BotName);
            TcpConnection = new TcpClientConnection(LoginSettings.ServerName, LoginSettings.ServerPort);

            IrcClient = new Client(IrcUser, LoginSettings.BotPassword, TcpConnection);

            IrcClient.IRCMessageParsed -= OnIRCMessageParsed;
            IrcClient.IRCMessageParsed += OnIRCMessageParsed;

            IrcClient.RegistrationCompleted -= OnWelcomedToServer;
            IrcClient.RegistrationCompleted += OnWelcomedToServer;
            
            IrcClient.Channels.CollectionChanged -= OnChannelCollectionChanged;
            IrcClient.Channels.CollectionChanged += OnChannelCollectionChanged;

            TcpConnection.Disconnected -= OnConnectionDisconnected;
            TcpConnection.Disconnected += OnConnectionDisconnected; 

            MsgHandler.SetClientService(this);

            EvtDispatcher.RegisterEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.RegisterEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.RegisterEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.RegisterEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.RegisterEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.RegisterEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND);
            EvtDispatcher.RegisterEvent<IRCJoinedChannelArgs>(IRCServiceEventNames.IRC_JOINED_CHANNEL);

            EvtDispatcher.AddListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);

            Initialized = true;
        }

        /// <summary>
        /// Connects the client.
        /// </summary>
        public void Connect()
        {
            AwaitConnected().ConfigureAwait(false);
        }

        private async Task AwaitConnected()
        {
            if (Connecting == true)
            {
                return;
            }

            Connecting = true;
            
            await IrcClient.ConnectAsync();
            
            Connecting = false;

            Logger.Verbose("Finished connecting to IRC server!");
        }

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        public void Disconnect()
        {
            if (IrcClient == null || ConnectedToServer == false)
            {
                Logger.Error("Attempting to disconnect from IRC while not connected!");
                return;
            }

            //Send a quit message
            IrcClient.SendAsync(new QuitMessage(string.Empty));
        }

        /// <summary>
        /// Send a message through the client.
        /// </summary>
        public void ProcessAndSendMessage(IServiceMessage serviceMessage)
        {
            if (string.IsNullOrEmpty(serviceMessage.Message) == true)
            {
                Logger.Warning($"Empty message in {nameof(ProcessAndSendMessage)}!");
                return;
            }

            string sentMessage = serviceMessage.Message;

            if (serviceMessage.OverridePrefix == true)
            {
                sentMessage = serviceMessage.CustomPrefix + serviceMessage.Message;
            }
            else if (string.IsNullOrEmpty(MsgSettings.MessagePrefix) == false)
            {
                sentMessage = MsgSettings.MessagePrefix + serviceMessage.Message;
            }

            //Send to the channel by default
            string recipient = LoginSettings.ChannelName;

            //Send to a specific user if it's a whisper message
            if (serviceMessage is WhisperMessage whisperMessage)
            {
                recipient = whisperMessage.RecipientName;
            }

            //Send the message
            IrcClient.SendAsync(new PrivMsgMessage(recipient, sentMessage));

            if (MsgSettings.LogToLogger == true)
            {
                Logger.Log(serviceMessage.LogLevel, LOG_PREFIX_STRING + sentMessage);
            }

            var outgoingArgs = new EvtOutgoingBotMessageArgs()
            {
                ServiceName = IrcServiceName,
                UserMessage = new EvtUserMsgData(string.Empty, IrcUser.Nick, sentMessage)
            };

            EvtDispatcher.DispatchEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE, outgoingArgs);
        }

        public void Dispose()
        {
            Disconnect();

            MsgHandler.CleanUp();

            if (IrcClient != null)
            {
                if (IrcClient.Channels?.Count > 0)
                {
                    //Invoke the disconnected event manually since there isn't enough time to receive the server message
                    //after sending the QUIT message on the client
                    //If the client is still in a channel, it's safe to say they'll be disconnected
                    //This is a hack to ensure the disconnected event is fired when removing the service
                    //Work to find a better way to do this
                    EvtDisconnectedArgs disconnectedArgs = new EvtDisconnectedArgs()
                    {
                        ServiceName = IrcServiceName
                    };
    
                    EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disconnectedArgs);
                }

                IrcClient.IRCMessageParsed -= OnIRCMessageParsed;
                IrcClient.RegistrationCompleted -= OnWelcomedToServer;
                IrcClient.Channels.CollectionChanged -= OnChannelCollectionChanged;

                IrcClient.Dispose();
            }
            
            if (TcpConnection != null)
            {
                TcpConnection.Disconnected -= OnConnectionDisconnected;
                TcpConnection.Dispose();
            }

            IrcClient = null;
            TcpConnection = null;
            IrcUser = null;

            ConnectedToServer = false;

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.COMMAND);
            EvtDispatcher.UnregisterEvent(IRCServiceEventNames.IRC_JOINED_CHANNEL);

            EvtDispatcher.RemoveListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnConnected);
        }

        private void OnWelcomedToServer(object sender, EventArgs e)
        {
            EvtConnectedArgs connectedArgs = new EvtConnectedArgs()
            {
                ServiceName = IrcServiceName,
                BotUsername = LoginSettings.BotName
            };

            EvtDispatcher.DispatchEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, connectedArgs);
            
            ConnectedToServer = true;
        }

        private void OnChannelCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //Joined channel
            if ((e.OldItems == null || e.OldItems.Count == 0) && e.NewItems?.Count > 0)
            {
                IRCJoinedChannelArgs ircJoinedChannelArgs = new IRCJoinedChannelArgs()
                {
                    ChannelName = LoginSettings.ChannelName,
                };

                EvtDispatcher.DispatchEvent<IRCJoinedChannelArgs>(IRCServiceEventNames.IRC_JOINED_CHANNEL, ircJoinedChannelArgs);
            }
            //Left channel and/or disconnected
            else if (e.NewItems == null || e.NewItems.Count == 0)
            {
                EvtDisconnectedArgs disconnectedArgs = new EvtDisconnectedArgs()
                {
                    ServiceName = IrcServiceName
                };

                EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disconnectedArgs);
            }
        }

        private void OnIRCMessageParsed(Client client, ParsedIRCMessage ircMessage)
        {
            //If it's a connection error, we're disconnected
            if (IsConnectionError(ircMessage.NumericReply) == true)
            {
                EvtConnectionErrorArgs connectionErrorArgs = new EvtConnectionErrorArgs()
                {
                    Error = new EvtErrorData($"Error connecting to {LoginSettings.ServerName}. CMD = {ircMessage.Command} | NUMERIC = {ircMessage.NumericReply} | IRC COMMAND = {ircMessage.IRCCommand}"),
                    ServiceName = IrcServiceName,
                    BotUsername = LoginSettings.BotName
                };

                EvtDispatcher.DispatchEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, connectionErrorArgs);
                ConnectedToServer = false;
            }
            else
            {
                //Handle messages to the channel
                if (ircMessage.IRCCommand == IRCCommand.PRIVMSG)
                {
                    HandlePRIVMSG(client, ircMessage);
                }
            }

            Logger.Verbose(ircMessage.ToString());
        }

        private void HandlePRIVMSG(Client client, ParsedIRCMessage ircMessage)
        {
            //Maybe there's a better way of doing this, but I'm not a fan of how NetIRC handles registering message handlers
            //There's no way to unregister and it's based on either an assembly and type name
            try
            {
                string lineWSReplaced = Utilities.Helpers.ReplaceAllWhitespaceWithSpace(ircMessage.Trailing);

                string userName = ircMessage.Prefix.From;

                //Send message event
                EvtUserMessageArgs umArgs = new EvtUserMessageArgs()
                {
                    ServiceName = IrcServiceName,
                    UserMessage = new EvtUserMsgData(ircMessage.Prefix.Raw, userName, lineWSReplaced)
                };

                EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, umArgs);

                //Parse command
                EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(lineWSReplaced,
                    CmdSettings.CommandIdentifier, new EvtUserMsgData(ircMessage.Prefix.Raw, userName, lineWSReplaced));
                
                if (chatCmdData != null)
                {
                    EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                    {
                        ServiceName = IrcServiceName,
                        Command = chatCmdData
                    };

                    EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
                }
            }
            catch (Exception e)
            {
                Logger.Error($"Error encountered in IRC service. {e.Message}");
            }
        }

        private void OnConnected(EvtConnectedArgs e)
        {
            if (e.ServiceName != IrcServiceName)
            {
                return;
            }

            IrcClient.SendAsync(new JoinMessage(LoginSettings.ChannelName));
        }

        private void OnConnectionDisconnected(object sender, EventArgs e)
        {
            ConnectedToServer = false;
            
            EvtDisconnectedArgs disconnectedArgs = new EvtDisconnectedArgs()
            {
                ServiceName = IrcServiceName
            };

            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disconnectedArgs);
        }

        private bool IsConnectionError(in IRCNumericReply ircNumericReply)
        {
            switch (ircNumericReply)
            {
                case IRCNumericReply.ERR_NICKNAMEINUSE:
                case IRCNumericReply.ERR_YOUREBANNEDCREEP:
                case IRCNumericReply.ERR_NOLOGIN:
                case IRCNumericReply.ERR_NOSUCHNICK:
                case IRCNumericReply.ERR_NOSUCHSERVER:
                case IRCNumericReply.ERR_BANNEDFROMCHAN:
                    return true;
                default:
                    return false;
            }
        }
    }
}
