﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection;

namespace TRBot.Commands
{
    /// <summary>
    /// States how many input sequences are currently running.
    /// </summary>
    public class NumRunningInputsCommand : BaseCommand
    {
        public NumRunningInputsCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            int curInputsRunning = InputHndlr.RunningInputCount;

            string message = string.Empty;

            //Account for singular and plural
            if (curInputsRunning == 1)
            {
                message = "There is 1 input sequence currently running!";
            }
            else
            {
                message = $"There are {curInputsRunning} input sequences currently running!";
            }

            QueueMessage(args.ServiceName, message);
        }
    }
}
