# Setting up virtual controllers for inputs
## GNU/Linux
Make sure the `uinput` kernel module is enabled with `sudo modprobe uinput`. Add permissions to read and write in `/dev/uinput` with `sudo chmod a+rw /dev/uinput`. TRBot creates and manages the `uinput` virtual controllers on GNU/Linux automatically, thus there is nothing to install.

If the native code fails to run on your distro, please follow [these instructions](./Building.md#uinput-gnu-linux) for building it on your machine.

## Windows
TRBot uses vJoy on Windows to send inputs to the game. vJoy is a separate component that needs to be installed.

1. Install [vJoy](https://sourceforge.net/projects/vjoystick/). Click on "Download Latest Version" and run the setup file.
2. After installation, you'll need to configure vJoy. This step needs to be done only once per controller.
3. Run a newly installed program called "Configure vJoy". Refer to the image below on how to set it up. Each tab is a separate virtual controller that must be configured. A few notes:
   - Check the "Enable vJoy" checkbox on the lower left to activate the virtual controller.
   - Make sure that at least 32 buttons are mapped on each virtual controller to ensure enough inputs are available. Keep in mind is some games and applications may not accept more than 32 buttons.

![vJoy configuration for virtual controller 1 with 32 buttons](./Images/Setup/ConfigurevJoy.png)

## Testing Virtual Controller Configuration
At this point, the virtual controllers should be set up and ready to use. Run TRBot and check the console window to see if it's able to acquire those virtual controllers. 

Look for the following series of messages:
```
Setting up virtual controller manager...
Acquired uinput device ID 0 at index 0!
Set up virtual controller manager uinput and acquired 1 controllers!
```
**Note:** On Windows, it'll mention vJoy instead of uinput.

Everything looking okay? Good, now it's time to test that the inputs work!

**Note:** The default console for TRBot is NES, which has no analog inputs. To test analog inputs, switch to a console with them, such as GameCube, with "!console gc". This requires a TRBot access level of Moderator (30) to switch the console (see the [initial tutorial](./Setup-Init.md#connecting)).

### Suggested method

#### **GNU/Linux**
1. Install `jstest-gtk` and run TRBot.
2. Open `jstest-gtk`. You should see a virtual controller titled "TRBot Joypad 0" with 32 buttons and 8 axes (see below).
3. Click on "TRBot Joypad 0" then "Properties" to open another window showing the inputs.
4. In TRBot, type buttons such as "a", "b", "up1s", and "down3s". If you see buttons or axes being pressed, you're all set!

![TRBot Joypad 0 virtual controller in jstest-gtk](./Images/Setup/TRBot_jstest-gtk.png)

#### **Windows**
1. Search for "Game Controllers" and open the game controller application.
2. Select the vJoy controller and click on "Properties".
3. Click on the "Test" tab. You should see a window like in the image below.
4. In TRBot, type buttons such as "a", "b", "up1s", and "down3s". If you see buttons or axes being pressed, you're all set! Note that the game controller window needs to be focused to update pressed inputs.

![Testing vJoy controllers inputs](https://www.howtogeek.com/wp-content/uploads/2016/02/htx22.png)

### Alternative Method
Another way to test inputs is on the [Dolphin](https://dolphin-emu.org/) emulator. Set the mappings in the emulator using text inputs through TRBot. After setting each mapping, you can test it out by typing the same input again. For vJoy, the *"vJoy Feeder (Demo)"* program can also be used to press buttons and axes on the virtual controllers. If everything is configured, you'll see buttons being pressed and axes being moved in Dolphin's controller configuration screen after typing the inputs in TRBot.

### Debugging issues
One common problem with debugging virtual controllers is that some games and emulators or their input mapping screens don't accept background input.

A simple way to handle this is delay the input you want to map so you have time to focus the window before it gets pressed. This is simpler than it sounds, and we'll use an example scenario to explain:
- We want to map the "a" button to the virtual controller in mGBA. Unfortunately, mGBA's input window doesn't accept background input. It's not possible to keep the mGBA window focused while typing an input in the TRBot window to map it.
- Let's delay the "a" button in TRBot by two seconds by typing and entering `#2s a`. Before two seconds pass, we click on the "a" button in mGBA's input window to prepare it for mapping.
- Behold! TRBot pressed "a" on the virtual controller while the mGBA window was focused. The input is mapped, allowing us to control the "a" button in mGBA with TRBot!

# Optional next step - Taking TRBot further with more tools
Congratulations, TRBot is all set up! At this point, players should be capable of playing games through chat!

If you'd like to tweak the input values for your game, see the [input data](./Adding-ConsolesInputs.md) documentation to learn more.

Want to take things even further? Check the [next section](./Setup-Misc.md) for additional settings, tools for displaying chat on stream, and more!
