﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TRBot.Data.Migrations
{
    public partial class CustomMsgsDisplayRanksEnabledState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Enabled",
                schema: "userdisplayranks",
                table: "UserDisplayRanks",
                type: "INTEGER",
                nullable: false,
                defaultValue: 1L);

            migrationBuilder.AddColumn<long>(
                name: "Enabled",
                schema: "custommessages",
                table: "CustomMessages",
                type: "INTEGER",
                nullable: false,
                defaultValue: 1L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "userdisplayranks",
                table: "UserDisplayRanks");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "custommessages",
                table: "CustomMessages");
        }
    }
}
