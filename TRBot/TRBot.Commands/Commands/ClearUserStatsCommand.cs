﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that clears a user's stats.
    /// </summary>
    public class ClearUserStatsCommand : BaseCommand
    {
        private const string CONFIRM_CLEAR_STR = "yes";
        private const string CONFIRM_STOP_STR = "no";

        //Track which users are attempting to clear their stats
        //This gives them an additional confirmation
        private List<string> ConfirmClearedStatsUsers = new List<string>(8);

        public ClearUserStatsCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            string creditsName = DatabaseMngr.GetCreditsName();

            string userExternalID = args.Command.UserMessage.UserExternalID;
            string userName = args.Command.UserMessage.Username;

            User user = DatabaseMngr.GetUserByExternalID(userExternalID);
            
            if (user == null)
            {
                QueueMessage(args.ServiceName, "Invalid or non-existent user.");
                return;
            }

            if (ConfirmClearedStatsUsers.Contains(userName) == false)
            {
                QueueMessage(args.ServiceName, $"WARNING {userName}: this clears all miscellaneous user stats, such as {creditsName.Pluralize(0)}, message/input counts, recent inputs, and simulate data. If you're sure, retype this command with \"{CONFIRM_CLEAR_STR}\" as an argument to clear or \"{CONFIRM_STOP_STR}\" to decline.");
                ConfirmClearedStatsUsers.Add(userName);
                return;
            }
        
            //Check for an argument
            List<string> arguments = args.Command.ArgumentsAsList;
            
            //Only accept the exact argument
            if (arguments.Count != 1)
            {
                QueueMessage(args.ServiceName, $"If you're sure you want to clear your stats, retype this command with \"{CONFIRM_CLEAR_STR}\" as an argument to clear or \"{CONFIRM_STOP_STR}\" to decline.");
                return;
            }
            
            string confirmation = arguments[0];
            
            //Confirm - clear stats
            if (confirmation == CONFIRM_CLEAR_STR)
            {
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    user = context.GetUserByExternalID(userExternalID);

                    //Clear stats and save
                    user.RecentInputs.Clear();

                    user.Stats.Credits = 0L;
                    user.Stats.TotalMessageCount = 0L;
                    user.Stats.ValidInputCount = 0L;
                    user.Stats.SimulateHistory = string.Empty;

                    context.SaveChanges();
                }

                ConfirmClearedStatsUsers.Remove(userName);

                QueueMessage(args.ServiceName, "Successfully cleared stats!");
            }
            //Ignore
            else if (confirmation == CONFIRM_STOP_STR)
            {
                ConfirmClearedStatsUsers.Remove(userName);
                
                QueueMessage(args.ServiceName, "Cancelled clearing stats!");
            }
        }
    }
}
