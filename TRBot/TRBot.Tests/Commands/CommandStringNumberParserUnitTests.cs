/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;
using TRBot.Commands;

namespace TRBot.Tests.Commands
{
    [TestFixture]
    public class CommandStringNumberParserUnitTests
    {
        private static CultureInfo DefaultCulture = null;
        private static CultureInfo CultureCommaDecSeprtr = null;

        //Static constructor is required to work around these instances being passed to null
        //in all test case sources even before SetUp is called in NUnit
        static CommandStringNumberParserUnitTests()
        {
            DefaultCulture = CultureInfo.InvariantCulture;

            //This creates a culture with commas as a decimal separator
            CultureCommaDecSeprtr = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            CultureCommaDecSeprtr.NumberFormat = (NumberFormatInfo)CultureCommaDecSeprtr.NumberFormat;
            CultureCommaDecSeprtr.NumberFormat.NumberDecimalSeparator = ",";
        }

        private static IEnumerable<TestCaseData> TryParseIntTestCases()
        {
            yield return new TestCaseData(DefaultCulture, "1", true, 1);
            yield return new TestCaseData(DefaultCulture, "0", true, 0);
            yield return new TestCaseData(DefaultCulture, "-1", true, -1);
            yield return new TestCaseData(DefaultCulture, "-2147483648", true, -2147483648);
            yield return new TestCaseData(DefaultCulture, "2147483647", true, 2147483647);
            yield return new TestCaseData(DefaultCulture, "t3stN0tNumb3r", false, 0);
        }

        private static IEnumerable<TestCaseData> TryParseIntCommaCultureTestCases()
        {
            yield return new TestCaseData(CultureCommaDecSeprtr, "1", true, 1);
            yield return new TestCaseData(CultureCommaDecSeprtr, "0", true, 0);
            yield return new TestCaseData(CultureCommaDecSeprtr, "-1", true, -1);
            yield return new TestCaseData(CultureCommaDecSeprtr, "-2147483648", true, -2147483648);
            yield return new TestCaseData(CultureCommaDecSeprtr, "2147483647", true, 2147483647);
            yield return new TestCaseData(CultureCommaDecSeprtr, "t3stN0tNumb3r", false, 0);
        }

        [TestCaseSource(nameof(TryParseIntCommaCultureTestCases))]
        [TestCaseSource(nameof(TryParseIntTestCases))]
        public void TryParseIntParsesCorrectly(CultureInfo currentCulture, string numberString,
            bool expectedReturnValue, int expectedNumber)
        {
            CommandStringNumberParser cmdStrNumParser = new CommandStringNumberParser(currentCulture);

            bool returnVal = cmdStrNumParser.TryParseInt(numberString, out int value);

            Assert.That(returnVal, Is.EqualTo(expectedReturnValue));
            Assert.That(value, Is.EqualTo(expectedNumber));
        }

        private static IEnumerable<TestCaseData> TryParseFloatTestCases()
        {
            yield return new TestCaseData(DefaultCulture, "1", true, 1f);
            yield return new TestCaseData(DefaultCulture, "0", true, 0f);
            yield return new TestCaseData(DefaultCulture, "-1", true, -1f);
            yield return new TestCaseData(DefaultCulture, "2.327", true, 2.327f);
            yield return new TestCaseData(DefaultCulture, "1,1", false, 0);
            yield return new TestCaseData(DefaultCulture, "t3stN0tNumb3r", false, 0);
        }

        private static IEnumerable<TestCaseData> TryParseFloatCommaCultureTestCases()
        {
            yield return new TestCaseData(CultureCommaDecSeprtr, "1", true, 1f);
            yield return new TestCaseData(CultureCommaDecSeprtr, "0", true, 0f);
            yield return new TestCaseData(CultureCommaDecSeprtr, "-1", true, -1f);
            yield return new TestCaseData(CultureCommaDecSeprtr, "2,327", true, 2.327f);
            yield return new TestCaseData(CultureCommaDecSeprtr, "800,8", true, 800.8f);
            yield return new TestCaseData(CultureCommaDecSeprtr, "1.1", false, 0);
            yield return new TestCaseData(CultureCommaDecSeprtr, "t3stN0tNumb3r", false, 0);
        }

        [TestCaseSource(nameof(TryParseFloatCommaCultureTestCases))]
        [TestCaseSource(nameof(TryParseFloatTestCases))]
        public void TryParseFloatParsesCorrectly(CultureInfo currentCulture, string numberString,
            bool expectedReturnValue, float expectedNumber)
        {
            CommandStringNumberParser cmdStrNumParser = new CommandStringNumberParser(currentCulture);

            bool returnVal = cmdStrNumParser.TryParseFloat(numberString, out float value);

            Assert.That(returnVal, Is.EqualTo(expectedReturnValue));
            Assert.That(value, Is.EqualTo(expectedNumber));
        }

        private static IEnumerable<TestCaseData> TryParseDoubleTestCases()
        {
            yield return new TestCaseData(DefaultCulture, "1", true, 1d);
            yield return new TestCaseData(DefaultCulture, "0", true, 0d);
            yield return new TestCaseData(DefaultCulture, "-1", true, -1d);
            yield return new TestCaseData(DefaultCulture, "2.327", true, 2.327d);
            yield return new TestCaseData(DefaultCulture, "1,1", false, 0);
            yield return new TestCaseData(DefaultCulture, "t3stN0tNumb3r", false, 0);
        }

        private static IEnumerable<TestCaseData> TryParseDoubleCommaCultureTestCases()
        {
            yield return new TestCaseData(CultureCommaDecSeprtr, "1", true, 1d);
            yield return new TestCaseData(CultureCommaDecSeprtr, "0", true, 0d);
            yield return new TestCaseData(CultureCommaDecSeprtr, "-1", true, -1d);
            yield return new TestCaseData(CultureCommaDecSeprtr, "2,327", true, 2.327d);
            yield return new TestCaseData(CultureCommaDecSeprtr, "800,8", true, 800.8d);
            yield return new TestCaseData(CultureCommaDecSeprtr, "1.1", false, 0);
            yield return new TestCaseData(CultureCommaDecSeprtr, "t3stN0tNumb3r", false, 0);
        }

        [TestCaseSource(nameof(TryParseDoubleCommaCultureTestCases))]
        [TestCaseSource(nameof(TryParseDoubleTestCases))]
        public void TryParseDoubleParsesCorrectly(CultureInfo currentCulture, string numberString,
            bool expectedReturnValue, double expectedNumber)
        {
            CommandStringNumberParser cmdStrNumParser = new CommandStringNumberParser(currentCulture);

            bool returnVal = cmdStrNumParser.TryParseDouble(numberString, out double value);

            Assert.That(returnVal, Is.EqualTo(expectedReturnValue));
            Assert.That(value, Is.EqualTo(expectedNumber));
        }
    }
}
