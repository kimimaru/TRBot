﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that gets or sets an invalid input combo's valid level threshold for a console.
    /// </summary>
    public sealed class GetSetInvalidInputComboLevelCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"console name (string)\", \"invalid input combo name (string)\", \"permission level (string/int) (optional for get)\"";

        public GetSetInvalidInputComboLevelCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with invalid arguments
            if (argCount < 2 || argCount > 3)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string consoleStr = arguments[0].ToLowerInvariant();
            string invalidInputComboName = arguments[1].ToLowerInvariant();
            string permLevelName = argCount == 2 ? null : arguments[2].ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }

                InvalidInputCombo invalidInputCombo = console.InvalidInputCombos.FirstOrDefault(combo => combo.Name == invalidInputComboName);

                if (invalidInputCombo == null)
                {
                    QueueMessage(args.ServiceName, $"No invalid input combo named \"{invalidInputComboName}\" exists for the \"{console.Name}\" console!");
                    return;
                }

                //Output the value
                if (argCount == 2)
                {
                    if (PermissionHelpers.TryParsePermissionLevel(invalidInputCombo.ValidLevelThreshold.ToString(),
                        out PermissionLevels permLevel) == true)
                    {
                        QueueMessage(args.ServiceName,
                            $"The invalid input combo \"{invalidInputCombo.Name}\" is valid for levels {(long)permLevel} ({permLevel.ToString()}) and up!");
                    }
                    else
                    {
                        QueueMessage(args.ServiceName, $"The invalid input combo \"{invalidInputCombo.Name}\" is invalid for everyone!");
                    }

                    return;
                }

                //Get permission level
                if (PermissionHelpers.TryParsePermissionLevel(permLevelName,
                    out PermissionLevels permLvl) == true)
                {
                    invalidInputCombo.ValidLevelThreshold = (long)permLvl;

                    QueueMessage(args.ServiceName,
                        $"Set the level threshold for the \"{invalidInputCombo.Name}\" invalid input combo to {(long)permLvl} ({permLvl.ToString()}) and up!");
                }
                else
                {
                    //If it's invalid, simply set it to an empty string
                    //This makes the input combo invalid for everyone regardless of level
                    invalidInputCombo.ValidLevelThreshold = 0L;

                    QueueMessage(args.ServiceName,
                        $"Removed the level threshold for the \"{invalidInputCombo.Name}\" invalid input combo. This invalid input combo is now invalid regardless of user level.");
                }
                
                context.SaveChanges();
            }
        }
    }
}
