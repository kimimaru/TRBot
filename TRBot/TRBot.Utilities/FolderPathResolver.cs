﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;

namespace TRBot.Utilities
{
    /// <summary>
    /// Helps resolve folder paths.
    /// </summary>
    public class FolderPathResolver : IFolderPathResolver
    {
        /// <summary>
        /// The path to the root folder.
        /// </summary>
        public string RootFolderPath { get; } = string.Empty;

        /// <summary>
        /// The path to the data folder.
        /// </summary>
        public string DataFolderPath { get; } = string.Empty;

        /// <summary>
        /// The path to the logs folder.
        /// </summary>
        public string LogsFolderPath { get; } = string.Empty;

        /// <summary>
        /// The path to the crashlogs folder.
        /// </summary>
        public string CrashLogsFolderPath { get; } = string.Empty;

        public FolderPathResolver(string rootFolderPath, string dataFolderPath,
            string logsFolderPath, string crashLogsFolderPath)
        {
            RootFolderPath = rootFolderPath;
            DataFolderPath = dataFolderPath;
            LogsFolderPath = logsFolderPath;
            CrashLogsFolderPath = crashLogsFolderPath;
        }
    }
}
