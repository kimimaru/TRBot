﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using TRBot.Connection;
using TRBot.CustomCode;
using TRBot.Data;
using TRBot.Routines;
using TRBot.Input;
using TRBot.Utilities.Logging;
using TRBot.Input.VirtualControllers;
using TRBot.Events;
using TRBot.Utilities;
using TRBot.WebSocket;

namespace TRBot.Commands
{
    /// <summary>
    /// Base class for a command.
    /// </summary>
    public abstract class BaseCommand
    {
        public bool Enabled = true;
        public string ClassName = string.Empty;
        public bool DisplayInHelp = true;
        public long Level = 0;
        public CommandCategories CommandCategory = CommandCategories.None;
        public string ValueStr = string.Empty;

        public ICommandHandler CmdHandler { get; protected set; }= null;
        public IRoutineHandler RoutineHandler { get; protected set; } = null;
        public ITRBotLogger Logger { get; protected set; } = null;
        public IFolderPathResolver FolderPathResolver { get; protected set; } = null;
        public ICustomCodeHandler CustomCodeHandler { get; protected set; } = null;
        public IDatabaseManager<BotDBContext> DatabaseMngr { get; protected set; } = null;
        public IClientServiceManager ClientServiceMngr { get; protected set; } = null;
        public IVirtualControllerContainer VControllerContainer { get; protected set; } = null;
        public IEventDispatcher EvtDispatcher { get; protected set; } = null;
        public IWebSocketManager WebSocketMngr { get; protected set; } = null;
        public IInputHandler InputHndlr { get; protected set; } = null;
        public IStringNumberParser StringNumParser { get; protected set; } = null;

        public BaseCommand()
        {
            
        }

        /// <summary>
        /// Sets required data for many commands to function.
        /// </summary>
        public void SetRequiredData(ICommandHandler cmdHandler, IRoutineHandler routineHandler,
            ITRBotLogger logger, IFolderPathResolver folderPathResolver,
            ICustomCodeHandler customCodeHandler,
            IDatabaseManager<BotDBContext> databaseMngr, IClientServiceManager clientServiceMngr,
            IVirtualControllerContainer vControllerContainer,
            IEventDispatcher evtDispatcher, IWebSocketManager webSocketMngr, IInputHandler inputHandlr,
            IStringNumberParser stringNumParser)
        {
            CmdHandler = cmdHandler;
            RoutineHandler = routineHandler;
            Logger = logger;
            FolderPathResolver = folderPathResolver;
            CustomCodeHandler = customCodeHandler;
            DatabaseMngr = databaseMngr;
            ClientServiceMngr = clientServiceMngr;
            VControllerContainer = vControllerContainer;
            EvtDispatcher = evtDispatcher;
            WebSocketMngr = webSocketMngr;
            InputHndlr = inputHandlr;
            StringNumParser = stringNumParser;
        }

        public virtual void Initialize()
        {
            
        }

        public virtual void CleanUp()
        {
            
        }

        public abstract void ExecuteCommand(EvtChatCommandArgs args);

        protected void QueueMessage(string serviceName, string message)
        {
            IClientService service = ClientServiceMngr.GetClientService(serviceName);
            service.MsgHandler.QueueMessage(message);
        }

        protected void QueueMessage(string serviceName, string message, in Serilog.Events.LogEventLevel logLevel)
        {
            QueueMessage(serviceName, message, string.Empty, logLevel);
        }

        protected void QueueMessage(string serviceName, string message, string msgSplitSeparator, in Serilog.Events.LogEventLevel logLevel = Serilog.Events.LogEventLevel.Information)
        {
            IClientService service = ClientServiceMngr.GetClientService(serviceName);
            service.MsgHandler.QueueMessage(message, msgSplitSeparator, logLevel);
        }
    }
}
