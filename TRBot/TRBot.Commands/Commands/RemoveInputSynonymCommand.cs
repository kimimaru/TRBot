﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;
using TRBot.Permissions;
using TRBot.Data;

namespace TRBot.Commands
{
    public class RemoveInputSynonymCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"console name\" \"synonymName\"";

        public RemoveInputSynonymCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.REMOVE_INPUT_SYNONYM_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to remove input synonyms.");
                    return;
                }
            }

            string consoleName = arguments[0].ToLowerInvariant();
            long consoleID = 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleName);

                //Check if a valid console is specified
                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"\"{consoleName}\" is not a valid console.");
                    return;
                }

                consoleID = console.ID;
            }

            string synonymName = arguments[1].ToLowerInvariant();

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                InputSynonym inputSynonym = context.InputSynonyms.FirstOrDefault(syn => syn.ConsoleID == consoleID && syn.SynonymName == synonymName);

                if (inputSynonym == null)
                {
                    QueueMessage(args.ServiceName, $"No input synonym \"{synonymName}\" exists for console {consoleName}.");
                    return;
                }

                context.InputSynonyms.Remove(inputSynonym);
                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Successfully removed input synonym \"{synonymName}\"!");
        }
    }
}
