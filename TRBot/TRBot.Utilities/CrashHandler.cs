﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using TRBot.Utilities.Logging;

namespace TRBot.Utilities
{
    /// <summary>
    /// Handles crashes through unhandled exceptions.
    /// </summary>
    public class CrashHandler
    {
        private readonly ITRBotLogger Logger = null;
        private readonly IFolderPathResolver FolderPathResolver = null;

        private readonly string GitVersion = string.Empty;
        private readonly string GitCommitHash = string.Empty;
        private readonly string GitCommitDate = string.Empty;

        public CrashHandler(ITRBotLogger logger, IFolderPathResolver folderPathResolver,
            string gitVersion, string gitCommitHash, string gitCommitDate)
        {
            Logger = logger;
            FolderPathResolver = folderPathResolver;

            GitVersion = gitVersion;
            GitCommitHash = gitCommitHash;
            GitCommitDate = gitCommitDate;

            AppDomain.CurrentDomain.UnhandledException -= HandleCrash;
            AppDomain.CurrentDomain.UnhandledException += HandleCrash;
        }

        ~CrashHandler()
        {
            if (AppDomain.CurrentDomain != null)
            {
                AppDomain.CurrentDomain.UnhandledException -= HandleCrash;
            }
        }

        /// <summary>
        /// A crash handler for unhandled exceptions.
        /// </summary>
        /// <param name="sender">The source of the unhandled exception event.</param>
        /// <param name="e">An UnhandledExceptionEventArgs that contains the event data.</param>
        private void HandleCrash(object sender, UnhandledExceptionEventArgs e)
        {
            //Get the exception object
            Exception exc = e.ExceptionObject as Exception;
            if (exc != null)
            {
                string crashLogPath = DebugUtility.GetCrashLogPath(FolderPathResolver);

                //Create the directory to the crash log path
                if (Directory.Exists(crashLogPath) == false)
                {
                    Directory.CreateDirectory(crashLogPath);
                }

                //Dump the message, stack trace, and logs to a file
                using (StreamWriter writer = File.CreateText(DebugUtility.GetCrashLogFilename(FolderPathResolver, GitVersion)))
                {
                    string versionText = $"Version: {GitVersion} - commit {GitCommitHash} on {GitCommitDate}\n\n";
                    string message = $"Message: {exc.Message} ({exc.GetType().Name})\n\nStack Trace:\n";
                    string trace = $"{exc.StackTrace}\n\n";

                    writer.Write(versionText);
                    writer.Write(message);
                    writer.Write(trace);

                    writer.Flush();

                    Logger?.Fatal($"CRASH: {exc.Message}");
                    Logger?.Fatal(exc.StackTrace);
                }
            }
        }
    }
}
