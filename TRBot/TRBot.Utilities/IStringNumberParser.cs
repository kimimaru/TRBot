﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Utilities
{
    /// <summary>
    /// An interface for parsing numbers from strings.
    /// </summary>
    public interface IStringNumberParser
    {
        /// <summary>
        /// Parses an integer from a string, returning a value indicating the success of the parsing.
        /// </summary>
        /// <param name="numberString">A string representing an integer number.</param>
        /// <param name="number">The returned integer parsed from the string. A default value if parsing failed.</param>
        /// <returns>true if parsing of the string succeeds, otherwise false.</returns>
        bool TryParseInt(string numberString, out int number);

        /// <summary>
        /// Parses a float from a string, returning a value indicating the success of the parsing.
        /// </summary>
        /// <param name="numberString">A string representing a single precision floating-point number.</param>
        /// <param name="number">The returned float parsed from the string. A default value if parsing failed.</param>
        /// <returns>true if parsing of the string succeeds, otherwise false.</returns>
        bool TryParseFloat(string numberString, out float number);

        /// <summary>
        /// Parses a double from a string, returning a value indicating the success of the parsing.
        /// </summary>
        /// <param name="numberString">A string representing a double precision floating-point number.</param>
        /// <param name="number">The returned double parsed from the string. A default value if parsing failed.</param>
        /// <returns>true if parsing of the string succeeds, otherwise false.</returns>
        bool TryParseDouble(string numberString, out double number);
    }
}
