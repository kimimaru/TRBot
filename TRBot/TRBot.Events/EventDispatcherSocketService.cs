/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using WebSocketSharp;
using TRBot.Utilities.Logging;
using TRBot.WebSocket;

namespace TRBot.Events
{
    /// <summary>
    /// WebSocketBehavior for the EventDispatcher.
    /// </summary>
    public class EventDispatcherSocketService : WebSocketService
    {
        private IEventDispatcher EvtDispatcher = null;
        private ITRBotLogger Logger = null;

        public EventDispatcherSocketService()
        {
            
        }

        public void Initialize(IEventDispatcher evtDispatcher, ITRBotLogger logger)
        {
            EvtDispatcher = evtDispatcher;
            Logger = logger;
        }

        protected override void OnServiceMessage(MessageEventArgs e)
        {
            EvtJsonArgs jsonArgs = new EvtJsonArgs(e.Data);

            //Avoid resending this event over the network if the implementation does so
            EvtDispatcher.DispatchEventWithoutExtraFeatures<EvtJsonArgs>(
                WebSocketEventNames.RECEIVED_EVENT_JSON, jsonArgs);
        }
    }
}