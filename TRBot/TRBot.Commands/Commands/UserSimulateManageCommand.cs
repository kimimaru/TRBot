﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Commands
{
    /// <summary>
    /// Helps a user manage their simulate data by checking and clearing it.
    /// </summary>
    public sealed class UserSimulateManageCommand : BaseCommand
    {
        private const string CLEAR_ARG = "clear";

        private readonly string UsageMessage = $"Usage: (\"{CLEAR_ARG}\") (optional)";
        private readonly string UsageMessage2 = $"Pass \"{CLEAR_ARG}\" as an argument to clear your simulate data.";

        public UserSimulateManageCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string thisUserExternalID = args.Command.UserMessage.UserExternalID;

            if (arguments.Count == 1)
            {
                string firstArg = arguments[0].ToLowerInvariant();

                //Check for clear
                if (firstArg != CLEAR_ARG)
                {
                    QueueMessage(args.ServiceName, UsageMessage);
                    return;
                }

                using (var context = DatabaseMngr.OpenContext())
                {
                    User user = context.GetUserByExternalID(thisUserExternalID);

                    if (user == null)
                    {
                        QueueMessage(args.ServiceName, "You're not in the database!");
                        return;
                    }

                    user.Stats.SimulateHistory = string.Empty;
                    context.SaveChanges();
                }

                QueueMessage(args.ServiceName, "Cleared all simulate data!");
                return;
            }

            int simulateLength = 0;

            using (var context = DatabaseMngr.OpenContext())
            {
                User thisUser = context.Users
                    .AsNoTrackingWithIdentityResolution()
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == thisUserExternalID);
                
                if (thisUser == null)
                {
                    QueueMessage(args.ServiceName, "You're not in the database!");
                    return;
                }

                string simHistory = thisUser.Stats.SimulateHistory;
                if (string.IsNullOrEmpty(simHistory) == false)
                {
                    simulateLength = thisUser.Stats.SimulateHistory.Length;
                }
            }

            //No arguments - display information
            if (arguments.Count == 0)
            {
                long maxSimulateLength = DatabaseMngr.GetSettingInt(SettingsConstants.MAX_USER_SIMULATE_STRING_LENGTH, 30000L);

                QueueMessage(args.ServiceName, $"Your simulate data is {simulateLength} / {maxSimulateLength} characters. {UsageMessage2}");
                return;
            }
        }
    }
}
