﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/
using System;

namespace TRBot.Connection
{
    /// <summary>
    /// Handles sending messages with rate limiting.
    /// </summary>
    public interface IBotMessageHandler
    {
        IClientService ClientService { get; }

        /// <summary>
        /// Whether messages should be queued for sending or not.
        /// If disabled, the message handler won't send new messages.
        /// </summary>
        bool MessageQueuingEnabled { get; }

        /// <summary>
        /// How many messages are in the queue.
        /// </summary>
        int ClientMessageCount { get; }

        /// <summary>
        /// The message throttler that restricts how often messages are sent.
        /// </summary>
        IBotMessageThrottler MessageThrottler { get; }

        /// <summary>
        /// The current message throttling option.
        /// </summary>
        MessageThrottlingOptions CurThrottleOption { get; }

        /// <summary>
        /// The character limit for bot messages. Bot messages longer than this value will be split into separate messages.
        /// <summary>
        int BotMessageCharLimit { get; set; }

        void CleanUp();

        /// <summary>
        /// Toggles messages to be queued on the message handler. If turned off, the message handler should not queue up new messages.
        /// </summary>
        /// <param name="enabled">If true, enables message queuing, otherwise disables it.</param>
        void ToggleMessageQueueing(bool enabled);

        void SetMessageThrottling(in MessageThrottlingOptions msgThrottleOption, in MessageThrottleData messageThrottleData);

        void SetClientService(IClientService clientService);

        /// <summary>
        /// Sends the next queued message through the client service. This returns false if this fails.
        /// </summary>
        /// <returns>true if the message was successfully sent. false if the client service is disconnected or the message fails to send.</returns>
        bool SendNextQueuedMessage();

        void QueueMessage(IServiceMessage serviceMessage, string msgSplitSeparator = "");
        
        void QueueMessage(string message, string msgSplitSeparator = "", in Serilog.Events.LogEventLevel logLevel = Serilog.Events.LogEventLevel.Information);
    }
}
