﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that changes the periodic input port.
    /// </summary>
    public sealed class GetSetPeriodicInputPortCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - no arguments (get value) or \"controller port (int)\"";

        public GetSetPeriodicInputPortCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //Ignore with too few arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            if (arguments.Count == 0)
            {
                long periodicInputPort = DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_PORT, 0L);

                QueueMessage(args.ServiceName, $"The default port for periodic inputs is currently {periodicInputPort + 1}. To change the periodic input port, pass a controller port as an argument."); 
                return;
            }

            //Check for sufficient permissions
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
                if (user == null || user.HasEnabledAbility(PermissionConstants.SET_PERIODIC_INPUT_PORT_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the default periodic input port!");
                    return;
                }
            }

            string portValStr = arguments[0].ToLowerInvariant();

            if (StringNumParser.TryParseInt(portValStr, out int newPort) == false)
            {
                QueueMessage(args.ServiceName, "That's not a valid number!");
                return;
            }

            int controllerCount = VControllerContainer.VControllerMngr.ControllerCount;

            if (newPort <= 0 || newPort > controllerCount)
            {
                QueueMessage(args.ServiceName, $"Please specify a number in the range of 1 through the current controller count ({controllerCount}).");
                return;
            }

            //Change to zero-based index for referencing
            int controllerNum = newPort - 1;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                Settings periodicInputPort = context.GetSetting(SettingsConstants.PERIODIC_INPUT_PORT);
                if (periodicInputPort == null)
                {
                    periodicInputPort = new Settings(SettingsConstants.PERIODIC_INPUT_PORT, string.Empty, 0L);
                    context.SettingCollection.Add(periodicInputPort);
                }

                periodicInputPort.ValueInt = controllerNum;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Set the default periodic input controller port to {newPort}!"); 
        }
    }
}
