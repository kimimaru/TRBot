﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Utilities.Logging;
using TRBot.Events;
using TwitchLib.Client;
using TwitchLib.Client.Models;
using TwitchLib.Client.Events;
using TwitchLib.Communication.Events;
using TwitchLib.PubSub.Events;

namespace TRBot.Connection.Twitch
{
    /// <summary>
    /// Twitch client interaction.
    /// </summary>
    public class TwitchClientService : IClientService
    {
        private const string LOG_PREFIX_STRING = "[" + ClientServiceNames.TWITCH_SERVICE + "] ";

        private TwitchClient twitchClient = null;
        //private TwitchPubSub twitchPubSub = null;

        private ConnectionCredentials Credentials = null;
        private string ChannelName = string.Empty;
        private bool AutoRelistenOnExceptions = true;

        /// <summary>
        /// Tells if the client is initialized.
        /// </summary>
        public bool IsInitialized => (twitchClient?.IsInitialized == true);

        /// <summary>
        /// Tells if the client is connected.
        /// </summary>
        public bool IsConnected => (twitchClient?.IsConnected == true);

        /// <summary>
        /// Whether the client is able to send messages.
        /// </summary>
        public bool CanSendMessages => (IsConnected == true && JoinedChannels?.Count > 0);

        /// <summary>
        /// The message handler for the client.
        /// </summary>
        public IBotMessageHandler MsgHandler { get; private set; } = null;

        /// <summary>
        /// Message settings for the service.
        /// </summary>
        public IMessageSettings MsgSettings { get; private set; } = null;

        /// <summary>
        /// Command settings for the service.
        /// </summary>
        public ICommandSettings CmdSettings { get; private set; } = null;

        /// <summary>
        /// The channels the client has joined.
        /// This exists to see the joined channel list without repeated allocations obtaining it from the client.
        /// </summary>
        public IReadOnlyList<JoinedChannel> JoinedChannels { get; private set; } = null;

        private ITRBotLogger Logger = null;
        private IEventDispatcher EvtDispatcher = null;

        private readonly string TwitchServiceName = ClientServiceNames.TWITCH_SERVICE;

        public TwitchClientService(ConnectionCredentials credentials, string channelName,
            in bool autoRelistenOnExceptions, ITRBotLogger logger, IEventDispatcher evtDispatcher,
            IBotMessageHandler msgHandler, IMessageSettings msgSettings, ICommandSettings cmdSettings)
        {
            twitchClient = new TwitchClient();
            //twitchPubSub = new TwitchPubSub();

            Credentials = credentials;
            ChannelName = channelName;
            AutoRelistenOnExceptions = autoRelistenOnExceptions;
            Logger = logger;
            EvtDispatcher = evtDispatcher;
            MsgHandler = msgHandler;
            MsgSettings = msgSettings;
            CmdSettings = cmdSettings;
        }

        /// <summary>
        /// Initializes the client.
        /// </summary>
        public void Initialize()
        {
            //Set a null character for chat commands since we're not using TwitchLib's in-built command handling
            twitchClient.Initialize(Credentials, ChannelName, '\0', '\0', AutoRelistenOnExceptions);

            twitchClient.OnMessageSent -= OnMessageSent;
            twitchClient.OnMessageSent += OnMessageSent;

            twitchClient.OnMessageReceived -= OnMessageReceived;
            twitchClient.OnMessageReceived += OnMessageReceived;

            twitchClient.OnNewSubscriber -= OnNewSubscriber;
            twitchClient.OnNewSubscriber += OnNewSubscriber;
            
            twitchClient.OnReSubscriber -= OnReSubscriber;
            twitchClient.OnReSubscriber += OnReSubscriber;

            twitchClient.OnWhisperReceived -= OnWhisperReceived;
            twitchClient.OnWhisperReceived += OnWhisperReceived;

            twitchClient.OnWhisperCommandReceived -= OnWhisperCommandReceived;
            twitchClient.OnWhisperCommandReceived += OnWhisperCommandReceived;

            twitchClient.OnJoinedChannel -= OnJoinedChannel;
            twitchClient.OnJoinedChannel += OnJoinedChannel;

            twitchClient.OnLeftChannel -= OnLeftChannel;
            twitchClient.OnLeftChannel += OnLeftChannel;

            twitchClient.OnConnected -= OnConnected;
            twitchClient.OnConnected += OnConnected;

            twitchClient.OnConnectionError -= OnConnectionError;
            twitchClient.OnConnectionError += OnConnectionError;

            twitchClient.OnReconnected -= OnReconnected;
            twitchClient.OnReconnected += OnReconnected;

            twitchClient.OnDisconnected -= OnDisconnected;
            twitchClient.OnDisconnected += OnDisconnected;

            MsgHandler.SetClientService(this);

            EvtDispatcher.RegisterEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.RegisterEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.RegisterEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.RegisterEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND);
            EvtDispatcher.RegisterEvent<OnJoinedChannelArgs>(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL);
            EvtDispatcher.RegisterEvent<OnLeftChannelArgs>(TwitchServiceEventNames.TWITCH_LEFT_CHANNEL);

            EvtDispatcher.RegisterEvent<EvtReconnectedArgs>(ClientServiceEventNames.RECONNECTED);
            EvtDispatcher.RegisterEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.RegisterEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.RegisterEvent<OnWhisperReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_RECEIVED);
            EvtDispatcher.RegisterEvent<OnWhisperCommandReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_COMMAND_RECEIVED);
            EvtDispatcher.RegisterEvent<OnNewSubscriberArgs>(TwitchServiceEventNames.TWITCH_NEWSUBSCRIBER);
            EvtDispatcher.RegisterEvent<OnReSubscriberArgs>(TwitchServiceEventNames.TWITCH_RESUBSCRIBER);

            EvtDispatcher.AddListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnClientConnected);
            EvtDispatcher.AddListener<OnJoinedChannelArgs>(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL, OnClientJoinedChannel);
            EvtDispatcher.AddListener<OnLeftChannelArgs>(TwitchServiceEventNames.TWITCH_LEFT_CHANNEL, OnClientLeftChannel);

            //EvtDispatcher.RegisterEvent<EventArgs>(TwitchServiceEventNames.PUB_SUB_CONNECTED);
            //EvtDispatcher.RegisterEvent<EventArgs>(TwitchServiceEventNames.PUB_SUB_DISCONNECTED);
            //EvtDispatcher.RegisterEvent<OnPubSubServiceErrorArgs>(TwitchServiceEventNames.PUB_SUB_ERROR);
            //EvtDispatcher.RegisterEvent<OnFollowArgs>(TwitchServiceEventNames.FOLLOW);

            /*twitchPubSub.OnPubSubServiceConnected -= OnPubSubConnected;
            twitchPubSub.OnPubSubServiceConnected += OnPubSubConnected;

            twitchPubSub.OnPubSubServiceClosed -= OnPubSubDisconnected;
            twitchPubSub.OnPubSubServiceClosed += OnPubSubDisconnected;

            twitchPubSub.OnPubSubServiceError -= OnPubSubServiceError;
            twitchPubSub.OnPubSubServiceError += OnPubSubServiceError;

            twitchPubSub.OnFollow -= OnFollow;
            twitchPubSub.OnFollow += OnFollow;

            twitchPubSub.OnLog -= OnLog;
            twitchPubSub.OnLog += OnLog;

            twitchPubSub.ListenToFollows(ChannelName);
            twitchPubSub.ListenToChatModeratorActions(Credentials.TwitchUsername, ChannelName);*/
        }

        /// <summary>
        /// Connects the client.
        /// </summary>
        public void Connect()
        {
            if (twitchClient.IsConnected == true)
            {
                Logger.Warning("Attempting to connect while already connected!");
                return;
            }
            
            twitchClient.Connect();
            //twitchPubSub.Connect();
        }

        /// <summary>
        /// Disconnects the client.
        /// </summary>
        public void Disconnect()
        {
            if (twitchClient.IsConnected == false)
            {
                Logger.Warning("Attempting to disconnect from Twitch while not connected!");
                return;
            }

            twitchClient.Disconnect();
            //twitchPubSub.Disconnect();
        }

        /// <summary>
        /// Send a message through the client.
        /// </summary>
        public void ProcessAndSendMessage(IServiceMessage serviceMessage)
        {
            if (string.IsNullOrEmpty(serviceMessage.Message) == true)
            {
                Logger.Warning($"Empty message in {nameof(ProcessAndSendMessage)}!");
                return;
            }

            string sentMessage = serviceMessage.Message;

            if (serviceMessage.OverridePrefix == true)
            {
                sentMessage = serviceMessage.CustomPrefix + serviceMessage.Message;
            }
            else if (string.IsNullOrEmpty(MsgSettings.MessagePrefix) == false)
            {
                sentMessage = MsgSettings.MessagePrefix + serviceMessage.Message;
            }
            
            //Send a whisper if it's a whisper message
            if (serviceMessage is WhisperMessage whisperMessage)
            {
                twitchClient.SendWhisper(whisperMessage.RecipientName, sentMessage, false);
            }
            else
            {
                twitchClient.SendMessage(JoinedChannels[0], sentMessage);
            }

            if (MsgSettings.LogToLogger == true)
            {
                Logger.Log(serviceMessage.LogLevel, LOG_PREFIX_STRING + sentMessage);
            }
        }

        public void Dispose()
        {
            if (twitchClient.IsConnected == true)
                twitchClient.Disconnect();

            //twitchPubSub.Disconnect();

            //twitchPubSub.OnPubSubServiceConnected -= OnPubSubConnected;
            //twitchPubSub.OnPubSubServiceClosed -= OnPubSubDisconnected;
            //twitchPubSub.OnPubSubServiceError -= OnPubSubServiceError;
            //twitchPubSub.OnFollow -= OnFollow;
            //twitchPubSub.OnLog -= OnLog;

            MsgHandler.CleanUp();

            EvtDispatcher.RemoveListener<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, OnClientConnected);
            EvtDispatcher.RemoveListener<OnJoinedChannelArgs>(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL, OnClientJoinedChannel);
            EvtDispatcher.RemoveListener<OnLeftChannelArgs>(TwitchServiceEventNames.TWITCH_LEFT_CHANNEL, OnClientLeftChannel);

            twitchClient.OnMessageSent -= OnMessageSent;
            twitchClient.OnMessageReceived -= OnMessageReceived;
            twitchClient.OnNewSubscriber -= OnNewSubscriber;
            twitchClient.OnReSubscriber -= OnReSubscriber;
            twitchClient.OnWhisperReceived -= OnWhisperReceived;
            twitchClient.OnWhisperCommandReceived -= OnWhisperCommandReceived;
            twitchClient.OnJoinedChannel -= OnJoinedChannel;
            twitchClient.OnLeftChannel -= OnLeftChannel;
            twitchClient.OnConnected -= OnConnected;
            twitchClient.OnConnectionError -= OnConnectionError;
            twitchClient.OnReconnected -= OnReconnected;
            twitchClient.OnDisconnected -= OnDisconnected;

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.OUTGOING_BOT_MESSAGE);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.COMMAND);
            EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL);
            EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.TWITCH_LEFT_CHANNEL);

            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.RECONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.DISCONNECTED);
            EvtDispatcher.UnregisterEvent(ClientServiceEventNames.CONNECTION_ERROR);
            EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.TWITCH_WHISPER_RECEIVED);
            EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.TWITCH_WHISPER_COMMAND_RECEIVED);
            EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.TWITCH_NEWSUBSCRIBER);
            EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.TWITCH_RESUBSCRIBER);

            //EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.PUB_SUB_CONNECTED);
            //EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.PUB_SUB_DISCONNECTED);
            //EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.PUB_SUB_ERROR);
            //EvtDispatcher.UnregisterEvent(TwitchServiceEventNames.FOLLOW);

            JoinedChannels = null;
        }

        private void OnMessageSent(object sender, OnMessageSentArgs e)
        {
            EvtOutgoingBotMessageArgs args = new EvtOutgoingBotMessageArgs()
            {
                ServiceName = TwitchServiceName,
                UserMessage = new EvtUserMsgData(string.Empty, e.SentMessage.DisplayName, e.SentMessage.Message)
            };

            EvtDispatcher.DispatchEvent<EvtOutgoingBotMessageArgs>(ClientServiceEventNames.OUTGOING_BOT_MESSAGE, args);
        }

        //Break up much of the message handling by sending events
        private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            string lineWSReplaced = Utilities.Helpers.ReplaceAllWhitespaceWithSpace(e.ChatMessage.Message);

            EvtUserMessageArgs umArgs = new EvtUserMessageArgs()
            {
                ServiceName = TwitchServiceName,
                UserMessage = new EvtUserMsgData(e.ChatMessage.UserId, e.ChatMessage.DisplayName,
                    lineWSReplaced)
            };

            EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, umArgs);

            //Parse command
            EvtChatCommandData chatCmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(lineWSReplaced,
                CmdSettings.CommandIdentifier, new EvtUserMsgData(e.ChatMessage.UserId, e.ChatMessage.DisplayName, lineWSReplaced));
            
            if (chatCmdData != null)
            {
                EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                {
                    ServiceName = TwitchServiceName,
                    Command = chatCmdData
                };

                EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
            }
        }

        private void OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            EvtDispatcher.DispatchEvent<OnNewSubscriberArgs>(TwitchServiceEventNames.TWITCH_NEWSUBSCRIBER, e);
        }

        private void OnReSubscriber(object sender, OnReSubscriberArgs e)
        {
            EvtDispatcher.DispatchEvent<OnReSubscriberArgs>(TwitchServiceEventNames.TWITCH_RESUBSCRIBER, e);
        }

        private void OnWhisperReceived(object sender, OnWhisperReceivedArgs e)
        {
            EvtDispatcher.DispatchEvent<OnWhisperReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_RECEIVED, e);
        }

        private void OnWhisperCommandReceived(object sender, OnWhisperCommandReceivedArgs e)
        {
            EvtDispatcher.DispatchEvent<OnWhisperCommandReceivedArgs>(TwitchServiceEventNames.TWITCH_WHISPER_COMMAND_RECEIVED, e);
        }

        private void OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            EvtDispatcher.DispatchEvent<OnJoinedChannelArgs>(TwitchServiceEventNames.TWITCH_JOINED_CHANNEL, e);
        }

        private void OnLeftChannel(object sender, OnLeftChannelArgs e)
        {
            EvtDispatcher.DispatchEvent<OnLeftChannelArgs>(TwitchServiceEventNames.TWITCH_LEFT_CHANNEL, e);
        }

        private void OnConnected(object sender, OnConnectedArgs e)
        {
            EvtConnectedArgs connectedArgs = new EvtConnectedArgs()
            {
                ServiceName = TwitchServiceName,
                BotUsername = e.BotUsername
            };

            EvtDispatcher.DispatchEvent<EvtConnectedArgs>(ClientServiceEventNames.CONNECTED, connectedArgs);
        }

        private void OnConnectionError(object sender, OnConnectionErrorArgs e)
        {
            EvtConnectionErrorArgs cErrArgs = new EvtConnectionErrorArgs()
            {
                Error = new EvtErrorData(e.Error.Message),
                ServiceName = TwitchServiceName,
                BotUsername = e.BotUsername
            };

            EvtDispatcher.DispatchEvent<EvtConnectionErrorArgs>(ClientServiceEventNames.CONNECTION_ERROR, cErrArgs);
        }

        private void OnReconnected(object sender, OnReconnectedEventArgs e)
        {
            EvtReconnectedArgs recArgs = new EvtReconnectedArgs()
            {
                ServiceName = TwitchServiceName
            };

            EvtDispatcher.DispatchEvent<EvtReconnectedArgs>(ClientServiceEventNames.RECONNECTED, recArgs);
        }

        private void OnDisconnected(object sender, OnDisconnectedEventArgs e)
        {
            EvtDisconnectedArgs disArgs = new EvtDisconnectedArgs()
            {
                ServiceName = TwitchServiceName
            };

            EvtDispatcher.DispatchEvent<EvtDisconnectedArgs>(ClientServiceEventNames.DISCONNECTED, disArgs);
        }

        //This is a workaround for a TwitchLib regression that doesn't
        //automatically rejoin the channel on reconnection
        private void OnClientConnected(EvtConnectedArgs e)
        {
            if (e.ServiceName != TwitchServiceName)
            {
                return;
            }

            //Refresh joined channels
            PopulateJoinedChannels();

            //Join the channel after connecting
            twitchClient.JoinChannel(ChannelName);  
        }

        private void OnClientJoinedChannel(OnJoinedChannelArgs e)
        {
            //When joining a channel, update the joined channels list
            PopulateJoinedChannels();
        }

        private void OnClientLeftChannel(OnLeftChannelArgs e)
        {
            //Update the joined channels list
            PopulateJoinedChannels();
        }

        private void OnFollow(object sender, OnFollowArgs e)
        {
            EvtDispatcher.DispatchEvent<OnFollowArgs>(TwitchServiceEventNames.TWITCH_FOLLOW, e);
        }

        private void OnLog(object sender, TwitchLib.PubSub.Events.OnLogArgs e)
        {
            Console.WriteLine($"PubSub log: {e.Data}");
        }

        private void EmoteOnlyOn(object sender, TwitchLib.Client.Events.OnEmoteOnlyArgs e)
        {
            EvtDispatcher.DispatchEvent<TwitchLib.Client.Events.OnEmoteOnlyArgs>(TwitchServiceEventNames.TWITCH_EMOTE_ONLY_ON, e);
        }

        private void EmoteOnlyOff(object sender, OnEmoteOnlyOffArgs e)
        {
            EvtDispatcher.DispatchEvent<OnEmoteOnlyOffArgs>(TwitchServiceEventNames.TWITCH_EMOTE_ONLY_OFF, e);
        }

        private void OnPubSubConnected(object sender, EventArgs e)
        {
            //twitchPubSub.SendTopics(Credentials.TwitchOAuth);
            EvtDispatcher.DispatchEvent<EventArgs>(TwitchServiceEventNames.TWITCH_PUB_SUB_CONNECTED, e);
        }

        private void OnPubSubDisconnected(object sender, EventArgs e)
        {
            EvtDispatcher.DispatchEvent<EventArgs>(TwitchServiceEventNames.TWITCH_PUB_SUB_DISCONNECTED, e);
        }

        private void OnPubSubServiceError(object sender, OnPubSubServiceErrorArgs e)
        {
            EvtDispatcher.DispatchEvent<OnPubSubServiceErrorArgs>(TwitchServiceEventNames.TWITCH_PUB_SUB_ERROR, e);
        }

        private void PopulateJoinedChannels()
        {
            JoinedChannels = twitchClient.JoinedChannels;
        }
    }
}