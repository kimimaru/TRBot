# Event Dispatcher
This page is primarily aimed at developers that wish to build applications that utilize TRBot data.

TRBot has an event dispatcher that is used to dispatch events throughout the application. Events are registered with a type, and pieces of code can add listeners to that event.

The event dispatcher used in TRBot is the `WebSocketEventDispatcher`. This provides the ability to also dispatch events over a WebSocket of choice, provided the following conditions are met:
1. [websocket_server_enabled](./Settings-Documentation.md#user-content-websocket_server_enabled) must be set to 1, which enables the WebSocket server.
2. [websocket_server_address](./Settings-Documentation.md#websocket_server_address) must be a valid WebSocket address (Ex. "ws://127.0.0.1:4350").
3. [event_dispatcher_extra_features_enabled](./Settings-Documentation.md#event_dispatcher_extra_features_enabled) must be set to 1, which enables the WebSocket component of the event dispatcher. It's disabled by default for performance reasons.
4. [event_dispatcher_websocket_path](./Settings-Documentation.md#event_dispatcher_websocket_path) must be set to something valid (Ex. "/evt").

Once the following conditions are met, **all** events dispatched throughout the application are **also** dispatched through the WebSocket as JSON!

All JSON sent through the WebSocket takes the following form:
```
{
    "eventType": string,
    "eventData": object
}
```

For example, here's what a "message" event, sent upon receiving a message from any service, would look like.

```
{
    "eventType":"message",
    "eventData": {
        "serviceName":"terminal",
        "userMessage": {
            "message": "a",
            "userExternalId": "terminaluser",
            "username": "TerminalUser"
        }
    }
}
```

## Event Types

Each project in TRBot can have its own event types. Here are the locations of each:
- [Service-related](../TRBot/TRBot.Connection/EventObjects)
- [Routine-related](../TRBot/TRBot.Routines/RoutineEventObjects.cs)

# Reading TRBot data from external applications
With the WebSocket functionality of the event dispatcher, it's possible to read all of the events from TRBot in a completely separate application with an opened WebSocket. For instance, this can be used to build a custom chat display, a GUI for the Democracy mode, or another application that does something upon receiving a message containing "hello bot". There are many possibilities!

# Sending TRBot data from external applications
TRBot accepts the following events sent to it through the WebSocket as JSON.

- `message`
- `input`
- `broadcast_message`
- `bot_message`
- `command`
- `input_vote`
- `input_vote_end`
- `input_mode_vote`
- `input_mode_vote_end`

They must be sent as part of an `EventResponse` that looks like the following:

```
{
    eventType = "myevent",
    eventData = {
       ...
    }
}
```

`eventType` is the name of the event (Ex. `message`). The `eventData` is the data for that event.

The `eventData` objects must match their C# representations in TRBot's code.

Note that due to the way inputs are handled by the main application, `input` events will not actually press inputs. Instead, invoke them by passing `message` events. This may be changed in a future release.

# Additional Information
Event data coming from TRBot is subject to change between versions to meet the needs of the application. External applications may also need to be updated accordingly if they rely on those events.
