﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Serilog.Events;

namespace TRBot.Connection
{
    /// <summary>
    /// A basic service message.
    /// </summary>
    public class BasicMessage : IServiceMessage
    {
        /// <summary>
        /// The content of the message.
        /// </summary>
        public string Message { get; } = string.Empty;

        /// <summary>
        /// Whether to override the message prefix.
        /// </summary>
        public bool OverridePrefix { get; } = false;

        /// <summary>
        /// The message prefix used if the prefix is overridden.
        /// </summary>
        public string CustomPrefix { get; } = string.Empty;

        /// <summary>
        /// The log level of the message if logged.
        /// </summary>
        public LogEventLevel LogLevel { get; } = LogEventLevel.Information;

        public BasicMessage(string message, in bool overridePrefix, string customPrefix,
            in Serilog.Events.LogEventLevel logLevel)
        {
            Message = message;
            OverridePrefix = overridePrefix;
            CustomPrefix = customPrefix;
            LogLevel = logLevel;
        }

        public virtual IServiceMessage CloneWithDiffMsg(string messageContent)
        {
            return new BasicMessage(messageContent, OverridePrefix, CustomPrefix, LogLevel);
        }
    }
}
