﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.IO;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// Exports a copy of all the bot data.
    /// <para>It's highly recommended to have this accessible ONLY to the host.</para>  
    /// </summary>
    public sealed class ExportBotDataCommand : BaseCommand
    {
        private const string CONFIRMATION_ARG = "confirm";

        public ExportBotDataCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 1)
            {
                QueueMessage(args.ServiceName, $"Enter \"{CONFIRMATION_ARG}\" as an argument to confirm exporting a copy of the bot data.");
                return;
            }

            string arg = arguments[0];

            if (arg != CONFIRMATION_ARG)
            {
                QueueMessage(args.ServiceName, $"Enter \"{CONFIRMATION_ARG}\" as an argument to confirm exporting a copy of the bot data.");
                return;
            }

            try
            {
                string timeStamp = DebugUtility.GetFileFriendlyTimeStamp();
                string backupPath = Path.Combine(FolderPathResolver.DataFolderPath, $"TRBot Data Export ({timeStamp})", DataConstants.DATABASE_FILE_NAME);

                FileHelpers.BackUpFile(DatabaseMngr.DatabasePath, backupPath, true);

                QueueMessage(args.ServiceName, "Successfully backed up bot data!");
                Logger.Information($"Exported bot data to \"{backupPath}\"");
            }
            catch (Exception exc)
            {
                QueueMessage(args.ServiceName, $"ERROR - Failed to export bot data.", Serilog.Events.LogEventLevel.Error);
                Logger.Error($"Failed to export bot data. {exc.Message}");
            }
        }
    }
}
