﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Updates all of a user's abilities, enabling the ones they should have at their current access level,
    /// and disabling the ones they shouldn't have at their access level.
    /// For use if changing a user's level outside the application, such as directly through the database.
    /// </summary>
    public sealed class UpdateAllUserAbilitiesCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"username\"";

        public UpdateAllUserAbilitiesCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            //This supports updating another user's abilities if provided as an argument, but only one user at a time
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            //Get the user calling this
            string usedName = args.Command.UserMessage.Username;
            string usedExternalID = args.Command.UserMessage.UserExternalID;

            //Check to update another user's abilities
            if (arguments.Count == 1)
            {
                long thisUserLevel = (long)PermissionLevels.User;

                using (var context = DatabaseMngr.OpenContext())
                {
                    User thisUser = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                    //First check if this user has permission to do this
                    if (thisUser.HasEnabledAbility(PermissionConstants.UPDATE_OTHER_USER_ABILITES) == false)
                    {
                        QueueMessage(args.ServiceName, "You do not have permission to update another user's abilities!");
                        return;
                    }

                    thisUserLevel = thisUser.Level;
                }

                string otherUserExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(arguments[0], args.ServiceName);

                User changedUser = DatabaseMngr.GetUserByExternalID(otherUserExternalID);

                if (changedUser == null)
                {
                    QueueMessage(args.ServiceName, "The specified user does not exist in the database!");
                    return;
                }

                //Prohibit updating abilities for higher levels
                if (thisUserLevel < changedUser.Level)
                {
                    QueueMessage(args.ServiceName, "You cannot update the abilities for someone higher in level than you!");
                    return;
                }

                usedName = arguments[0];
                usedExternalID = otherUserExternalID;
            }

            //Fully update the abilities
            DatabaseMngr.UpdateUserAutoGrantAbilities(usedExternalID);

            QueueMessage(args.ServiceName, $"Updated {usedName}'s abilities!");
        }
    }
}
