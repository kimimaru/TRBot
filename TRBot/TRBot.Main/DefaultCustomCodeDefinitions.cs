/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Reflection;
using TRBot.Commands;
using TRBot.Connection;
using TRBot.Input.Consoles;

namespace TRBot.Main
{
    internal static class DefaultCustomCodeDefinitions
    {
        internal static readonly Assembly[] References = new Assembly[]
        {
            typeof(Console).Assembly,
            typeof(List<int>).Assembly,
            typeof(ExecCommand).Assembly,
            typeof(IClientService).Assembly,
            typeof(TRBot.Connection.Twitch.TwitchClientService).Assembly,
            typeof(TRBot.Connection.IRC.IRCClientService).Assembly,
            typeof(TRBot.Connection.XMPP.XMPPClientService).Assembly,
            typeof(TRBot.Connection.WebSocket.WebSocketClientService).Assembly,
            typeof(TRBot.Connection.Matrix.MatrixClientService).Assembly,
            typeof(IGameConsole).Assembly,
            typeof(TRBot.Data.CommandData).Assembly,
            typeof(TRBot.Utilities.Logging.TRBotLogger).Assembly,
            typeof(TRBot.Input.Parsing.IParser).Assembly,
            typeof(TRBot.Permissions.PermissionAbility).Assembly,
            typeof(TRBot.Routines.BaseRoutine).Assembly,
            typeof(TRBot.Utilities.EnumUtility).Assembly,
            typeof(TRBot.Input.VirtualControllers.IVirtualController).Assembly,
        };

        internal static readonly string[] Imports = new string[]
        {
            nameof(System),
            $"{nameof(System)}.{nameof(System.Collections)}.{nameof(System.Collections.Generic)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Input)}.{nameof(TRBot.Input.Parsing)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Input)}.{nameof(TRBot.Input.Consoles)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Input)}.{nameof(TRBot.Input.VirtualControllers)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Connection)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Data)}",
            $"{nameof(TRBot)}.{nameof(TRBot.Utilities)}"
        };
    }
}