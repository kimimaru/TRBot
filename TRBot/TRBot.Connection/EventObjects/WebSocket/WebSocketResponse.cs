/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Newtonsoft.Json;

namespace TRBot.Connection.WebSocket
{
    /// <summary>
    /// Represents a WebSocket response.
    /// </summary>
    public class WebSocketResponse
    {
        /// <summary>
        /// The response user object.
        /// </summary>
        [JsonProperty("user")]
        public WebSocketUserObject User = null;

        /// <summary>
        /// The response message object.
        /// </summary>
        [JsonProperty("message")]
        public WebSocketMsgObject Message = null;

        public WebSocketResponse()
        {

        }
    }

    public class WebSocketUserObject
    {
        /// <summary>
        /// The external ID of the user.
        /// </summary>
        [JsonProperty("externalId")]
        public string ExternalID = string.Empty;

        /// <summary>
        /// The name of the user.
        /// </summary>
        [JsonProperty("username")]
        public string Username = string.Empty;

        public WebSocketUserObject()
        {

        }
    }

    public class WebSocketMsgObject
    {
        /// <summary>
        /// The text of the message.
        /// </summary>
        [JsonProperty("text")]
        public string Text = string.Empty;

        public WebSocketMsgObject()
        {

        }
    }
}
