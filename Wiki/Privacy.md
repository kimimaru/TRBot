# TRBot and Data
TRBot is software that allows remote, collaborative play of video games. As such, there is inevitably an exchange of data among devices. This document serves to describe what kinds of data one can expect to be collected and processed **from TRBot alone**.

First, some terminology:
- "user" = Anyone playing games with TRBot. This is you if you're playing in a stream or chat room in which TRBot is deployed.
- "host" = The individual or group hosting TRBot on their machine. This is you if you're running a stream with TRBot.

Keep in mind that this is **not** a legal document. 

For a TLDR:
- TRBot stores data in a SQLite database that is **entirely local to the host's machine**.
- TRBot stores at least a username for each user in the database.
- Users can opt out of data for the purpose of games, including betting credits, simulate, etc. They can clear their data at any time by themselves.
- Any potentially sensitive or personal information is obtained only through users who have opted **into** simulate data and provided said information themselves.
- Hosts are able to do what they want with the data. **This falls outside of TRBot's realm.**

The following sections will provide more details.

## Data Storage
TRBot's data is stored in a single SQLite database file on the host's machine. This data is **entirely** local; none of it is uploaded or sent anywhere, unless the host does so manually themselves.

## Data Collected
All data TRBot collects from users can be seen in the structures at the following files:
- [User.cs](../TRBot/TRBot.Data/Models/User.cs)
- [UserStats.cs](../TRBot/TRBot.Data/Models/UserStats.cs)

In summary, here are the important notes of data collected:
- Name - The display name of the user. This name comes from the service the user sent a message through (Ex. Twitch, IRC, XMPP, etc).
- ExternalID - The ID of the user as it comes from the service the user sent a message through. The ExternalID is used as an identifier to find all of the information about the user in the database. This information includes Credits, ControllerPort, and permissions associated with the user.
- Credits - Virtual credits that are solely used in games and such.
- ControllerPort - The default controller port the user wants their inputs to go to. This is used primarily in multiplayer games run with TRBot.
- TotalMessageCount - Total number of messages TRBot has seen from this user. This doesn't include **any** message contents.
- ValidInputCount - Total number of valid inputs TRBot has seen this user perform. This doesn't include **any** message contents either.
- ConsentOptions - The types of data the user consents into. Here's a list of all the types of data:
  - Stats - Regards Credits, TotalMessageCount, and ValidInputCount
  - Memes - Determines whether the bot can respond with memes in chat when seeing specific phrases from this user
  - Simulate - Covers simulate data
  - RecentInputs - Determines whether to record the user's most recent inputs solely for the purpose of the [`ListUserRecentInputsCommand`](../TRBot/TRBot.Commands/Commands/ListUserRecentInputsCommand.cs) (default: "!recentinput).
  - Tutorial - Determines whether to show bot tutorial messages to the user if they are new
- SimulateHistory - A record of what the user typed in through the service, up to a max number of characters (default = 30,000), when it's not an input. This is collected **only** if the user is opted into simulate data. The data is used **solely** to "simulate" this user when invoking the [`UserSimulateCommand`](../TRBot/TRBot.Commands/Commands/UserSimulateCommand.cs) (default: "!simulate"), which creates a custom sentence that sounds somewhat authentic and usually silly. **Users can clear this data and opt back out at any time.**

## Data Management
Users can opt out of any type of data at any time through the [`UserManageDataConsentCommand`](../TRBot/TRBot.Commands/Commands/UserManageDataConsentCommand.cs) (default: "!managedata). They can clear bot stats data (such as Credits and TotalMessageCount), through the [`ClearUserStatsCommand`](../TRBot/TRBot.Commands/Commands/ClearUserStatsCommand.cs) (default: "!clearstats"). They can also clear their SimulateHistory with the [`UserSimulateManageCommand`](../TRBot/TRBot.Commands/Commands/UserSimulateManageCommand.cs) (default: "!simulatemng").

## Purpose of collection
TRBot collects data from users solely for its own function. The purposes are the following:
- Using data about the user, such as their access level and controller port, to decide how to handle inputs from that user.
- For games, such as betting credits, "simulating" a user via Markov chains (silly sentences based on what they said), input exercises, and more.
- For giving tips to the user to help improve their play, or telling the user when they increase in rank.
- For deciding if the user has permission to do certain actions, like invoke certain commands or perform certain inputs.

## Closing
TRBot collects some data to function, but only SimulateHistory could be personal and is opt-in. **All of TRBot's data is stored locally on the host's machine**.

However, keep in mind that a malicious host can use the data in unintended ways once collected.

If you have questions or concerns, [please file an issue](https://codeberg.org/kimimaru/TRBot/issues/new). If the issue relates to a specific host, ask the host how they use the data instead of filing an issue.

In general, if you do not trust a particular host, do not play on their stream or chat room.
