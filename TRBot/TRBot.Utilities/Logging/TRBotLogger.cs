﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace TRBot.Utilities.Logging
{
    /// <summary>
    /// Handles logging.
    /// </summary>
    public class TRBotLogger : ITRBotLogger
    {
        public LogEventLevel MinLoggingLevel
        {
            get => LogLevelSwitch.MinimumLevel;
            set => LogLevelSwitch.MinimumLevel = value;
        }

        /// <summary>
        /// The logger instance.
        /// </summary>
        private Logger LoggerInstance = null;

        private LoggingLevelSwitch LogLevelSwitch = new LoggingLevelSwitch();

        public TRBotLogger(string filePath, in LogEventLevel minLogLevel,
            in RollingInterval fileRollingInterval, in long logFileSizeLimit, TimeSpan? fileWriteInterval)
        {
            LogLevelSwitch.MinimumLevel = minLogLevel;

            //Set the minimum level passed to sinks to Verbose to catch everything when creating the config
            //Otherwise, even if the minimum level is set to lower than Information at runtime, the sinks won't
            //be passed lower level events and thus won't appear
            LoggerInstance = new LoggerConfiguration().WriteTo.Console(levelSwitch: LogLevelSwitch)
                                .MinimumLevel.Is(LogEventLevel.Verbose)
                                .WriteTo.File(filePath, levelSwitch: LogLevelSwitch,
                                    rollingInterval: fileRollingInterval, rollOnFileSizeLimit: true,
                                    fileSizeLimitBytes: logFileSizeLimit,
                                    flushToDiskInterval: fileWriteInterval)
                                .CreateLogger();
        }

        /// <summary>
        /// Disposes the logger.
        /// </summary>
        public void Dispose()
        {
            LoggerInstance.Dispose();
        }

        /// <summary>
        /// Logs a verbose log.
        /// </summary>
        public void Verbose(string message)
        {
            LoggerInstance.Verbose(message);
        }

        /// <summary>
        /// Logs a debug log.
        /// </summary>
        public void Debug(string message)
        {
            LoggerInstance.Debug(message);
        }

        /// <summary>
        /// Logs an information log.
        /// </summary>
        public void Information(string message)
        {
            LoggerInstance.Information(message);
        }

        /// <summary>
        /// Logs a warning.
        /// </summary>
        public void Warning(string message)
        {
            LoggerInstance.Warning(message);
        }

        /// <summary>
        /// Logs an error.
        /// </summary>
        public void Error(string message)
        {
            LoggerInstance.Error(message);
        }

        /// <summary>
        /// Logs a fatal log.
        /// </summary>
        public void Fatal(string message)
        {
            LoggerInstance.Fatal(message);
        }

        /// <summary>
        /// Writes a log with a given log level.
        /// </summary>
        public void Log(LogEventLevel logEventLevel, string message)
        {
            LoggerInstance.Write(logEventLevel, message);
        }
    }
}
