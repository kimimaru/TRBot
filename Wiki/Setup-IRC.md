## Connecting TRBot to IRC
This continues from the [initial setup](./Setup-Init.md), if you choose to connect TRBot over IRC.

The service name for IRC in the **Services** table of the **TRBotData.db** database is `irc`.

TRBot can also connect over IRC to a specific channel on a given server. It has its own configuration; the default template file is named "IRCLoginSettings.txt", located in the Data folder. The file path can be changed with the `ConfigFile` row in the database.

- *ServerName* = The IRC server to connect to (Ex. "irc.libera.chat" for Libera.Chat)
- *ServerPort* = The IRC server port. This is 6667 for the vast majority of IRC servers and there's little need to change it.
- *BotName* = The username of your bot account.
- *BotPassword* = The password for your bot account. Leave as "" if you have no password defined for the account.
- *ChannelName* = The name of the IRC channel to join. After connecting to the IRC server, the bot will join this channel and be able to send and receive messages.

Keep in mind that each IRC server has their own set of rules, so make sure to follow them when creating the bot account. In particular, some servers require you to register your nickname with the NickServ before it's allowed to perform actions such as joining and creating channels. 

TRBot internally uses [NetIRC](https://github.com/fredimachado/NetIRC) as a library to handle the IRC integration.
