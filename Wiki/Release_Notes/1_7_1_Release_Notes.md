TRBot v1.7.1. If you're on 1.7, it's highly recommended to upgrade to this version.

- Fixed macros failing to parse due to changes in the `RegexOptions` used.
- Added a new `TerminalClientService` and `TerminalEventHandler`, which allow TRBot to be used through the console window it runs in.
- Added a `ClientSettings` field in **Settings.txt**. The `ClientType` field determines whether TRBot will use the Terminal or connect to Twitch chat (0 and 1, respectively). Changes to this setting apply only once the bot is restarted.
- Updated the tutorial link in `TutorialCommand`.

Please see the [Getting Started](https://github.com/teamradish/TRTwitchPlaysBot/wiki/Getting-Started) wiki page for setting up TRBot.
