﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TRBot.Data.Migrations
{
    /// <inheritdoc />
    public partial class RemoveInputDataMaxAxisPercent : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxAxisPercent",
                schema: "inputs",
                table: "Inputs");

            migrationBuilder.AlterColumn<double>(
                name: "DefaultAxisVal",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL",
                oldDefaultValue: 0.5);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "DefaultAxisVal",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                defaultValue: 0.5,
                oldClrType: typeof(double),
                oldType: "REAL");

            migrationBuilder.AddColumn<double>(
                name: "MaxAxisPercent",
                schema: "inputs",
                table: "Inputs",
                type: "REAL",
                nullable: false,
                defaultValue: 100.0);
        }
    }
}
