﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// Lists all commands.
    /// </summary>
    public sealed class ListCmdsCommand : BaseCommand
    {
        private const string DISABLED_ARG = "disabled";

        private string UsageMessage = "Usage: no arguments (all categories), \"categories (string)\", \"disabled (also list disabled commands) (optional)\"";

        public ListCmdsCommand()
        {

        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            //Add the ValueStr as an argument if there are no arguments
            if (string.IsNullOrEmpty(ValueStr) == false && arguments.Count == 0)
            {
                arguments.AddRange(ValueStr.Split(' ', StringSplitOptions.RemoveEmptyEntries));
            }

            string categoryArgStr = string.Empty;
            string disabledArgStr = string.Empty;
            bool showDisabled = false;

            CommandCategories categoriesToShow = CommandCategories.None;

            if (arguments.Count == 0)
            {
                CommandCategories[] allCmdCategories = EnumUtility.GetValues<CommandCategories>.EnumValues;
                for (int i = 0; i < allCmdCategories.Length; i++)
                {
                    categoriesToShow |= allCmdCategories[i];
                }
            }
            else
            {
                categoryArgStr = arguments[0].ToLowerInvariant();
                if (EnumUtility.TryParseEnumValue(categoryArgStr, out categoriesToShow) == false)
                {
                    QueueMessage(args.ServiceName, $"Invalid command categories. Choose a valid category such as \"{CommandCategories.User}\", or multiple categories with \"{CommandCategories.User},{CommandCategories.Input}\"");
                    return;
                }
            }
            
            if (arguments.Count == 2)
            {
                disabledArgStr = arguments[1].ToLowerInvariant();

                //Validate argument
                if (disabledArgStr != DISABLED_ARG)
                {
                    QueueMessage(args.ServiceName, UsageMessage);
                    return;
                }

                showDisabled = true;
            }

            //Get the user so we can show only the commands they have access to
            User infoUser = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
            StringBuilder stringBuilder = null;

            long userLevel = infoUser?.Level ?? 0L;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Show the commands that should be displayed based on our argument
                IQueryable<CommandData> commandList = null;

                //Show commands that meet the criteria
                if (showDisabled == true)
                {
                    commandList = context.Commands.Where(c => c.Enabled <= 1 && (c.Category & categoriesToShow) != 0 && c.DisplayInList > 0 && c.Level <= userLevel);
                }
                else
                {
                    commandList = context.Commands.Where(c => c.Enabled == 1 && (c.Category & categoriesToShow) != 0 && c.DisplayInList > 0 && c.Level <= userLevel);
                }

                int cmdCount = commandList.Count();

                if (cmdCount == 0)
                {
                    QueueMessage(args.ServiceName, "There are no displayable commands!");
                    return;
                }

                //Order them alphabetically
                commandList = commandList.OrderBy(c => c.Name);

                //The capacity is estimated by the number of commands times the average string length of each one
                stringBuilder = new StringBuilder(cmdCount * 12);

                stringBuilder.Append("Hi ").Append(args.Command.UserMessage.Username).Append(", here's a list of relevant commands: ");

                foreach (CommandData cmd in commandList)
                {
                    stringBuilder.Append(cmd.Name);

                    //Note if the command is disabled
                    if (cmd.Enabled <= 0)
                    {
                        stringBuilder.Append(" (disabled)");
                    }

                    stringBuilder.Append(',').Append(' ');
                }
            }

            stringBuilder.Remove(stringBuilder.Length - 2, 2);

            QueueMessage(args.ServiceName, stringBuilder.ToString(), ", ");
        }
    }
}
