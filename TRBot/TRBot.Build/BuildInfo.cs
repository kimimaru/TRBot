﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace TRBot.Build
{
    /// <summary>
    /// Build information.
    /// </summary>
    public static class BuildInfo
    {
        private const string FALLBACK_VERSION_STR = "0.0.0";
        private const string FALLBACK_REPOSITORY_URL = "https://codeberg.org/kimimaru/TRBot";

        /// <summary>
        /// The tag used as the version number of the application.
        /// </summary>
        public static string GitBaseTag
        {
            get
            {
                if (string.IsNullOrEmpty(ThisAssembly.Git.BaseTag) == false)
                {
                    return ThisAssembly.Git.BaseTag;
                }

                return FALLBACK_VERSION_STR;
            }
        }

        /// <summary>
        /// The commit hash of the application.
        /// </summary>
        public static string GitCommitHash
        {
            get
            {
                if (string.IsNullOrEmpty(ThisAssembly.Git.Commit) == false)
                {
                    return ThisAssembly.Git.Commit;
                }

                return "unknown_hash";
            }
        }

        /// <sumary>
        /// The commit date of the application.
        /// </summary>
        public static string GitCommitDate
        {
            get
            {
                if (DateTime.TryParse(ThisAssembly.Git.CommitDate, out DateTime dateTime) == true)
                {
                    return dateTime.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss") + " UTC";
                }

                return "unknown_date";
            }
        }

        /// <summary>
        /// The URL of the repository.
        /// </summary>
        public static string GitRepositoryURL
        {
            get
            {
                if (string.IsNullOrEmpty(ThisAssembly.Git.RepositoryUrl) == false)
                {
                    return ThisAssembly.Git.RepositoryUrl;
                }

                return FALLBACK_REPOSITORY_URL;
            }
        }
    }
}
