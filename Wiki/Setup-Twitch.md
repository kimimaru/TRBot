## Connecting TRBot to Twitch
This continues from the [initial setup](./Setup-Init.md), if you choose to connect TRBot over Twitch.

The service name for Twitch in the **Services** table of the **TRBotData.db** database is `twitch`.

Twitch needs additional configuration to connect. The `ConfigFile` row in the database points to a JSON file used in the configuration. If the config file is not present, TRBot will create a template for you. By default, this config file is named "TwitchLoginSettings.txt" and is located in the Data folder.

The settings in the config file are described below:

- *BotName* = Username of the Twitch account for the bot to connect as. This has to be a valid Twitch account that **must already be created.**
- *Password* = OAuth token for your Twitch account. **This HAS to be an OAuth token and CANNOT be your raw password!** Generate an OAuth token [here](https://twitchtokengenerator.com/).
- *ChannelName* = The name of the Twitch channel to have the bot connect to. Multiple channels are not currently supported.

After these are set, run TRBot again and you should see it connect to the channel.
<br />***IMPORTANT:*** If you don't see the bot's connection message on the channel, make sure the channel doesn't have chat restrictions, such as Followers-only, or have the bot account adhere to the restrictions so it can chat.
<br />***IMPORTANT 2:*** To improve the experience of using TRBot in Twitch chat, the bot account should be a VIP or moderator of the channel so it doesn't have restrictions on repeated messages. If you do this, you may want to also raise the [message_throttle_count](./Settings-Documentation.md#message_throttle_count) to the moderator values outlined [here](https://dev.twitch.tv/docs/irc/guide#command--message-limits) so your bot can send more messages.

TRBot internally uses [TwitchLib](https://github.com/TwitchLib/TwitchLib) to handle Twitch connection.
