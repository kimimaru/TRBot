﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TRBot.Connection
{
    /// <summary>
    /// Helpers regarding connections.
    /// </summary>
    public static class ConnectionHelpers
    {
        /// <summary>
        /// Tells if a message is a chat command in a given service.
        /// </summary>
        /// <param name="serviceName">The service name.</param>
        /// <param name="message">The message content.</param>
        /// <returns> true if the message is considered a chat command in the service, otherwise false.</returns>
        public static bool IsServiceChatCommand(string serviceName, string message)
        {
            //Check for forward slash at the start of the message
            if (message.StartsWith('/') == false)
            {
                return false;
            }

            //If the service uses forward slashes at the start of the message for commands, the message is a chat command
            switch (serviceName)
            {
                case ClientServiceNames.TWITCH_SERVICE:
                case ClientServiceNames.IRC_SERVICE:
                case ClientServiceNames.XMPP_SERVICE:
                case ClientServiceNames.MATRIX_SERVICE:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Tells if a string is a command.
        /// </summary>
        /// <param name="message">The message content.</param>
        /// <param name="serviceName">The command identifier.</param>
        /// <returns> true if the message is recognized as a command, otherwise false.</returns>
        public static bool IsCommand(string message, string commandIdentifier)
        {
            return message.StartsWith(commandIdentifier, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Parses arguments into a space-separated list, considering strings enclosed in quotes as a single argument.
        /// </summary>
        /// <param name="argsWSReplacedStr">The string of command arguments, with all other whitespace characters replaced with a space.</param>
        /// <returns>A list of strings representing the arguments.</returns>
        public static List<string> ParseCommandArgumentsWithQuotes(string argsWSReplacedStr)
        {
            const string quoteRegex = @"[\""].+?[\""]|[^ ]+";

            MatchCollection matchCollection = Regex.Matches(argsWSReplacedStr, quoteRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            List<string> args = Regex.Matches(argsWSReplacedStr,  quoteRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase)
                .Select(x =>
                {
                    var val = x.Value;

                    //Remove only the enclosing quotes at the start and end of the string
                    if (val.Length > 1 && val[0] == '"' && val[val.Length - 1] == '"')
                    {
                        val = val.Remove(val.Length - 1, 1).Remove(0, 1);
                    }

                    return val;
                })
                .ToList();

            return args;
        }

        /// <summary>
        /// Parses a command data event from a string.
        /// </summary>
        /// <paramref name="commandInputStringWSAsSpace">A command string with all other whitespace characters replaced with a space.</paramref>
        /// <paramref name="commandIdentifier">The identifier used to start the command.</paramref>
        /// <paramref name="evtUserMsgData">User message data.</paramref>
        /// <returns>An <see cref="EvtChatCommandData"/> parsed from the command string. null if it fails to parse.
        /// </returns>
        public static EvtChatCommandData ParseCommandDataFromStringWsAsSpace(string commandInputStringWSAsSpace,
            string commandIdentifier, EvtUserMsgData evtUserMsgData)
        {
            //Check for a command
            if (commandInputStringWSAsSpace.Length > 0 && IsCommand(commandInputStringWSAsSpace, commandIdentifier) == true)
            {
                //Build args list
                List<string> argsList = ParseCommandArgumentsWithQuotes(commandInputStringWSAsSpace);
                string argsAsStr = string.Empty;

                //Remove the command itself and the space from the string
                if (argsList.Count > 1)
                {
                    argsAsStr = commandInputStringWSAsSpace.Remove(0, argsList[0].Length + 1);
                }

                //Remove command identifier
                string cmdText = argsList[0].Remove(0, commandIdentifier.Length);
                
                //Now remove the first entry from the list, which is the command, retaining only the arguments
                argsList.RemoveAt(0);

                return new EvtChatCommandData(argsList, argsAsStr, evtUserMsgData, commandIdentifier, cmdText);
            }

            return null;
        }
    }
}
