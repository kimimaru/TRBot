﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;
using TRBot.Data;
using TRBot.Permissions;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Routines
{
    /// <summary>
    /// A routine that periodically makes an input.
    /// This is useful for newer console games that go to sleep if no inputs are pressed after a while.
    /// </summary>
    public class PeriodicInputRoutine : BaseRoutine
    {
        private DateTime CurInputTime;

        public PeriodicInputRoutine()
            : base(true)
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            CurInputTime = DateTime.UtcNow;
        }

        public override void CleanUp()
        {
            base.CleanUp();
        }

        public override void UpdateRoutine(in DateTime currentTimeUTC)
        {
            TimeSpan timeDiff = currentTimeUTC - CurInputTime;

            long inputTimeMS = DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_TIME, -1L);

            //Don't do anything if the input time is less than 0 to prevent spam
            if (inputTimeMS < 0L)
            {
                CurInputTime = currentTimeUTC;
                return;
            }

            //Check if we surpassed the time
            if (timeDiff.TotalMilliseconds < inputTimeMS)
            {
                return;
            }
            
            //Refresh time
            CurInputTime = currentTimeUTC;

            //Don't process anything if inputs are disabled
            if (InputHndlr.InputsEnabled == false)
            {
                return;
            }

            string periodicInputValue = DatabaseMngr.GetSettingString(SettingsConstants.PERIODIC_INPUT_VALUE, string.Empty);
            int controllerPort = (int)DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_PORT, 0L);

            //Don't perform the input if it's empty
            if (string.IsNullOrEmpty(periodicInputValue) == true)
            {
                EvtDispatcher.BroadcastMessage($"Failed periodic input: {SettingsConstants.PERIODIC_INPUT_VALUE} is null or empty."); 
                return;
            }

            //Don't perform the input if the controller port is out of range
            if (controllerPort < 0 || controllerPort >= VControllerContainer.VControllerMngr.ControllerCount)
            {
                EvtDispatcher.BroadcastMessage($"Failed periodic input: The controller port is {controllerPort}, which is out of range for this virtual controller. Change the \"{SettingsConstants.PERIODIC_INPUT_PORT}\" setting to a valid number.");
                return;
            }

            //Set up the console
            GameConsole usedConsole = null;

            int lastConsoleID = (int)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole lastConsole = context.Consoles
                    .AsNoTracking()
                    .Include(c => c.InputList)
                    .Include(c => c.InvalidInputCombos)
                    .ThenInclude(ic => ic.InvalidInputs)
                    .FirstOrDefault(c => c.ID == lastConsoleID);

                if (lastConsole != null)
                {
                    //Create a new console using data from the database
                    usedConsole = new GameConsole(lastConsole.Name, lastConsole.InputList, lastConsole.InvalidInputCombos);
                }
            }

            //If there are no valid inputs, don't attempt to parse
            if (usedConsole == null)
            {
                EvtDispatcher.BroadcastMessage("Failed periodic input: The current console does not point to valid data. Please set a different console to use, or if none are available, add one.");
                return;
            }

            if (usedConsole.ConsoleInputs.Count == 0)
            {
                EvtDispatcher.BroadcastMessage($"Failed periodic input: The current console, \"{usedConsole.Name}\", does not have any available inputs.");
                return;
            }

            ParsedInputSequence inputSequence = default;

            try
            {
                int defaultDur = (int)DatabaseMngr.GetUserOrGlobalDefaultInputDur(string.Empty);
                int maxDur = (int)DatabaseMngr.GetUserOrGlobalMaxInputDur(string.Empty);

                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //Get input synonyms for this console
                    IQueryable<InputSynonym> synonyms = context.InputSynonyms.Where(syn => syn.ConsoleID == lastConsoleID);
    
                    StandardParserValidator validator = new StandardParserValidator(null,
                        (long)PermissionLevels.Superadmin, usedConsole, VControllerContainer.VControllerMngr, Logger);

                    IParser standardParser = new StandardParserFactory(context.Macros, synonyms,
                            usedConsole.GetInputNames(InputNameFiltering.Enabled), 0, VControllerContainer.VControllerMngr.ControllerCount - 1,
                            defaultDur, maxDur, true, validator)
                        .Create();

                    //Parse inputs to get our parsed input sequence
                    inputSequence = standardParser.ParseInputs(periodicInputValue);
                }
            }
            catch (Exception exception)
            {
                string excMsg = exception.Message;

                //Handle parsing exceptions
                inputSequence.ParsedInputResult = ParsedInputResults.Invalid;

                EvtDispatcher.BroadcastMessage($"Failed periodic input: {excMsg}");
                return;
            }

            //Check for non-valid messages
            if (inputSequence.ParsedInputResult != ParsedInputResults.Valid)
            {
                string message = inputSequence.Error;
                if (string.IsNullOrEmpty(message) == true)
                {
                    message = "Input is invalid";
                }
                
                EvtDispatcher.BroadcastMessage($"Failed periodic input: {message}");
                return;
            }

            //Now, perform the input
            if (InputHndlr.InputsHalted == true)
            {
                EvtDispatcher.BroadcastMessage("Inputs are currently halted! Unable to perform periodic input.");
            }
            else
            {
                InputHndlr.CarryOutInput(inputSequence.Inputs, usedConsole, VControllerContainer.VControllerMngr);
                
                long messageEnabled = DatabaseMngr.GetSettingInt(SettingsConstants.PERIODIC_INPUT_MESSAGE_ENABLED, 1L);
                
                //Print a message with the input to show it's processed if the message is enabled
                if (messageEnabled > 0L)
                {
                    EvtDispatcher.BroadcastMessage($"Periodic input: {periodicInputValue}");
                }
            }
        }
    }
}
