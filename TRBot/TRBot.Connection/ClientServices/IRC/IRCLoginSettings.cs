﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Connection.IRC
{
    /// <summary>
    /// Connection settings for IRC.
    /// </summary>
    public sealed class IRCLoginSettings
    {
        public string ServerName = string.Empty;
        public int ServerPort = 6667;
        public string BotName = string.Empty;
        public string BotPassword = string.Empty;
        public string ChannelName = string.Empty;
    }
}
