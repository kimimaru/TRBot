﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Updates a user's level.
    /// This will also adjust the user's abilities.
    /// </summary>
    public sealed class SetUserLevelCommand : BaseCommand
    {
        public SetUserLevelCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count != 2)
            {
                QueueMessage(args.ServiceName, "Usage: \"username\" \"level\"");
                return;
            }

            string levelUsername = arguments[0];
            string levelStr = arguments[1];

            string curUserName = args.Command.UserMessage.Username;
            if (levelUsername == curUserName)
            {
                QueueMessage(args.ServiceName, "You cannot set your own level!");
                return;
            }

            string levelUserExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(levelUsername, args.ServiceName);

            User levelUser = DatabaseMngr.GetUserByExternalID(levelUserExternalID);

            if (levelUser == null)
            {
                QueueMessage(args.ServiceName, "User does not exist in database!");
                return;
            }

            User curUser = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

            if (curUser == null)
            {
                QueueMessage(args.ServiceName, "Invalid user of this command; something went wrong?!");
                return;
            }

            if (levelUser.Level >= curUser.Level)
            {
                QueueMessage(args.ServiceName, "You can't set the level of a user whose level is equal to or greater than yours!");
                return;
            }

            //Parse the level
            if (PermissionHelpers.TryParsePermissionLevel(levelStr, out PermissionLevels permLvl) == false)
            {
                QueueMessage(args.ServiceName, "Invalid level specified.");
                return;
            }

            long levelNum = (long)permLvl;

            //Invalid - attempting to set level higher than or equal to own
            if (levelNum >= curUser.Level)
            {
                QueueMessage(args.ServiceName, "You cannot set a level greater than or equal to your own!");
                return;
            }

            //Set level, adjust abilities, and save
            DatabaseMngr.AdjustUserLvlAndAbilitiesOnLevel(levelUser.ExternalID, levelNum);

            QueueMessage(args.ServiceName, $"Set {levelUser.Name}'s level to {levelNum}, {permLvl}!");
        }
    }
}
