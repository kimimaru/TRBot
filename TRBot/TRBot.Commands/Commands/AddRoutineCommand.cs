﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that adds a routine.
    /// </summary>
    public sealed class AddRoutineCommand : BaseCommand
    {
        private const string NULL_ARG = "null";

        private string UsageMessage = $"Usage - \"routine name\", \"type w/namespace\" (can be \"null\"), \"argString - optional\" (can be \"null\"), \"enabled (bool)\" \"resetOnReload (bool)\"";

        public AddRoutineCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with too few arguments
            if (argCount < 4)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            int minArgCount = argCount - 2;

            string routineName = arguments[0].ToLowerInvariant();
            string className = arguments[1];
            string valueStr = string.Empty;
            string enabledStr = arguments[minArgCount];
            string resetOnReloadStr = arguments[minArgCount + 1];

            //Set the class name to null if it's the null argument
            if (className == NULL_ARG)
            {
                className = string.Empty;
            }

            //Combine all the arguments in between as the value string
            for (int i = 2; i < minArgCount; i++)
            {
                valueStr += arguments[i];
                if (i < (minArgCount - 1))
                {
                    valueStr += " ";
                }
            }

            //Set value string to null if it's the null argument
            if (valueStr == NULL_ARG)
            {
                valueStr = string.Empty;
            }

            if (bool.TryParse(enabledStr, out bool cmdEnabled) == false)
            {
                QueueMessage(args.ServiceName, "Incorrect routine enabled state specified.");
                return;
            }

            if (bool.TryParse(resetOnReloadStr, out bool resetOnReload) == false)
            {
                QueueMessage(args.ServiceName, "Incorrect routine resetOnReload state specified.");
                return;
            }

            bool added = RoutineHandler.AddRoutine(routineName, className, valueStr, cmdEnabled, resetOnReload);

            if (added == true)
            {
                //Add this routine to the database
                using (BotDBContext context = DatabaseMngr.OpenContext())
                {
                    //If this routine already exists, remove it so it can be replaced with the new data
                    RoutineData routData = context.Routines.FirstOrDefault((rtine) => rtine.Name == routineName);
                    if (routData != null)
                    {
                        //Remove routine
                        context.Routines.Remove(routData);
                    }

                    //Add the new one
                    context.Routines.Add(new RoutineData(routineName, className, cmdEnabled, resetOnReload, valueStr));
                    
                    context.SaveChanges();
                }

                QueueMessage(args.ServiceName, $"Successfully added routine \"{routineName}\"!");
            }
            else
            {
                QueueMessage(args.ServiceName, $"Failed to add routine \"{routineName}\".");
            }
        }
    }
}
