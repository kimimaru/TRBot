﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using TRBot.Connection;
using TRBot.Input.Consoles;
using TRBot.Data;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that removes an input from the invalid input combo for a console.
    /// </summary>
    public sealed class RemoveInvalidInputComboCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"console name (string)\", \"invalid combo name (string)\", \"input name (string) (optional)\"";

        public RemoveInvalidInputComboCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            int argCount = arguments.Count;

            //Ignore with not enough arguments
            if (argCount < 2 || argCount > 3)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            string consoleStr = arguments[0].ToLowerInvariant();
            string invalidComboName = arguments[1].ToLowerInvariant();
            string inputName = argCount != 3 ? string.Empty : arguments[2].ToLowerInvariant();

            bool comboRemoved = false;

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleStr);

                if (console == null)
                {
                    QueueMessage(args.ServiceName, $"No console named \"{consoleStr}\" found.");
                    return;
                }

                //Check if the invalid combo with the given name exists
                InvalidInputCombo invalidCombo = console.InvalidInputCombos.FirstOrDefault(combo => combo.Name == invalidComboName);

                if (invalidCombo == null)
                {
                    QueueMessage(args.ServiceName, $"No invalid input combo named \"{invalidComboName}\" exists for the \"{console.Name}\" console!");
                }

                //Remove the input from the invalid input combo if specified
                if (argCount == 3)
                {
                    //Check if the input is in the invalid input combo
                    InputData existingInput = invalidCombo.InvalidInputs.FirstOrDefault(inpData => inpData.Name == inputName);

                    if (existingInput == null)
                    {
                        QueueMessage(args.ServiceName, $"No input named \"{inputName}\" is part of the invalid input combo \"{invalidComboName}\" for the \"{console.Name}\" console!");
                        return;
                    }

                    //Remove the input
                    invalidCombo.InvalidInputs.Remove(existingInput);
                }

                //Remove the combo if no input was specified or there are no more inputs in the combo
                if (argCount == 2 || invalidCombo.InvalidInputs.Count == 0)
                {
                    //Remove the invalid combo for this console and save
                    console.InvalidInputCombos.Remove(invalidCombo);

                    comboRemoved = true;
                }

                context.SaveChanges();
            }
            
            if (comboRemoved == true)
            {
                QueueMessage(args.ServiceName, $"Successfully removed invalid input combo \"{invalidComboName}\" for the \"{consoleStr}\" console!");
            }
            else
            {
                QueueMessage(args.ServiceName, $"Successfully removed \"{inputName}\" from the invalid input combo \"{invalidComboName}\" for the \"{consoleStr}\" console!");
            }
        }
    }
}
