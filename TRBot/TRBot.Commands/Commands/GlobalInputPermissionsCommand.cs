﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// Gets or sets the global minimum level required to perform inputs.
    /// </summary>
    public class GlobalInputPermissionsCommand : BaseCommand
    {
        private string UsageMessage = "Usage: \"level (string/int) (optional)\"";
        private string CachedPermsStr = string.Empty;

        public GlobalInputPermissionsCommand()
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();

            string[] names = EnumUtility.GetNames<PermissionLevels>.EnumNames;

            for (int i = 0; i < names.Length; i++)
            {
                CachedPermsStr += names[i];

                if (i < (names.Length - 1))
                {
                    CachedPermsStr += ", ";
                }
            }
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            long curInputPerms = DatabaseMngr.GetSettingInt(SettingsConstants.GLOBAL_INPUT_LEVEL, 0L);

            //See the permissions
            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"Inputs are allowed for {(PermissionLevels)curInputPerms} and above. To set the permissions, add one as an argument: {CachedPermsStr}");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                //Check if this user has the ability to set this level
                if (user != null && user.HasEnabledAbility(PermissionConstants.SET_GLOBAL_INPUT_LEVEL_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You do not have the ability to set the global input level!");
                    return;
                }
            }

            string permsStr = arguments[0];

            //Try to parse the permission level
            if (PermissionHelpers.TryParsePermissionLevel(permsStr, out PermissionLevels permLevel) == false)
            {
                QueueMessage(args.ServiceName, $"Please enter a valid permission: {CachedPermsStr}");
                return;
            }

            //The permissions are already this value
            if (curInputPerms == (long)permLevel)
            {
                QueueMessage(args.ServiceName, $"The permissions are already {permLevel}!");
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
            
                //Check if this user is lower than this level and deny it if so
                if (user.Level < (long)permLevel)
                {
                    QueueMessage(args.ServiceName, "You cannot set the global input level greater than your own level!");
                    return;
                }

                Settings inputPermsSetting = context.GetSetting(SettingsConstants.GLOBAL_INPUT_LEVEL);

                //Set new value and save
                inputPermsSetting.ValueInt = (long)permLevel;

                context.SaveChanges();
            }

            QueueMessage(args.ServiceName, $"Set input permissions to {permLevel} and above!");
        }
    }
}
