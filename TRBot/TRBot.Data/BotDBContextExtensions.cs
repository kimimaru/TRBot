﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using TRBot.Permissions;
using TRBot.Input.Consoles;
using Microsoft.EntityFrameworkCore;

namespace TRBot.Data
{
    /// <summary>
    /// Helps retrieve data from the database.
    /// </summary>
    public static class BotDBContextExtensions
    {
        /// <summary>
        /// Obtains a setting from the database with an opened context.
        /// </summary>
        /// <param name="context">The open database context.</param>
        /// <param name="settingName">The name of the setting to retrieve.</param>
        /// <returns>A Settings object corresponding to settingName. If the setting is not found, null.</returns>
        public static Settings GetSetting(this BotDBContext context, string settingName)
        {
            return context.SettingCollection.FirstOrDefault((set) => set.Key == settingName);
        }

        /// <summary>
        /// Obtains a setting integer value from the database with an opened context.
        /// </summary>
        /// <param name="context">The open database context.</param>
        /// <param name="settingName">The name of the setting to retrieve.</param>
        /// <param name="defaultVal">The default value to fallback to if not found.</param>
        /// <returns>An integer for settingName. If the setting is not found, the default value.</returns>
        public static long GetSettingInt(this BotDBContext context, string settingName, in long defaultVal)
        {
            Settings setting = context.SettingCollection.AsNoTracking().FirstOrDefault((set) => set.Key == settingName);

            return setting != null ? setting.ValueInt : defaultVal;
        }

        /// <summary>
        /// Obtains a setting string value from the database with an opened context.
        /// </summary>
        /// <param name="context">The open database context.</param>
        /// <param name="settingName">The name of the setting to retrieve.</param>
        /// <param name="defaultVal">The default value to fallback to if not found.</param>
        /// <returns>A string value for settingName. If the setting is not found, the default value.</returns>
        public static string GetSettingString(this BotDBContext context, string settingName, string defaultVal)
        {
            Settings setting = context.SettingCollection.AsNoTracking().FirstOrDefault((set) => set.Key == settingName);

            return setting != null ? setting.ValueStr : defaultVal;
        }

        /// <summary>
        /// A helper method to obtain the command identifier with an opened context.
        /// </summary>
        /// <param name="context">The open database context.</param>
        /// <returns>The string value of the command identifier.</returns>
        public static string GetCommandIdentifier(this BotDBContext context)
        {
            return context.GetSettingString(SettingsConstants.COMMAND_IDENTIFIER, "!");
        }

        /// <summary>
        /// A helper method to obtain the name of the bot credits with an opened context.
        /// </summary>
        /// <param name="context">The open database context.</param>
        /// <returns>The name of the bot credits.</returns>
        public static string GetCreditsName(this BotDBContext context)
        {
            return context.GetSettingString(SettingsConstants.CREDITS_NAME, "Credits");
        }

        /// <summary>
        /// Obtains a user object by their ExternalID from the database with an opened context.
        /// </summary>
        /// <param name="context">The open datbase context.</param>
        /// <param name="userExternalId">The ID of the user.</param>
        /// <returns>A user object with the given ExternalID. null if not found.</returns>
        public static User GetUserByExternalID(this BotDBContext context, string userExternalId)
        {
            if (string.IsNullOrEmpty(userExternalId) == true)
            {
                return null;
            }

            return context.Users.FirstOrDefault(u => u.ExternalID == userExternalId);
        }
    }
}
