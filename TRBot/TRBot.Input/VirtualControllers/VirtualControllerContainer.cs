﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace TRBot.Input.VirtualControllers
{
    /// <summary>
    /// A container for virtual controller information.
    /// </summary>
    public class VirtualControllerContainer : IVirtualControllerContainer
    {
        /// <summary>
        /// The current virtual controller manager.
        /// </summary>
        public IVirtualControllerManager VControllerMngr { get; private set; } = null;

        /// <summary>
        /// The current virtual controller type.
        /// </summary>
        public VirtualControllerTypes VControllerType { get; private set; } = VirtualControllerTypes.Dummy;

        public VirtualControllerContainer(IVirtualControllerManager virtualControllerManager,
            in VirtualControllerTypes vControllerType)
        {
            VControllerMngr = virtualControllerManager;
            VControllerType = vControllerType;
        }

        /// <summary>
        /// Sets the virtual controller manager.
        /// </summary>
        /// <param name="virtualControllerManager">The <see cref="IVirtualControllerManager"/> to use.</param>
        public void SetVirtualControllerManager(IVirtualControllerManager virtualControllerManager)
        {
            VControllerMngr = virtualControllerManager;
        }

        /// <summary>
        /// Sets the virtual controller type.
        /// </summary>
        /// <param name="vControllerType">The <see cref="VirtualControllerTypes"/> to set.</param>
        public void SetVirtualControllerType(in VirtualControllerTypes vControllerType)
        {
            VControllerType = vControllerType;
        }
    }
}
