This tutorial serves to demonstrate all the features of TRBot's input syntax.

#### Table of Contents
* [Overview](#overview)
* [Input Sequences](#input-sequences)
* [Simultaneous Inputs](#simultaneous-inputs)
* [Holding and Releasing Inputs](#holding-and-releasing-inputs)
* [Delaying Inputs](#delaying-inputs)
* [Repeated Inputs](#repeated-inputs)
* [Multi-Controller Inputs](#multi-controller-inputs)
* [Macros](#macros)
* [Dynamic Macros](#dynamic-macros)
* [Comments](#comments)
* [Input Synonyms](#input-synonyms)

## Overview

TRBot's standard syntax takes the following form:

`(&#) (- or _) (input) (#%) (#ms or #s) (+)`

| Component   | Description | Optional/Required | Examples|
| ----------- | ----------- | ----------------- | --------|
| `&#`        | The controller port number the input should apply to. | Optional | `&1`, `&2` |
| `_` or `-`  | A hold/release modifier. `-` means to release the input, and `_` means to hold the input until either the end of the sequence or a corresponding release input. | Optional | `_a`, `-a` |
| `input`     | The name of the input to press. | Required | `start`, `a`, `b` |
| `#%`        | The percentage to press the input, commonly used on analog sticks and triggers. Supports up to 3 decimal places. | Optional | `50%`, `4.782%` |
| `#ms` or `#s` | The duration the input is held, in milliseconds or seconds, but not both. Seconds supports up to 3 decimal places. | Optional | `450ms`, `2s`, `4.187s`, `4187ms` |
| `+`         | Indicates the input is executed simultaneously with the following input(s). | Optional | `a+b`, `r250ms+down500ms+y2.5s` |

Inputs are *not* case-sensitive and are whitespace-independent. This means that the parser will pick up the input regardless if it's capitalized or entered with spaces.

The available inputs vary by the console in use. The valid input list can be viewed by running the `InputInfoCommand` (default: "!inputs"). A blank input is usually available, with "#" as the default blank input.

If a duration isn't specified, it defaults to the [default_input_duration](./Settings-Documentation.md#default_input_duration) setting, which can be viewed with the `DefaultInputDurCommand` (default: "!defaultinputdur"). The default on a clean TRBot installation is 200 milliseconds.

Complete input example:
* `&1right70%500ms` - this holds "right" 70% for 500 milliseconds on player 1's controller

#### Parsing Order

Inputs are parsed from left to right with no delay in between. Ex. `a b` will press "a" first then "b". `a #500ms b` will press "a", wait 500 milliseconds, then press "b".

## Input Sequences

It's possible to perform as many inputs as desired in a message. Each string of inputs is known as an input sequence. Input sequences can be performed with or without whitespace. For example, `a b` is equivalent to `ab`. The inputs earlier in the sequence will be performed first. For example, in `a300ms b100ms`, "a" will be pressed for 300 milliseconds, then "b" will be pressed for 100 milliseconds.

![Moving and jumping in Donkey Kong Country 2](./Images/Tutorial/InputSequenceTutorial.gif)

**Example:** `left500ms b500ms right500ms b500ms`

The maximum duration of an input sequence is determined by the [max_input_duration](./Settings-Documentation.md#max_input_duration) setting. No input sequence can surpass this duration. For example, if the max duration is 60 seconds, typing `a61s` or `a30s b30001ms` will exceed 60 seconds, so the input will fail.

The maximum input duration can be viewed with the `MaxInputDurCommand` (default: "!maxinputdur"). TRBot will output a message if an input sequence would exceed the max duration.

Keep in mind that some platforms have a character limit; in these cases, inputs may fail without a message from TRBot.

## Simultaneous Inputs

Append "+" at the end of an input to chain another input. `a+b` will press both "a" and "b" at the same time. `a+b+c` will press "a", "b", and "c" at the same time. There is no limit to how many inputs can be chained together, and each input can have its own duration. For example, `a200ms+b1s` will press both "a" and "b" at the same time, but "a" will be held for 200 milliseconds while "b" will be held for one second.

![Obtaining a key in The Legend of Zelda: A Link to the Past](./Images/Tutorial/SimultaneousInputsTutorial.gif)

**Example:** `down140ms+right2300ms a #330ms right300ms+a left840ms+down350ms up300ms a #1500ms select`

As shown above, this can be used for diagonal movements and more precise plays, since more than one action can be performed at the same time.

For inputs on the same controller axis, such as an analog stick, the **lattermost input takes precedence.** For example, if "left" and "right" modify the same axis, `-right+left` will **move the axis left**, while `left+-right` will **let go of the axis**.

## Holding and Releasing Inputs

Release inputs by prepending "-" before the input name. If an input is held or ongoing, this will stop it early.

Hold inputs by prepending "_" before the input name. Inputs held this way stay held until either the end of the input sequence or until it sees a matching release input. `_a1s` will release the "a" input after 1 second, while `_a1s #400ms -a b` will release the "a" input after 1400 milliseconds.

![Moving in a circle in Super Mario Galaxy](./Images/Tutorial/HoldReleaseTutorial.gif)

**Example:** `_down plus . . . _left40% -left+_right #200ms -down _up300ms -right+_left -up`

Tip: Combine this with simultaneous inputs to hold or release multiple inputs at the same time.

## Delaying Inputs

As mentioned earlier, a blank input is available, with the default being "#" for pre-configured consoles. Since this input isn't used for anything, it can effectively be used to delay any other input or set of inputs. `right #100ms up` will press "right", wait 100 milliseconds, then press "up". 

![Defeating a Flare Dancer in The Legend of Zelda: Ocarina of Time](./Images/Tutorial/DelayingInputsTutorial.gif)

**Example:** `#p right+up z #100ms cleft . . cleft #1000ms b z down400ms . a`

In the example above, delays are used strategically to attack the enemy once it's vulnerable.

**Note:** In the example above, "#p" is a macro, which will be covered later. Here, it simply unpauses the game and waits a brief period.

## Percentages for Analog Inputs

For analog inputs, it's possible to specify a percentage with `input#%`, where "#" is a number from 0 - 100. This supports up to 3 decimal places (Ex. "37.845%"). For example, `right50%`, can be used to press an analog stick halfway to the right.

![Walking on a narrow path in Paper Mario: The Thousand Year Door](./Images/Tutorial/PercentageAnalogTutorial.gif)

**Example:** `left300ms . left50%600ms left40%1100ms down45%600ms _left . a..a..a.`

This can be used for precision when moving and aiming in various types of games. Specifying a percentage on non-analog inputs has no effect and acts as if the input was pressed normally.

## Repeated Inputs

Inputs can be repeated a number of times in the form of `[...]*#`, with "#" as the number of repetitions. For example, `[a #100ms]*2` is equivalent to "a #100ms a #100ms". The max number of repetitions is currently 999, but this may be changed in the future.

![Repeatedly crouching to clip through the floor in Rayman](./Images/Tutorial/RepeatedInputsTutorial.gif)

**Example:** `left2s+right _down [r121ms #17ms]*120`

In the above example, "r121ms #17ms" is repeated rapidly 120 times while holding down to perform a glitch to clip through the floor.

Repeated inputs can also be nested. For example, `[up [b]*2]*2` expands out to `up [b]*2 up [b]*2`, which then expands to `up b b up b b`.

Repeated inputs can mitigate the character limit on a given platform and make input sequences more concise and readable.

## Multi-Controller Inputs

TRBot's syntax supports passing inputs to different controllers in the same input string. For example, `&1right &2left` will press "right" for player 1 then "left" for player 2. This is useful in co-op or competitive games with multiple controllers in use.

![Controlling Player 1 and Player 2 at the same time in Sonic The Hedgehog 2](./Images/Tutorial/Multi-ControllerInputsTutorial.gif)

**Example:** `&1left500ms+&2right500ms`

Specifying an invalid controller port will cause the input to fail. The number of controllers in use can be viewed with the `ControllerCountCommand` (default: "!controllercount"), and one can view or set their controller port with the `ControllerPortCommand` (default: "!port").

## Macros

A macro is a collection of inputs that can be assigned to TRBot with the `AddMacroCommand` (default: "!addmacro"). Macros are stored in the **Macros** table in the database and can be viewed with the `ListMacrosCommand` (default: "!macros"). Once created, macros can be used as if they're normal inputs.

Macros start with "#" and have a name, which may be used to describe what the macro does. For example, `#masha` can be a macro for `[a34ms #34ms]*20`, which will press "a" many times very rapidly.

![Jumping multiple times in Super Tux](./Images/Tutorial/MacroTutorial.gif)

**Example:** `#500ms #jumplots`
- `#jumplots` is a macro equivalent to `[a100ms #67ms]*10`

Macros can even contain other macros. For example, a macro named `#mashatwice` can be `#masha #masha`.

Macros can be very powerful assets, especially in more complex games such as 3D platformers, where the character may have moves that can be performed via a sequence of specific inputs. Macros can be used to eliminate the tedium involved in remembering and performing these input sequences.

## Dynamic Macros

Further expanding on macros are dynamic macros, which accept variables and substitute the values input into them. They come in the generic form of `#macroname(*,...)` with "\*" representing the arguments.

When defined, the values of the dynamic macro arguments come in the form of `<#>` with "#" as the argument number, starting from 0. For example, `#mash(\*)` accepts one argument and can be `[<0>34ms #34ms]*20`. If using this macro, one can type `#mash(a)`, which will replace "<0>" with "a", resulting in `[a34ms #34ms]*20`. Substituting "a" for "b" or any other input is also valid in this context.

![Moving and crouching very quickly in Super Tux](./Images/Tutorial/DynamicMacroTutorial.gif)

**Example:** `#500ms #pressreps(left) #pressreps(down)`
- `#pressreps(*)` is a dynamic macro equivalent to `[<0>34ms #150ms]*7`

It's possible to expand the earlier example with an additional argument: `#mash(*,*)`. If the new macro was then `[<0>34ms #34ms]*<1>`, calling the macro with `#mash(y,30)` will result in `[y34ms #34ms]*30`.

Dynamic macros are referred to by their generic form when used for general purposes, such as adding or removing them, or viewing them with the `ShowMacroCommand` (default: "!showmacro"). For the example above, the generic form is `#mash(*,*)`.

**Note that due to the nature of dynamic macros and the liberties with their syntax, the parser cannot validate them upon creation.** It is up to the players to verify that the dynamic macro works. Inputting a dynamic macro that doesn't parse will simply result in the input failing with no special error message.

Dynamic macros have many creative uses, so have fun with them!

## Comments

Comments can be used to provide human-readable information about an input sequence without affecting the input sequence itself. They come in the form of `//comment here//`, where anything enclosed in two leading and trailing forward slashes ("// //") is trimmed out of the input sequence.

For example, one can create a macro with a comment noting what the macro is used for. A macro called `#sfup` could be `_down #200ms -down+_up a1s //Perform a sideflip up//`. The parser would trim out the comment, leaving it at `_down #200ms -down+_up a1s`.

Comments can be included **anywhere** in the input string. `up10 //this is a comment//%52 //another comment is here//0ms` would turn into `up10%520ms` after the parser removes all comments and whitespace.

## Input Synonyms

While not part of TRBot's syntax, input synonyms can be used to define alternatives for existing inputs. They can be added with the `AddInputSynonymCommand` (default: "!addsyn") and are stored in the **InputSynonyms** table in the database. Input synonyms are defined on a per-console basis. An example of an input synonym could be ".", which could be a synonym for "#", the default blank input.

In a game where the "a" button makes the character jump, one can make "jump" a synonym of "a". As a result, the parser will replace "jump" with "a", and the character will jump.

It's also possible to make more complex synonyms: for example, "slide" can be `_down a`. This gives input synonyms a similar quality to macros, but they are not designed for such a purpose (more below). Thus, it's recommended to keep input synonyms as simple as possible.

Input synonyms will directly replace any match, which can break input sequences on occassion. For example, if "triangle" is an input name and "a" is an input synonym for "triangle", the final output of inputting "triangle" would be "tritrianglengle", which is not a valid input. In these instances, it's recommended to instead [add a new input](./Adding-ConsolesInputs.md) with a different name but the same button and/or axis value so it parses correctly. View all input synonyms with the `ListInputSynonymsCommand` (default: "!listsyn").
