# Custom Command and Routine changes in TRBot 2.5

TRBot 2.5 introduces fundamental changes to how client services are handled. Since it is now possible to have multiple client services at once, much code around sending messages has been changed.

Here's what you need to know about migrating custom commands and routines from versions prior to 2.5 to 2.5:

- `MessageHandler` has been removed as there is now an instance for each active client service. The `ClientServiceMngr.GetClientService` method can be used to obtain a client service, and thus their `MsgHandler`.
  - For custom commands, the name of the service that sent the command can be obtained through the `EvtChatCommandArgs`. Pass this value into `ClientServiceMngr.GetClientService`, or use `QueueMessage` with the service name to easily send a message through a specific client service.
  - For custom routines, broadcast a message to all client services with `MessageHelpers.BroadcastMessage` (found in the `TRBot.Connection` namespace). This internally dispatches an `ClientServiceEventNames.BROADCAST_MESSAGE` event of type `EvtBroadcastMessageArgs` through the `EvtDispatcher`. If you want to send a message to a specific client service, obtain the service with `ClientServiceMngr.GetClientService`.

For commands, the following code:

```
MessageHandler.QueueMessage("Test");
```

Turns into this:

```
QueueMessage(args.ServiceName, "Test");
```

For routines, it turns into this:

```
MessageHelpers.BroadcastMessage(EvtDispatcher, "Test");
```

