/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using TRBot.Input;
using TRBot.Input.Parsing;
using TRBot.Input.Consoles;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities.Logging;
using NSubstitute;

namespace TRBot.Tests.Benchmarks
{
    [MemoryDiagnoser]
    public class InputValidationBenchmarks
    {
        private ITRBotLogger DummyLogger = null;
        private IPostParser PostParser = null;

        public InputValidationBenchmarks()
        {
            DummyLogger = Substitute.For<ITRBotLogger>();
            PostParser = new StandardPostParser();
        }

        [Benchmark]
        public void RunValidationFailControllerPort()
        {
            RunValidation(GetFailControllerPortMessage(), GetSuccessConsole(), 0, 0, GetSuccessRestrictedInputs());
        }

        [Benchmark]
        public void RunValidationFailRestrictedInputs()
        {
            RunValidation(GetFailBtnComboMessage(), GetSuccessConsole(), 0, 1, GetFailRestrictedInputs());
        }

        [Benchmark]
        public void RunValidationFailBtnCombo()
        {
            RunValidation(GetFailBtnComboMessage(), GetFailBtnComboConsole(), 0, 1, GetSuccessRestrictedInputs());
        }

        [Benchmark]
        public void RunValidationFailInputLvl()
        {
            RunValidation(GetSuccessMessage(), GetFailInputLvlConsole(), 0, 1, GetSuccessRestrictedInputs());
        }

        [Benchmark]
        public void RunNewValidationSuccess()
        {
            RunValidation(GetSuccessMessage(), GetSuccessConsole(), 0, 1, GetSuccessRestrictedInputs());
        }

        private void RunValidation(string message, IGameConsole usedConsole, in long userLevel,
            in int maxPortNum, HashSet<string> userRestrictedInputs)
        {
            IVirtualControllerManager dummyVConMngr = new DummyControllerManager(DummyLogger);
            dummyVConMngr.Initialize();
            dummyVConMngr.InitControllers(4);

            StandardParserValidator validator = new StandardParserValidator(userRestrictedInputs,
                userLevel, usedConsole, dummyVConMngr, DummyLogger);

            IParser standardParser = new StandardParserFactory(null, null,
                    usedConsole.GetInputNames(InputNameFiltering.Enabled), 0, maxPortNum, 200, 60000, true, validator)
                .Create();

            //Parse inputs to get our parsed input sequence
            ParsedInputSequence inputSequence = standardParser.ParseInputs(message);

            if (inputSequence.ParsedInputResult == ParsedInputResults.Invalid)
            {
                return;
            }

            //Get a blank input
            InputData blankInpData = usedConsole.GetAvailableBlankInput(userLevel, userRestrictedInputs);

            if (blankInpData != null)
            {
                MidInputDelayData midInputDelayData = PostParser.InsertMidInputDelays(inputSequence,
                    0, 0, usedConsole, blankInpData);
                
                //If it's successful, replace the input list and duration
                if (midInputDelayData.Success == true)
                {
                    int oldDur = inputSequence.TotalDuration;
                    inputSequence.Inputs = midInputDelayData.NewInputs;
                    inputSequence.TotalDuration = midInputDelayData.NewTotalDuration;
                }
            }
        }

        private string GetFailControllerPortMessage()
        {
            return "&3_right500ms [x300ms .]*10 _x [&2up17ms #17ms]*2 _y1s [up34ms#34ms]*4 start+b+left250ms -right [y34ms #34ms]*3";
        }

        private HashSet<string> GetFailRestrictedInputs()
        {
            return new HashSet<string>()
            {
                { "up" },
                { "b" },
            };
        }

        private string GetFailBtnComboMessage()
        {
            //                                                                    Fails here
            //                                                                         v
            return "_right500ms [x300ms .]*10 _x [up17ms #17ms]*2 _y1s [up34ms#34ms]*4 start+b+left250ms -right [y34ms #34ms]*3";
        }

        private GameConsole GetFailBtnComboConsole()
        {
            return new GameConsole("failbtncombo", new List<InputData>()
            {
                ConsoleHelpers.CreateButtonInput("a", 0),
                ConsoleHelpers.CreateButtonInput("b", 1),
                ConsoleHelpers.CreateButtonInput("x", 2),
                ConsoleHelpers.CreateButtonInput("y", 3),
                ConsoleHelpers.CreateButtonInput("start", 4),
                ConsoleHelpers.CreateAxis("left", 0, 0.5d, 0d, 0.5d),
                ConsoleHelpers.CreateAxis("right", 0, 0.5d, 1d, 0.5d),
                ConsoleHelpers.CreateAxis("up", 1, 0.5d, 0d, 0.5d),
                ConsoleHelpers.CreateAxis("down", 1, 0.5d, 1d, 0.5d),
                ConsoleHelpers.CreateBlankInput("#"),
                ConsoleHelpers.CreateBlankInput("."),
            },
            new List<InvalidInputCombo>()
            {
                new InvalidInputCombo("test", new List<InputData>()
                {
                    ConsoleHelpers.CreateButtonInput("x", 2),
                    ConsoleHelpers.CreateButtonInput("y", 3),
                    ConsoleHelpers.CreateButtonInput("start", 4),
                })
            });
        }

        private GameConsole GetFailInputLvlConsole()
        {
            GameConsole console = new GameConsole("failinputlvl", new List<InputData>()
            {
                ConsoleHelpers.CreateButtonInput("a", 0),
                ConsoleHelpers.CreateButtonInput("b", 1),
                ConsoleHelpers.CreateButtonInput("x", 2),
                ConsoleHelpers.CreateButtonInput("y", 3),
                ConsoleHelpers.CreateButtonInput("start", 4),
                ConsoleHelpers.CreateAxis("left", 0, 0.5d, 0d, 0.5d),
                ConsoleHelpers.CreateAxis("right", 0, 0.5d, 1d, 0.5d),
                ConsoleHelpers.CreateAxis("up", 1, 0.5d, 0d, 0.5d),
                ConsoleHelpers.CreateAxis("down", 1, 0.5d, 1d, 0.5d),
                ConsoleHelpers.CreateBlankInput("#"),
                ConsoleHelpers.CreateBlankInput("."),
            }, new List<InvalidInputCombo>());

            console.ConsoleInputs["up"].Level = 10;

            return console;
        }

        private string GetSuccessMessage()
        {
            return "_right500ms [up300ms .]*10 [x34ms#34ms]*4 -right down+b+left250ms";
        }

        private HashSet<string> GetSuccessRestrictedInputs()
        {
            return new HashSet<string>();
        }

        private GameConsole GetSuccessConsole()
        {
            return new GameConsole("success", new List<InputData>()
            {
                ConsoleHelpers.CreateButtonInput("a", 0),
                ConsoleHelpers.CreateButtonInput("b", 1),
                ConsoleHelpers.CreateButtonInput("x", 2),
                ConsoleHelpers.CreateButtonInput("y", 3),
                ConsoleHelpers.CreateButtonInput("start", 4),
                ConsoleHelpers.CreateAxis("left", 0, 0.5d, 0d, 0.5d),
                ConsoleHelpers.CreateAxis("right", 0, 0.5d, 1d, 0.5d),
                ConsoleHelpers.CreateAxis("up", 1, 0.5d, 0d, 0.5d),
                ConsoleHelpers.CreateAxis("down", 1, 0.5d, 1d, 0.5d),
                ConsoleHelpers.CreateBlankInput("#"),
                ConsoleHelpers.CreateBlankInput("."),
            }, new List<InvalidInputCombo>());
        }
    }
}