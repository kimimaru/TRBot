/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using TRBot.Data;
using TRBot.Connection;
using TRBot.Utilities;

namespace TRBot.Commands
{
    /// <summary>
    /// A command that inputs a message as another user.
    /// This is often used for administrative purposes.
    /// </summary>
    public class MockUserCommand : BaseCommand
    {
        private string UsageMessage = $"Usage - \"Either: externalID, or comma-separated: username, service name (Ex.\"user,terminal\")\", \"message to send\"";
    
        public MockUserCommand()
        {
    
        }
    
        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;
            if (arguments.Count < 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }
    
            User user = DatabaseMngr.GetUserByExternalID(args.Command.UserMessage.UserExternalID);
            if (user == null)
            {
                QueueMessage(args.ServiceName, "The user invoking this command isn't in the database.");
                return;
            }

            string mockedUserArgs = arguments[0];
            
            string mockedUserExternalID = arguments[0];
            string mockedUserDisplayName = arguments[0];

            //Split to see which username and service to use
            string[] splitArgs = arguments[0].Split(',', 2, StringSplitOptions.RemoveEmptyEntries);

            //Invalid arguments
            if (splitArgs.Length > 2)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            //Parse the external ID from the username and service name from both strings (Ex. "user1,terminal")
            if (splitArgs.Length == 2)
            {
                mockedUserDisplayName = splitArgs[0];
                string mockedServiceName = splitArgs[1].ToLowerInvariant();

                if (ClientServiceNames.AllServiceNames.Contains(mockedServiceName) == false)
                {
                    QueueMessage(args.ServiceName, "Invalid service name!");
                    return;
                }

                mockedUserExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(mockedUserDisplayName,
                    mockedServiceName);
                
                //If no user is found, this is a new user, so use the display name as the external ID 
                if (string.IsNullOrEmpty(mockedUserExternalID) == true)
                {
                    mockedUserExternalID = mockedUserDisplayName.ToLowerInvariant();
                }
            }

            User mockedUser = DatabaseMngr.GetUserByExternalID(mockedUserExternalID);
            if (mockedUser == null)
            {
                //Try to find the user using the username and the current service
                //The idea is to create a new user only if one doesn't already exist
                string checkedExternalID = DatabaseMngr.GetUserExternalIDFromNameAndService(mockedUserDisplayName, args.ServiceName);

                mockedUser = DatabaseMngr.GetUserByExternalID(checkedExternalID);
            }

            if (mockedUser != null)
            {
                if (mockedUser.ExternalID == user.ExternalID)
                {
                    QueueMessage(args.ServiceName, "You don't need to mock yourself - just type something!");
                    return;
                }

                if (mockedUser.Level >= user.Level)
                {
                    QueueMessage(args.ServiceName, "Cannot mock a user with a level equal to or greater than your own!");
                    return;
                }

                mockedUserExternalID = mockedUser.ExternalID;
                mockedUserDisplayName = mockedUser.Name;
            }

            string fullMessage = args.Command.ArgumentsAsString.Remove(0, mockedUserArgs.Length + 1);
    
            EvtUserMessageArgs evtUserMsgArgs = new EvtUserMessageArgs()
            {
                ServiceName = args.ServiceName,
                UserMessage = new EvtUserMsgData(mockedUserExternalID, mockedUserDisplayName, fullMessage)
            };
    
            EvtDispatcher.DispatchEvent<EvtUserMessageArgs>(ClientServiceEventNames.MESSAGE, evtUserMsgArgs);

            if (ConnectionHelpers.IsCommand(fullMessage, args.Command.CommandIdentifier) == true)
            {
                string fullMsgWSReplaced = Helpers.ReplaceAllWhitespaceWithSpace(fullMessage);
                string cmdIdentifier = DatabaseMngr.GetCommandIdentifier();

                EvtChatCommandData cmdData = ConnectionHelpers.ParseCommandDataFromStringWsAsSpace(fullMsgWSReplaced, cmdIdentifier,
                    new EvtUserMsgData(mockedUserExternalID, mockedUserDisplayName, fullMsgWSReplaced));

                EvtChatCommandArgs chatCmdArgs = new EvtChatCommandArgs()
                {
                    ServiceName = args.ServiceName,
                    Command = cmdData
                };
    
                EvtDispatcher.DispatchEvent<EvtChatCommandArgs>(ClientServiceEventNames.COMMAND, chatCmdArgs);
            }
        }
    }
}