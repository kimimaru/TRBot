﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Permissions;

namespace TRBot.Commands
{
    /// <summary>
    /// Displays or changes the current vote time in the Democracy input mode.
    /// </summary>
    public sealed class GetSetDemocracyVoteTimeCommand : BaseCommand
    {
        private const long MIN_VOTING_TIME = 1000L;
        private const long MAX_VOTING_TIME_WARNING = 120000L;
        private string UsageMessage = "Usage: \"voting time (int) - in milliseconds\"";
        
        public GetSetDemocracyVoteTimeCommand()
        {
            
        }

        public override void ExecuteCommand(EvtChatCommandArgs args)
        {
            List<string> arguments = args.Command.ArgumentsAsList;

            long curVoteTime = DatabaseMngr.GetSettingInt(SettingsConstants.DEMOCRACY_VOTE_TIME, 10000L);

            //See the time
            if (arguments.Count == 0)
            {
                QueueMessage(args.ServiceName, $"The current Democracy voting time is {curVoteTime}. To set the vote time, add it as an argument, in milliseconds.");
                return;
            }

            //Invalid number of arguments
            if (arguments.Count > 1)
            {
                QueueMessage(args.ServiceName, UsageMessage);
                return;
            }

            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Check if the user has the ability to set the vote time
                User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

                if (user != null && user.HasEnabledAbility(PermissionConstants.SET_DEMOCRACY_VOTE_TIME_ABILITY) == false)
                {
                    QueueMessage(args.ServiceName, "You don't have the ability to set the Democracy voting time!");
                    return;
                }
            }

            string numStr = arguments[0];

            //Parse
            if (long.TryParse(numStr, out long parsedTime) == false)
            {
                QueueMessage(args.ServiceName, "Invalid number!");
                return;
            }

            //Same time
            if (curVoteTime == parsedTime)
            {
                QueueMessage(args.ServiceName, $"The current voting time is already {curVoteTime}!");
                return;
            }

            //Check min value
            if (parsedTime < MIN_VOTING_TIME)
            {
                QueueMessage(args.ServiceName, $"{parsedTime} is a very low voting time and may not be useful in the long run! Please set it to at least {MIN_VOTING_TIME} milliseconds.");
                return;
            }

            if (parsedTime > MAX_VOTING_TIME_WARNING)
            {
                QueueMessage(args.ServiceName, $"{parsedTime} milliseconds is a long voting time that may slow down the stream. Consider setting the time lower than {MAX_VOTING_TIME_WARNING} milliseconds.");
            }
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                //Set the value and save
                Settings resModeSetting = context.GetSetting(SettingsConstants.DEMOCRACY_VOTE_TIME);
                resModeSetting.ValueInt = parsedTime;

                context.SaveChanges();
            }
            
            QueueMessage(args.ServiceName, $"Changed the Democracy voting time from {curVoteTime} to {parsedTime}!");
        }
    }
}
