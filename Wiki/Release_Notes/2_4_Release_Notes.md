TRBot v2.4.0, featuring an event dispatcher, improved database query performance, and numerous code refinements.

Summary of changes:
- A new WebSocket event dispatcher has been added. The event dispatcher will dispatch a specific set of events, like messages and inputs, which external applications can read and react to. 
  - For example, this feature can be used to make a separate GUI application displaying all the chat messages coming into TRBot.
- Messages and commands are now queued up and run in a separate task instead of the instant they come in. This means that there should be fewer delays during high-traffic streams with lots of messages.
- Comments have been added to the parser. See the [syntax tutorial section on comments](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Syntax-Tutorial.md#user-content-comments) for more details.
- A new [`periodic_message_prereq_msg_count`](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Settings-Documentation.md#periodic_message_prereq_msg_count) setting has been added, which indicates the number of messages in chat that are required to restart the timer for the periodic message.
- Greatly improved performance on important database queries by making them read-only. There is now less time between receiving a message and executing an input sequence.
- A new **UserDisplayRanks** table has been added to the database, defining cosmetic ranks for user based on their `ValidInputCount`.
- A new WebSocketManager has been introduced to simplify handling WebSocket services. Now only one WebSocket server is run to reduce resource use.
- Added [`GetDBSettingCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/TRBot/TRBot.Commands/Commands/GetDBSettingCommand.cs) and [`SetDBSettingCommand`](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/TRBot/TRBot.Commands/Commands/SetDBSettingCommand.cs) to allow users with Admin level and up to obtain and change database values without having direct access to the host machine.
- Refactored a lot of the code to make better use of dependency injection and inversion of control.
  - `DataContainer` has been completely removed.
  - `InputHandler`, `DatabaseManager`, and `TRBotLogger`, among others, have been made into instances implementing interfaces and injected into classes that depend on them.
- The experimental `XDotoolControllerManager` and `XDotoolController` on GNU/Linux platforms has been removed, as [better solutions are already available](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Setup-Misc.md#mapping-button-presses-to-keyboard-mouse-or-custom-scripts).
  - Hosts that rely on this are encouraged to either stay on older versions or fork the project and add the controller back in.
- Fixed being able to add input macros and synonyms with whitespace.
- Added a free license to the documentation (CC BY-SA 4.0).

**IMPORTANT for those upgrading from previous 2.0+ releases:**
- There have been breaking changes to custom commands and routines as a result of the dependency injection refactoring. Consult the [2.4.0 migration guide](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Custom-Cmd-Routine-Changes-2_4.md) for how to proceed. The [custom commands](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Custom-Commands.md) and [custom routines](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Custom-Routines.md) documentation has also been updated accordingly. **The legacy method of writing custom commands is no longer available!**

**IMPORTANT for those upgrading from 1.x releases:** Please read the [migration guide](https://codeberg.org/kimimaru/TRBot/src/tag/2.3.1/Wiki/Migrating-Data.md) and download the **TRBotDataMigrationTool** if you're upgrading from 1.8!
- **This is the last release containing builds for the 1.x migration tool!** If you need to upgrade, either build from source, or use a previous version of the tool then run the new version of TRBot to apply migrations to your database.

To learn how to manage data in the SQLite database, please read the [guide for managing data](https://codeberg.org/kimimaru/TRBot/src/tag/2.4.0/Wiki/Managing-Data.md).

Please [file an issue](https://codeberg.org/kimimaru/TRBot/issues/new) if you encounter any bugs so we can address them as soon as possible.
