/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using NSubstitute;
using NUnit.Framework;
using TRBot.Input;
using TRBot.Input.Consoles;
using TRBot.Input.Parsing;
using TRBot.Input.VirtualControllers;
using TRBot.Utilities.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TRBot.Tests.Input
{
    [TestFixture]
    public class InputHandlerUnitTests
    {
        private IInputHandler InputHandler = null;

        private ITRBotLogger Logger = null;

        private IGameConsole GameCnsole = null;
        private IVirtualControllerManager VControllerMngr = null;

        [SetUp]
        public void Setup()
        {
            Logger = Substitute.For<ITRBotLogger>();
            GameCnsole = Substitute.For<IGameConsole>();
            VControllerMngr = CreateMockVControllerMngr(1);

            SetupMocks();

            InputHandler = new InputHandler(Logger);
            InputHandler.ToggleInputHandling(true);
        }

        private void SetupMocks()
        {
            GameCnsole.Name.ReturnsForAnyArgs("Console");

            Dictionary<string, InputData> inputDict = new Dictionary<string, InputData>()
                {
                    { "a", new InputData("a", 0, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "b", new InputData("b", 1, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "x", new InputData("x", 2, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "y", new InputData("y", 3, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "start", new InputData("start", 4, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "select", new InputData("select", 5, 0, InputTypes.Button, 0.5d, 0d, 0.5d, 0) },
                    { "l", new InputData("l", 6, 4, InputTypes.Axis, 0, 1, 0d) },
                    { "r", new InputData("r", 7, 5, InputTypes.Axis, 0, 1, 0d) },
                    { "up", new InputData("up", 0, 0, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "down", new InputData("down", 0, 0, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "left", new InputData("left", 0, 1, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "right", new InputData("right", 0, 1, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "rup", new InputData("rup", 0, 2, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "rdown", new InputData("rdown", 0, 2, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "rleft", new InputData("rleft", 0, 3, InputTypes.Axis, 0.5d, 0d, 0.5d, 0) },
                    { "rright", new InputData("rright", 0, 3, InputTypes.Axis, 0.5d, 1d, 0.5d, 0) },
                    { "#", new InputData("#", 0, 0, InputTypes.Blank, 0.5d, 0d, 0.5d, 0) },
                };

            GameCnsole.ConsoleInputs.ReturnsForAnyArgs(inputDict);
        }

        private static IEnumerable<TestCaseData> TimedCompletionTestCases()
        {
            yield return new TestCaseData(new List<List<ParsedInput>>()
                {
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 100d, 8, InputDurationTypes.Milliseconds, 0)
                    }
                }, 8);
            yield return new TestCaseData(new List<List<ParsedInput>>()
                { 
                    new List<ParsedInput>()
                    {
                        new ParsedInput("#", false, false, 0d, 17, InputDurationTypes.Milliseconds, 0)
                    },
                    new List<ParsedInput>()
                    {
                        new ParsedInput("up", false, false, 50d, 17, InputDurationTypes.Milliseconds, 0)
                    }
                }, 34);
            yield return new TestCaseData(new List<List<ParsedInput>>()
                { 
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 0d, 17, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("up", false, false, 0d, 50, InputDurationTypes.Milliseconds, 0),
                    }
                }, 50);
            yield return new TestCaseData(new List<List<ParsedInput>>()
                { 
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 0d, 17, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("b", false, false, 0d, 19, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("right", false, false, 0d, 21, InputDurationTypes.Milliseconds, 0),
                    },
                    new List<ParsedInput>()
                    {
                        new ParsedInput("left", false, false, 0d, 2, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("#", false, false, 0d, 3, InputDurationTypes.Milliseconds, 0),
                    },
                    new List<ParsedInput>()
                    {
                        new ParsedInput("up", false, false, 0d, 4, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("down", false, false, 0d, 4, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("left", false, false, 0d, 4, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("right", false, false, 0d, 4, InputDurationTypes.Milliseconds, 0),
                    },
                    new List<ParsedInput>()
                    {
                        new ParsedInput("up", false, false, 50d, 18, InputDurationTypes.Milliseconds, 0)
                    }
                }, 46);
            yield return new TestCaseData(new List<List<ParsedInput>>()
                { 
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 100d, 1, InputDurationTypes.Milliseconds, 0)
                    }
                }, 1);
            yield return new TestCaseData(new List<List<ParsedInput>>()
                { 
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 100d, 5, InputDurationTypes.Milliseconds, 0)
                    }
                }, 5);
            yield return new TestCaseData(new List<List<ParsedInput>>()
                { 
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 100d, 25, InputDurationTypes.Milliseconds, 0)
                    }
                }, 25);
        }

        [TestCaseSource(nameof(TimedCompletionTestCases))]
        public async Task InputHandlerCompletesInTime(List<List<ParsedInput>> inputSequence, int minExecutionTime)
        {
            Stopwatch sw = Stopwatch.StartNew();
            
            await InputHandler.CarryOutInput(inputSequence, GameCnsole, VControllerMngr);

            sw.Stop();

            long totalMs = sw.ElapsedMilliseconds;

            Assert.That(totalMs, Is.GreaterThanOrEqualTo(minExecutionTime));
        }

        private static TestCaseData[] ButtonPressReleaseTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(new ParsedInput("a", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput("b", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput("x", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput("y", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
            };
        }

        [TestCaseSource(nameof(ButtonPressReleaseTestCases))]
        public async Task InputHandlerPressesAndReleasesButton(ParsedInput input)
        {
            GameCnsole.ConsoleInputs.ReturnsForAnyArgs(new Dictionary<string, InputData>()
            {
                { input.Name, new InputData(input.Name, 0, 0, InputTypes.Button, 0.5d, 0d, 0.5d) }
            });

            GameCnsole.IsBlankInput(input).Returns(false);
            GameCnsole.GetButtonValue(input.Name, out InputButton btn).Returns(callInfo =>
            {
                callInfo[1] = new InputButton(0);
                return true;   
            });
            GameCnsole.GetAxisValue(input.Name, out InputAxis axis).Returns(false);
            
            IVirtualController vController = VControllerMngr.GetController(0);

            List<List<ParsedInput>> inputSeq = new List<List<ParsedInput>>() { new List<ParsedInput>() { input } };

            await InputHandler.CarryOutInput(inputSeq, GameCnsole, VControllerMngr);

            vController.Received(1).PressButton(0);
            vController.Received(2).ReleaseButton(0);
        }

        private static TestCaseData[] AxisPressReleaseTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(new ParsedInput("up", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput("down", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput("left", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput("right", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
            };
        }

        [TestCaseSource(nameof(AxisPressReleaseTestCases))]
        public async Task InputHandlerPressesAndReleasesAxis(ParsedInput input)
        {
            GameCnsole.ConsoleInputs.ReturnsForAnyArgs(new Dictionary<string, InputData>()
            {
                { input.Name, new InputData(input.Name, 0, 0, InputTypes.Axis, 0d, 1d, 0.5d) }
            });

            GameCnsole.IsBlankInput(input).Returns(false);
            GameCnsole.GetButtonValue(input.Name, out InputButton btn).Returns(false);
            GameCnsole.GetAxis(input, out InputAxis axis).Returns(callInfo =>
            {
                callInfo[1] = new InputAxis(0, 0d, 1d, 0.5d);
                return true;   
            });
            
            IVirtualController vController = VControllerMngr.GetController(0);

            List<List<ParsedInput>> inputSeq = new List<List<ParsedInput>>() { new List<ParsedInput>() { input } };

            await InputHandler.CarryOutInput(inputSeq, GameCnsole, VControllerMngr);

            vController.Received(1).PressAxis(0, 0d, 1d, 100d);
            vController.Received(2).ReleaseAxis(0, 0.5d);
        }

        private static TestCaseData[] BlankInputTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(new ParsedInput("#", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0)),
                new TestCaseData(new ParsedInput(".", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0))
            };
        }

        [TestCaseSource(nameof(BlankInputTestCases))]
        public async Task InputHandlerDoesntPressBlankInput(ParsedInput input)
        {
            GameCnsole.ConsoleInputs.ReturnsForAnyArgs(new Dictionary<string, InputData>()
            {
                { input.Name, new InputData(input.Name, 0, 0, InputTypes.Blank, 0d, 1d, 0.5d) }
            });

            GameCnsole.IsBlankInput(input).Returns(true);
            GameCnsole.GetButtonValue(input.Name, out InputButton btn).Returns(true);
            GameCnsole.GetAxis(input, out InputAxis axis).Returns(true);
            
            IVirtualController vController = VControllerMngr.GetController(0);

            List<List<ParsedInput>> inputSeq = new List<List<ParsedInput>>() { new List<ParsedInput>() { input } };

            await InputHandler.CarryOutInput(inputSeq, GameCnsole, VControllerMngr);

            vController.DidNotReceiveWithAnyArgs().PressAxis(Arg.Any<int>(), Arg.Any<double>(), Arg.Any<double>(), Arg.Any<double>());
            vController.DidNotReceive().ReleaseAxis(Arg.Any<int>(), Arg.Any<double>());
        }

        private static TestCaseData[] UpdateVControllerTestCases()
        {
            return new TestCaseData[]
            {
                new TestCaseData(new ParsedInput("#", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0), InputTypes.Blank, 0),
                new TestCaseData(new ParsedInput("a", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0), InputTypes.Button, 3),
                new TestCaseData(new ParsedInput("up", false, false, 100d, 1,
                    InputDurationTypes.Milliseconds, 0), InputTypes.Axis, 3)
            };
        }

        [TestCaseSource(nameof(UpdateVControllerTestCases))]
        public async Task InputHandlerUpdatesVController(ParsedInput input, InputTypes inputType, int expectedUpdates)
        {
            GameCnsole.ConsoleInputs.ReturnsForAnyArgs(new Dictionary<string, InputData>()
            {
                { input.Name, new InputData(input.Name, 0, 0, inputType, 0d, 1d, 0.5d) }
            });

            GameCnsole.IsBlankInput(input).Returns(inputType == InputTypes.Blank);
            GameCnsole.GetButtonValue(input.Name, out InputButton btn).Returns(inputType == InputTypes.Button);
            GameCnsole.GetAxis(input, out InputAxis axis).Returns(inputType == InputTypes.Axis);
            
            IVirtualController vController = VControllerMngr.GetController(0);

            List<List<ParsedInput>> inputSeq = new List<List<ParsedInput>>() { new List<ParsedInput>() { input } };

            await InputHandler.CarryOutInput(inputSeq, GameCnsole, VControllerMngr);

            vController.ReceivedWithAnyArgs(expectedUpdates).UpdateController();
        }

        [Test]
        public async Task InputHandlerProcessesNoInputsWhenInputHandlingOff()
        {
            InputHandler.ToggleInputHandling(false);

            List<List<ParsedInput>> inputSeq = new List<List<ParsedInput>>()
                {
                    new List<ParsedInput>()
                    {
                        new ParsedInput("a", false, false, 100d, 100, InputDurationTypes.Milliseconds, 0),
                        new ParsedInput("up", false, false, 100d, 500, InputDurationTypes.Milliseconds, 0),
                    }
                };

            IVirtualController vController = VControllerMngr.GetController(0);

            await InputHandler.CarryOutInput(inputSeq, GameCnsole, VControllerMngr);

            vController.DidNotReceiveWithAnyArgs().UpdateController();
        }

        private IVirtualControllerManager CreateMockVControllerMngr(int numControllers)
        {
            IVirtualControllerManager vControllerMngr = Substitute.For<IVirtualControllerManager>();

            vControllerMngr.Initialized.ReturnsForAnyArgs(true);
            vControllerMngr.MinControllers.ReturnsForAnyArgs(1);
            vControllerMngr.MaxControllers.ReturnsForAnyArgs(numControllers);
            vControllerMngr.ControllerCount.ReturnsForAnyArgs(numControllers);
            
            for (int i = 0; i < numControllers; i++)
            {
                IVirtualController vControllerMock = Substitute.For<IVirtualController>();
                vControllerMngr.GetController(i).ReturnsForAnyArgs(vControllerMock);
            }

            return vControllerMngr;
        }
    }
}