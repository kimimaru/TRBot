TRBot v1.5.

* Updated license to AGPL-3.0.
* TwitchLib updated from 3.0.1 to 3.1.0.
* Linux virtual controller support via uinput.
* Abstracted virtual controller API with switchable virtual controllers at runtime.
* ~1000x faster input parser, and several parser bug fixes.
* Improved performance of macro population.
* Assignable invalid button combos (Ex. prevent the a+b+start+select soft reset on GB/GBC).
* Reconnection attempts on disconnect.
* Savestates are now normal inputs that can be used in command strings.
* Experimental PC game support via xdotool on Linux.
* More configuration options:
  * Configurable bot connection message.
  * Restrict specific inputs based on user access level.
  * Adjustable sleep time to use less CPU (previous calculation was broken and didn't sleep at all).

Please read the [wiki](https://github.com/teamradish/TRTwitchPlaysBot/wiki) for information on getting started.
