﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using TRBot.Utilities;
using static TRBot.Input.Consoles.ConsoleConstants;

namespace TRBot.Input.Consoles
{
    /// <summary>
    /// Represents an input axis. Min and max axis values are normalized in the range <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.
    /// </summary>
    public struct InputAxis
    {
        /// <summary>
        /// The value of the axis.
        /// </summary>
        public int AxisVal;

        /// <summary>
        /// The minimum value of the axis, normalized.
        /// </summary>
        public double MinAxisVal;

        /// <summary>
        /// The maximum value of the axis, normalized.
        /// </summary>
        public double MaxAxisVal;

        /// <summary>
        /// The default value of the axis, normalized.
        /// </summary>
        public double DefaultAxisVal;

        /// <summary>
        /// Constructs an input axis.
        /// </summary>
        /// <param name="axisVal">The value of the input axis.</param>
        /// <param name="minAxisVal">The normalized minimum value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        /// <param name="maxAxisVal">The normalized maximum value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        /// <param name="defaultAxisVal">The normalized default value of the axis. This is clamped from <see cref="MIN_NORMALIZED_AXIS_VAL"/> to <see cref="MAX_NORMALIZED_AXIS_VAL"/>.</param>
        public InputAxis(in int axisVal, in double minAxisVal, in double maxAxisVal, in double defaultAxisVal)
        {
            AxisVal = axisVal;
            MinAxisVal = Math.Clamp(minAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            MaxAxisVal = Math.Clamp(maxAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
            DefaultAxisVal = Math.Clamp(defaultAxisVal, MIN_NORMALIZED_AXIS_VAL, MAX_NORMALIZED_AXIS_VAL);
        }

        public override bool Equals(object obj)
        {
            if (obj is InputAxis inputAxis)
            {
                return (this == inputAxis);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = (hash * 23) + AxisVal.GetHashCode();
                hash = (hash * 23) + MinAxisVal.GetHashCode();
                hash = (hash * 23) + MaxAxisVal.GetHashCode();
                hash = (hash * 23) + DefaultAxisVal.GetHashCode();
                return hash;
            }
        }

        public static bool operator==(InputAxis a, InputAxis b)
        {
            return (a.AxisVal == b.AxisVal
                && Helpers.IsApproximate(a.MinAxisVal, b.MinAxisVal, 0.0009d)
                && Helpers.IsApproximate(a.MaxAxisVal, b.MaxAxisVal, 0.0009d)
                && Helpers.IsApproximate(a.DefaultAxisVal, b.DefaultAxisVal, 0.0009d));
        }

        public static bool operator!=(InputAxis a, InputAxis b)
        {
            return !(a == b);
        }
    }
}
