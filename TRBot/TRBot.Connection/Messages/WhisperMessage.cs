﻿/* Copyright (C) 2019-2023 Thomas "Kimimaru" Deeb
 * 
 * This file is part of TRBot, software for playing games through text.
 *
 * TRBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * TRBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBot.  If not, see <https://www.gnu.org/licenses/>.
*/

using Serilog.Events;

namespace TRBot.Connection
{
    /// <summary>
    /// A whisper service message.
    /// </summary>
    public class WhisperMessage : BasicMessage
    {
        /// <summary>
        /// The name of the recipient of the whisper message.
        /// </summary>
        public string RecipientName { get; private set; }

        public WhisperMessage(string recipientName, string message, in bool overridePrefix, string customPrefix,
            in Serilog.Events.LogEventLevel logLevel)
            : base(message, overridePrefix, customPrefix, logLevel)
        {
            RecipientName = recipientName;
        }

        public override IServiceMessage CloneWithDiffMsg(string messageContent)
        {
            return new WhisperMessage(RecipientName, messageContent, OverridePrefix, CustomPrefix, LogLevel);
        }
    }
}
